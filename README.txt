*********************************************
How To Run CMA
*********************************************
1) do mvn install at modules/cma-api first
2) in order to run cma test cases, first you need to package cma-alfresco amp. do mvn package at cma-alfresco root
3) inject the amp to alfresco.war (see readme.txt under the cma-alfresco project)
4) start alfresco then do mvn install at cma root 

*********************************************
CMA Dependencies
*********************************************
following jars are required in order to run CMA
- acegi-security-0.8.3.jar
- alfresco-core-4.0.0.jar
- alfresco-repository-4.0.0.jar
- alfresco-data-model-4.0.0.jar
- base64-1.0.0.jar
- castor-1.2.jar
- commons-codec.1.2.jar
- commons-httpclient-3.1.jar
- commons-logging-1.1.1.jar
- serlvet-api-2.5.jar
- spring-core-2.5.3.jar
- spring-2.0.2.jar

* base64-1.0.0.jar is available in RivetLogic Maven Repository

maven repository
------------------------------------------
  	  <repository>
	    <releases>
	      <enabled>true</enabled>
 	      <updatePolicy>always</updatePolicy>
	      <checksumPolicy>warn</checksumPolicy>
	    </releases>
	    <id>rivetlogic</id>
	    <name>Rivet Logic</name>
	    <url>http://mvn.rivetlogic.com/artifactory/repo/</url>
	  </repository>
------------------------------------------

dependency
------------------------------------------
    <dependency>
      <groupId>com.rivetlogic</groupId>
      <artifactId>base64</artifactId>
      <version>1.0.0</version>
    </dependency>
------------------------------------------


*********************************************
How To Run Test Cases
*********************************************
- CMA-Alfresco AMP must be injected to Alfresco, and Alfresco must be running prior executing CMA test cases.
- open modules/cma-impl/src/test/resources/cma_test.properties and update repository uri and admin id/password

*********************************************
How To Skip Test Cases
*********************************************
To avoid build failures to due test case failures, you may skip test cases by adding the following option to maven command
-Dmaven.test.skip=true

*********************************************
How To Enable Debug from CMA
*********************************************
add cma to rootLogger in modules/cma-impl/src/main/resources/log4j.properties

log4j.rootLogger=info, cma
