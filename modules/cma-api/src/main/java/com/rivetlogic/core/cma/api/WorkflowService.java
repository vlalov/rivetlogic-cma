/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.api;

import java.util.List;
import java.util.Map;
import java.io.InputStream;
import java.io.Serializable;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.workflow.WorkflowDefinition;
import org.alfresco.service.cmr.workflow.WorkflowDeployment;
import org.alfresco.service.cmr.workflow.WorkflowInstance;
import org.alfresco.service.cmr.workflow.WorkflowPath;
import org.alfresco.service.cmr.workflow.WorkflowTask;
import org.alfresco.service.cmr.workflow.WorkflowTaskQuery;
import org.alfresco.service.cmr.workflow.WorkflowTaskState;
import org.alfresco.service.cmr.workflow.WorkflowTimer;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.repo.Ticket;

public interface WorkflowService {

	/**
	 * Cancel an in-flight Workflow instance identified by a given workflowId
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param workflowId
	 *            A Workflow instance id to cancel
	 * @return An updated Workflow instance
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	WorkflowInstance cancelWorkflow(Ticket ticket, String workflowId)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Create a Workflow package If a pre-existing container is provided, Create
	 * a Workflow package by adding the Workflow package aspect to a given
	 * pre-existing container
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param container
	 *            A pre-existing container (optional)
	 * @return A Workflow package (a container of content to route through the
	 *         Workflow)
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	NodeRef createPackage(Ticket ticket, NodeRef container) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Delete an in-flight Workflow instance identified by a given workflowId It
	 * may not go through appropriate cancel events
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param workflowId
	 *            A Workflow instance id to delete
	 * @return An updated Workflow instance
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	WorkflowInstance deleteWorkflow(Ticket ticket, String workflowId)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Deploy a Workflow definition. The specified content object must be of
	 * type bpm:workflowdetinfition
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param workflowDefinition
	 *            A nodeRef associated with the content object containing the
	 *            definition
	 * @return A Workflow deployment descriptor
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	WorkflowDeployment deployDefintion(Ticket ticket, NodeRef workflowDefinition)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Deploy a Workflow definition
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param engineId
	 *            A bpm engine id
	 * @param definitionFile
	 *            A Workflow definition stream
	 * @param mimetype
	 *            The mimetype of the Workflow definition
	 * @return A Workflow deployment descriptor
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	WorkflowDeployment deployDefinition(Ticket ticket, String engineId, InputStream definitionFile,
			String mimetype) throws InvalidTicketException, CmaRuntimeException;

	/**
	 * End a Workflow task identified by a given taskId
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param taskId
	 *            a Workflow task to end
	 * @param transitionId
	 *            A task transition to take upon completion (optional. take a
	 *            default transition if null)
	 * @return An updated task
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	WorkflowTask endTask(Ticket ticket, String taskId, String transitionId)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Fire a custom event against a path identified by a given pathId
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param pathId
	 *            A Workflow path id to fire event on
	 * @param event
	 *            Name of a custom event
	 * @return An updated Workflow path
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	WorkflowPath fireEvent(Ticket ticket, String pathId, String event)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get all in-flight Workflow instances of a Workflow definition identified
	 * by a given workflowDefinitionId
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param workflowDefinitionId
	 *            A Workflow definition id
	 * @return A list of Workflow instances
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	List<WorkflowInstance> getActiveWorkflows(Ticket ticket, String workflowDefinitionId)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get deployed Workflow definitions
	 * 
	 * if includePrevious is false, it only returns latest deployed Workflow
	 * definitions
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param includePrevious
	 *            whether to include previous versions or not
	 * @return A list of deployed Workflow definitions
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	List<WorkflowDefinition> getDefinitions(Ticket ticket, boolean includePrevious)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get all deployed Workflow definitions for a given name
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param workflowName
	 *            A unique Workflow name
	 * @return A list of deployed Workflow definitions
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	List<WorkflowDefinition> getAllDefinitionsByName(Ticket ticket, String workflowName)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get all tasks assigned to an authority identified by a given authority
	 * name
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param authorityName
	 *            Name of authority
	 * @param state
	 *            A WorkflowTaskState to filter
	 * @return All Workflow tasks assigned to the authority filtered by the
	 *         given task
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	List<WorkflowTask> getAssignedTasks(Ticket ticket, String authorityName,
			WorkflowTaskState state) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Get all pooled Workflow tasks for a given authority name
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param authorityName
	 *            Name of authority
	 * @return All pooled Workflow tasks
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	List<WorkflowTask> getPooledTasks(Ticket ticket, String authorityName)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get a WorkflowDefinition identified by a given workflowDefinitionId
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param workflowDefinitionId
	 *            A Workflow Definition Id
	 * @return A WorkflowDefinition or null if not found
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	WorkflowDefinition getDefinitionById(Ticket ticket, String workflowDefinitionId)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get a WorkflowDefinition identified by a given name
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param workflowName
	 *            Name of Workflow
	 * @return A WorkfowDeifnition or null if not found
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	WorkflowDefinition getDefinitionByName(Ticket ticket, String workflowName)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get a graphical view of Workflow definition identified by a given
	 * workflowDefinitionId
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param workflowDefinitionId
	 *            A workflowDefintion id
	 * @return image view of the workflow definition
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	// TODO: it may not be possible to pull the image as a byte array
	byte[] getDefintionImage(Ticket ticket, String workflowDefinitionId)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get a WorkflowTask identified by a given taskId
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param taskId
	 *            A Workflow task id
	 * @return A Workflow task or null if not found
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	WorkflowTask getTaskById(Ticket ticket, String taskId) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Get Workflow tasks associated with a given path
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param pathId
	 *            A path id
	 * @return A list of associated Workflow tasks
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	List<WorkflowTask> getTaskForWorkflowPath(Ticket ticket, String pathId)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get all active timers for the specified Workflow identified by a given
	 * workflowId
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param workflowId
	 *            A Workflow id
	 * @return A list of active Workflow timers
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	List<WorkflowTimer> getTimers(Ticket ticket, String workflowId)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get A Workflow instance identified by a given workflowId
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param workflowId
	 *            A Workflow Id
	 * @return A WorkflowInstance or null if not found
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	WorkflowInstance getWorkflowById(Ticket ticket, String workflowId)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get all Workflow paths for a specified Workflow
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param workflowId
	 *            A Workflow Id to get paths for
	 * @return A list of Workflow paths
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	List<WorkflowPath> getWorkflowPaths(Ticket ticket, String workflowId)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get All Workflow that act upon the specified content
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param packageItem
	 *            A nodeRef associated with content item to get workflows for
	 * @param active
	 *            whether to get active workflows only or not
	 * @return A list of Workflows
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	List<WorkflowInstance> getWorkflowsForContent(Ticket ticket, NodeRef packageItem,
			boolean active) throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Check if the specififed Workflow Definition is deployed
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param workflowdefintion
	 *            A nodeRef associated with Workflow Definition
	 * @return true if Workflow Definition is deployed
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	boolean isDefinitionDeployed(Ticket ticket, NodeRef workflowdefintion)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Query Workflow tasks
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param query
	 *            A query string
	 * @return A list of Workflow tasks
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	List<WorkflowTask> queryTasks(Ticket ticket, WorkflowTaskQuery query)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Signal the transition from one Workflow Node to another
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param pathId
	 *            A Workflow path to signal on
	 * @param transitionId
	 *            A transition to follow. Take a default transition if null
	 * @return An updated Workflow path
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	WorkflowPath signal(Ticket ticket, String pathId, String transitionId)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Start a Workflow instance for a Workflow identified by a given
	 * workflowDefintionId
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param workflowDefintionId
	 *            A Workflow definition Id to start
	 * @param params
	 *            The initial set of parameters for the Start Task properties
	 * @return the initial workflow path
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	WorkflowPath startWorkflow(Ticket ticket, String workflowDefintionId,
			Map<QName, Serializable> params) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Start a Workflow instance from an existing Start Task template node
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param templateDefinition
	 *            A nodeRef representing the Start Task properties
	 * @return the initial workflow path
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	WorkflowPath startWorkflowFromTemplate(Ticket ticket, NodeRef templateDefinition)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Undeploy an existing Workflow definition identified by a given
	 * workflowDefinitionId
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param workflowDefinitionId
	 *            A Workflow Definition Id to undeploy
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	void undeployDefinition(Ticket ticket, String workflowDefinitionId)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Update the properties and associations of a Workflow task
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param taskId
	 *            A Task Id to update
	 * @param properties
	 *            A map of properties to set on the task (optional)
	 * @param add
	 *            A map of items to associate with the task (optional)
	 * @param remove
	 *            A map of items to dis-associate with the task (optional)
	 * @return An updated Workflow task
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	WorkflowTask updateTask(Ticket ticket, String taskId, Map<QName, Serializable> properties,
			Map<QName, List<NodeRef>> add, Map<QName, List<NodeRef>> remove)
			throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * Get a WorkflowTask identified by a given workflowInstanceId
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param workflowInstanceId
	 *            A Workflow instance id
	 * @return A Workflow task or null if not found
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	WorkflowTask getStartTask(Ticket ticket, String workflowInstanceId) throws InvalidTicketException,
			CmaRuntimeException;
}
