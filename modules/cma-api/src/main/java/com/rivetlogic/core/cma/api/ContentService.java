/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.api;

import java.io.InputStream;
import java.io.OutputStream;

import org.alfresco.service.cmr.lock.NodeLockedException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.repo.Ticket;

public interface ContentService {
	/**
	 * Write content(s) to a node given a property(s) where to push it
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            node to write content to
	 * @param property
	 *            QName of property to get content from
	 * @param fileName
	 *            filename where to get the uploaded content
	 * @throws InvalidTicketException
	 * @throws ContentLockedException
	 * @throws CmaRuntimeException
	 */
	void writeContent(Ticket ticket, NodeRef nodeRef, QName property,
			String fileName) throws InvalidTicketException,
			NodeLockedException, CmaRuntimeException;

	/**
	 * Write content(s) to a node given a property(s) where to push it
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            node to write content to
	 * @param property
	 *            QName of property to get content from
	 * @param input
	 *            stream where to get the uploaded content
	 * @throws InvalidTicketException
	 * @throws ContentLockedException
	 * @throws CmaRuntimeException
	 */
	void writeContentFromStream(Ticket ticket, NodeRef nodeRef, QName property,
			InputStream input) throws InvalidTicketException,
			NodeLockedException, CmaRuntimeException;

	/**
	 * Read content from repo into file
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            Node to write content to cm:content)
	 * @param property
	 *            QName of property to put content in
	 * @param fileName
	 *            filename where to put the downloaded content
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	void readContent(Ticket ticket, NodeRef nodeRef, QName property,
			String fileName) throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Read content from repo into streams
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            Node to write content to
	 * @param property
	 *            QName of property to put content in
	 * @param output
	 *            stream where to put the downloaded content
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	void readContentIntoStream(Ticket ticket, NodeRef nodeRef, QName property,
			OutputStream output) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Returns a URI to stream the content from
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            Node to write content to
	 * @param property
	 *            property to contain the content (IGNORED for now, defaults to
	 *            cm:content)
	 * @return Uri to stream content from
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	String getContentUri(Ticket ticket, NodeRef nodeRef, QName property)
			throws InvalidTicketException, CmaRuntimeException;
}
