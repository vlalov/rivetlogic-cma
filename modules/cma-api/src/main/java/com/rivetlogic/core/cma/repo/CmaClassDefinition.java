/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.repo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.alfresco.service.cmr.dictionary.AspectDefinition;
import org.alfresco.service.cmr.dictionary.AssociationDefinition;
import org.alfresco.service.cmr.dictionary.ChildAssociationDefinition;
import org.alfresco.service.cmr.dictionary.ClassDefinition;
import org.alfresco.service.cmr.dictionary.ModelDefinition;
import org.alfresco.service.cmr.dictionary.PropertyDefinition;
import org.alfresco.service.namespace.QName;

/**
 * This class is an implementation of Alfresco ClassDefinition which holds
 * ClassDefinition information retrieved from Alfresco
 * 
 * @author Hyanghee Lim
 *
 */
public class CmaClassDefinition implements ClassDefinition {

	private Map<QName, AssociationDefinition> associations = new HashMap<QName, AssociationDefinition>();
	private Map<QName, ChildAssociationDefinition> childAssociations = new HashMap<QName, ChildAssociationDefinition>();
	private List<AspectDefinition> defaultAspects = new Vector<AspectDefinition>();
    private List<AspectDefinition> inheritedDefaultAspects = new Vector<AspectDefinition>();
	private String description;
	private ModelDefinition model;
	private QName name;
	private QName parentName;
	private Map<QName, PropertyDefinition> properties = new HashMap<QName, PropertyDefinition>();
	private String title;
	private boolean isArchive;
	private boolean isAspect;
	private boolean isContainer;
	

	public Map<QName, AssociationDefinition> getAssociations() {
		return associations;
	}

	public Map<QName, ChildAssociationDefinition> getChildAssociations() {
		return childAssociations;
	}

	public List<AspectDefinition> getDefaultAspects() {
		return inheritedDefaultAspects;
	}

	/**
	 * not supported
	 * @throws UnsupportedOperationException
	 */
	public Map<QName, Serializable> getDefaultValues() {
		throw new UnsupportedOperationException("The method getDefaultValues() is not supported");
	}

	public String getDescription() {
		return description;
	}

	public ModelDefinition getModel() {
		return model;
	}

	public QName getName() {
		return name;
	}

	public QName getParentName() {
		return parentName;
	}

	public Map<QName, PropertyDefinition> getProperties() {
		return properties ;
	}

	public String getTitle() {
		return title;
	}

	public void setIsArchive(boolean isArchive) {
		this.isArchive = isArchive;
	}

	public void setIsAspect(boolean isAspect) {
		this.isAspect = isAspect;
	}

	public void setIsContainer(boolean isContainer) {
		this.isContainer = isContainer;
	}

	public void setAssociations(Map<QName, AssociationDefinition> associations) {
		this.associations = associations;
	}

	public void setChildAssociations(Map<QName, ChildAssociationDefinition> childAssociations) {
		this.childAssociations = childAssociations;
	}

	public void setDefaultValues(Map<QName, Serializable> defaultValues) {
		throw new UnsupportedOperationException("This method is not supported");
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setModel(ModelDefinition model) {
		this.model = model;
	}

	public void setName(QName name) {
		this.name = name;
	}

	public void setParentName(QName parentName) {
		this.parentName = parentName;
	}

	public void setProperties(Map<QName, PropertyDefinition> properties) {
		this.properties = properties ;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isArchive() {
		return isArchive;
	}

	public boolean isAspect() {
		return isAspect;
	}

	public boolean isContainer() {
		return isContainer;
	}

	public List<AspectDefinition> getDefaultAspects(boolean inherited) {
        return inherited ? getDefaultAspects() : defaultAspects;
	}
	
	public void setInheritedDefaultAspects(List<AspectDefinition> inheritedDefaultAspects) {
		this.inheritedDefaultAspects = inheritedDefaultAspects;
	}

	public void setDefaultAspects(List<AspectDefinition> defaultAspects) {
		this.defaultAspects = defaultAspects;
	}

	public Boolean getArchive() {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<QName> getDefaultAspectNames() {
		// TODO Auto-generated method stub
		return null;
	}

	public Boolean getIncludedInSuperTypeQuery() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getAnalyserResourceBundleName() {
		// TODO Auto-generated method stub
		return null;
	}

	public ClassDefinition getParentClassDefinition() {
		// TODO Auto-generated method stub
		return null;
	}

}
