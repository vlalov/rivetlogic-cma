/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.api;

import org.alfresco.service.cmr.repository.ContentIOException;
import org.alfresco.service.cmr.repository.NoTransformerException;
import org.alfresco.service.cmr.repository.NodeRef;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.repo.Ticket;

public interface TransformationService {
	/**
	 * Transforms the content from the reader and writes the content back out to
	 * the writer.
	 * 
	 * @param source
	 *            the source content node (cm:content is the default content
	 *            property used and no ability to set mimetypes at this time) 
	 * @param destination
	 *            the target content node (cm:content is the default content
	 *            property used and no ability to set mimetypes at this time)
	 *            
	 * @throws NoTransformerException
	 *             if no transformer exists for the given source and target
	 *             mimetypes of the reader and writer
	 * @throws ContentIOException
	 *             if the transformation fails
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void transform(Ticket ticket, NodeRef source, NodeRef destination)
			throws InvalidTicketException, CmaRuntimeException,
			NoTransformerException, ContentIOException;

	/**
	 * Returns whether a transformer exists that can read the content from the
	 * reader and write the content back out to the writer.
	 * 
	 * @param source
	 *            the source content node (cm:content is the default content
	 *            property used and no ability to set mimetypes at this time) 
	 * @param destination
	 *            the target content node (cm:content is the default content
	 *            property used and no ability to set mimetypes at this time)
	 * 
	 * @return true if a transformer exists, false otherwise
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public boolean isTransformable(Ticket ticket, NodeRef source,
			NodeRef destination) throws InvalidTicketException,
			CmaRuntimeException;
}
