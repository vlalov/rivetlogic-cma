/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.repo;

import java.util.List;
import java.util.Locale;

import org.alfresco.repo.dictionary.IndexTokenisationMode;
import org.alfresco.service.cmr.dictionary.ClassDefinition;
import org.alfresco.service.cmr.dictionary.ConstraintDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.dictionary.ModelDefinition;
import org.alfresco.service.cmr.dictionary.PropertyDefinition;
import org.alfresco.service.namespace.QName;

/**
 * This class is an implementation of Alfresco PropertyDefinition which holds
 * PropertyDefinition information retrieved from Alfresco
 * 
 * @author Hyanghee Lim
 *
 */
public class CmaPropertyDefinition implements PropertyDefinition {

	private DataTypeDefinition dataType;
	private String defaultValue;
	private String description;
	private ModelDefinition model;
	private QName name;
	private String title;
	private boolean isMandatory;
	private boolean isMultiValued;
	private boolean isOverride;
	private IndexTokenisationMode indexTokenisationMode;

	public DataTypeDefinition getDataType() {
		return dataType;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public String getDescription() {
		return description;
	}

	public ModelDefinition getModel() {
		return model;
	}

	public QName getName() {
		return name;
	}

	public String getTitle() {
		return title;
	}

	public boolean isMandatory() {
		return isMandatory;
	}

	public boolean isMultiValued() {
		return isMultiValued;
	}

	/**
	 * not supported
	 * @throws UnsupportedOperationException
	 */
	public boolean isIndexed() {
		throw new UnsupportedOperationException("The method isIndexed() is not supported");
	}

	/**
	 * not supported
	 * @throws UnsupportedOperationException
	 */
	public boolean isIndexedAtomically() {
		throw new UnsupportedOperationException("The method isIndexedAtomically() is not supported");
	}

	/**
	 * not supported
	 * @throws UnsupportedOperationException
	 */
	public boolean isMandatoryEnforced() {
		throw new UnsupportedOperationException("The method isMandatoryEnforced() is not supported");
	}

	/**
	 * not supported
	 * @throws UnsupportedOperationException
	 */
	public List<ConstraintDefinition> getConstraints() {
		throw new UnsupportedOperationException("The method getConstraints() is not supported");
	}

	/**
	 * not supported
	 * @throws UnsupportedOperationException
	 */
	public ClassDefinition getContainerClass() {
		throw new UnsupportedOperationException("The method getContainerClass() is not supported");
	}

	/**
	 * not supported
	 * @throws UnsupportedOperationException
	 */
	public boolean isProtected() {
		throw new UnsupportedOperationException("The method isProtected() is not supported");
	}

	/**
	 * not supported
	 * @throws UnsupportedOperationException
	 */
	public boolean isStoredInIndex() {
		throw new UnsupportedOperationException("The method isStoredInIndex() is not supported");
	}

	/**
	 * not supported
	 * @throws UnsupportedOperationException
	 */
	public boolean isTokenisedInIndex() {
		throw new UnsupportedOperationException("The method isTokenisedInIndex() is not supported");
	}

	public void setDataType(DataTypeDefinition dataType) {
		this.dataType = dataType;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setModel(ModelDefinition model) {
		this.model = model;
	}

	public void setName(QName name) {
		this.name = name;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setIsMandatory(boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	public void setIsMultiValued(boolean isMultiValued) {
		this.isMultiValued = isMultiValued;
	}

	public IndexTokenisationMode getIndexTokenisationMode() {
		return indexTokenisationMode;
	}

	public void setIndexTokenisationMode(IndexTokenisationMode indexTokenisationMode) {
		this.indexTokenisationMode = indexTokenisationMode;
	}

	public void setIndexTokenisationModeAsString(String indexTokenisationMode) {
		this.indexTokenisationMode 
			= IndexTokenisationMode.valueOf(indexTokenisationMode);
	}

	public boolean isOverride() {
		return this.isOverride;
	}

	public void setOverride(boolean isOverride) {
		this.isOverride = isOverride;
	}

	public String getAnalyserResourceBundleName() {
		// TODO Auto-generated method stub
		return null;
	}

	public String resolveAnalyserClassName() {
		// TODO Auto-generated method stub
		return null;
	}

	public String resolveAnalyserClassName(Locale arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}
