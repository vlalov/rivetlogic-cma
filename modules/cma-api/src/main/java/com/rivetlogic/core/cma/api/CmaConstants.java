/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.api;


public interface CmaConstants {
	
	// request parameters
	public static final String PARAM_ACTION = "action";
	public static final String PARAM_ACTIONCONDITION = "actionCondition";
	public static final String PARAM_ACTIONID = "actionId";
	public static final String PARAM_ACTIVE = "active";
	public static final String PARAM_ADD = "add";
	public static final String PARAM_ALFRESCO_TICKET = "alf_ticket";
	public static final String PARAM_ALLOW = "allow";
	public static final String PARAM_ASSOCTYPEQNAME = "assocTypeQname";
	public static final String PARAM_ASSOCQNAME = "assocQname";
	public static final String PARAM_ASPECTS = "aspects";
	public static final String PARAM_ASPECTTYPEQNAME = "aspectTypeQname";
	public static final String PARAM_ATTRIBUTENAME= "attributeName";
	public static final String PARAM_AUTHORITY = "authority";
	public static final String PARAM_AUTHORITYNAME = "authorityName";
	public static final String PARAM_AUTHORITYNAMES = "authorityNames";
	public static final String PARAM_CATEGORYREF = "categoryRef";
	public static final String PARAM_CHECKCONDITIONS = "checkConditions";
	public static final String PARAM_CHILDASSOCREF = "childAssociationRef";
	public static final String PARAM_CHILDNAME = "childName";
	public static final String PARAM_CLASSNAME = "className";
	public static final String PARAM_CONTENTQNAME = "contentQName";
	public static final String PARAM_CONTENTQNAMEVALUE = "{http://www.alfresco.org/model/content/1.0}content";
	public static final String PARAM_DATATYPE = "dataType";
	public static final String PARAM_DEEP = "deep";
	public static final String PARAM_DEFINITIONFILE = "definitionFile";
	public static final String PARAM_DEPTH = "depth";
	public static final String PARAM_DESTINATIONNODEREF = "destinationNodeRef";
	public static final String PARAM_ENABLED = "enabled";
	public static final String PARAM_ENDRESULT = "endResult";
	public static final String PARAM_ENGINEID = "engineId";
	public static final String PARAM_EVENT = "event";
	public static final String PARAM_EXACTPERMISSIONMATCH = "exactPermissionMatch";
	public static final String PARAM_EXECUTEASYNCHRONOUSLY = "executeAsynchronously";
	public static final String PARAM_FILENAME = "fileName"; 
	public static final String PARAM_FOLDERNAME = "folderName";
	public static final String PARAM_GROUP = "group";
	public static final String PARAM_GROUPNAME = "groupName";
	public static final String PARAM_IDENTIFIER = "identifier";
	public static final String PARAM_INCLUDECONTAININGAUTHORITIES = "includeContainingAuthorities";
	public static final String PARAM_INCLUDECONTAININPERMISSIONS = "includeContainingPermissions";
	public static final String PARAM_INCLUDEPREVIOUS ="includePrevious";
	public static final String PARAM_INDEX = "index";
	public static final String PARAM_INHERITPARENTPERMISSIONS = "inheritParentPermissions";
	public static final String PARAM_JAVACLASS = "javaClass";
	public static final String PARAM_MAJORVERSION = "majorVersion";
	public static final String PARAM_MAXRESULTS = "maxResults";
	public static final String PARAM_MIMETYPE = "mimetype";
	public static final String PARAM_MODE = "mode";
	public static final String PARAM_MODEL = "model";
	public static final String PARAM_NAME = "name";
	public static final String PARAM_NEWNAME = "newName";
	public static final String PARAM_NEWPASSWORD = "newPassword";
	public static final String PARAM_NODENAME = "nodeName";
	public static final String PARAM_NODEREF = "nodeRef";
	public static final String PARAM_NODEREFS = "nodeRefs";
	public static final String PARAM_NODETYPEQNAME = "nodeTypeQname";
	public static final String PARAM_OFCLASSNAME = "ofClassName";
	public static final String PARAM_OLDPASSWORD = "oldPassword";
	public static final String PARAM_PACKAGEITEM = "packageItem";
	public static final String PARAM_PARAMS = "params";
	public static final String PARAM_PARENTGROUP = "parentGroup";
	public static final String PARAM_PARENTGROUPNAME = "parentGroupName";
	public static final String PARAM_PASSWORD = "password";
	public static final String PARAM_PATHID = "pathId";
	public static final String PARAM_PERMISSION = "permission";
	public static final String PARAM_PERMISSIONSTOCHECKFOR = "permissionsToCheckFor";
	public static final String PARAM_PERSON = "person";
	public static final String PARAM_PRIMARYONLY = "primaryOnly";
	public static final String PARAM_PROPERTY = "property";
	public static final String PARAM_PROPERTIES = "properties";
	public static final String PARAM_PROTOCOL = "protocol";
	public static final String PARAM_PROPERTYQNAME = "propertyQname";
	public static final String PARAM_PROPERTYVALUE = "propertyValue";
	public static final String PARAM_QNAME = "qname";
	public static final String PARAM_QUERY = "queryTerm";
	public static final String PARAM_QUERY_LANGUAGE = "queryLanguage";
	public static final String PARAM_RECURSE = "recurse";
	public static final String PARAM_RECURSIVE = "recursive";
	public static final String PARAM_REMOVE = "remove";
	public static final String PARAM_REQUIREDPROPERTIES = "requiredProperties";
	public static final String PARAM_RETURNALLPROPERTIES = "returnAllProperties";
	public static final String PARAM_RETURNASPECTS = "returnAspects";
	public static final String PARAM_RETURNCHILDASSOCS = "returnChildAssocs";
	public static final String PARAM_RETURNPEERASSOCS = "returnPeerAssocs";
	public static final String PARAM_RETURNPERMISSIONS = "returnPermissions";
	public static final String PARAM_REVISIONHISTORY = "revisionHistory";
	public static final String PARAM_SESSION_ID = "session_id";
	public static final String PARAM_SORTDEFINITIONS = "sortDefinitions";
	public static final String PARAM_SSOICKET = "ssoTicket";
	public static final String PARAM_STATE = "state";
	public static final String PARAM_STORE = "store";
	public static final String PARAM_STOREREF = "storeRef";
	public static final String PARAM_STARTRESULT = "startResult";
	public static final String PARAM_TASKID = "taskID";
	public static final String PARAM_TEMPLATEDEFINITION = "templateDefinition";
	public static final String PARAM_TRANSITIONID ="transactionId";
	public static final String PARAM_TYPE = "type";
	public static final String PARAM_TYPEQNAME = "typeQname";
	public static final String PARAM_TYPEQNAMEPATTERN = "typeQnamePattern";
	public static final String PARAM_QNAMEPATTERN = "qnamePattern";
	public static final String PARAM_USESERIALIZATION = "useSerializable";
	public static final String PARAM_USETRANSACTION = "useTransaction";
	public static final String PARAM_USERNAME = "userName";
	public static final String PARAM_VERSION = "cmaVersion";
	public static final String PARAM_VERSIONCHILDREN = "versionChildren";
	public static final String PARAM_VERSIONSERVICEVERSION = "version";
	public static final String PARAM_WORKFLOWDEFINITIONID = "workflowDefinitionId";
	public static final String PARAM_WORKFLOWID = "workflowId";
	public static final String PARAM_WORKFLOWNAME = "workflowName";
	public static final String PARAM_WORKFLOWINSTANCEID = "workflowInstanceId";

	
	// service names
	public static final String PARAM_SERVICE = "service";
	
	public static final String SERVICE_ACTION = "actionService";
	public static final String SERVICE_AUTHENTICATION = "authenticationService";
	public static final String SERVICE_CLASSIFICATION = "classificationService";
	public static final String SERVICE_CONTENT = "contentService";
	public static final String SERVICE_DICTIONARY = "dictionaryService";
	public static final String SERVICE_LIBRARY = "libraryService";
	public static final String SERVICE_MIMETYPE = "mimetypeService";
	public static final String SERVICE_NODE = "nodeService";
	public static final String SERVICE_PEOPLE = "peopleService";
	public static final String SERVICE_SEARCH = "searchService";
	public static final String SERVICE_SECURITY = "securityService";
	public static final String SERVICE_TRANSFORMATION = "transformationService";
	public static final String SERVICE_VERSION = "versionService";
	public static final String SERVICE_WORKFLOW = "workflowService";

	// method name parameters
	public static final String PARAM_METHOD = "method";
	
	// authentication service methods
	public static final String METHOD_AUTHENTICATION_AUTHENTICATE = "authenticate";
	public static final String METHOD_AUTHENTICATION_CHANGEPASSWORD = "changePassword";
	public static final String METHOD_AUTHENTICATION_CREATEAUTHENTICATION = "createAuthentication";
	public static final String METHOD_AUTHENTICATION_DELETEAUTHENTICATION = "deleteAuthentication";
	public static final String METHOD_AUTHENTICATION_GETAUTHENTICATIONENABLED = "getAuthenticationEnabled";
	public static final String METHOD_AUTHENTICATION_INVALIDATETICKET = "invalidate";
	public static final String METHOD_AUTHENTICATION_INVALIDATEUSERSESSION = "invalidateUserSession";
	public static final String METHOD_AUTHENTICATION_SETAUTHENTICATIONENABLED = "setAuthenticationEnabled";
	public static final String METHOD_AUTHENTICATION_SETPASSWORD = "setPassword";
	public static final String METHOD_AUTHENTICATION_SSOAUTHENTICATE = "ssoAuthenticate";
	public static final String METHOD_AUTHENTICATION_VALIDATE = "validate";
	
	//action service methods
	public static final String METHOD_ACTION_CREATEACTION = "createAction";
	public static final String METHOD_ACTION_CREATEACTIONWITHPARAMS = "createActionWithParams";
	public static final String METHOD_ACTION_CREATEACTIONCONDITION = "createActionCondition";
	public static final String METHOD_ACTION_CREATEACTIONCONDITIONWITHPARAMS = "createActionConditionWithParams";
	public static final String METHOD_ACTION_CREATECOMPOSITEACTION = "createCompositeAction";
	public static final String METHOD_ACTION_EVALUATEACTION = "evaluateAction";
	public static final String METHOD_ACTION_EVALUATEACTIONCONDITION = "evaluateActionCondition";
	public static final String METHOD_ACTION_EXECUTEACTION = "executeAction";
	public static final String METHOD_ACTION_EXECUTEACTIONWITHCHECKCONDITIONS = "executeActionWithCheckConditions";
	public static final String METHOD_ACTION_EXECUTEACTIONEXECUTEASYNCHRONOUSLY = "executeActionExecuteAsynchronously";
	public static final String METHOD_ACTION_GETACTION = "getAction";
	public static final String METHOD_ACTION_GETACTIONS = "getActions";
	public static final String METHOD_ACTION_REMOVEACTION = "removeAction";
	public static final String METHOD_ACTION_REMOVEALLACTIONS = "removeAllActions";
	public static final String METHOD_ACTION_SAVEACTION = "saveAction";
	
	//classification service methods
	public static final String METHOD_CLASSIFICATION_CREATECATEGORY = "createCategory";
	public static final String METHOD_CLASSIFICATION_CREATECLASSIFICATION = "createClassification";
	public static final String METHOD_CLASSIFICATION_CREATEROOTCATEGORY = "createRootCategory";
	public static final String METHOD_CLASSIFICATION_DELETECATEGORY = "deleteCategory";
	public static final String METHOD_CLASSIFICATION_DELETECLASSIFICATION = "deleteClassification";
	public static final String METHOD_CLASSIFICATION_GETCATEGORIES = "getCategories";
	public static final String METHOD_CLASSIFICATION_GETCHILDREN = "getChildren";
	public static final String METHOD_CLASSIFICATION_GETCLASSIFICATIONASPECTS = "getClassificationAspects";
	public static final String METHOD_CLASSIFICATION_GETCLASSIFICATIONS = "getClassifications";
	public static final String METHOD_CLASSIFICATION_GETROOTCATEGORIES = "getRootCategories";
	
	// content service methods
	public static final String METHOD_CONTENT_GETCONTENTURI = "getContentUri";
	public static final String METHOD_CONTENT_READCONTENT = "readContent";
	public static final String METHOD_CONTENT_WRITECONTENT = "writeContent";
	
	// dictionary service methods
	public static final String METHOD_DICTIONARY_GETALLASPECTS = "getAllAspects";
	public static final String METHOD_DICTIONARY_GETALLDATATYPES = "getAllDataTypes";
	public static final String METHOD_DICTIONARY_GETALLMODELS = "getAllModels";
	public static final String METHOD_DICTIONARY_GETALLPROPERTIES = "getAllProperties";
	public static final String METHOD_DICTIONARY_GETALLTYPES = "getAllTypes";
	public static final String METHOD_DICTIONARY_GETANONYMOUSTYPE = "getAnonymousType";
	public static final String METHOD_DICTIONARY_GETASPECT = "getAspect";
	public static final String METHOD_DICTIONARY_GETASPECTS = "getAspects";
	public static final String METHOD_DICTIONARY_GETASSOCIATION = "getAssociation";
	public static final String METHOD_DICTIONARY_GETCLASS = "getClass";
	//public static final String METHOD_DICTIONARY_GETDATATYPEPROPERTIES = "getDateTypeProperties";
	public static final String METHOD_DICTIONARY_GETDATATYPEBYQNAME = "getDataTypeByQName";
	public static final String METHOD_DICTIONARY_GETDATATYPEBYCLASS = "getDataTypeByClass";
	public static final String METHOD_DICTIONARY_GETDATATYPES = "getDataTypes";
	public static final String METHOD_DICTIONARY_GETMODEL = "getModel";
	public static final String METHOD_DICTIONARY_GETPROPERTIES = "getProperties";
	public static final String METHOD_DICTIONARY_GETPROPERTY = "getProperty";
	public static final String METHOD_DICTIONARY_GETTYPE = "getType";
	public static final String METHOD_DICTIONARY_GETTYPES = "getTypes";
	public static final String METHOD_DICTIONARY_ISSUBCLASS = "isSubClass";
	
	// library service methods
	public static final String METHOD_LIBRARY_CHECKOUT = "checkOut";
	public static final String METHOD_LIBRARY_CHECKIN = "checkIn";
	public static final String METHOD_LIBRARY_CANCELCHECKOUT = "cancelCheckOut";
	
	// Mimetype service methods
	public static final String METHOD_MIMETYPE_GETDISPLAYSBYEXTENSION = "getDisplaysByExtension";
	public static final String METHOD_MIMETYPE_GETDISPLAYSBYMIMETYPE= "getDisplaysByMimetype";
	public static final String METHOD_MIMETYPE_GETEXTENSION = "getExtension";
	public static final String METHOD_MIMETYPE_GETEXTENSIONSBYMIMETYPE = "getExtensionsByMimetype";
	public static final String METHOD_MIMETYPE_GETMIMETYPES = "getMimetypes";
	public static final String METHOD_MIMETYPE_GETMIMETYPESBYEXTENSION = "getMimetypesByExtension";
	public static final String METHOD_MIMETYPE_GUESSMIMETYPE = "guessMimetype";
	public static final String METHOD_MIMETYPE_ISTEXT = "isText";
		
	// node service methods
	public static final String METHOD_NODE_ADDASPECT = "addAspect";
	public static final String METHOD_NODE_ADDASPECTSWITHOUTPROP = "addAspectsWithoutProp";
	public static final String METHOD_NODE_ADDCHILD = "addChild";
	public static final String METHOD_NODE_COPYFILE = "copyFile";
	public static final String METHOD_NODE_COPYNODE = "copyNode";
	public static final String METHOD_NODE_COPYNODES = "copyNodes";
	public static final String METHOD_NODE_CREATEASSOCIATION = "createAssociation";
	public static final String METHOD_NODE_CREATEFILE = "createFile";
	public static final String METHOD_NODE_CREATEFILEFROMSTREAM = "createFileFromStream";
	public static final String METHOD_NODE_CREATEFOLDER = "createFolder";
	public static final String METHOD_NODE_CREATENODE = "createNode";
	public static final String METHOD_NODE_CREATESTORE = "createStore";
	public static final String METHOD_NODE_DELETE = "delete";
	public static final String METHOD_NODE_EXISTSNODEREF = "existsNodeRef";
	public static final String METHOD_NODE_EXISTSSTOREREF = "existsStoreRef";
	public static final String METHOD_NODE_GETASPECTS = "getAspects";
	public static final String METHOD_NODE_GETCHILDASSOCIATIONS = "getChildAssociations";
	public static final String METHOD_NODE_GETCHILDASSOCS = "getChildAssocs";
	public static final String METHOD_NODE_GETCHILDBYNAME = "getChildByName";
	public static final String METHOD_NODE_GETENCODING = "getEncoding";
	public static final String METHOD_NODE_GETMIMETYPE = "getMimeType";
	public static final String METHOD_NODE_GETNODE = "getNode";
	public static final String METHOD_NODE_GETPATH = "getPath";
	public static final String METHOD_NODE_GETPATHS = "getPaths";
	public static final String METHOD_NODE_GETPARENTASSOCIATIONS = "getParentAssociations";
	public static final String METHOD_NODE_GETPARENTASSOCS = "getParentAssocs";
	public static final String METHOD_NODE_GETPRIMARYPARENT = "getPrimaryParent";
	public static final String METHOD_NODE_GETPROPERTIES = "getProperties";
	public static final String METHOD_NODE_GETPROPERTY = "getProperty";
	public static final String METHOD_NODE_GETROOTNODE = "getRootNode";
	public static final String METHOD_NODE_GETSOURCEASSOCS = "getSourceAssocs";
	public static final String METHOD_NODE_GETSTOREARCHIVENODE = "getStoreArchiveNode";
	public static final String METHOD_NODE_GETSTORES = "getStores";
	public static final String METHOD_NODE_GETTARGETASSOCS = "getTargetAssocs";
	public static final String METHOD_NODE_GETTYPE = "getType";
	public static final String METHOD_NODE_HASASPECT = "hasAspect";
	public static final String METHOD_NODE_MOVE = "move";
	public static final String METHOD_NODE_MOVENODE = "moveNode";
	public static final String METHOD_NODE_MOVENODES = "moveNodes";
	public static final String METHOD_NODE_REMOVEASPECT = "removeAspect";
	public static final String METHOD_NODE_REMOVEASSOCIATION = "removeAssociation";
	public static final String METHOD_NODE_REMOVECHILD = "removeChild";
	public static final String METHOD_NODE_REMOVECHILDASSOCIATION = "removeChildAssociation";
	public static final String METHOD_NODE_REMOVEPROPERTY = "removeProperty";
	public static final String METHOD_NODE_REMOVESECONDARYCHILDASSOCIATION = "removeSecondayChildAssociation";
	public static final String METHOD_NODE_RENAME = "rename";
	public static final String METHOD_NODE_RESTORENODE = "restoreNode";
	public static final String METHOD_NODE_SETCHILDASSOCIATIONINDEX = "setChildAssociationIndex";
	public static final String METHOD_NODE_SETPROPERTIES = "setProperties";
	public static final String METHOD_NODE_SETPROPERTY = "setProperty";
	public static final String METHOD_NODE_SETTYPE = "setType";

	// people service methods
	public static final String METHOD_PEOPLE_ADDAUTHORITY = "addAuthority";
	public static final String METHOD_PEOPLE_ADDAUTHORITYBYNAME = "addAuthorityByName";
	public static final String METHOD_PEOPLE_ADDAUTHORITIESBYNAME = "addAuthoritiesByName";
	public static final String METHOD_PEOPLE_CREATEGROUP = "createGroup";
	public static final String METHOD_PEOPLE_CREATEPERSON = "createPerson";
	public static final String METHOD_PEOPLE_DELETEGROUP = "deleteGroup";
	public static final String METHOD_PEOPLE_DELETEPERSON = "deletePerson";
	public static final String METHOD_PEOPLE_GETALLPEOPLE = "getAllPeople";
	public static final String METHOD_PEOPLE_GETCONTAINERGROUPS = "getContainerGroups";
	public static final String METHOD_PEOPLE_GETGROUP = "getGroup";
	public static final String METHOD_PEOPLE_GETMEMBERS = "getMembers";
	public static final String METHOD_PEOPLE_GETPERSON = "getPerson";
	public static final String METHOD_PEOPLE_REMOVEAUTHORITY = "removeAuthority";
	public static final String METHOD_PEOPLE_SETPERSONPROPERTIES = "setPersonProperties";
	
	// search service methods
	public static final String METHOD_SEARCH_QUERY = "query";

	// security service methods
	public static final String METHOD_SECURITY_CLEARPERMISSION = "clearPermission";
	public static final String METHOD_SECURITY_DELETEPERMISSION = "deletePermission";
	public static final String METHOD_SECURITY_DELETEPERMISSIONS = "deletePermissions";
	public static final String METHOD_SECURITY_FINDNODESBYASSIGNEDPERMISSION = "findNodesByAssignedPermission";
	public static final String METHOD_SECURITY_FINDNODESBYASSIGNEDPERMISSIONFORCURRENTUSER = "findNodesByAssignedPermissionForCurrentUser";
	public static final String METHOD_SECURITY_GETALLAUTHORITIES = "getAllAuthorities";
	public static final String METHOD_SECURITY_GETALLPERMISSION = "getAllPermission";
	public static final String METHOD_SECURITY_GETALLSETPERMISSIONS = "getAllSetPermissions";
	public static final String METHOD_SECURITY_GETALLSETPERMISSIONSFORAUTHORITY = "getAllSetPermissionsForAuthority";
	public static final String METHOD_SECURITY_GETALLSETPERMISSIONSFORCURRENTUSER = "getAllSetPermissionsForCurrentUser";
	public static final String METHOD_SECURITY_GETINHERITPARENTPERMISSIONS = "getInheritParentPermissions";
	public static final String METHOD_SECURITY_GETPERMISSIONS = "getPermissions";
	public static final String METHOD_SECURITY_GETOWNERAUTHORITY = "getOwnerAuthority";
	public static final String METHOD_SECURITY_GETSETTABLEPERMISSIONSBYNODEREF = "getSettablePermissionsByNodeRef";
	public static final String METHOD_SECURITY_GETSETTABLEPERMISSIONSBYQNAME = "getSettablePermissionsByQName";
	public static final String METHOD_SECURITY_HASPERMISSION = "hasPermission";
	public static final String METHOD_SECURITY_SETINHERITPARENTPERMISSIONS = "setInheritParentPermissions";
	public static final String METHOD_SECURITY_SETPERMISSION = "setPermission";
	
	//Transformation service methods
	public static final String METHOD_TRANSFORMATION_ISTRANSFORMABLE = "isTransformable";
	public static final String METHOD_TRANSFORMATION_TRANSFORM = "transform";
	
	//version service methods
	public static final String METHOD_VERSION_GETVERSIONSTOREREFERENCE = "getVersionStoreReference";
	public static final String METHOD_VERSION_CREATEVERSION = "createVersion";
	public static final String METHOD_VERSION_CREATEVERSIONWITHVERSIONCHILDREN = "createVersionWithVersionChildren";
	public static final String METHOD_VERSION_CREATEVERSIONWITHNODEREFS = "createVersionWithNodeRefs";
	public static final String METHOD_VERSION_GETVERSIONHISTORY = "getVersionHistory";
	public static final String METHOD_VERSION_GETCURRENTVERSION = "getCurrentVersion";
	public static final String METHOD_VERSION_REVERT = "revert";
	public static final String METHOD_VERSION_REVERTDEEP = "revertDeep";
	public static final String METHOD_VERSION_REVERTTOVERSION = "revertToVersion";
	public static final String METHOD_VERSION_REVERTTOVERSIONDEEP = "revertToVersionDeep";
	public static final String METHOD_VERSION_RESTORE = "restore";
	public static final String METHOD_VERSION_RESTOREDEEP = "restoreDeep";
	public static final String METHOD_VERSION_DELETEVERSIONHISTORY = "deleteVersionHistory";
	
	//workflow service methods
	public static final String METHOD_WORKFLOW_CANCELWORKFLOW = "cancelWorkflow";
	public static final String METHOD_WORKFLOW_CREATEPACKAGE = "createPackage";
	public static final String METHOD_WORKFLOW_DELETEWORKFLOW = "deleteWorkflow";
	public static final String METHOD_WORKFLOW_DEPLOYDEFINITIONWITHNODEREFOFCONTENTOBJECT = "deployDefinitionWithNodeRefOfContentObject";
	public static final String METHOD_WORKFLOW_DEPLOYDEFINITION = "deployDefinition";
	public static final String METHOD_WORKFLOW_ENDTASK = "endTask";
	public static final String METHOD_WORKFLOW_FIREEVENT = "fireEvent";
	public static final String METHOD_WORKFLOW_GETACTIVEWORKFLOWS = "getActiveWorkflows";
	public static final String METHOD_WORKFLOW_GETDEFINITIONS = "getDefinitions";
	public static final String METHOD_WORKFLOW_GETALLDEFINITIONSBYNAME = "getAllDefinitionsByName";
	public static final String METHOD_WORKFLOW_GETASSIGNEDTASKS	= "getAssignedTasks";
	public static final String METHOD_WORKFLOW_GETPOOLEDTASKS = "getPooledTasks";
	public static final String METHOD_WORKFLOW_GETDEFINITIONBYID = "getDefinitionById";
	public static final String METHOD_WORKFLOW_GETDEFINITIONBYNAME = "getDefinitionByName";
	public static final String METHOD_WORKFLOW_GETDEFINITIONIMAGE = "getDefintionImage";
	public static final String METHOD_WORKFLOW_GETTASKBYID = "getTaskById";
	public static final String METHOD_WORKFLOW_GETTASKFORWORKFLOWPATH = "getTaskForWorkflowPath";
	public static final String METHOD_WORKFLOW_GETTIMERS = "getTimers";
	public static final String METHOD_WORKFLOW_GETWORKFLOWBYID = "getWorkflowById";
	public static final String METHOD_WORKFLOW_GETWORKFLOWPATHS = "getWorkflowPaths";
	public static final String METHOD_WORKFLOW_GETWORKFLOWSFORCONTENT = "getWorkflowsForContent";
	public static final String METHOD_WORKFLOW_ISDEFINITIONDEPLOYED = "isDefinitionDeployed";
	public static final String METHOD_WORKFLOW_QUERYTASKS = "queryTasks";
	public static final String METHOD_WORKFLOW_SIGNAL = "signal";
	public static final String METHOD_WORKFLOW_STARTWORKFLOW = "startWorkflow";
	public static final String METHOD_WORKFLOW_STARTWORKFLOWFROMTEMPLATE = "startWorkflowFromTemplate";
	public static final String METHOD_WORKFLOW_UNDEPLOYDEFINITION = "undeployDefinition";
	public static final String METHOD_WORKFLOW_UPDATETASK = "updateTask";
	public static final String METHOD_WORKFLOW_GETSTARTTASK = "getStartTask";
	
	// url patterns
	public static final String GET_URI_PARAM_ASSIGN_PATTERN = "=";
	public static final String GET_URI_PARAM_CONCAT_PATTERN = "&";
	public static final String GET_URI_PARAM_PATTERN = "?";
	public static final String QUERYSTRING_PARAM_ASSIGN_PATTERN = ",";
	public static final String QUERYSTRING_PARAM_CONCAT_PATTERN = "&";

	// server response content type
	public static final String RESPONSE_BINARY = "cma/binary";
	public static final String RESPONSE_ERROR = "cma/error";
	public static final String RESPONSE_NONE = "cma/none";
	public static final String RESPONSE_XML = "cma/response";
	public static final String RESPONSE_SERIALIZBLE = "cma/serializable";
	public static final String RESPONSE_SERVER_ERROR = "text/xml";
	public static final String REQUEST_READCONTENT = "cma/readContent";
	public static final String REQUEST_WRITECONTENT = "cma/writeContent";
	public static final String REQUEST_TEXT = "cma/text";
	public static final String CONTENTTYPE_XML = "text/xml";
	public static final String CONTENTTYPE_NONE = "text/plain";
	public static final String CONTENTTYPE_ERROR = "application/octet-stream";
	public static final String CONTENTTYPE_SERIALIZABLE = "application/octet-stream";
	public static final String CONTENTTYPE_XML_ENCODING = "UTF-8";
	public static final String CONTENTTYPE_NONE_ENCODING = "UTF-8";
	public static final String CONTENTTYPE_ERROR_ENCODING = "binary";
	public static final String CONTENTTYPE_SERIALIZABLE_ENCODING = "binary";
	
	public static final String HEADER_RESPONSETYPE = "cmaResponseType";
	public static final String HEADER_REQUESTTYPE = "cmaRequestType";
	public static final String HEADER_COOKIE = "Cookie";
	
	// cma mapping version
	public static final String CMA_VERSION = "4.1.2-E-0";
	public static final int DEFAULT_NUMBER_OF_VERSIONS = 5;
	public static final int DEFAULT_NUMBER_OF_SERVICES = 12;
	public static final int DEFAULT_NUMBER_OF_METHODS = 3;
	
	// file download buffer length
	public static final int READ_BUFFER_LENGTH = 32 * 1024;
	
	// alfresco content download url
    public static final String ALFRESCO_CONTENT_DOWNLOAD_URL = "/d/a/{0}/{1}/{2}/{3}";
    
}
