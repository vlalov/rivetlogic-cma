/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.api;

import java.util.Map;
import java.util.Set;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.security.AccessPermission;
import org.alfresco.service.cmr.security.AccessStatus;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.repo.Ticket;

public interface SecurityService {
	
    public static final String FULL_CONTROL = "FullControl";
    public static final String READ = "Read";
    public static final String WRITE = "Write";
    public static final String DELETE = "Delete";
    public static final String ADD_CHILDREN = "AddChildren";
    public static final String READ_PROPERTIES = "ReadProperties";
    public static final String READ_CHILDREN = "ReadChildren";
    public static final String WRITE_PROPERTIES = "WriteProperties";
    public static final String DELETE_NODE = "DeleteNode";
    public static final String DELETE_CHILDREN = "DeleteChildren";
    public static final String CREATE_CHILDREN = "CreateChildren";
    public static final String LINK_CHILDREN = "LinkChildren";
    public static final String DELETE_ASSOCIATIONS = "DeleteAssociations";
    public static final String READ_ASSOCIATIONS = "ReadAssociations";
    public static final String CREATE_ASSOCIATIONS = "CreateAssociations";
    public static final String READ_PERMISSIONS = "ReadPermissions";
    public static final String CHANGE_PERMISSIONS = "ChangePermissions";
    public static final String EXECUTE = "Execute";
    public static final String READ_CONTENT = "ReadContent";
    public static final String WRITE_CONTENT = "WriteContent";
    public static final String EXECUTE_CONTENT = "ExecuteContent";
    public static final String TAKE_OWNERSHIP = "TakeOwnership";
    public static final String SET_OWNER = "SetOwner";
    public static final String COORDINATOR = "Coordinator";
    public static final String CONTRIBUTOR = "Contributor";
    public static final String EDITOR = "Editor";
    public static final String CONSUMER = "Consumer";
    public static final String LOCK = "Lock";
    public static final String UNLOCK = "Unlock";
    public static final String CHECK_OUT = "CheckOut";
    public static final String CHECK_IN = "CheckIn";
    public static final String CANCEL_CHECK_OUT = "CancelCheckOut";

	/**
	 * Get the Owner Authority
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @return the owner authority
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	public String getOwnerAuthority(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get the All Authorities
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @return the All authorities
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	public String getAllAuthorities(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get the All Permission
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @return the All permission
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	public String getAllPermission(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get all the AccessPermissions that are granted/denied to the current
	 * authentication for the given node
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef -
	 *            the reference to the node
	 * @return the set of allowed permissions
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	public Set<AccessPermission> getPermissions(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get the permissions that can be set for a given node
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef -
	 *            the reference to the node
	 * @return the set of allowed permissions for the given nodeRef
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	public Set<String> getSettablePermissions(Ticket ticket, NodeRef nodeRef) 
			throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * Get the permissions that can be set for a given type
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param type
	 *            type QName
	 * @return set of permissions for the given type
	 * @return the set of allowed permissions for the given nodeRef
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	public Set<String> getSettablePermissions(Ticket ticket, QName type) 
	throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * Get all the AccessPermissions that are set for anyone for the given node
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef -
	 *            the reference to the node
	 * @return the set of allowed permissions
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	public Set<AccessPermission> getAllSetPermissions(Ticket ticket,
			NodeRef nodeRef) throws InvalidTicketException, CmaRuntimeException;


	/**
	 * Check that the given authentication has a particular permission for the
	 * given node. (The default behaviour is to inherit permissions)
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 * @param permission
	 * @return - access status
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	public AccessStatus hasPermission(Ticket ticket, NodeRef nodeRef,
			String permission) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Delete all the permission assigned to the node
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	public void deletePermissions(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Delete all permission for the given authority.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 * @param authority
	 *            (if null then this will match all authorities)
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	public void clearPermission(Ticket ticket, NodeRef nodeRef, String authority)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Find and delete a access control entry by node, authentication and
	 * permission. It is possible to delete
	 * <ol>
	 * <li> a specific permission;
	 * <li> all permissions for an authority (if the permission is null);
	 * <li> entries for all authorities that have a specific permission (if the
	 * authority is null); and
	 * <li> all permissions set for the node (if both the permission and
	 * authority are null).
	 * </ol>
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node that the entry applies to
	 * @param authority
	 *            the authority recipient (if null then this will match all
	 *            authorities)
	 * @param permission
	 *            the entry permission (if null then this will match all
	 *            permissions)
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	public void deletePermission(Ticket ticket, NodeRef nodeRef,
			String authority, String permission) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Set a specific permission on a node.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 * @param authority
	 * @param permission
	 * @param allow
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	public void setPermission(Ticket ticket, NodeRef nodeRef, String authority,
			String permission, boolean allow) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Set the global inheritance behavior for permissions on a node.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 * @param inheritParentPermissions
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	public void setInheritParentPermissions(Ticket ticket, NodeRef nodeRef,
			boolean inheritParentPermissions) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Return the global inheritance behavior for permissions on a node.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 * @return inheritParentPermissions
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	public boolean getInheritParentPermissions(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get all permissions set for the current user.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @return - A map of noderefs to permissions set
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	@Deprecated
	public Map<NodeRef, Set<AccessPermission>> getAllSetPermissionsForCurrentUser(
			Ticket ticket) throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get all the permissions set for the given authority
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param authority
	 * @return - A map of noderefs to permissions set
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	@Deprecated
	public Map<NodeRef, Set<AccessPermission>> getAllSetPermissionsForAuthority(
			Ticket ticket, String authority) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Find all the nodes where the current user has explicitly been assigned
	 * the specified permission.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param permission -
	 *            the permission to find
	 * @param allow
	 *            -search for allow (true) or deny
	 * @param includeContainingAuthorities -
	 *            include permissions for authorities that contain the current
	 *            user in the list
	 * @param includeContainingPermissions -
	 *            true; do an exact match: false; search for any permission that
	 *            woudl imply the one given
	 * @return - the set of nodes where the user is assigned the permission
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	@Deprecated
	public Set<NodeRef> findNodesByAssignedPermissionForCurrentUser(
			Ticket ticket, String permission, boolean allow,
			boolean includeContainingAuthorities,
			boolean includeContainingPermissions)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Find all the nodes where the current user has explicitly been assigned
	 * the specified permission.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param permission -
	 *            the permission to find
	 * @param allow
	 *            -search for allow (true) or deny
	 * @param includeContainingAuthorities -
	 *            include permissions for authorities that contain the current
	 *            user in the list
	 * @param exactPermissionMatch -
	 *            true; do an exact match: false; search for any permission that
	 *            would imply the one given
	 * @return - the set of nodes where the user is assigned the permission
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	@Deprecated
	public Set<NodeRef> findNodesByAssignedPermission(Ticket ticket,
			String authority, String permission, boolean allow,
			boolean includeContainingAuthorities, boolean exactPermissionMatch)
			throws InvalidTicketException, CmaRuntimeException;
}
