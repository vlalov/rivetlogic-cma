/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.repo;

import java.util.Locale;

import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.dictionary.ModelDefinition;
import org.alfresco.service.namespace.QName;

/**
 * This class is an implementation of Alfresco DataTypeDefinition which holds
 * DataTypeDefinition information retrieved from Alfresco
 * 
 * @author Hyanghee Lim
 *
 */
public class CmaDataTypeDefinition implements DataTypeDefinition {

	private String description;
	private String javaClassName;
	private ModelDefinition model;
	private QName name;
	private String title;

	/**
	 * not supported
	 * @throws UnsupportedOperationException
	 */
	public String getAnalyserClassName() {
		throw new UnsupportedOperationException("The method getAnalyserClassName() is not supported");
	}

	/**
	 * not supported
	 * @throws UnsupportedOperationException
	 */
	public String getAnalyserClassName(Locale locale) {
		throw new UnsupportedOperationException("The method getAnalyserClassName(Locale arg) is not supported");
	}

	public String getDescription() {
		return description;
	}

	public String getJavaClassName() {
		return javaClassName;
	}

	public ModelDefinition getModel() {
		return model;
	}

	public QName getName() {
		return name;
	}

	public String getTitle() {
		return title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setJavaClassName(String javaClassName) {
		this.javaClassName = javaClassName;
	}

	public void setModel(ModelDefinition model) {
		this.model = model;
	}

	public void setName(QName name) {
		this.name = name;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAnalyserResourceBundleName() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getDefaultAnalyserClassName() {
		// TODO Auto-generated method stub
		return null;
	}

	public String resolveAnalyserClassName() {
		// TODO Auto-generated method stub
		return null;
	}

	public String resolveAnalyserClassName(Locale arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}
