/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.repo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.security.AccessPermission;
import org.alfresco.service.cmr.security.AccessStatus;
import org.alfresco.service.namespace.QName;
import org.exolab.castor.mapping.MapItem;

/**
 * Node
 * 
 * This class is a representation of Alfresco Node containing nodeRef,
 * properties, peer associations, child associations, and aspects
 * 
 * @author Hyanghee Lim
 * 
 */
public class Node implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5423073207897806062L;
	private NodeRef nodeRef;
	private QName type;
	private Map<QName, Serializable> properties;
	private List<AssociationRef> sourceAssocs;
	private List<AssociationRef> targetAssocs;
	private List<ChildAssociationRef> childAssocs;
	private Set<QName> aspects;
	private Set<AccessPermission> permissions;
	private Map<String, AccessStatus> permissionsToCheckFor;

	/**
	 * @return the NodeRef this Node object represents
	 */
	public NodeRef getNodeRef() {
		return nodeRef;
	}

	/**
	 * 
	 * @return the type of this Node
	 */
	public QName getType() {
		return type;
	}

	/**
	 * @return properties of this Node
	 */
	public Map<QName, Serializable> getProperties() {
		return properties;
	}

	/**
	 * @return source associations of this Node
	 */
	public List<AssociationRef> getSourceAssocs() {
		return sourceAssocs;
	}

	/**
	 * @return target associations of this Node
	 */
	public List<AssociationRef> getTargetAssocs() {
		return targetAssocs;
	}

	/**
	 * @return child associations of this Node
	 */
	public List<ChildAssociationRef> getChildAssocs() {
		return childAssocs;
	}

	/**
	 * @return aspects of this Node as a set
	 */
	public Set<QName> getAspects() {
		return aspects;
	}

	public Set<AccessPermission> getPermissions() {
		return permissions;
	}

	public void addToPropertyMap(MapItem item) {
		QName qName = (QName) item.getKey();
		Serializable value = (Serializable) item.getValue();
		properties.put(qName, value);
	}

	/**
	 * @param nodeRef
	 */
	public void setNodeRef(NodeRef nodeRef) {
		this.nodeRef = nodeRef;
	}

	/**
	 * 
	 * @param type
	 */
	public void setType(QName type) {
		this.type = type;
	}

	/**
	 * @param properties
	 */
	public void setProperties(Map<QName, Serializable> properties) {
		this.properties = properties;
	}

	/**
	 * @param targetAssocs
	 */
	public void setTargetAssocs(List<AssociationRef> targetAssocs) {
		this.targetAssocs = targetAssocs;
	}

	/**
	 * @param sourceAssocs
	 */
	public void setSourceAssocs(List<AssociationRef> sourceAssocs) {
		this.sourceAssocs = sourceAssocs;
	}

	/**
	 * @param childAssocs
	 */
	public void setChildAssocs(List<ChildAssociationRef> childAssocs) {
		this.childAssocs = childAssocs;
	}

	/**
	 * @param aspects
	 */
	public void setAspects(Set<QName> aspects) {
		this.aspects = aspects;
	}


	/**
	 * @param aspects
	 */
	@Deprecated
	public void setAspects(Vector<QName> aspects) {
		if (aspects != null) {
			this.aspects = new HashSet<QName>(aspects.size());
			this.aspects.addAll(aspects);
		}
	}

	public void setPermissions(Set<AccessPermission> permissions) {
		this.permissions = permissions;
	}

	public String toString() {
		return nodeRef.toString();
	}

	public int hashCode() {
		return nodeRef.hashCode();
	}

	public Map<String, AccessStatus> getPermissionsToCheckFor() {
		return permissionsToCheckFor;
	}

	public void setPermissionsToCheckFor(
			Map<String, AccessStatus> permissionsToCheckFor) {
		this.permissionsToCheckFor = permissionsToCheckFor;
	}

}
