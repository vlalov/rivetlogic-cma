/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.api;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.exception.AuthorityExistsException;
import com.rivetlogic.core.cma.exception.AuthorityNotFoundException;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.repo.Ticket;

public interface PeopleService {
	
	/**
	 * Create a new user with the given properties
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param userName
	 *        new user name
	 * @param properties
	 *        user properties
	 * @return User nodeRef
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public NodeRef createPerson(Ticket ticket, String userName, char [] password, Map<QName, Serializable> properties)  
			throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * Delete a user identified by the given user name
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param person
	 *            a nodeRef associated wit a person
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void deletePerson(Ticket ticket, NodeRef person)
			throws InvalidTicketException, CmaRuntimeException;
	
	
	/**
	 * Get all users from the repository
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @return all users in the repository
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<NodeRef> getAllPeople(Ticket ticket) 
			throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * Get a user identified by the given user name 
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param userName
	 *            Username to retrieve
	 * @return User nodeRef
	 * @throws InvalidTicketException
	 * @throws AuthorityNotFoundException
	 *           when an authority (a person or a group) is not found by the user name
	 * @throws CmaRuntimeException
	 */
	public NodeRef getPerson(Ticket ticket, String userName)
			throws InvalidTicketException, AuthorityNotFoundException, CmaRuntimeException;
	
	/**
	 * Set the properities on a user identnfied by a user name
	 *  
	 * @param ticket
	 * @param person
	 *         a nodeRef associated with a person
	 * @param properties
	 * @throws InvalidTicketException
	 * @throws AuthorityNotFoundException
	 *           when an authority (a person or a group) is not found by the user name
	 * @throws CmaRuntimeException
	 */
	public void setPersonProperties(Ticket ticket, NodeRef person, Map<QName, Serializable> properties)
		throws InvalidTicketException, AuthorityNotFoundException, CmaRuntimeException;
	
	
	/**
	 * Get a group identified by the given group name
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param groupName
	 *            a group name
	 * @return Group NodeRef
	 * @throws InvalidTicketException
	 * @throws AuthorityNotFoundException
	 *           when an authority (a person or a group) is not found by the user name
	 * @throws CmaRuntimeException
	 */
	public NodeRef getGroup(Ticket ticket, String groupName)
		throws InvalidTicketException, AuthorityNotFoundException, CmaRuntimeException;
	
	/**
	 * Delete a group identified by the given group name
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param group
	 *            a nodeRef associated with a group name
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void deleteGroup(Ticket ticket, NodeRef group)
		throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * Create a new group as a child of a top-level group 
	 * 
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param groupName
	 *            a group name
	 * @return a nodeRef associated with the group created 
	 *         or an existing group if the group already exists 
	 * @throws InvalidTicketException
	 * @throws AuthorityExistsException
	 *           when an authority (a person or a group) 
	 *           with the given name already exists
	 * @throws CmaRuntimeException
	 */
	public NodeRef createGroup(Ticket ticket, String groupName)
		throws InvalidTicketException, AuthorityExistsException, CmaRuntimeException;

	/**
	 * Create a new group as a child of the specified parent group
	 *  
	 * if the parent group is null, the new group becomes a child of a top-level group 
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param parentGroup
	 *            the parent group to create a new group to
	 * @param groupName
	 *            a group name
	 * @return Group NodeRef
	 * @throws InvalidTicketException
	 * @throws AuthorityExistsException
	 *           when an authority (a person or a group) 
	 *           with the given name already exists
	 * @throws CmaRuntimeException
	 */
	public NodeRef createGroup(Ticket ticket, NodeRef parentGroup, String groupName)
		throws InvalidTicketException, AuthorityExistsException, CmaRuntimeException;
	
	/**
	 * Get members of a given group
	 *  
	 * @param ticket
	 *            Ticket received post authentication
	 * @param group
	 *            Group nodeRef
	 * @return all members of a given group
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<NodeRef> getMembers(Ticket ticket, NodeRef group) 
		throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * Get members of a given group 
	 * include all members of its sub-groups if recurse is true 
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param group
	 *            Group nodeRef
	 * @param recurse
	 *            include sub-groups or not
	 * @return all members of a give group and its sub-groups
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<NodeRef> getMembers(Ticket ticket, NodeRef group, boolean recurse) 
		throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * get the groups that contain the specified person
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param person
	 *            Person nodeRef
	 * @return a list of groups that contain the specified person
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<NodeRef> getContainerGroups(Ticket ticket, NodeRef person) 
		throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * Add an authority (user or group) to a given group
	 * @param ticket
	 *            Ticket received post authentication
	 * @param parentGroup
	 *            the parent group to add the authority to
	 * @param authority
	 *            Group or person NodeRef
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void addAuthority(Ticket ticket, NodeRef parentGroup, NodeRef authority)
		throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Add an authority (user or group) to a given group by name
	 * @param ticket
	 *            Ticket received post authentication
	 * @param parentGroupName
	 *            the parent group to add the authority to
	 * @param authorityName
	 *            Group or person name
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void addAuthority(Ticket ticket, String parentGroupName, String authorityName)
		throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Add authorities (user or group) to a given group by name
	 * @param ticket
	 *            Ticket received post authentication
	 * @param parentGroupName
	 *            the parent group to add the authority to
	 * @param authorityNames
	 *            Group or person names
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void addAuthorities(Ticket ticket, String parentGroupName, 
			List<String> authorityNames)
		throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Remove an authority (user or group) from a given group
	 * @param ticket
	 *            Ticket received post authentication
	 * @param parentGroup
	 *            the parent group to remove the authority from
	 * @param authority
	 *            Group or person NodeRef
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void removeAuthority(Ticket ticket, NodeRef parentGroup, NodeRef authority)
		throws InvalidTicketException, CmaRuntimeException;
}

