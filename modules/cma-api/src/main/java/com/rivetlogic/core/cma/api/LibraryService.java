/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.api;

import org.alfresco.service.cmr.lock.NodeLockedException;
import org.alfresco.service.cmr.repository.AspectMissingException;
import org.alfresco.service.cmr.repository.NodeRef;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.repo.Ticket;

public interface LibraryService {
	/**
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            Node to act upon
	 * @param destination
	 *            Destination space to check out a working copy to, if null, the
	 *            current parent space will be used for the working-copy
	 * @return The working-copy nodeRef
	 * @throws InvalidTicketException
	 * @throws ContentLockedException
	 * @throws CmaRuntimeException
	 */
	NodeRef checkOut(Ticket ticket, NodeRef nodeRef, NodeRef destination)
			throws InvalidTicketException, NodeLockedException,
			CmaRuntimeException;

	/**
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            Working copy to check in
	 * @param revisionHistory
	 *            Comment to attach to the checked in version
	 * @param majorVersion
	 *            Boolean to indicate if this is a major revision (updates the
	 *            major version number instead of the minor)
	 * @return Reference to the original checked-out file after it's updated
	 * @throws InvalidTicketException
	 * @throws ContentNotLockedException
	 * @throws CmaRuntimeException
	 */
	NodeRef checkIn(Ticket ticket, NodeRef nodeRef, String revisionHistory,
			boolean majorVersion) throws InvalidTicketException,
			AspectMissingException, CmaRuntimeException;

	/**
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            Working copy to cancel the checkout for
	 * @return Reference to the original checked-out file
	 * @throws InvalidTicketException
	 * @throws ContentNotLockedException
	 * @throws CmaRuntimeException
	 */
	NodeRef cancelCheckOut(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, AspectMissingException,
			CmaRuntimeException;
}
