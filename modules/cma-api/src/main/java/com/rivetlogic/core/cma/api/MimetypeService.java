/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.api;

import java.util.List;
import java.util.Map;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.repo.Ticket;

/**
 * This service interface provides support for Mimetypes.
 * 
 * @author Hyanghee Lim
 *
 */
public interface MimetypeService {

	/**
	 * Get all human readable mimetype descriptions indexed by mimetype extension
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @return a map of mimetypes indexed by extension
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public Map<String, String> getDisplaysByExtension(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get all human readable mimetype descriptions indexed by mimetype
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @return a map of mimetypes indexed by extension
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public Map<String, String> getDisplaysByMimetype(Ticket ticket)
		throws InvalidTicketException, CmaRuntimeException;
 
	/**
	 * Get the extension for the specified mimetype
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param mimetype
	 * @return extension
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public String getExtension(Ticket ticket, String mimetype) 
		throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * Get all mimetype extensions indexed by mimetype
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @return a map of extensions indexed by mimetype
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public Map<String, String> getExtensionsByMimetype(Ticket ticket)
		throws InvalidTicketException, CmaRuntimeException;
	 
	/**
	 * Get all mimetypes
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @return all mimetypes
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<String> getMimetypes(Ticket ticket)
		throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get all mimetypes indexed by extension
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @return a map of mimetypes indexed by extension
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public Map<String, String> getMimetypesByExtension(Ticket ticket)
		throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Provides a non-null best guess of the appropriate mimetype given a filename.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param fileName
	 * 			file name to guess mimetype from
	 * @return mimetype
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public String guessMimetype(Ticket ticket, String fileName)
		throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * Check if a given mimetype represents a text format.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param mimetype
	 * @return is a text format
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public boolean isText(Ticket ticket, String mimetype)
		throws InvalidTicketException, CmaRuntimeException;
	
}
