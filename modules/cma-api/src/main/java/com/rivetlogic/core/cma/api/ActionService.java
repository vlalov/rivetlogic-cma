/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.api;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionCondition;
import org.alfresco.service.cmr.action.CompositeAction;
import org.alfresco.service.cmr.repository.NodeRef;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.repo.Ticket;

public interface ActionService {
	/**
	 * Create a new action
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param name	the action definition name
	 * @return		the action
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	Action createAction(Ticket ticket, String name) throws InvalidTicketException,
	CmaRuntimeException;
	
	/**
	 * Create a new action specifying the initial set of parameter values
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param name		the action definition name
	 * @param params	the parameter values
	 * @return			the action
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	Action createAction(Ticket ticket, String name, Map<String, Serializable> params) throws InvalidTicketException,
	CmaRuntimeException;
	
	/**
	 * Create a composite action 
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @return	the composite action
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	CompositeAction createCompositeAction(Ticket ticket) throws InvalidTicketException,
	CmaRuntimeException;
	
	/**
	 * Create an action condition
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param name	the action condition definition name
	 * @return		the action condition
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	ActionCondition createActionCondition(Ticket ticket, String name) throws InvalidTicketException,
	CmaRuntimeException;
	
	/**
	 * Create an action condition specifying the initial set of parameter values
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param name		the action condition definition name
	 * @param params	the parameter values
	 * @return			the action condition
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	ActionCondition createActionCondition(Ticket ticket, String name, Map<String, Serializable> params) throws InvalidTicketException,
	CmaRuntimeException;
	
	/**
	 * The actions conditions are always checked.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 *  
	 * @param action				the action
	 * @param actionedUponNodeRef	the actione'd-upon node reference
	 * @return Serializable
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	Serializable executeAction(Ticket ticket, Action action, NodeRef actionedUponNodeRef) throws InvalidTicketException,
	CmaRuntimeException;
	
	/**
	 * The action is executed based on the asynchronous attribute of the action.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * 
	 * @param action				the action
	 * @param actionedUponNodeRef	the actione'd-upon node reference
	 * @param checkConditions		indicates whether the conditions should be checked
	 * @return Serializable
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	Serializable executeAction(Ticket ticket, Action action, NodeRef actionedUponNodeRef, boolean checkConditions) throws InvalidTicketException,
	CmaRuntimeException;
	
	/**
	 * Executes the specified action upon the node reference provided.
	 * <p>
	 * If specified that the conditions should be checked then any conditions
	 * set on the action are evaluated.
	 * <p>
	 * If the conditions fail then the action is not executed.
	 * <p>
	 * If an action has no conditions then the action will always be executed.
	 * <p>
	 * If the conditions are not checked then the action will always be executed.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param action				the action
	 * @param actionedUponNodeRef	the actione'd-upon node reference
	 * @param checkConditions		indicates whether the conditions should be checked before
	 * 								executing the action
	 * @param executeAsynchronously	indicates whether the action should be executed asynchronously or not, this value overrides
	 * 								the value set on the action its self
	 * @return Serializable
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	Serializable executeAction(Ticket ticket, Action action, NodeRef actionedUponNodeRef, boolean checkConditions, boolean executeAsynchronously) throws InvalidTicketException,
	CmaRuntimeException;
	
	/**
	 * Evaluate the conditions set on an action.
	 * <p>
	 * Returns true if the action has no conditions.
	 * <p>
	 * If the action has more than one condition their results are combined using the 'AND' 
	 * logical operator.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param action				the action
	 * @param actionedUponNodeRef	the actione'd-upon node reference
	 * @return						true if the condition succeeds, false otherwise
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	boolean evaluateAction(Ticket ticket, Action action, NodeRef actionedUponNodeRef) throws InvalidTicketException,
	CmaRuntimeException;
	
	/**
	 * Evaluate an action condition.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param condition				the action condition
	 * @param actionedUponNodeRef	the actione'd-upon node reference
	 * @return						true if the condition succeeds, false otherwise
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	boolean evaluateActionCondition(Ticket ticket, ActionCondition condition, NodeRef actionedUponNodeRef) throws InvalidTicketException,
	CmaRuntimeException;
	
	/**
	 * Save an action against a node reference.
	 * <p>
	 * The node will be made configurable if it is not already.
	 * <p>
	 * If the action already exists then its details will be updated.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef	the node reference
	 * @param action	the action
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	void saveAction(Ticket ticket, NodeRef nodeRef, Action action) throws InvalidTicketException,
	CmaRuntimeException;
	
	/**
	 * Gets all the actions currently saved on the given node reference.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef	the node reference
	 * @return			the list of actions
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	List<Action> getActions(Ticket ticket, NodeRef nodeRef) throws InvalidTicketException,
	CmaRuntimeException;
	
	/**
	 * Gets an action stored against a given node reference.
	 * <p>
	 * Returns null if the action can not be found.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef	the node reference
	 * @param actionId	the action id
	 * @return			the action
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	Action getAction(Ticket ticket, NodeRef nodeRef, String actionId) throws InvalidTicketException,
	CmaRuntimeException;
	
	/**
	 * Removes an action associated with a node reference.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef		the node reference
	 * @param action		the action
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	void removeAction(Ticket ticket, NodeRef nodeRef, Action action) throws InvalidTicketException,
	CmaRuntimeException;
	
	/**
	 * Removes all actions associated with a node reference
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef	the node reference
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	void removeAllActions(Ticket ticket, NodeRef nodeRef) throws InvalidTicketException,
	CmaRuntimeException;
}
