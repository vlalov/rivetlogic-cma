/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.api;

import java.util.Collection;

import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.CategoryService.Depth;
import org.alfresco.service.cmr.search.CategoryService.Mode;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.repo.Ticket;

public interface ClassificationService {

	/**
	 * Create a new category
	 * 
	 * @param parent
	 *            a parent nodeRef to create a new category to
	 * @param name
	 *            a new category name
	 * @return a nodeRef associated with the new category
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	NodeRef createCategory(Ticket ticket, NodeRef parent, String name)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Create a new classification. This will extend the category types in the
	 * data dictionary All it needs is the type name and the attribute in which
	 * to store noderefs to categories.
	 * 
	 * @param storeRef
	 *            a storeRef to create to
	 * @param aspectName
	 *            a new aspect name
	 * @param name
	 *            a new classification name
	 * @return a nodeRef associated with the new classification
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	NodeRef createClassification(Ticket ticket, StoreRef storeRef, QName aspectName, String name)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Create a root category
	 * 
	 * @param storeRef
	 *            a storeRef to create to
	 * @param aspectName
	 *            a new aspect name
	 * @param name
	 *            a new root category name
	 * @return a nodeRef associated with the new classification
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	NodeRef createRootCategory(Ticket ticket, StoreRef storeRef, QName aspectName, String name)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Delete a category
	 * 
	 * @param nodeRef
	 *            a nodeRef associated with a category
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	void deleteCategory(Ticket ticket, NodeRef nodeRef) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Delete a classification
	 * 
	 * @param storeRef
	 *            a store to remove the classification from
	 * @param aspectName
	 *            aspect name
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	void deleteClassification(Ticket ticket, StoreRef storeRef, QName aspectName)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get a list of all the categories appropriate for a given property. The
	 * full list of categories that may be assigned for this aspect.
	 * 
	 * @param storeRef
	 *            a store to get categories from
	 * @param aspectQName
	 *            aspect name
	 * @param depth
	 *            the enumeration depth for what level to recover
	 * @return a list of all the nodes found identified by their ChildAssocRef's
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	Collection<ChildAssociationRef> getCategories(Ticket ticket, StoreRef storeRef,
			QName aspectQName, Depth depth) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Get the children of a given category node
	 * 
	 * @param categoryRef
	 *            the category node
	 * @param mode
	 *            the enumeration mode for what to recover
	 * @param depth
	 *            the enumeration depth for what level to recover
	 * @return a list of all the nodes found identified by their ChildAssocRef's
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	Collection<ChildAssociationRef> getChildren(Ticket ticket, NodeRef categoryRef, Mode mode,
			Depth depth) throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get all the types that represent categories
	 * 
	 * @return a list of all QNames found
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	Collection<QName> getClassificationAspects(Ticket ticket) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Get all the classification entries
	 * 
	 * @param storeRef
	 *            a store to get classifications from
	 * @return a list of all the nodes found identified by their ChildAssocRef's
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	Collection<ChildAssociationRef> getClassifications(Ticket ticket, StoreRef storeRef)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get the root categories for an aspect/classification
	 * 
	 * @param storeRef
	 *            a store to get categories from
	 * @param aspectName
	 *            aspect name
	 * @return a list of all the nodes found identified by their ChildAssocRef's
	 * @throws InvalidTicketException
	 *             if failed to authenticate
	 * @throws CmaRuntimeException
	 *             if failed to retrieve a result
	 */
	Collection<ChildAssociationRef> getRootCategories(Ticket ticket, StoreRef storeRef,
			QName aspectName) throws InvalidTicketException,
			CmaRuntimeException;

}
