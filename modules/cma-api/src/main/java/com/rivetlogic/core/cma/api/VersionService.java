/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.api;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

import org.alfresco.service.cmr.repository.AspectMissingException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.version.ReservedVersionNameException;
import org.alfresco.service.cmr.version.Version;
import org.alfresco.service.cmr.version.VersionHistory;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.repo.Ticket;

public interface VersionService {
    /**
     * Gets the reference to the version store
     * 
     * @param ticket
	 *            Ticket received post authentication
     * @return  reference to the version store
     * @throws InvalidTicketException
	 * @throws CmaRuntimeException
     */
    public StoreRef getVersionStoreReference(Ticket ticket) throws InvalidTicketException, CmaRuntimeException;
    
    /**
     * Creates a new version based on the referenced node.
     * <p>
     * If the node has not previously been versioned then a version history and
     * initial version will be created.
     * <p>
     * If the node referenced does not or can not have the version aspect
     * applied to it then an exception will be raised.
     * <p>
     * The version properties are stored as version meta-data against the newly
     * created version.
     * 
     * @param ticket
	 *            Ticket received post authentication
     * @param  nodeRef              a node reference
     * @param  versionProperties    the version properties that are stored with the newly created
     *                              version
     * @return                      the created version object
     * @throws ReservedVersionNameException
     *                              thrown if a reserved property name is used int he version properties 
     *                              provided
     * @throws AspectMissingException
     *                              thrown if the version aspect is missing  
     * @throws InvalidTicketException
	 * @throws CmaRuntimeException                            
     */
    public Version createVersion(Ticket ticket,
            NodeRef nodeRef, 
            Map<String, Serializable> versionProperties)
    throws InvalidTicketException, CmaRuntimeException, ReservedVersionNameException, AspectMissingException;

    /**
     * Creates a new version based on the referenced node.
     * <p>
     * If the node has not previously been versioned then a version history and
     * initial version will be created.
     * <p>
     * If the node referenced does not or can not have the version aspect
     * applied to it then an exception will be raised.
     * <p>
     * The version properties are stored as version meta-data against the newly
     * created version.
     * 
     * @param ticket
	 *            Ticket received post authentication
     * @param nodeRef               a node reference
     * @param versionProperties     the version properties that are stored with the newly created
     *                              version
     * @param versionChildren       if true then the children of the referenced node are also
     *                              versioned, false otherwise
     * @return                      the created version object(s)
     * @throws ReservedVersionNameException
     *                              thrown if a reserved property name is used int he version properties 
     *                              provided
     * @throws AspectMissingException
     *                              thrown if the version aspect is missing
     * @throws InvalidTicketException
	 * @throws CmaRuntimeException
     */
    public Collection<Version> createVersion(Ticket ticket,
            NodeRef nodeRef, 
            Map<String, Serializable> versionProperties,
            boolean versionChildren)
            throws InvalidTicketException, CmaRuntimeException, ReservedVersionNameException, AspectMissingException;

    /**
     * Creates new versions based on the list of node references provided.
     * 
     * @param ticket
	 *            Ticket received post authentication
     * @param nodeRefs              a list of node references
     * @param versionProperties     version property values
     * @return                      a collection of newly created versions
     * @throws ReservedVersionNameException
     *                              thrown if a reserved property name is used int he version properties 
     *                              provided
     * @throws AspectMissingException
     *                              thrown if the version aspect is missing
     * @throws InvalidTicketException
	 * @throws CmaRuntimeException
     */
    public Collection<Version> createVersion(Ticket ticket,
            Collection<NodeRef> nodeRefs, 
            Map<String, Serializable> versionProperties)
            throws InvalidTicketException, CmaRuntimeException, ReservedVersionNameException, AspectMissingException;

    /**
     * Gets the version history information for a node.
     * <p>
     * If the node has not been versioned then null is returned.
     * <p>
     * If the node referenced does not or can not have the version aspect
     * applied to it then an exception will be raised.
     * 
     * @param ticket
	 *            Ticket received post authentication
     * @param  nodeRef  a node reference
     * @return          the version history information
     * @throws AspectMissingException
     *                  thrown if the version aspect is missing
     * @throws InvalidTicketException
	 * @throws CmaRuntimeException
     */
    public VersionHistory getVersionHistory(Ticket ticket, NodeRef nodeRef)
    throws InvalidTicketException, CmaRuntimeException, AspectMissingException;     
	
	/**
	 * Gets the version object for the current version of the node reference
	 * passed.
	 * <p>
	 * Returns null if the node is not versionable or has not been versioned.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef   the node reference
	 * @return			the version object for the current version
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public Version getCurrentVersion(Ticket ticket, NodeRef nodeRef) throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * The node reference will be reverted to the current version.
     * <p>
     * A deep revert will be performed.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param 	nodeRef					the node reference
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void revert(Ticket ticket, NodeRef nodeRef) throws InvalidTicketException, CmaRuntimeException;
    
    /**
     * The node reference will be reverted to the current version.
     * 
     * 
     * @param ticket
	 *            Ticket received post authentication
     * @param nodeRef                       the node reference
     * @param deep                          true if a deep revert is to be performed, flase otherwise
     * @throws InvalidTicketException
	 * @throws CmaRuntimeException
     */
    public void revert(Ticket ticket, NodeRef nodeRef, boolean deep) throws InvalidTicketException, CmaRuntimeException;
    
    /**
     * A deep revert will take place by default.
     * 
     * @param ticket
	 *            Ticket received post authentication
     * @param nodeRef   the node reference
     * @param version   the version to revert to
     * @throws InvalidTicketException
	 * @throws CmaRuntimeException
     */
    public void revert(Ticket ticket, NodeRef nodeRef, Version version) throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * Revert the state of the node to the specified version.  
	 * <p>
	 * Any changes made to the node will be lost and the state of the node will reflect
	 * that of the version specified.
	 * <p>
	 * The version label property on the node reference will remain unchanged. 
	 * <p>
	 * If the node is further versioned then the new version will be created at the head of 
	 * the version history graph.  A branch will not be created.
     * <p>
     * If a deep revert is to be performed then any child nodes that are no longer present will
     * be deep restored (if appropriate) otherwise child associations to deleted, versioned nodes
     * will not be restored.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param 	nodeRef			the node reference
	 * @param 	version			the version to revert to
     * @param   deep            true is a deep revert is to be performed, false otherwise.
     * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void revert(Ticket ticket, NodeRef nodeRef, Version version, boolean deep) throws InvalidTicketException, CmaRuntimeException;
    
    /**
     * By default a deep restore is performed.
     * 
     * @param ticket
	 *            Ticket received post authentication
     * @param nodeRef           the node reference to a node that no longer exists in the store
     * @param parentNodeRef     the new parent of the restored node
     * @param assocTypeQName    the assoc type qname
     * @param assocQName        the assoc qname
     * @return                  the newly restored node reference
     * @throws InvalidTicketException
	 * @throws CmaRuntimeException
     */
    public NodeRef restore(Ticket ticket,
            NodeRef nodeRef,
            NodeRef parentNodeRef, 
            QName assocTypeQName,
            QName assocQName) throws InvalidTicketException, CmaRuntimeException;
    
    /**
     * Restores a node not currently present in the store, but that has a version
     * history.
     * <p>
     * The restored node will be at the head (most resent version).
     * <p>
     * Restoration will fail if there is no version history for the specified node id in
     * the specified store.
     * <p>
     * If the node already exists in the store then an exception will be raised.
     * <p>
     * Once the node is restored it is reverted to the head version in the appropriate 
     * version history tree.  If deep is set to true then this will be a deep revert, false 
     * otherwise.
     * 
     * @param ticket
	 *            Ticket received post authentication
     * @param nodeRef           the node reference to a node that no longer exists in 
     *                          the store
     * @param parentNodeRef     the new parent of the restored node
     * @param assocTypeQName    the assoc type qname
     * @param assocQName        the assoc qname  
     * @param deep              true is a deep revert should be performed once the node has been 
     *                          restored, false otherwise
     * @return                  the newly restored node reference    
     * @throws InvalidTicketException
	 * @throws CmaRuntimeException                        
     */
    public NodeRef restore(Ticket ticket,
            NodeRef nodeRef,
            NodeRef parentNodeRef, 
            QName assocTypeQName,
            QName assocQName,
            boolean deep) throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * Delete the version history associated with a node reference.
	 * <p>
	 * This operation is permanent, all versions in the version history are
	 * deleted and cannot be retrieved.
	 * <p>
	 * The current version label for the node reference is reset and any subsequent versions
	 * of the node will result in a new version history being created.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param 	nodeRef					the node reference
	 * @throws	AspectMissingException	thrown if the version aspect is missing
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void deleteVersionHistory(Ticket ticket, NodeRef nodeRef)
		throws AspectMissingException, InvalidTicketException, CmaRuntimeException;
}
