/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.api;

import java.util.List;

import org.apache.commons.httpclient.Cookie;

import com.rivetlogic.core.cma.exception.AuthenticationFailure;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.repo.Ticket;

public interface AuthenticationService {
	/**
	 * Authenticate against the repository
	 * 
	 * @param repositoryUri
	 *            URI to the repository to authenticate against
	 * @param userName
	 *            Username
	 * @param password
	 *            Password
	 * @return Ticket to use for services
	 * @throws AuthenticationFailure
	 */
	public Ticket authenticate(String repositoryUri, String userName,
			char[] password) throws AuthenticationFailure;

	/**
	 * change the password of the given user name
	 * 
	 * @param ticket
	 *            Ticket to authenticate
	 * @param userName
	 * 			  username to change password for
	 * @param oldPassword
	 * 			  old password
	 * @param newPassword
	 * 			  new password
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void changePassword(Ticket ticket, String userName, 
			char [] oldPassword, char [] newPassword)
				throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * create an authentication for the given user
	 * 
	 * @param ticket
	 *            Ticket to authenticate
	 * @param userName
	 * 			 username to create an authentication for 
	 * @param password
	 *  		 authentication password
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void createAuthentication(Ticket ticket, String userName, char [] password)
				throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * delete an authentication of the given user
	 * 
	 * @param ticket
	 *            Ticket to authenticate
	 * @param userName
	 * 			 username to delete an authentication from
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void deleteAuthentication(Ticket ticket, String userName)
			throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * get if an authentication is enabled for the given user
	 * @param ticket
	 *            Ticket to authenticate
	 * @param userName
	 * 			  userName to get the result for
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public boolean getAuthenticationEnabled(Ticket ticket, String userName)
			throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * Invalidate given ticket (log out)
	 * 
	 * @param ticket
	 *            Ticket to invalidate
	 */
	public void invalidateTicket(Ticket ticket) throws CmaRuntimeException;

	
	/**
	 * invalidate the given user's ticket
	 * 
	 * @param ticket
	 *            Ticket to authenticate
	 * @param userName
	 * 			 username to invalidate a ticket
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void invalidateUserSession(Ticket ticket, String userName) 
		throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * enable or disable an authentication for the given user
	 * 
	 * @param ticket
	 *            Ticket to authenticate
	 * @param userName
	 * 			  username to enable or disable an authentication for 
	 * @param enabled
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void setAuthenticationEnabled(Ticket ticket, String userName, boolean enabled)
		throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * set a password for the given user name
	 * 
	 * @param ticket
	 *            Ticket to authenticate
	 * @param userName
	 * 			  username to change password for
	 * @param password
	 * 			  password
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void setPassword(Ticket ticket, String userName, char [] password)
				throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * Authenticate against an SSO server
	 * 
	 * @param repositoryUri
	 * @param userName
	 * @param ssoTicket
	 * @param cookies
	 * @return ticket to use for services
	 * @throws AuthenticationFailure
	 */
	public Ticket ssoAuthenticate(String repositoryUri, String userName,
			SsoTicket ssoTicket, List<Cookie> cookies)
			throws AuthenticationFailure;

	/**
	 * Verifies that a ticket is valid, if invalid an exception is thrown
	 * 
	 * @param ticket
	 *            Ticket to validate
	 * @throws InvalidTicketException
	 */
	public void validate(Ticket ticket) throws InvalidTicketException,
			CmaRuntimeException;
}
