/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.repo;

import java.io.OutputStream;
import java.util.Collection;
import java.util.Date;

import org.alfresco.repo.dictionary.DictionaryDAO;
import org.alfresco.service.cmr.dictionary.ModelDefinition;
import org.alfresco.service.cmr.dictionary.NamespaceDefinition;
import org.alfresco.service.namespace.QName;

/**
 * This class is an implementation of Alfresco ModelDefinition which holds
 * ModelDefinition information retrieved from Alfresco
 * 
 * @author Hyanghee Lim
 *
 */
public class CmaModelDefinition implements ModelDefinition {

	private String author;
	private String description;
	private QName name;
	private Date publishedDate;
	private String version;

	public String getAuthor() {
		return author;
	}

	public String getDescription() {
		return description;
	}

	public QName getName() {
		return name;
	}

	public Date getPublishedDate() {
		return publishedDate;
	}

	public String getVersion() {
		return version;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(QName name) {
		this.name = name;
	}

	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Collection<NamespaceDefinition> getImportedNamespaces() {
		// TODO Auto-generated method stub
		return null;
	}

	public Collection<NamespaceDefinition> getNamespaces() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isNamespaceDefined(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isNamespaceImported(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public String getAnalyserResourceBundleName() {
		// TODO Auto-generated method stub
		return null;
	}

	public long getChecksum(XMLBindingType arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public DictionaryDAO getDictionaryDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	public void toXML(XMLBindingType arg0, OutputStream arg1) {
		// TODO Auto-generated method stub
		
	}
}
