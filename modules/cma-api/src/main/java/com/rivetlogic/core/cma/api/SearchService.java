/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.api;

import java.util.List;

import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.repo.Node;
import com.rivetlogic.core.cma.repo.SortDefinition;
import com.rivetlogic.core.cma.repo.Ticket;

public interface SearchService {
	enum QueryLanguage {
		lucene
	}

	/**
	 * Query the repository for nodes that match the query string
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param store
	 *            Store to look into
	 * @param queryLanguage
	 *            Query language used
	 * @param query
	 *            Query string
	 * @param requiredProperties
	 *            Which properties should the system return, remember this is a
	 *            remote call, the more properties listed, the slower it'll be
	 * @param returnPeerAssocs
	 *            Should we return the peer-associations as well
	 * @param returnChildAssocs
	 *            Should we return the child-associations as well
	 * @param returnAspects
	 *            Should we return the aspects as well
	 * @param returnCurrentUserPermissions
	 *            Should we return the current user's permissions as well
	 * @param maxResults
	 *            Maximum results to return
	 * @param sortDefinitions
	 *            Sort definitions
	 * @return List of nodes that match the search criteria
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	@Deprecated
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			List<QName> requiredProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects,
			boolean returnCurrentUserPermissions, int maxResults,
			List<SortDefinition> sortDefinitions)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Query the repository for nodes that match the query string
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param store
	 *            Store to look into
	 * @param queryLanguage
	 *            Query language used
	 * @param query
	 *            Query string
	 * @param requiredProperties
	 *            Which properties should the system return, remember this is a
	 *            remote call, the more properties listed, the slower it'll be
	 * @param returnPeerAssocs
	 *            Should we return the associations as well
	 * @param returnChildAssocs
	 *            Should we return the child-associations as well
	 * @param returnAspects
	 *            Should we return the aspects as well
	 * @param returnCurrentUserPermissions
	 *            Should we return the current user's permissions as well
	 * @param startResult
	 *            Return results starting at this index
	 * @param endResult
	 *            Return results up to this index
	 * @param sortDefinitions
	 *            Sort definitions
	 * @return List of nodes that match the search criteria
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	@Deprecated
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			List<QName> requiredProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects,
			boolean returnCurrentUserPermissions, int startResult,
			int endResult, List<SortDefinition> sortDefinitions)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Query the repository for nodes that match the query string
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param store
	 *            Store to look into
	 * @param queryLanguage
	 *            Query language used
	 * @param query
	 *            Query string
	 * @param returnAllProperties
	 *            Should we return all properties of the node
	 * @param returnPeerAssocs
	 *            Should we return the associations as well
	 * @param returnChildAssocs
	 *            Should we return the child-associations as well
	 * @param returnAspects
	 *            Should we return the aspects as well
	 * @param returnCurrentUserPermissions
	 *            Should we return the current user's permissions as well
	 * @param maxResults
	 *            Maximum results to return
	 * @param sortDefinitions
	 *            Sort definitions
	 * @return List of nodes that match the search criteria
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	@Deprecated
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			boolean returnAllProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects,
			boolean returnCurrentUserPermissions, int maxResults,
			List<SortDefinition> sortDefinitions)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Query the repository for nodes that match the query string
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param store
	 *            Store to look into
	 * @param queryLanguage
	 *            Query language used
	 * @param query
	 *            Query string
	 * @param returnAllProperties
	 *            Should we return all properties of the node
	 * @param returnPeerAssocs
	 *            Should we return the associations as well
	 * @param returnChildAssocs
	 *            Should we return the child-associations as well
	 * @param returnAspects
	 *            Should we return the aspects as well
	 * @param returnCurrentUserPermissions
	 *            Should we return the current user's permissions as well
	 * @param startResult
	 *            Return results starting at this index
	 * @param endResult
	 *            Return results up to this index
	 * @param sortDefinitions
	 *            Sort definitions
	 * @return List of nodes that match the search criteria
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	@Deprecated
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			boolean returnAllProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects,
			boolean returnCurrentUserPermissions, int startResult,
			int endResult, List<SortDefinition> sortDefinitions)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Query the repository for nodes that match the query string
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param store
	 *            Store to look into
	 * @param queryLanguage
	 *            Query language used
	 * @param query
	 *            Query string
	 * @param requiredProperties
	 *            Which properties should the system return, remember this is a
	 *            remote call, the more properties listed, the slower it'll be
	 * @param returnPeerAssocs
	 *            Should we return the peer-associations as well
	 * @param returnChildAssocs
	 *            Should we return the child-associations as well
	 * @param returnAspects
	 *            Should we return the aspects as well
	 * @param returnCurrentUserPermissions
	 *            Should we return the permissions as well
	 * @param maxResults
	 *            Maximum results to return
	 * @return List of nodes that match the search criteria
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	@Deprecated
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			List<QName> requiredProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects,
			boolean returnCurrentUserPermissions, int maxResults)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Query the repository for nodes that match the query string
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param store
	 *            Store to look into
	 * @param queryLanguage
	 *            Query language used
	 * @param query
	 *            Query string
	 * @param requiredProperties
	 *            Which properties should the system return, remember this is a
	 *            remote call, the more properties listed, the slower it'll be
	 * @param returnPeerAssocs
	 *            Should we return the associations as well
	 * @param returnChildAssocs
	 *            Should we return the child-associations as well
	 * @param returnAspects
	 *            Should we return the aspects as well
	 * @param returnCurrentUserPermissions
	 *            Should we return the permissions as well
	 * @param startResult
	 *            Return results starting at this index
	 * @param endResult
	 *            Return results up to this index
	 * @return List of nodes that match the search criteria
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	@Deprecated
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			List<QName> requiredProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects,
			boolean returnCurrentUserPermissions, int startResult, int endResult)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Query the repository for nodes that match the query string
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param store
	 *            Store to look into
	 * @param queryLanguage
	 *            Query language used
	 * @param query
	 *            Query string
	 * @param returnAllProperties
	 *            Should we return all properties of the node
	 * @param returnPeerAssocs
	 *            Should we return the associations as well
	 * @param returnChildAssocs
	 *            Should we return the child-associations as well
	 * @param returnAspects
	 *            Should we return the aspects as well
	 * @param returnCurrentUserPermissions
	 *            Should we return the permissions as well
	 * @param maxResults
	 *            Maximum results to return
	 * @return List of nodes that match the search criteria
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	@Deprecated
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			boolean returnAllProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects,
			boolean returnCurrentUserPermissions, int maxResults)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Query the repository for nodes that match the query string
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param store
	 *            Store to look into
	 * @param queryLanguage
	 *            Query language used
	 * @param query
	 *            Query string
	 * @param returnAllProperties
	 *            Should we return all properties of the node
	 * @param returnPeerAssocs
	 *            Should we return the associations as well
	 * @param returnChildAssocs
	 *            Should we return the child-associations as well
	 * @param returnAspects
	 *            Should we return the aspects as well
	 * @param returnCurrentUserPermissions
	 *            Should we return the permissions as well
	 * @param startResult
	 *            Return results starting at this index
	 * @param endResult
	 *            Return results up to this index
	 * @return List of nodes that match the search criteria
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	@Deprecated
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			boolean returnAllProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects,
			boolean returnCurrentUserPermissions, int startResult, int endResult)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Query the repository for nodes that match the query string
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param store
	 *            Store to look into
	 * @param queryLanguage
	 *            Query language used
	 * @param query
	 *            Query string
	 * @param requiredProperties
	 *            Which properties should the system return, remember this is a
	 *            remote call, the more properties listed, the slower it'll be
	 * @param returnPeerAssocs
	 *            Should we return the peer-associations as well
	 * @param returnChildAssocs
	 *            Should we return the child-associations as well
	 * @param returnAspects
	 *            Should we return the aspects as well
	 * @param returnCurrentUserPermissions
	 *            Should we return the current user's permissions as well
	 * @param permissionsToCheckFor
	 *            a list of low level permissions (e.g. ReadProperties,
	 *            ReadContent) to get access status of the current user on each
	 *            node
	 * @param maxResults
	 *            Maximum results to return
	 * @param sortDefinitions
	 *            Sort definitions
	 * @return List of nodes that match the search criteria
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			List<QName> requiredProperties,
			boolean returnPeerAssocs, boolean returnChildAssocs,
			boolean returnAspects, boolean returnCurrentUserPermissions,
			List<String> permissionsToCheckFor,
			int maxResults, List<SortDefinition> sortDefinitions)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Query the repository for nodes that match the query string
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param store
	 *            Store to look into
	 * @param queryLanguage
	 *            Query language used
	 * @param query
	 *            Query string
	 * @param requiredProperties
	 *            Which properties should the system return, remember this is a
	 *            remote call, the more properties listed, the slower it'll be
	 * @param returnPeerAssocs
	 *            Should we return the associations as well
	 * @param returnChildAssocs
	 *            Should we return the child-associations as well
	 * @param returnAspects
	 *            Should we return the aspects as well
	 * @param returnCurrentUserPermissions
	 *            Should we return the current user's permissions as well
	 * @param permissionsToCheckFor
	 *            a list of low level permissions (e.g. ReadProperties,
	 *            ReadContent) to get access status of the current user on each
	 *            node
	 * @param startResult
	 *            Return results starting at this index
	 * @param endResult
	 *            Return results up to this index
	 * @param sortDefinitions
	 *            Sort definitions
	 * @return List of nodes that match the search criteria
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			List<QName> requiredProperties,
			boolean returnPeerAssocs, boolean returnChildAssocs,
			boolean returnAspects, boolean returnCurrentUserPermissions,
			List<String> permissionsToCheckFor,
			int startResult, int endResult, List<SortDefinition> sortDefinitions)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Query the repository for nodes that match the query string
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param store
	 *            Store to look into
	 * @param queryLanguage
	 *            Query language used
	 * @param query
	 *            Query string
	 * @param returnAllProperties
	 *            Should we return all properties of the node
	 * @param returnPeerAssocs
	 *            Should we return the associations as well
	 * @param returnChildAssocs
	 *            Should we return the child-associations as well
	 * @param returnAspects
	 *            Should we return the aspects as well
	 * @param returnCurrentUserPermissions
	 *            Should we return the current user's permissions as well
	 * @param permissionsToCheckFor
	 *            a list of low level permissions (e.g. ReadProperties,
	 *            ReadContent) to get access status of the current user on each
	 *            node
	 * @param maxResults
	 *            Maximum results to return
	 * @param sortDefinitions
	 *            Sort definitions
	 * @return List of nodes that match the search criteria
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			boolean returnAllProperties,
			boolean returnPeerAssocs, boolean returnChildAssocs,
			boolean returnAspects, boolean returnCurrentUserPermissions,
			List<String> permissionsToCheckFor,
			int maxResults, List<SortDefinition> sortDefinitions)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Query the repository for nodes that match the query string
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param store
	 *            Store to look into
	 * @param queryLanguage
	 *            Query language used
	 * @param query
	 *            Query string
	 * @param returnAllProperties
	 *            Should we return all properties of the node
	 * @param returnPeerAssocs
	 *            Should we return the associations as well
	 * @param returnChildAssocs
	 *            Should we return the child-associations as well
	 * @param returnAspects
	 *            Should we return the aspects as well
	 * @param returnCurrentUserPermissions
	 *            Should we return the current user's permissions as well
	 * @param permissionsToCheckFor
	 *            a list of low level permissions (e.g. ReadProperties,
	 *            ReadContent) to get access status of the current user on each
	 *            node
	 * @param startResult
	 *            Return results starting at this index
	 * @param endResult
	 *            Return results up to this index
	 * @param sortDefinitions
	 *            Sort definitions
	 * @return List of nodes that match the search criteria
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			boolean returnAllProperties,
			boolean returnPeerAssocs, boolean returnChildAssocs,
			boolean returnAspects, boolean returnCurrentUserPermissions,
			List<String> permissionsToCheckFor,
			int startResult, int endResult, List<SortDefinition> sortDefinitions)
			throws InvalidTicketException, CmaRuntimeException;

}
