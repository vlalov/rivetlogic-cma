/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.repo;

import java.io.Serializable;

public class SortDefinition implements Serializable {

	/**
	 * This class is a duplication of Alfresco SortDefintion class
	 * in order to be able to pass sort definitions as search parameters
	 * 
	 * @author Hyanghee Lim
	 */
	private static final long serialVersionUID = 4240006175894125814L;

	public enum SortType {
		FIELD, DOCUMENT, SCORE
	};

	SortType sortType;
	String field;
	boolean ascending;

	public SortDefinition(SortType sortType, String field, boolean ascending) {
		this.sortType = sortType;
		this.field = field;
		this.ascending = ascending;
	}

	public boolean isAscending() {
		return ascending;
	}

	public String getField() {
		return field;
	}

	public SortType getSortType() {
		return sortType;
	}

}
