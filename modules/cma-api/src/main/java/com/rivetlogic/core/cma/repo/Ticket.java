/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.repo;

import java.io.Serializable;

import org.alfresco.service.cmr.repository.NodeRef;

public class Ticket implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 377274039049511976L;
	private String repositoryUri;
	private String ticket;
	private NodeRef userNodeRef;
		
	/**
	 * @param repositoryUri
	 * @param ticket
	 */
	@Deprecated
	public Ticket(String repositoryUri, String ticket) {
		this.repositoryUri = repositoryUri;
		this.ticket = ticket;
		this.userNodeRef = null;
	}
	
	/**
	 * 
	 * @param repositoryUri
	 * @param ticket
	 * @param userNodeRef
	 */
	public Ticket(String repositoryUri, String ticket, NodeRef userNodeRef) {
		this.repositoryUri = repositoryUri;
		this.ticket = ticket;
		this.userNodeRef = userNodeRef;
	}
	
	public String getTicket() {
		return ticket;
	}
	
	public String getRepositoryUri() {
		return repositoryUri;
	}
	
	public NodeRef getUserNodeRef() {
		return userNodeRef;
	}

	public void setRepositoryUri(String repositoryUri) {
		this.repositoryUri = repositoryUri;
	}

}
