package com.rivetlogic.core.cma.repo;

import java.io.Serializable;

import org.alfresco.service.cmr.security.AccessPermission;
import org.alfresco.service.cmr.security.AccessStatus;
import org.alfresco.service.cmr.security.AuthorityType;

/**
 * Serializable Implmentation of AccessPermission
 * 
 * @author Hyanghee Lim
 *
 */
public class AccessPermissionImpl implements AccessPermission, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2756012528786112222L;
	private AccessStatus accessStatus;
	private String authority;
	private AuthorityType authorityType;
	private String permission;
	private int position = 0;

	public AccessPermissionImpl(AccessPermission accessPermission) {
		this.accessStatus = accessPermission.getAccessStatus();
		this.authority = accessPermission.getAuthority();
		this.authorityType = accessPermission.getAuthorityType();
		this.permission = accessPermission.getPermission();
		this.position = accessPermission.getPosition();
		accessPermission.isInherited();
	}
	
	public AccessStatus getAccessStatus() {
		return accessStatus;
	}

	public String getAuthority() {
		return authority;
	}

	public AuthorityType getAuthorityType() {
		return authorityType;
	}

	public String getPermission() {
		return permission;
	}

	public void setAccessStatus(AccessStatus accessStatus) {
		this.accessStatus = accessStatus;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public void setAuthorityType(AuthorityType authorityType) {
		this.authorityType = authorityType;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public boolean isInherited() {
		return (position > 0);
	}

	public boolean isSetDirectly() {
		return (position == 0);
	}

	@Override
    public String toString() {
		return accessStatus + " " + this.permission + " - " 
			+ this.authority + " (" + this.authorityType + ")";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)	{
			return true;
		}
		if (!(o instanceof AccessPermissionImpl)) {
			return false;
		}
		AccessPermissionImpl other = (AccessPermissionImpl) o;
		return this.getPermission().equals(other.getPermission())
			&& (this.getAccessStatus() == other.getAccessStatus() 
			&& (this.getAccessStatus().equals(other.getAccessStatus())));
	}

	@Override
	public int hashCode() {
		return ((authority.hashCode() * 37) + 
				permission.hashCode()) * 37 + accessStatus.hashCode();
	}

}
