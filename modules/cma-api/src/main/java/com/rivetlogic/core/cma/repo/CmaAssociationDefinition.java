/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.repo;

import org.alfresco.service.cmr.dictionary.AssociationDefinition;
import org.alfresco.service.cmr.dictionary.ClassDefinition;
import org.alfresco.service.cmr.dictionary.ModelDefinition;
import org.alfresco.service.namespace.QName;

/**
 * This class is an implementation of Alfresco AssociationDefinition which holds
 * AssociationDefinition information retrieved from Alfresco
 * 
 * the ClassDefinitions of sourceClass and targetClass 
 * will only have class QNames
 * @author Hyanghee Lim
 *
 */
public class CmaAssociationDefinition implements AssociationDefinition {

	private String description;
	private ModelDefinition model;
	private QName name;
	private ClassDefinition sourceClass;
	private QName sourceRoleName;
	private ClassDefinition targetClass;
	private QName targetRoleName;
	private String title;
	private boolean isChild;
	private boolean isProected;
	private boolean isSourceMandatory;
	private boolean isSourceMany;
	private boolean isTargetMandatory;
	private boolean isTargetMandatoryEnforced;
	private boolean isTargetMany;
	private QName sourceClassQName;
	private QName targetClassQName;

	public String getDescription() {
		return description;
	}

	public ModelDefinition getModel() {
		return model;
	}

	public QName getName() {
		return name;
	}

	/**
	 * @return ClassDefinition that only contains QName
	 */
	public ClassDefinition getSourceClass() {
		return sourceClass;
	}

	public QName getSourceClassQName() {
		return sourceClassQName;
	}

	public QName getSourceRoleName() {
		return sourceRoleName;
	}

	/**
	 * @return ClassDefinition that only contains QName
	 */
	public ClassDefinition getTargetClass() {
		return targetClass;
	}

	public QName getTargetClassQName() {
		return targetClassQName;
	}

	public QName getTargetRoleName() {
		return targetRoleName;
	}

	public String getTitle() {
		return title;
	}

	public boolean isChild() {
		return isChild;
	}

	public boolean isProtected() {
		return isProected;
	}

	public boolean isSourceMandatory() {
		return isSourceMandatory;
	}

	public boolean isSourceMany() {
		return isSourceMany;
	}

	public boolean isTargetMandatory() {
		return isTargetMandatory;
	}

	public boolean isTargetMandatoryEnforced() {
		return isTargetMandatoryEnforced;
	}

	public boolean isTargetMany() {
		return isTargetMany;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setModel(ModelDefinition model) {
		this.model = model;
	}

	public void setName(QName name) {
		this.name = name;
	}

	public void setSourceClassQName(QName sourceClassQName) {
		this.sourceClassQName = sourceClassQName;
		CmaClassDefinition source = new CmaClassDefinition();
		source.setName(sourceClassQName); 
		sourceClass = source;
	}

	public void setSourceRoleName(QName sourceRoleName) {
		this.sourceRoleName = sourceRoleName;
	}

	public void setTargetClassQName(QName targetClassQName) {
		this.targetClassQName = targetClassQName;
		CmaClassDefinition target = new CmaClassDefinition();
		target.setName(targetClassQName); 
		targetClass = target;
	}

	public void setTargetRoleName(QName targetRoleName) {
		this.targetRoleName = targetRoleName;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setIsChild(boolean isChild) {
		this.isChild = isChild;
	}

	public void setIsProtected(boolean isProected) {
		this.isProected = isProected;
	}

	public void setIsSourceMandatory(boolean isSourceMandatory) {
		this.isSourceMandatory = isSourceMandatory;
	}

	public void setIsSourceMany(boolean isSourceMany) {
		this.isSourceMany = isSourceMany;
	}

	public void setIsTargetMandatory(boolean isTargetMandatory) {
		this.isTargetMandatory = isTargetMandatory;
	}

	public void setIsTargetMandatoryEnforced(boolean isTargetMandatoryEnforced) {
		this.isTargetMandatoryEnforced = isTargetMandatoryEnforced;
	}

	public void setIsTargetMany(boolean isTargetMany) {
		this.isTargetMany = isTargetMany;
	}
}
