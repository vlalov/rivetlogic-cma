/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.repo;

import org.alfresco.service.cmr.dictionary.ChildAssociationDefinition;

/**
 * This class is an implementation of Alfresco ChildAssociationDefinition which holds
 * ChildAssociationDefinition information retrieved from Alfresco
 * 
 * @author Hyanghee Lim
 *
 */
public class CmaChildAssociationDefinition extends CmaAssociationDefinition
		implements ChildAssociationDefinition {

	private boolean duplicateChildNamesAllowed;
	private boolean propagateTimestamps;
	private String requiredChildName;

	public boolean getDuplicateChildNamesAllowed() {
		return duplicateChildNamesAllowed;
	}

	public String getRequiredChildName() {
		return requiredChildName;
	}

	public void setDuplicateChildNamesAllowed(boolean duplicateChildNamesAllowed) {
		this.duplicateChildNamesAllowed = duplicateChildNamesAllowed;
	}

	public void setRequiredChildName(String requiredChildName) {
		this.requiredChildName = requiredChildName;
	}

	public boolean getPropagateTimestamps() {
		return propagateTimestamps;
	}

	public void setPropagateTimestamps(boolean propagateTimestamps) {
		this.propagateTimestamps = propagateTimestamps;
	}

}
