/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.api;

import java.util.Collection;

import org.alfresco.service.cmr.dictionary.AspectDefinition;
import org.alfresco.service.cmr.dictionary.AssociationDefinition;
import org.alfresco.service.cmr.dictionary.ClassDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.dictionary.ModelDefinition;
import org.alfresco.service.cmr.dictionary.PropertyDefinition;
import org.alfresco.service.cmr.dictionary.TypeDefinition;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.repo.Ticket;

public interface DictionaryService {
	/**
	 * @param ticket
	 *            Ticket received post authentication
	 * @return the names of all models that have been registered with the
	 *         Repository
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public Collection<QName> getAllModels(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * @param model
	 *            the model name to retrieve
	 * @return the specified model (or null, if it doesn't exist)
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public ModelDefinition getModel(Ticket ticket, QName model)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * @return the names of all data types that have been registered with the
	 *         Repository
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	Collection<QName> getAllDataTypes(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * @param model
	 *            the model to retrieve data types for
	 * @return the names of all data types defined within the specified model
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	Collection<QName> getDataTypes(Ticket ticket, QName model)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * @param name
	 *            the name of the data type to retrieve
	 * @return the data type definition (or null, if it doesn't exist)
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	DataTypeDefinition getDataType(Ticket ticket, QName name)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * @param javaClass
	 *            java class to find datatype for
	 * @return the data type definition (or null, if a mapping does not exist)
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	DataTypeDefinition getDataType(Ticket ticket, Class javaClass)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * @return the names of all types that have been registered with the
	 *         Repository
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	Collection<QName> getAllTypes(Ticket ticket) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * @param model
	 *            the model to retrieve types for
	 * @return the names of all types defined within the specified model
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	Collection<QName> getTypes(Ticket ticket, QName model)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * @param name
	 *            the name of the type to retrieve
	 * @return the type definition (or null, if it doesn't exist)
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	TypeDefinition getType(Ticket ticket, QName name)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Construct an anonymous type that combines the definitions of the
	 * specified type and aspects.
	 * 
	 * @param type
	 *            the type to start with
	 * @param aspects
	 *            the aspects to combine with the type
	 * @return the anonymous type definition
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	TypeDefinition getAnonymousType(Ticket ticket, QName type,
			Collection<QName> aspects) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * @return the names of all aspects that have been registered with the
	 *         Repository
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	Collection<QName> getAllAspects(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * @param model
	 *            the model to retrieve aspects for
	 * @return the names of all aspects defined within the specified model
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	Collection<QName> getAspects(Ticket ticket, QName model)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * @param name
	 *            the name of the aspect to retrieve
	 * @return the aspect definition (or null, if it doesn't exist)
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	AspectDefinition getAspect(Ticket ticket, QName name)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * @param name
	 *            the name of the class (type or aspect) to retrieve
	 * @return the class definition (or null, if it doesn't exist)
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	ClassDefinition getClass(Ticket ticket, QName name)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Determines whether a class is a sub-class of another class
	 * 
	 * @param className
	 *            the sub-class to test
	 * @param ofClassName
	 *            the class to test against
	 * @return true => the class is a sub-class (or itself)
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	boolean isSubClass(Ticket ticket, QName className, QName ofClassName)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Gets the definition of the property as defined by the specified Class.
	 * 
	 * Note: A sub-class may override the definition of a property that's
	 * defined in a super-class.
	 * 
	 * @param className
	 *            the class name
	 * @param propertyName
	 *            the property name
	 * @return the property definition (or null, if it doesn't exist)
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	PropertyDefinition getProperty(Ticket ticket, QName className,
			QName propertyName) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Gets the definition of the property as defined by its owning Class.
	 * 
	 * @param propertyName
	 *            the property name
	 * @return the property definition (or null, if it doesn't exist)
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	PropertyDefinition getProperty(Ticket ticket, QName propertyName)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get all properties defined across all models with the given data type.
	 * 
	 * Note that DataTypeDefinition.ANY will only match this type and can not be
	 * used as get all properties.
	 * 
	 * If dataType is null then this method will return *ALL* properties
	 * regardless of data type.
	 * 
	 * @param dataType
	 * @return a collection of datatype properties
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	Collection<QName> getAllProperties(Ticket ticket, QName dataType)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get all properties defined for the given model with the given data type.
	 * 
	 * Note that DataTypeDefinition.ANY will only match this type and can not be
	 * used as get all properties.
	 * 
	 * If dataType is null then this method will return *ALL* properties
	 * regardless of data type.
	 * 
	 * @param dataType
	 * @return a collection of model properties
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	Collection<QName> getProperties(Ticket ticket, QName model, QName dataType)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Get all properties for the specified model
	 * 
	 * @param model
	 * @return a collection of model properties
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	Collection<QName> getProperties(Ticket ticket, QName model)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Gets the definition of the association as defined by its owning Class.
	 * 
	 * ClassDefinitions of sourceClass and targetClass 
	 * will only contain the QNames of Class
	 * 
	 * @param associationName
	 *            the property name
	 * @return the association definition (or null, if it doesn't exist)
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	AssociationDefinition getAssociation(Ticket ticket, QName associationName)
			throws InvalidTicketException, CmaRuntimeException;
}
