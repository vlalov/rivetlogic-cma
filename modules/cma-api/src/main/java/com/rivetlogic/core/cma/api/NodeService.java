/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.api;

import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.alfresco.service.cmr.dictionary.InvalidAspectException;
import org.alfresco.service.cmr.dictionary.InvalidTypeException;
import org.alfresco.service.cmr.lock.NodeLockedException;
import org.alfresco.service.cmr.repository.AssociationExistsException;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.InvalidChildAssociationRefException;
import org.alfresco.service.cmr.repository.InvalidNodeRefException;
import org.alfresco.service.cmr.repository.InvalidStoreRefException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.Path;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.repo.Node;
import com.rivetlogic.core.cma.repo.Ticket;

/**
 * @author sumer
 * 
 */
public interface NodeService {

	/**
	 * Gets a list of all available node store references
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @return list of StoreRefs
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<StoreRef> getStores(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Create a new store for the given protocol and identifier
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param protocol
	 *            the implementation protocol
	 * @param identifier
	 *            the protocol-specific identifier
	 * @return a reference to the store
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public StoreRef createStore(Ticket ticket, String protocol,
			String identifier) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Checks if the store exists and returns true if it exists
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param storeRef
	 *            a reference to the store to look for
	 * @return returns true if store exists, otherwise false
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public boolean exists(Ticket ticket, StoreRef storeRef)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Checks if the store exists and returns true if it exists
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            a reference to the node to look for
	 * @return returns true if node exists, otherwise false
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public boolean exists(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * gets the reference to the root node of the store based on the storeRef
	 * given as parameter
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param storeRef
	 *            a reference to an existing store
	 * @return a reference to the root node of the store
	 * @throws InvalidStoreRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public NodeRef getRootNode(Ticket ticket, StoreRef storeRef)
			throws InvalidStoreRefException, InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Set the ordering index of the child association given the ChildAssocRef
	 * that must be moved in the order and index
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param childAssocRef
	 *            the child association that must be moved in the order
	 * @param index
	 *            an arbitrary index that will affect the return order
	 * @throws InvalidChildAssociationRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void setChildAssociationIndex(Ticket ticket,
			ChildAssociationRef childAssocRef, int index)
			throws InvalidChildAssociationRefException, InvalidTicketException,
			CmaRuntimeException;

	/**
	 * get the type QName for the node
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            a nodeRef associated with the node
	 * @return the type name
	 * @throws InvalidNodeRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public QName getType(Ticket ticket, NodeRef nodeRef)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Re-sets the type of the node
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node reference
	 * @param typeQName
	 *            the type QName
	 * @throws InvalidNodeRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void setType(Ticket ticket, NodeRef nodeRef, QName typeQName)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException;

	/**
	 * applies an aspect and set the properties to the node given the aspect
	 * qname and properties to be set
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node reference
	 * @param aspectTypeQName
	 *            the aspect to apply to the node
	 * @throws InvalidNodeRefException
	 * @throws InvalidAspectException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void addAspect(Ticket ticket, NodeRef nodeRef, QName aspectTypeQName)
			throws InvalidNodeRefException, InvalidAspectException,
			InvalidTicketException, CmaRuntimeException;

	/**
	 * applies an aspect to the node given the aspect qname
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node reference
	 * @param aspectTypeQName
	 *            the aspect to apply to the node
	 * @param aspectProperties
	 *            a minimum of the mandatory properties required for the aspect
	 * @throws InvalidNodeRefException
	 * @throws InvalidAspectException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void addAspect(Ticket ticket, NodeRef nodeRef,
			QName aspectTypeQName, Map<QName, Serializable> aspectProperties)
			throws InvalidNodeRefException, InvalidAspectException,
			InvalidTicketException, CmaRuntimeException;

	/**
	 * Remove an aspect and all the related properties from a node
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node reference
	 * @param aspectTypeQName
	 *            the aspect to apply to the node
	 * @throws InvalidNodeRefException
	 * @throws InvalidAspectException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void removeAspect(Ticket ticket, NodeRef nodeRef,
			QName aspectTypeQName) throws InvalidNodeRefException,
			InvalidAspectException, InvalidTicketException, CmaRuntimeException;

	/**
	 * determines if a given node aspect is present on a node
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node reference
	 * @param aspectTypeQName
	 *            the aspect to apply to the node
	 * @return returns true if the aspect has been applied to the given node,
	 *         otherwise false
	 * @throws InvalidNodeRefException
	 * @throws InvalidAspectException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public boolean hasAspect(Ticket ticket, NodeRef nodeRef,
			QName aspectTypeQName) throws InvalidNodeRefException,
			InvalidAspectException, InvalidTicketException, CmaRuntimeException;

	/**
	 * get the set of all aspects applied to the node, including mandatory
	 * aspects
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node reference
	 * @return Returns a set of all aspects applied to the node, including
	 *         mandatory aspects
	 * @throws InvalidNodeRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public Set<QName> getAspects(Ticket ticket, NodeRef nodeRef)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Makes a parent-child association between the given nodes.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param parentRef
	 *            the parent node reference
	 * @param childRef
	 *            the child node reference
	 * @param assocTypeQName
	 *            the qualified name of the association type
	 * @param qname
	 *            the qualified name of the association
	 * @return returns a reference to the newly created child association
	 * @throws InvalidNodeRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public ChildAssociationRef addChild(Ticket ticket, NodeRef parentRef,
			NodeRef childRef, QName assocTypeQName, QName qname)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException;

	/**
	 * delete the child node
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param parentRef
	 *            the parent node reference
	 * @param childRef
	 *            the child node reference
	 * @throws InvalidNodeRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void removeChild(Ticket ticket, NodeRef parentRef, NodeRef childRef)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Remove the specific child association
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param childAssocRef
	 *            the association to remove
	 * @return returns true if the association existed, otherwise false.
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public boolean removeChildAssociation(Ticket ticket,
			ChildAssociationRef childAssocRef) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Remove a specific secondary child association.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param childAssocRef
	 *            the association to remove
	 * @return returns true if the association existed, otherwise false.
	 * @throws InvalidChildAssociationRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	@Deprecated
	public boolean removeSeconaryChildAssociation(Ticket ticket,
			ChildAssociationRef childAssocRef)
			throws InvalidChildAssociationRefException, InvalidTicketException,
			CmaRuntimeException;

	/**
	 * Remove a specific secondary child association.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param childAssocRef
	 *            the association to remove
	 * @return returns true if the association existed, otherwise false.
	 * @throws InvalidChildAssociationRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public boolean removeSecondaryChildAssociation(Ticket ticket,
			ChildAssociationRef childAssocRef)
			throws InvalidChildAssociationRefException, InvalidTicketException,
			CmaRuntimeException;

	/**
	 * remove the property value given the propertyQname
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node reference
	 * @param qname
	 *            the fully qualified name of the property
	 * @throws InvalidNodeRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void removeProperty(Ticket ticket, NodeRef nodeRef, QName qname)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException;

	/**
	 * gets a list of all parent-child associations that exist where the given
	 * node is the child
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node reference
	 * @return returns a list of all parent-child associations that exist where
	 *         the given node is the child
	 * @throws InvalidNodeRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<ChildAssociationRef> getParentAssocs(Ticket ticket,
			NodeRef nodeRef) throws InvalidNodeRefException,
			InvalidTicketException, CmaRuntimeException;

	/**
	 * gets a list of all parent-child associations that exist where the given
	 * node is the child given the typeqname and qnames of the associations that
	 * must match
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node reference
	 * @param typeQNamePattern
	 *            the pattern that the type qualified name of the association
	 *            must match
	 * @param qnamePattern
	 *            the pattern that the qnames of the assocs must match
	 * @return returns a list of all parent-child associations that exist where
	 *         the given node is the child
	 * @throws InvalidNodeRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<ChildAssociationRef> getParentAssocs(Ticket ticket,
			NodeRef nodeRef, QName typeQNamePattern,
			QName qnamePattern) throws InvalidNodeRefException,
			InvalidTicketException, CmaRuntimeException;

	/**
	 * get all the child associations of the given node
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node reference
	 * @return returns a collection of ChildAssocRef instances. If the node is
	 *         not a container then the result will be empty
	 * @throws InvalidNodeRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<ChildAssociationRef> getChildAssocs(Ticket ticket,
			NodeRef nodeRef) throws InvalidNodeRefException,
			InvalidTicketException, CmaRuntimeException;

	/**
	 * get all the child associations of the given node given the typeqname and
	 * qnames of the associations that must match
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node reference
	 * @param typeQNamePattern
	 *            the pattern that the type qualified name of the association
	 *            must match
	 * @param qnamePattern
	 *            the pattern that the qnames of the assocs must match
	 * @return returns a list of ChildAssocRef instances.
	 * @throws InvalidNodeRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<ChildAssociationRef> getChildAssocs(Ticket ticket,
			NodeRef nodeRef, QName typeQNamePattern,
			QName qnamePattern) throws InvalidNodeRefException,
			InvalidTicketException, CmaRuntimeException;

	/**
	 * Get the node with the given name within the context of the parent node.
	 * The name is case-insensitive
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node reference
	 * @param assocTypeQName
	 *            the type of the association
	 * @param childName
	 *            the name of the node as per the property cm:name
	 * @return returns the child node or null if not found
	 * @throws InvalidNodeRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public NodeRef getChildByName(Ticket ticket, NodeRef nodeRef,
			QName assocTypeQName, String childName)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException;

	/**
	 * get the primary parent-child association of the node
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node reference
	 * @return returns the primary parent-child association of the node
	 * @throws InvalidNodeRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public ChildAssociationRef getPrimaryParent(Ticket ticket, NodeRef nodeRef)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException;

	/**
	 * creates the association to the destination nodeRef from the source
	 * nodeRef
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param sourceRef
	 *            a reference to the real node
	 * @param targetRef
	 *            a reference to a node
	 * @param assocTypeQName
	 *            the qualified name of the association type
	 * @return AssociationRef
	 * @throws InvalidNodeRefException
	 * @throws AssociationExistsException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public AssociationRef createAssociation(Ticket ticket, NodeRef sourceRef,
			NodeRef targetRef, QName assocTypeQName)
			throws InvalidNodeRefException, AssociationExistsException,
			InvalidTicketException, CmaRuntimeException;

	/**
	 * remove association from the source nodeRef to the destination nodeRef
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param sourceRef
	 *            the association source node
	 * @param targetRef
	 *            the association target node
	 * @param assocTypeQName
	 *            the qualified name of the association type
	 * @throws InvalidNodeRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void removeAssociation(Ticket ticket, NodeRef sourceRef,
			NodeRef targetRef, QName assocTypeQName)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException;

	/**
	 * gets all associations from the given source where the associations
	 * qualified names match the pattern provided.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param sourceRef
	 *            the association source
	 * @param qnamePattern
	 *            the qualified name of the association type
	 * @return returns a list of NodeAssocRef instances for which the given node
	 *         is a source
	 * @throws InvalidNodeRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<AssociationRef> getTargetAssocs(Ticket ticket,
			NodeRef sourceRef, QName qnamePattern)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException;

	/**
	 * gets all associations to the given target where the associations
	 * qualified names match the pattern provided.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param targetRef
	 *            the association target
	 * @param qnamePattern
	 *            the association qname pattern to match against
	 * @return returns a list of NodeAssocRef instances for which the given node
	 *         is a source
	 * @throws InvalidNodeRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<AssociationRef> getSourceAssocs(Ticket ticket,
			NodeRef targetRef, QName qnamePattern)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException;

	/**
	 * get the archive node parent
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param storeRef
	 *            the store that items were deleted from
	 * @return returns the archive node parent
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public NodeRef getStoreArchiveNode(Ticket ticket, StoreRef storeRef)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Restore an individual node (along with its sub-tree nodes) to the target
	 * location. The archived node must have the archived aspect set against it.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param archivedNodeRef
	 *            the archived node
	 * @param destinationParentNodeRef
	 *            the parent to move the node into or null to use the original
	 * @param assocTypeQName
	 *            the primary association type name to use in the new location
	 *            or null to use the original
	 * @param assocQName
	 *            the primary association name to use in the new location or
	 *            null to use the original
	 * @return returns the reference to the newly created node
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public NodeRef restoreNode(Ticket ticket, NodeRef archivedNodeRef,
			NodeRef destinationParentNodeRef, QName assocTypeQName,
			QName assocQName) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * get the node information based on the requested parameters
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            a node reference
	 * @param requiredProperties
	 *            Which properties should the system return
	 * @param returnPeerAssocs
	 *            Should we return the associations
	 * @param returnChildAssocs
	 *            Should we return the child-associations
	 * @param returnAspects
	 *            Should we return the aspects
	 * @param returnCurrentUserPermissions
	 * 			  Should we return the current user's permissions
	 * @return node
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	@Deprecated
	public Node getNode(Ticket ticket, NodeRef nodeRef,
			List<QName> requiredProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects, 
			boolean returnCurrentUserPermissions)
			throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * get the node information based on the requested parameters
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            a node reference
	 * @param requiredProperties
	 *            Which properties should the system return
	 * @param returnPeerAssocs
	 *            Should we return the associations
	 * @param returnChildAssocs
	 *            Should we return the child-associations
	 * @param returnAspects
	 *            Should we return the aspects
	 * @param returnCurrentUserPermissions
	 * 			  Should we return the current user's permissions
	 * @param permissionsToCheckFor
	 *            a list of low level permissions to check for the current user
	 * @return node
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public Node getNode(Ticket ticket, NodeRef nodeRef,
			List<QName> requiredProperties, 
			boolean returnPeerAssocs, boolean returnChildAssocs, 
			boolean returnAspects, boolean returnCurrentUserPermissions,
			List<String> permissionsToCheckFor)
			throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * create file and upload content
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeName
	 *            name of node to be created
	 * @param container
	 *            destination node reference
	 * @param properties
	 *            map of properties to keyed by their qualified names
	 * @param fileName
	 *            name of the file to be uploaded
	 * @return reference to the newly created file
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public NodeRef createFile(Ticket ticket, String nodeName,
			NodeRef container, Map<QName, Serializable> properties,
			String fileName) throws InvalidTicketException, CmaRuntimeException;

	/**
	 * create file and upload content
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeName
	 *            name of node to be created
	 * @param container
	 *            destination node reference
	 * @param properties
	 *            map of properties to keyed by their qualified names
	 * @param input
	 *            stream where to get the uploaded content
	 * @return reference to the newly created file
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public NodeRef createFileFromStream(Ticket ticket, String nodeName,
			NodeRef container, Map<QName, Serializable> properties,
			InputStream input) throws InvalidTicketException, CmaRuntimeException;
	
	/**
	 * create folder and set the properties given the parent nodeRef
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param folderName
	 *            name of folder to be created
	 * @param container
	 *            the destination node reference
	 * @param properties
	 *            map of properties to keyed by their qualified names
	 * @return reference to the newly created folder
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public NodeRef createFolder(Ticket ticket, String folderName,
			NodeRef container, Map<QName, Serializable> properties)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * Creates a new, non-abstract, real node as a primary child of the given
	 * parent node.
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param parentRef
	 *            the destination node reference
	 * @param assocTypeQName
	 *            the type of the association to create.
	 * @param assocQName
	 *            the qualified name of the association
	 * @param nodeTypeQName
	 *            a reference to the node type
	 * @param properties
	 *            Initial properties to set, can be null if no properties are to
	 *            be set
	 * @return a reference to the newly created child association
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public ChildAssociationRef createNode(Ticket ticket, NodeRef parentRef,
			QName assocTypeQName, QName assocQName, QName nodeTypeQName,
			Map<QName, Serializable> properties) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * delete the node from the Alfresco repository given the NodeRef
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param deleteRef
	 *            the reference of the node to be deleted
	 * @return node reference
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 * @throws ContentLockedException
	 */
	public NodeRef deleteNode(Ticket ticket, NodeRef deleteRef)
			throws InvalidTicketException, CmaRuntimeException,
			NodeLockedException;

	/**
	 * copy the file into the destination NodeRef
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param originalRef
	 *            the file to copy
	 * @param destinationContainer
	 *            the new parent node to copy the file to
	 * @return the node reference of the copied file
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public NodeRef copyFile(Ticket ticket, NodeRef originalRef,
			NodeRef destinationContainer) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * copy node into the destination nodeRef in the Alfresco repository
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param originalRef
	 *            the node to copy
	 * @param destinationContainer
	 *            the new parent node to copy node to
	 * @param recursive
	 *            copy child nodes if this parent is the primary parent
	 * @return the node reference of the copied node
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public NodeRef copyNode(Ticket ticket, NodeRef originalRef,
			NodeRef destinationContainer, Boolean recursive)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * copy node into the destination nodeRef in the Alfresco repository
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param originalRefs
	 *            the list of nodes to copy
	 * @param destinationContainer
	 *            the new parent node to copy node to
	 * @param recursive
	 *            copy child nodes if this parent is the primary parent
	 * @return the node references of the copied nodes
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<NodeRef> copyNodes(Ticket ticket, List<NodeRef> originalRefs,
			NodeRef destinationContainer, Boolean recursive)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * move a file or folder to a new location
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param originalRef
	 *            the node to move
	 * @param destinationContainer
	 *            the new parent node to move the node to
	 * @return the new node reference
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public NodeRef moveNode(Ticket ticket, NodeRef originalRef,
			NodeRef destinationContainer) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * move a file or folder to a new location
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param originalRefs
	 *            the list of nodes to move
	 * @param destinationContainer
	 *            the new parent node to move the node to
	 * @return the new node references
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public List<NodeRef> moveNodes(Ticket ticket, List<NodeRef> originalRefs,
			NodeRef destinationContainer) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * move the node into the destination nodeRef in the Alfresco repository.
	 * This involves changing the node's primary parent and possibly the name of
	 * the association referencing it
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeToMoveRef
	 *            the node to move
	 * @param newParentRef
	 *            the new parent of the moved node
	 * @param assocTypeQName
	 *            the type of the association to create
	 * @param assocQName
	 *            the qualified name of the new child association
	 * @return the reference to the newly created child association
	 * @throws InvalidNodeRefException
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public ChildAssociationRef moveNode(Ticket ticket, NodeRef nodeToMoveRef,
			NodeRef newParentRef, QName assocTypeQName, QName assocQName)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException;

	/**
	 * rename a file or folder in its current location
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the file or folder to rename
	 * @param newName
	 *            the new name
	 * @return the node reference of the renamed node
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public NodeRef rename(Ticket ticket, NodeRef nodeRef, String newName)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * get the property of the node from Alfresco repository given the NodeRef
	 * and propertyQName
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node reference
	 * @param property
	 *            the qualified name of the property
	 * @return the value of the property
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public Serializable getProperty(Ticket ticket, NodeRef nodeRef,
			QName property) throws InvalidTicketException, CmaRuntimeException;

	/**
	 * get all the properties of the node from Alfresco Repository given the
	 * NodeRef
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node reference
	 * @return returns all properties keyed by their qualified name
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public Map<QName, Serializable> getProperties(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, CmaRuntimeException;

	/**
	 * set the property for the node given the NodeRef and propertyQName and
	 * propertyValue
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node reference
	 * @param property
	 *            the fully qualified name of the property
	 * @param value
	 *            the value to be set
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void setProperty(Ticket ticket, NodeRef nodeRef, QName property,
			Serializable value) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * set the properties for the node If the property name is not correct
	 * alfresco ignores it
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param nodeRef
	 *            the node reference
	 * @param properties
	 *            all the properties of the node keyed by their qualified names
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	public void setProperties(Ticket ticket, NodeRef nodeRef,
			Map<QName, Serializable> properties) throws InvalidTicketException,
			CmaRuntimeException;

	/**
	 * The root node has an entry in the path(s) returned. For this reason,
	 * there will always be <b>at least one</b> path element in the returned
	 * path(s). The first element will have a null parent reference and qname.
	 * 
	 * @param nodeRef
	 * @return Returns the path to the node along the primary node path
	 * @throws InvalidNodeRefException
	 *             if the node could not be found
	 * 
	 */
	public Path getPath(Ticket ticket, NodeRef nodeRef) throws InvalidNodeRefException, InvalidTicketException,
	CmaRuntimeException;

	/**
	 * The root node has an entry in the path(s) returned. For this reason,
	 * there will always be <b>at least one</b> path element in the returned
	 * path(s). The first element will have a null parent reference and qname.
	 * 
	 * @param nodeRef
	 * @param primaryOnly
	 *            true if only the primary path must be retrieved. If true, the
	 *            result will have exactly one entry.
	 * @return Returns a List of all possible paths to the given node
	 * @throws InvalidNodeRefException
	 *             if the node could not be found
	 */
	public List<Path> getPaths(Ticket ticket, NodeRef nodeRef, boolean primaryOnly)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException;
	
	/**
	 * Gets the mimeType of the nodeRef
	 * 
	 * @param ticket
	 * 			 Ticket received post authentication
	 * @param nodeRef
	 * 			the node reference
	 * @param propertyQName
	 * 				the name of the property
	 * @return MimeType
	 * @throws InvalidNodeRefException
	 * 						if node could not be found
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 * @throws InvalidTypeException
	 * 						if node is not of type content
	 */
	public String getMimeType(Ticket ticket, NodeRef nodeRef, QName propertyQName) throws InvalidNodeRefException, InvalidTicketException,
	CmaRuntimeException, InvalidTypeException;
		
	/**
	 * Gets the encoding of the nodeRef
	 * 
	 * @param ticket
	 * 			 Ticket received post authentication
	 * @param nodeRef
	 * 			the node reference
	 * @param propertyQName
	 * 				the name of the property
	 * @return Encoding
	 * @throws InvalidNodeRefException
	 * 				if node could not be found
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 * @throws InvalidTypeException
	 * 							if node is not of type content
	 */
	public String getEncoding(Ticket ticket, NodeRef nodeRef, QName propertyQName) throws InvalidNodeRefException, InvalidTicketException,
	CmaRuntimeException, InvalidTypeException;
}
