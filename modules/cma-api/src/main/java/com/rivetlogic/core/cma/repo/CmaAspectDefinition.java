/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.repo;

import org.alfresco.service.cmr.dictionary.AspectDefinition;

/**
 * This class is an implementation of Alfresco AspectDefinition which holds
 * AspectDefinition information retrieved from Alfresco
 * 
 * @author Hyanghee Lim
 * 
 */
public class CmaAspectDefinition extends CmaClassDefinition implements
		AspectDefinition {

}
