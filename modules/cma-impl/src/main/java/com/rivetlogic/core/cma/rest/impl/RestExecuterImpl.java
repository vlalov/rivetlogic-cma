/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.rest.impl;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Vector;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rivetlogic.core.cma.api.CmaConstants;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.mapping.CmaResult;
import com.rivetlogic.core.cma.mapping.CmaUnmarshaller;
import com.rivetlogic.core.cma.rest.api.NameValuePair;
import com.rivetlogic.core.cma.rest.api.RestExecuter;
import com.rivetlogic.crypto.Base64;

/**
 * This class uses HTTP method calls to Alfresco CMA WebScripts
 * to process CMA method calls and returns unmarshalled responses from CMA WebScripts
 * 
 * @author Hyanghee Lim
 *
 */
public class RestExecuterImpl implements RestExecuter {
	
	
	/**
	 * CmaUnmarshaller to unmarshal server response
	 */
	private CmaUnmarshaller cmaUnmarshaller;
	
	private HttpClient httpClient;
	
	private HttpConnectionManager connectionManager;

	/**
	 * HttpConnectionManager params
	 */
	private int maxHostConnections;
	private int maxTotalConnections;
	
	
	private static Log logger = LogFactory.getLog(RestExecuterImpl.class);
	
	public RestExecuterImpl() {
		maxHostConnections = RestExecuter.DEFAULT_MAX_HOST_CONNECTIONS;
		maxTotalConnections = RestExecuter.DEFAULT_MAX_TOTAL_CONNECTIONS;
	}
	
	public CmaResult execute(HttpMethod method, String targetUri, 
			String mappingFile, Vector<NameValuePair> params)
			throws InvalidTicketException, CmaRuntimeException, Throwable {
		return execute(method, targetUri, mappingFile, params, (List<Cookie>) null);
	}

	public CmaResult execute(HttpMethod method, String targetUri, 
			String mappingFile, Vector<NameValuePair> params, List<Cookie> cookies)
			throws InvalidTicketException, CmaRuntimeException, Throwable {

		HttpMethodBase httpMethod = createHttpMethod(method, targetUri, params);
		httpMethod.addRequestHeader(CmaConstants.HEADER_REQUESTTYPE, CmaConstants.REQUEST_TEXT);
		// add cookies to header
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				httpMethod.addRequestHeader(CmaConstants.HEADER_COOKIE, cookie.toExternalForm());
			}
		}
		// execute a HTTP method call
		try {
			executeHttpMethod(httpMethod);
			return processResponse(httpMethod, mappingFile);
		} catch (IOException e) {
			throw new CmaRuntimeException(e);
		} finally {
			httpMethod.releaseConnection();
		}
	}
	
	public CmaResult execute(HttpMethod method, String targetUri, 
			Vector<NameValuePair> params)
			throws InvalidTicketException, CmaRuntimeException, Throwable {
		return execute(method, targetUri, params, (List<Cookie>) null);
	}

	public CmaResult execute(HttpMethod method, String targetUri, 
			Vector<NameValuePair> params, List<Cookie> cookies)
			throws InvalidTicketException, CmaRuntimeException, Throwable {

		HttpMethodBase httpMethod = createHttpMethod(method, targetUri, params);
		httpMethod.addRequestHeader(CmaConstants.HEADER_REQUESTTYPE, CmaConstants.REQUEST_TEXT);
		// add cookies to header
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				httpMethod.addRequestHeader(CmaConstants.HEADER_COOKIE, cookie.toExternalForm());
			}
		}
		// execute a HTTP method call
		try {
			executeHttpMethod(httpMethod);
			return processResponse(httpMethod);
		} catch (IOException e) {
			throw new CmaRuntimeException(e);
		} finally {
			httpMethod.releaseConnection();
		}
	}
	
	@Deprecated
	public CmaResult execute(HttpMethod method, String targetUri, 
			Vector<NameValuePair> params, String fileName)
			throws InvalidTicketException, CmaRuntimeException, Throwable {	
		return execute(method, targetUri, params, fileName, -1);
	}
	
	@Deprecated
	public CmaResult execute(HttpMethod method, String targetUri, 
			String mappingFile, Vector<NameValuePair> params, String fileName)
			throws InvalidTicketException, CmaRuntimeException, Throwable {
		return execute(method, targetUri, mappingFile, params, fileName, -1);
	}
	
	@Deprecated
	public CmaResult execute(HttpMethod method, String targetUri,  
			String mappingFile, Vector<NameValuePair> params, InputStream input)
			throws InvalidTicketException, CmaRuntimeException, Throwable {
		return execute(method, targetUri, mappingFile, params, input, -1);
	}
		
	@Deprecated
	public CmaResult execute(HttpMethod method, String targetUri,  
			Vector<NameValuePair> params, InputStream input)
			throws InvalidTicketException, CmaRuntimeException, Throwable {
		return execute(method, targetUri, params, input, -1);
	}

	public CmaResult execute(HttpMethod method, String targetUri,  
			String mappingFile, Vector<NameValuePair> params, OutputStream output)
			throws InvalidTicketException, CmaRuntimeException, Throwable {
		HttpMethodBase httpMethod;
		// read content
		if (method.equals(HttpMethod.GET)) {
			httpMethod = createHttpMethod(HttpMethod.POST, targetUri, params);
		} else if (method.equals(HttpMethod.POST)) {
			httpMethod = createHttpMethod(method, targetUri, params);
		} else {
			throw new CmaRuntimeException("Unknown HttpMethod requested: " + method);
		}
		httpMethod.addRequestHeader(CmaConstants.HEADER_REQUESTTYPE, CmaConstants.REQUEST_READCONTENT);
		
		try {
			executeHttpMethod(httpMethod);
			String responseType = getResponseType(httpMethod);
			if (responseType.equals(CmaConstants.RESPONSE_BINARY)) {
				writeToStream(httpMethod.getResponseBodyAsStream(), output);
				return new CmaResult(CmaConstants.CMA_VERSION, CmaConstants.RESPONSE_BINARY);
			} else if (responseType.equals(CmaConstants.RESPONSE_ERROR)) {
				throw (Throwable) deserializeObject(httpMethod.getResponseBodyAsStream());
			} else {
				throw new CmaRuntimeException(httpMethod.getResponseBodyAsString());
			}
		} catch(IOException e) {
			throw new CmaRuntimeException(e);
		} finally {
			httpMethod.releaseConnection();
		}
	}
	
	public CmaResult execute(HttpMethod method, String targetUri,
			String mappingFile, Vector<NameValuePair> params, String fileName,
			long contentLength) throws InvalidTicketException,
			CmaRuntimeException, Throwable {
		HttpMethodBase httpMethod;
		FileInputStream input = null;
		if (method.equals(HttpMethod.GET)) { // read content
			httpMethod = createHttpMethod(HttpMethod.POST, targetUri, params);
			httpMethod.addRequestHeader(CmaConstants.HEADER_REQUESTTYPE, CmaConstants.REQUEST_READCONTENT);
		} else if (method.equals(HttpMethod.POST)) { // write content
			input = new FileInputStream(fileName);
			httpMethod = createFilePostMethod(targetUri, params, input, contentLength);
			httpMethod.addRequestHeader(CmaConstants.HEADER_REQUESTTYPE, CmaConstants.REQUEST_WRITECONTENT);
		} else {
			throw new CmaRuntimeException("Unknown HttpMethod requested: " + method);
		}
		try {
			executeHttpMethod(httpMethod);
			File file = new File(fileName);
			return processResponse(httpMethod, mappingFile, file);
		} catch(IOException e) {
			throw new CmaRuntimeException(e);
		} finally {
			if (input != null) {
				input.close();
			}
			httpMethod.releaseConnection();
		}
	}

	public CmaResult execute(HttpMethod method, String targetUri,
			String mappingFile, Vector<NameValuePair> params,
			InputStream input, long contentLength)
			throws InvalidTicketException, CmaRuntimeException, Throwable {
		HttpMethodBase httpMethod;
		// read content
		if (method.equals(HttpMethod.POST)) {
			httpMethod = createFilePostMethod(targetUri, params, input, contentLength);
			httpMethod.addRequestHeader(CmaConstants.HEADER_REQUESTTYPE, CmaConstants.REQUEST_WRITECONTENT);
		} else {
			throw new CmaRuntimeException("Unknown HttpMethod requested: " + method);
		}
		try {
			executeHttpMethod(httpMethod);
			return processResponse(httpMethod, mappingFile, null);
		} catch(IOException e) {
			throw new CmaRuntimeException(e);
		} finally {
			// the input should be closed where it was created
			httpMethod.releaseConnection();
		}
	}

	public CmaResult execute(HttpMethod method, String targetUri,
			Vector<NameValuePair> params, InputStream input, long contentLength)
			throws InvalidTicketException, CmaRuntimeException, Throwable {
		HttpMethodBase httpMethod;
		// read content
		if (method.equals(HttpMethod.POST)) {
			httpMethod = createFilePostMethod(targetUri, params, input, contentLength);
			httpMethod.addRequestHeader(CmaConstants.HEADER_REQUESTTYPE, CmaConstants.REQUEST_WRITECONTENT);
		} else {
			throw new CmaRuntimeException("Unknown HttpMethod requested: " + method);
		}
		try {
			executeHttpMethod(httpMethod);
			return processResponse(httpMethod);
		} catch(IOException e) {
			throw new CmaRuntimeException(e);
		} finally {
			// the input should be closed where it was created
			httpMethod.releaseConnection();
		}
	}
	
	public CmaResult execute(HttpMethod method, String targetUri,
			Vector<NameValuePair> params, String fileName, long contentLength)
			throws InvalidTicketException, CmaRuntimeException, Throwable {
		HttpMethodBase httpMethod;
		FileInputStream input = null;
		if (method.equals(HttpMethod.GET)) { // read content
			httpMethod = createHttpMethod(HttpMethod.POST, targetUri, params);
			httpMethod.addRequestHeader(CmaConstants.HEADER_REQUESTTYPE, CmaConstants.REQUEST_READCONTENT);
		} else if (method.equals(HttpMethod.POST)) { // write content
			input = new FileInputStream(fileName);
			httpMethod = createFilePostMethod(targetUri, params, input, contentLength);
			httpMethod.addRequestHeader(CmaConstants.HEADER_REQUESTTYPE, CmaConstants.REQUEST_WRITECONTENT);
		} else {
			throw new CmaRuntimeException("Unknown HttpMethod requested: " + method);
		}
		try {
			executeHttpMethod(httpMethod);
			File file = new File(fileName);
			return processResponse(httpMethod, file);
		} catch(IOException e) {
			throw new CmaRuntimeException(e);
		} finally {
			if (input != null) {
				input.close();
			}
			httpMethod.releaseConnection();
		}
	}

	/**
	 * Process a server response 
	 * 
	 * @param httpMethod
	 *         HttpMethodBase
	 * @param mappingFile
	 *         A mapping file for unmarshalling
	 * @return CmaResult that contains a response object
	 * @throws InvalidTicketException
	 *          failed to authenticate
	 * @throws IOException
	 *          failed to read a response
	 * @throws CmaRuntimeException
	 *          failed to execute httpMethod
	 */
	private CmaResult processResponse(HttpMethodBase httpMethod, String mappingFile)
		throws InvalidTicketException, IOException, CmaRuntimeException, Throwable {
		return processResponse(httpMethod, mappingFile, null);
	}
	
	/**
	 * Process a server response 
	 * 
	 * @param httpMethod
	 *         HttpMethodBase
	 * @return CmaResult that contains a response object
	 * @throws InvalidTicketException
	 *          failed to authenticate
	 * @throws IOException
	 *          failed to read a response
	 * @throws CmaRuntimeException
	 *          failed to execute httpMethod
	 */
	private CmaResult processResponse(HttpMethodBase httpMethod)
		throws InvalidTicketException, IOException, CmaRuntimeException, Throwable {
		return processResponse(httpMethod, null, null);
	}
	
	/**
	 * Process a server response 
	 * 
	 * @param httpMethod
	 *         HttpMethodBase
	 * @param File
	 *         file to write to or read from
	 * @return CmaResult that contains a response object
	 * @throws InvalidTicketException
	 *          failed to authenticate
	 * @throws IOException
	 *          failed to read a response
	 * @throws CmaRuntimeException
	 *          failed to execute httpMethod
	 */
	private CmaResult processResponse(HttpMethodBase httpMethod, File file)
		throws InvalidTicketException, IOException, CmaRuntimeException, Throwable {
		return processResponse(httpMethod, null, file);
	}

	/**
	 * Process a server response 
	 * 
	 * @param httpMethod
	 *         HttpMethodBase
	 * @param mappingFile
	 *         A mapping file for unmarshalling
	 * @param fileName 
	 *         A file name to write content to
	 * @return CmaResult that contains a response object
	 * @throws InvalidTicketException
	 *          failed to authenticate
	 * @throws IOException
	 *          failed to read a response
	 * @throws CmaRuntimeException
	 *          failed to execute httpMethod
	 */
	private CmaResult processResponse(HttpMethodBase httpMethod, String mappingFile, File file)
		throws InvalidTicketException, IOException, CmaRuntimeException, Throwable {
		
		String responseType = getResponseType(httpMethod);
		if (logger.isDebugEnabled()) {
			if (!responseType.equals(CmaConstants.RESPONSE_BINARY) &&
					! responseType.equals(CmaConstants.RESPONSE_ERROR) ) {
				logger.debug(httpMethod.getResponseBodyAsString());
			}
		}
		if (responseType.equals(CmaConstants.RESPONSE_SERIALIZBLE)) {
			Serializable obj = (Serializable) deserializeObject(httpMethod.getResponseBodyAsStream());
			return new CmaResult(CmaConstants.CMA_VERSION, obj);
		} else if (responseType.equals(CmaConstants.RESPONSE_XML)) {
			return (CmaResult) cmaUnmarshaller.unmarshal(new ByteArrayInputStream(
												httpMethod.getResponseBody()), 
												mappingFile);
		} else if (responseType.equals(CmaConstants.RESPONSE_BINARY)) {
			writeToFile(httpMethod.getResponseBodyAsStream(), file);
			return new CmaResult(CmaConstants.CMA_VERSION, CmaConstants.RESPONSE_BINARY);
		} else if (responseType.equals(CmaConstants.RESPONSE_ERROR)) {
			throw (Throwable) deserializeObject(httpMethod.getResponseBodyAsStream());
		} else if (responseType.equals(CmaConstants.RESPONSE_NONE)) {
			return new CmaResult(CmaConstants.CMA_VERSION, CmaConstants.RESPONSE_NONE);
		} else {
			throw new CmaRuntimeException(httpMethod.getResponseBodyAsString());
		}
	}
	
	/**
	 * Execute a Http Method 
	 * 
	 * @param httpMethod
	 *         a Http Method to execute
	 * @param cookies
	 *         HTTP Cookies
	 * @throws InvalidTicketException
	 *          failed to authenticate
	 * @throws CmaRuntimeException
	 *          failed to execute httpMethod
	 */
	private void executeHttpMethod (HttpMethodBase httpMethod) 
			throws IOException, InvalidTicketException, CmaRuntimeException {
		int statusCode = httpClient.executeMethod(httpMethod);		
		if (statusCode != 1) {
			if (httpMethod.getStatusCode() == HttpServletResponse.SC_FORBIDDEN
					|| httpMethod.getStatusCode() == HttpServletResponse.SC_UNAUTHORIZED ) {
					throw new InvalidTicketException(httpMethod.getResponseBodyAsString());
			}
		} else {
			throw new CmaRuntimeException("HttpMethod failed at " + httpMethod.getURI());
		}
	}
	
	/**
	 * Create HttpMethod with a target repository uri and given parameters
	 * 
	 * @param method
	 *            HttpMethod type (GET, POST, DELETE)
	 * @param targetUri
	 *            target repository uri
	 * @param params
	 *            a collection of parameters
	 * @return HttpMethod ready for execution
	 * @exception CmaRuntimeException 
	 * 				unknown HttpMethod type is requested
	 */
	private HttpMethodBase createHttpMethod(HttpMethod method,
			String targetUri, Vector<NameValuePair> params)
			throws CmaRuntimeException {

		if (method.equals(HttpMethod.GET)) {
			return createGetMethod(method, targetUri, params);
		} else if (method.equals(HttpMethod.POST)) {
			return createPostMethod(method, targetUri, params);
		} else if (method.equals(HttpMethod.DELETE)) {
			return new DeleteMethod(targetUri);
		} else {
			throw new CmaRuntimeException("Unknown HttpMethod requested: " + method);
		}
	}
	
	/**
	 * create HTTP Get method with given parameters
	 * 
	 * @param method
	 *         A Http Method to create
	 * @param targetUri
	 *         a repository uri
	 * @param params
	 *         method call parameters
	 * @return HttpMethodBase
	 */
	private HttpMethodBase createGetMethod(HttpMethod method,
				String targetUri, Vector<NameValuePair> params) {
		// concatenate parameters to the get method if any
		if (params != null && params.size() > 0) {
			StringBuffer buffer = new StringBuffer();
			buffer.append(targetUri + CmaConstants.GET_URI_PARAM_PATTERN);

			Iterator<NameValuePair> itr = params.iterator();
			while (itr.hasNext()) {
				NameValuePair pair = itr.next();
				buffer.append(pair.getName()
						+ CmaConstants.GET_URI_PARAM_ASSIGN_PATTERN
						+ serializeToString(pair.getValue()));
				if (itr.hasNext()) {
					buffer.append(CmaConstants.GET_URI_PARAM_CONCAT_PATTERN);
				}
			}
			targetUri = buffer.toString();
		}
		logger.debug("Created a HTTP Get Method to URI: " + targetUri);
		return new GetMethod(targetUri);	
	}
	
	/**
	 * create HTTP Post method with given parameters
	 * 
	 * @param method
	 *         A HTTP Method to create
	 * @param targetUri
	 *         a repository uri
	 * @param params
	 *         method call parameters
	 * @return HttpMethodBase
	 */
	private HttpMethodBase createPostMethod(HttpMethod method,
				String targetUri, Vector<NameValuePair> params) {
		PostMethod postMethod = new PostMethod(targetUri);
		// add parameters to the post method
		if (params != null && params.size() > 0) {
			Iterator<NameValuePair> itr = params.iterator();
			while (itr.hasNext()) {
				NameValuePair pair = itr.next();
				postMethod.addParameter(pair.getName(), 
							serializeToString(pair.getValue()));
			}
		}
		logger.debug("Created a HTTP Post Method to URI: " + targetUri);
		return postMethod;
	}
	
	/**
	 * Create a HttpMethod with a target repository uri and given string parameters and a file
	 * 
	 * @param targetUri
	 *         a repository uri
	 * @param params
	 *         method call parameters
	 * @param input
	 *         InputStream to read file from
	 * @return HttpMethodBase with InputStream reqest entity
	 * @throws CmaRuntimeException
	 */
//	private HttpMethodBase createFilePostMethod(String targetUri,
//			Vector<NameValuePair> params, InputStream input) throws CmaRuntimeException {
//		
//		PostMethod postMethod = new PostMethod(targetUri);
//		String queryString = createQueryString(params);
//		postMethod.setQueryString(queryString);
//		postMethod.setRequestEntity(new InputStreamRequestEntity(input));
//		logger.debug("Created a HTTP Post Method with InputStreamRequestEntity to URI: " + targetUri);
//		return postMethod;
//	}

	/**
	 * Create a HttpMethod with a target repository uri and given string parameters and a file
	 * 
	 * @param targetUri
	 *         a repository uri
	 * @param params
	 *         method call parameters
	 * @param input
	 *         InputStream to read file from
	 * @return HttpMethodBase with InputStream reqest entity
	 * @throws CmaRuntimeException
	 */
	private HttpMethodBase createFilePostMethod(String targetUri,
			Vector<NameValuePair> params, InputStream input, long contentLength) 
			throws CmaRuntimeException {
		
		PostMethod postMethod = new PostMethod(targetUri);
		String queryString = createQueryString(params);
		postMethod.setQueryString(queryString);
		postMethod.setRequestEntity(new InputStreamRequestEntity(input, contentLength));
		logger.debug("Created a HTTP Post Method with InputStreamRequestEntity to URI: " + targetUri);
		return postMethod;
	}

	/**
	 * Create a query string from a list of parameters
	 * 
	 * @param params
	 *          a list of parameters
	 * @return String
	 * @throws CmaRuntimeException
	 */
	private String createQueryString(Vector<NameValuePair> params)
			throws CmaRuntimeException {
		StringBuffer buffer = new StringBuffer();
		
		try {
			URLCodec urlCodec = new URLCodec();
			Iterator<NameValuePair> itr = params.iterator();
			// add parameters
			while (itr.hasNext()) {
				NameValuePair pair = itr.next();
				buffer.append(urlCodec.encode(pair.getName()));
				buffer.append(CmaConstants.QUERYSTRING_PARAM_ASSIGN_PATTERN);
				buffer.append(serializeToString(pair.getValue()));
				if (itr.hasNext()) {
					buffer.append(CmaConstants.QUERYSTRING_PARAM_CONCAT_PATTERN);
				}
			}
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
		return buffer.toString();
	}
	
	/**
	 * Serialize a given object to a string
	 * 
	 * @param object
	 *         Serializable
	 * @return a string that represents the given object 
	 * 
	 */
	private String serializeToString(Serializable object) {
		return Base64.encodeObject(object, Base64.DONT_BREAK_LINES);
		
	}
	
	/**
	 * Deserialize a throwable from a given InputStream
	 * 
	 * @param input 
	 *         InputStream to deserialize a throwable from
	 * @return Throwable
	 * @throws IOException
	 *          failed to close streams
	 * @throws ClassNotFoundException
	 *          failed to find the class of a serialized object
	 *//*
	private Throwable deserializeThorwable(InputStream input) 
		throws IOException, ClassNotFoundException {
		
		ObjectInputStream objStream = null;
		try{ 
			objStream = new ObjectInputStream(input);
			Throwable throwable = (Throwable) objStream.readObject();
			return throwable;
		} catch (ClassNotFoundException e) {
			logger.debug("An unknown type of exception was received from the server");
			throw e;
		} finally {
			input.close();
			if (objStream != null) {
				objStream.close();
			}
		}
	}*/
		
	/**
	 * Deserialize an object from a given InputStream
	 * 
	 * @param input 
	 *         InputStream to deserialize an object from
	 * @return Serializable
	 * @throws IOException
	 *          failed to close streams
	 * @throws ClassNotFoundException
	 *          failed to find the class of a serialized object
	 */
	private Serializable deserializeObject(InputStream input) 
		throws IOException, ClassNotFoundException {
		
		ObjectInputStream objStream = null;
		try{ 
			objStream = new ObjectInputStream(input);
			Serializable obj = (Serializable) objStream.readObject();
			return obj;
		} catch (ClassNotFoundException e) {
			logger.debug("An unknown type of exception was received from the server");
			throw e;
		} catch (EOFException e) {
			// FIXME: this is not a good approach
			// come up with something better
			return null;
		} finally {
			input.close();
			if (objStream != null) {
				objStream.close();
			}
		}
	}
	
	/**
	 * Parse an error response from the repository
	 * (NOT SUPPORTED)
	 * 
	 * @param input
	 *         InputStream to read an error response from
	 * @return CmaRumtimeException
	 * @throws IOException
	 *          Failed to close InputStream
	 * @throws CmaRuntimeException
	 *          Failed to unmarshall an error response
	 *//*
	private CmaRuntimeException parseServerErrorResponse(InputStream input)
		throws IOException, CmaRuntimeException {
		try{ 
			CmaErrorResponse error 
			= (CmaErrorResponse) cmaUnmarshaller.unmarshal(input, 
					cmaMappingService.getErrorMapping(CmaConstants.CMA_VERSION));
			CmaRuntimeException e 
				= new CmaRuntimeException(error.getMessage() + "\n" + error.getCallstack());
			return e;
		} finally {
			input.close();
		}
	}*/
	
	/**
	 * write a file from InputStream
	 * 
	 * @param input
	 * 			where to read content from
	 * @param fileName
	 * 			file to write content to
	 * @throws IOException
	 */
	private void writeToFile(InputStream input, File file) throws IOException {
		logger.debug("writing to file: " + file.getName());
		FileOutputStream fos = new FileOutputStream(file);
		byte[] buffer = new byte[256];
		int read = 0;
		while ((read = input.read(buffer)) > 0) {
			fos.write(buffer, 0, read);
		}
		input.close();
		fos.close();
	}
	
	/**
	 * write to OutputStream from InputStream
	 * 
	 * @param input
	 * 			where to read content from
	 * @param output
	 * 			file to write content to
	 * @throws IOException
	 */
	private void writeToStream(InputStream input, OutputStream output) throws IOException {
		
		byte[] buffer = new byte[256];
		int read = 0;
		while ((read = input.read(buffer)) > 0) {
			output.write(buffer, 0, read);
		}
		input.close();
		// OutputStream should be closed where it was created
	}
	
	/**
	 * get a response content type from a HttpMethod
	 * @param httpMethod
	 *        HttpMethodBase
	 * @return a string representation of the response content type
	 */
	private String getResponseType(HttpMethodBase httpMethod) {
		
		Header header = httpMethod.getResponseHeader(CmaConstants.HEADER_RESPONSETYPE);
		if (header != null) {
			logger.debug("Response Content Type: " + header.getValue());
			return header.getValue();
		} else {
			return "";
		}
	}

	/**
	 * set CmaUnmarshaller
	 * @param cmaUnmarshaller
	 */
	public void setCmaUnmarshaller(CmaUnmarshaller cmaUnmarshaller) {
		this.cmaUnmarshaller = cmaUnmarshaller;
	}
	
	/**
	 * setup httpClient
	 */
	public void init() {
		connectionManager = new MultiThreadedHttpConnectionManager();
		HttpConnectionManagerParams params = new HttpConnectionManagerParams();
		params.setDefaultMaxConnectionsPerHost(maxHostConnections);
		params.setMaxTotalConnections(maxTotalConnections);
		connectionManager.setParams(params);
		httpClient = new HttpClient(connectionManager);
	}

	/**
	 * set max host connections
	 * @param maxHostConnections
	 */
	public void setMaxHostConnections(int maxHostConnections) {
		this.maxHostConnections = maxHostConnections;
	}

	/**
	 * set max total connections
	 * @param maxTotalConnections
	 */
	public void setMaxTotalConnections(int maxTotalConnections) {
		this.maxTotalConnections = maxTotalConnections;
	}

}
