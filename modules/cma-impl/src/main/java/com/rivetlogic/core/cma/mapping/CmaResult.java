/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.exolab.castor.mapping.MapItem;

/**
 * CmaResult
 * 
 * This is a general wrapper class for server response
 * 
 * @author Hyanghee Lim
 * 
 */
public class CmaResult {

	private String version;
	private Object result;

	/**
	 * Default constructor
	 */
	public CmaResult() {
	}

	/**
	 * Constructor with version and result
	 * 
	 * @param version
	 *            CMA client version
	 * @param result
	 *            server response
	 */
	public CmaResult(String version, Object result) {
		this.version = version;
		this.result = result;
	}

	/**
	 * 
	 * @return version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * 
	 * @return response object
	 */
	public Object getResult() {
		return result;
	}

	/**
	 * 
	 * @param version
	 *            CMA version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * 
	 * @param result
	 *            response object
	 */
	public void setResult(Object result) {
		this.result = result;
	}

	/**
	 * Get a contained object from a mapping helper and set it as result
	 * 
	 * @param helper
	 *            CmaMappingHelper that contains an response object unmarshalled
	 */
	public void setContainedObject(CmaMappingHelper helper) {
		result = helper.getContainedObject();
	}

	/**
	 * Add a Map item to a result map
	 * 
	 * @param item
	 *            Castor Map Item
	 */
	public void addItemtoMap(MapItem item) {
		getResultAsMap().put(item.getKey(), item.getValue());
	}

	/**
	 * Get a contained object from a mapping helper and add to a result vector
	 * 
	 * @param helper
	 */
	public void addContainedObjectToVector(CmaMappingHelper helper) {
		getResultAsVector().add(helper.getContainedObject());
	}

	/**
	 * Get a result as a string
	 * @return string
	 */
	public String getResultAsString() {
		return result.toString();
	}

	/**
	 * Get a result as a map
	 * 
	 * @return map
	 */
	public Map getResultAsMap() {
		if (result == null) {
			result = new HashMap();
		}
		return (Map) result;
	}

	/**
	 * Get a result as a set
	 * @return set
	 */
	public Set getResultAsSet() {
		if (result == null) {
			result = new HashSet();
		}
		return (Set) result;
	}

	/**
	 * Get a result as a vector
	 * 
	 * @return vector
	 */
	public Vector getResultAsVector() {
		if (result == null) {
			result = new Vector();
		}
		return (Vector) result;
	}
}
