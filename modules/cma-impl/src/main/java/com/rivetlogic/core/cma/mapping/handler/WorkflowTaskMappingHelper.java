/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping.handler;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.alfresco.service.cmr.workflow.WorkflowPath;
import org.alfresco.service.cmr.workflow.WorkflowTask;
import org.alfresco.service.cmr.workflow.WorkflowTaskDefinition;
import org.alfresco.service.cmr.workflow.WorkflowTaskState;
import org.alfresco.service.namespace.QName;
import org.exolab.castor.mapping.MapItem;

import com.rivetlogic.core.cma.mapping.CmaMappingHelper;

/**
 * This class is a helper class for unmarshalling WorkflowTask
 * 
 * @author Hyanghee Lim
 *
 */
public class WorkflowTaskMappingHelper implements CmaMappingHelper {

	private String id;
	private String name;
	private String title;
	private String description;
	private WorkflowTaskDefinitionMappingHelper definition;
	private WorkflowPathMappingHelper path;
	private Map<QName, Serializable> properties;
	private WorkflowTaskState state;
	
	public WorkflowTaskMappingHelper () {
		properties = new HashMap<QName, Serializable> ();
	}
	
	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}
	
	public WorkflowTaskDefinitionMappingHelper getDefinition() {
		return definition;
	}
	
	public WorkflowPathMappingHelper getPath() {
		return path;
	}
	
	public Map<QName, Serializable> getProperties() {
		return properties;
	}
	
	public WorkflowTaskState getState() {
		return state;
	}
	
	public void addItemtoProperties(MapItem item) {
		properties.put((QName)item.getKey(), (Serializable)item.getValue());
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setDefinition(WorkflowTaskDefinitionMappingHelper definition) {
		this.definition = definition;
	}
	
	public void setPath(WorkflowPathMappingHelper path) {
		this.path = path;
	}
	
	public void setProperties(Map<QName, Serializable> properties) {
		this.properties = properties;
	}
	
	public void setState(WorkflowTaskState state) {
		this.state = state;
	}
	

	/**
	 * Get a WorkflowPath from contained variables
	 */
	public Object getContainedObject() {
		WorkflowTask task = new WorkflowTask(id, (WorkflowTaskDefinition) definition.getContainedObject(), name, title, description, state, (WorkflowPath) path.getContainedObject(), properties);
		return task;
	}
}
