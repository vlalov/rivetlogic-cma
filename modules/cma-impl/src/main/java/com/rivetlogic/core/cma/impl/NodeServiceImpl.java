/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.alfresco.service.cmr.dictionary.InvalidAspectException;
import org.alfresco.service.cmr.dictionary.InvalidTypeException;
import org.alfresco.service.cmr.lock.NodeLockedException;
import org.alfresco.service.cmr.repository.AssociationExistsException;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.InvalidChildAssociationRefException;
import org.alfresco.service.cmr.repository.InvalidNodeRefException;
import org.alfresco.service.cmr.repository.InvalidStoreRefException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.Path;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rivetlogic.core.cma.api.CmaConstants;
import com.rivetlogic.core.cma.api.NodeService;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.mapping.CmaMappingService;
import com.rivetlogic.core.cma.mapping.CmaResult;
import com.rivetlogic.core.cma.repo.Node;
import com.rivetlogic.core.cma.repo.Ticket;
import com.rivetlogic.core.cma.rest.api.NameValuePair;
import com.rivetlogic.core.cma.rest.api.RestExecuter;
import com.rivetlogic.core.cma.rest.api.RestExecuter.HttpMethod;
import com.rivetlogic.core.cma.util.ParameterCheck;

/**
 * @author Sweta Chalasani
 *
 */
public class NodeServiceImpl implements NodeService {
	
	private String serviceUri;
	
	/**
	 * xml mapping file
	 */
	private CmaMappingService cmaMappingService;

	/**
	 * RestExecuter
	 */
	private RestExecuter restExecuter;

	private static Log logger = LogFactory.getLog(NodeServiceImpl.class);
	
	public NodeRef createFolder(Ticket ticket, String folderName, NodeRef container,
			Map<QName, Serializable> properties) throws InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("destinationNodeRef", container);
		ParameterCheck.mandatoryString("folderName", folderName);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_CREATEFOLDER;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_FOLDERNAME, folderName));
		params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, container));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTIES, (Serializable) properties));
		params.add(new NameValuePair(CmaConstants.PARAM_USESERIALIZATION, true));
	 	
		try {
			return (NodeRef) execute(ticket, methodName, params, true);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}

	public ChildAssociationRef createNode(Ticket ticket, NodeRef parentRef,
			QName assocTypeQName, QName assocQName, QName nodeTypeQName,
			Map<QName, Serializable> properties) throws InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("destinationNodeRef", parentRef);
		ParameterCheck.mandatory("assocTypeQName", assocTypeQName);
		ParameterCheck.mandatory("assocQName", assocQName);
		ParameterCheck.mandatory("nodeTypeQName", nodeTypeQName);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_CREATENODE;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, parentRef));
		params.add(new NameValuePair(CmaConstants.PARAM_ASSOCTYPEQNAME, assocTypeQName));
		params.add(new NameValuePair(CmaConstants.PARAM_ASSOCQNAME, assocQName));
		params.add(new NameValuePair(CmaConstants.PARAM_NODETYPEQNAME, nodeTypeQName));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTIES, (Serializable) properties));
		params.add(new NameValuePair(CmaConstants.PARAM_USESERIALIZATION, true));
		
		try {
			return (ChildAssociationRef) execute(ticket, methodName, params, true);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}

	public NodeRef deleteNode(Ticket ticket, NodeRef deleteRef)
			throws InvalidTicketException, CmaRuntimeException,
			NodeLockedException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", deleteRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_DELETE;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, deleteRef));
		
		try {
			return (NodeRef) execute(ticket, methodName, params);
		} catch (NodeLockedException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}

	public NodeRef copyFile(Ticket ticket, NodeRef originalRef,
			NodeRef destinationContainer) throws InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", originalRef);
		ParameterCheck.mandatory("destinationNodeRef", destinationContainer);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_COPYFILE;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, originalRef));
		params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, destinationContainer));
		
		try {
			return (NodeRef) execute(ticket, methodName, params);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}

	public NodeRef copyNode(Ticket ticket, NodeRef originalRef,
			NodeRef destinationContainer, Boolean recursive)
			throws InvalidTicketException, CmaRuntimeException {
			//check parameters
			ParameterCheck.mandatory("ticket", ticket);
			ParameterCheck.mandatory("NodeRef", originalRef);
			ParameterCheck.mandatory("destinationNodeRef", destinationContainer);
			
			//create parameters
			String methodName = CmaConstants.METHOD_NODE_COPYNODE;
			Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
			params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
			params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, originalRef));
			params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, destinationContainer));
			params.add(new NameValuePair(CmaConstants.PARAM_RECURSIVE, recursive));
			
			try {
				return (NodeRef) execute(ticket, methodName, params);
			} catch (InvalidTicketException e) {
				throw e;
			} catch (CmaRuntimeException e) {
				throw e;
			} catch (Exception e) {
				throw new CmaRuntimeException(e);
			}
	}

	@SuppressWarnings("unchecked")
	public List<NodeRef> copyNodes(Ticket ticket, List<NodeRef> originalRefs,
			NodeRef destinationContainer, Boolean recursive)
			throws InvalidTicketException, CmaRuntimeException {
			//check parameters
			ParameterCheck.mandatory("ticket", ticket);
			ParameterCheck.mandatoryCollection("A list of nodeRefs", originalRefs);
			ParameterCheck.mandatory("destinationNodeRef", destinationContainer);
			
			//create parameters
			String methodName = CmaConstants.METHOD_NODE_COPYNODES;
			Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
			params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
			params.add(new NameValuePair(CmaConstants.PARAM_NODEREFS, (Serializable)originalRefs));
			params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, destinationContainer));
			params.add(new NameValuePair(CmaConstants.PARAM_RECURSIVE, recursive));
			
			try {
				return (List<NodeRef>) execute(ticket, methodName, params);
			} catch (InvalidTicketException e) {
				throw e;
			} catch (CmaRuntimeException e) {
				throw e;
			} catch (Exception e) {
				throw new CmaRuntimeException(e);
			}
	}

	public NodeRef moveNode(Ticket ticket, NodeRef originalRef,
			NodeRef destinationContainer) throws InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", originalRef);
		ParameterCheck.mandatory("destinationNodeRef", destinationContainer);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_MOVE;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, originalRef));
		params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, destinationContainer));
		
		try {
			return (NodeRef) execute(ticket, methodName, params);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<NodeRef> moveNodes(Ticket ticket, List<NodeRef> originalRefs,
			NodeRef destinationContainer) throws InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("A list of nodeRefs", originalRefs);
		ParameterCheck.mandatory("destinationNodeRef", destinationContainer);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_MOVENODES;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREFS, (Serializable)originalRefs));
		params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, destinationContainer));
		
		try {
			return (List<NodeRef>) execute(ticket, methodName, params);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public NodeRef rename(Ticket ticket, NodeRef nodeRef, String newName)
			throws InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		ParameterCheck.mandatoryString("newName", newName);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_RENAME;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_NEWNAME, newName));
		
		try {
			return (NodeRef) execute(ticket, methodName, params);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}

	public Serializable getProperty(Ticket ticket, NodeRef nodeRef, QName property)
			throws InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		ParameterCheck.mandatory("propertyQname", property);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETPROPERTY;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTYQNAME, property));
		params.add(new NameValuePair(CmaConstants.PARAM_USESERIALIZATION, true));
		
		try {
			return (Serializable) execute(ticket, methodName, params, true);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public Map<QName, Serializable> getProperties(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETPROPERTIES;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_USESERIALIZATION, true));
		
		try {
			return (Map<QName, Serializable>) execute(ticket, methodName, params, true);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}

	public void setProperty(Ticket ticket, NodeRef nodeRef, QName property,
			Serializable value) throws InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		ParameterCheck.mandatory("propertyQname", property);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_SETPROPERTY;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTYQNAME, property));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTYVALUE, value));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		
		try {
			execute(ticket, methodName, params);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}

	public void setProperties(Ticket ticket, NodeRef nodeRef,
			Map<QName, Serializable> properties) throws InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		ParameterCheck.mandatory("property", properties);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_SETPROPERTIES;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTIES, (Serializable) properties));

		try {
			execute(ticket, methodName, params);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}

	public void addAspect(Ticket ticket, NodeRef nodeRef, QName aspectTypeQName,
		Map<QName, Serializable> aspectProperties)
		throws InvalidNodeRefException, InvalidAspectException,
		InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		ParameterCheck.mandatory("aspectTypeQName", aspectTypeQName);
		ParameterCheck.mandatory("property", aspectProperties);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_ADDASPECT;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_ASPECTTYPEQNAME, aspectTypeQName));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTIES, (Serializable) aspectProperties));
		
		try {
			execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidAspectException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public void addAspect(Ticket ticket, NodeRef nodeRef, QName aspectTypeQName)
	throws InvalidNodeRefException, InvalidAspectException,
	InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		ParameterCheck.mandatory("aspectTypeQName", aspectTypeQName);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_ADDASPECTSWITHOUTPROP;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_ASPECTTYPEQNAME, aspectTypeQName));
		
		try {
			execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidAspectException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public ChildAssociationRef moveNode(Ticket ticket, NodeRef nodeToMoveRef,
			NodeRef newParentRef, QName assocTypeQName, QName assocQName)
			throws InvalidNodeRefException, InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeToMoveRef);
		ParameterCheck.mandatory("destinationNodeRef", newParentRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_MOVENODE;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeToMoveRef));
		params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, newParentRef));
		params.add(new NameValuePair(CmaConstants.PARAM_ASSOCTYPEQNAME, assocTypeQName));
		params.add(new NameValuePair(CmaConstants.PARAM_ASSOCQNAME, assocQName));
		
		try {
			return (ChildAssociationRef) execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public NodeRef createFile(Ticket ticket, String nodeName, NodeRef container,
			Map<QName, Serializable> properties, String fileName)
			throws InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("destinationNodeRef", container);
		ParameterCheck.mandatoryString("nodeName", nodeName);
		ParameterCheck.mandatoryString("fileName", fileName);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_CREATEFILE;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_NODENAME, nodeName));
		params.add(new NameValuePair(CmaConstants.PARAM_FILENAME, fileName));
		params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, container));
		
		params.add(new NameValuePair(CmaConstants.PARAM_CONTENTQNAME, QName.createQName(CmaConstants.PARAM_CONTENTQNAMEVALUE)));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTIES, (Serializable) properties));
		params.add(new NameValuePair(CmaConstants.PARAM_USESERIALIZATION, true));
		
		String targetUri = ticket.getRepositoryUri() + serviceUri;
		try {
			CmaResult result = 
				(CmaResult) restExecuter.execute(HttpMethod.POST, targetUri, params, fileName);
			return (NodeRef) result.getResult();
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}
	

	public NodeRef createFileFromStream(Ticket ticket, String nodeName,
			NodeRef container, Map<QName, Serializable> properties,
			InputStream input) throws InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("destinationNodeRef", container);
		ParameterCheck.mandatoryString("nodeName", nodeName);
		ParameterCheck.mandatory("inputStream", input);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_CREATEFILEFROMSTREAM;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_NODENAME, nodeName));
		params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, container));
		
		params.add(new NameValuePair(CmaConstants.PARAM_CONTENTQNAME, QName.createQName(CmaConstants.PARAM_CONTENTQNAMEVALUE)));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTIES, (Serializable) properties));
		params.add(new NameValuePair(CmaConstants.PARAM_USESERIALIZATION, true));
		
		String targetUri = ticket.getRepositoryUri() + serviceUri;
		try {
			CmaResult result = (CmaResult) restExecuter.execute(HttpMethod.POST, targetUri, 
								params, input);
			return (NodeRef) result.getResult();
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}

	@Deprecated
	public Node getNode(Ticket ticket, NodeRef nodeRef,
			List<QName> requiredProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects, 
			boolean returnCurrentUserPermissions)
			throws InvalidTicketException, CmaRuntimeException {
		return getNode(ticket, nodeRef, requiredProperties,
				returnPeerAssocs, returnChildAssocs, returnAspects,
				returnCurrentUserPermissions, null);
	}

	public Node getNode(Ticket ticket, NodeRef nodeRef,
			List<QName> requiredProperties, 
			boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects, 
			boolean returnCurrentUserPermissions,
			List<String> permissionsToCheckFor)
			throws InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETNODE;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_REQUIREDPROPERTIES, 
				(Serializable) requiredProperties));
		params.add(new NameValuePair(CmaConstants.PARAM_PERMISSIONSTOCHECKFOR, 
				(Serializable)permissionsToCheckFor));

		params.add(new NameValuePair(CmaConstants.PARAM_RETURNPEERASSOCS, returnPeerAssocs));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNCHILDASSOCS, returnChildAssocs));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNASPECTS, returnAspects));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNPERMISSIONS, 
				returnCurrentUserPermissions));
		params.add(new NameValuePair(CmaConstants.PARAM_USESERIALIZATION, true));
		
		try {
			return (Node) execute(ticket, methodName, params, true);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}

	public ChildAssociationRef addChild(Ticket ticket, NodeRef parentRef,
			NodeRef childRef, QName assocTypeQName, QName qname)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("destinationNodeRef", parentRef);
		ParameterCheck.mandatory("assocTypeQName", assocTypeQName);
		ParameterCheck.mandatory("qName", qname);
		ParameterCheck.mandatory("NodeRef", childRef);

		//create parameters
		String methodName = CmaConstants.METHOD_NODE_ADDCHILD;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, parentRef));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, childRef));
		params.add(new NameValuePair(CmaConstants.PARAM_ASSOCTYPEQNAME, assocTypeQName));
		params.add(new NameValuePair(CmaConstants.PARAM_QNAME, qname));
		
		try {
			return (ChildAssociationRef) execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public AssociationRef createAssociation(Ticket ticket, NodeRef sourceRef,
			NodeRef targetRef, QName assocTypeQName)
			throws InvalidNodeRefException, AssociationExistsException,
			InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", sourceRef);
		ParameterCheck.mandatory("destinationNodeRef", targetRef);
		ParameterCheck.mandatory("assocTypeQName", assocTypeQName);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_CREATEASSOCIATION;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, sourceRef));
		params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, targetRef));
		params.add(new NameValuePair(CmaConstants.PARAM_ASSOCTYPEQNAME, assocTypeQName));
		
		try {
			return (AssociationRef) execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (AssociationExistsException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public StoreRef createStore(Ticket ticket, String protocol, String identifier)
			throws InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("protocol", protocol);
		ParameterCheck.mandatoryString("identifier", identifier);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_CREATESTORE;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_PROTOCOL, protocol));
		params.add(new NameValuePair(CmaConstants.PARAM_IDENTIFIER, identifier));
		
		try {
			return (StoreRef) execute(ticket, methodName, params);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public boolean exists(Ticket ticket, StoreRef storeRef)
			throws InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("StoreRef", storeRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_EXISTSSTOREREF;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_STOREREF, storeRef));
		
		try {
			return ((Boolean) execute(ticket, methodName, params)).booleanValue();
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public boolean exists(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_EXISTSNODEREF;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		
		try {
			return ((Boolean) execute(ticket, methodName, params)).booleanValue();
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public Set<QName> getAspects(Ticket ticket, NodeRef nodeRef)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);

		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETASPECTS;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		try {
			return (Set<QName>) execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public List<ChildAssociationRef> getChildAssocs(Ticket ticket, NodeRef nodeRef)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);

		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETCHILDASSOCS;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		
		try {
			return (List<ChildAssociationRef>) execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public List<ChildAssociationRef> getChildAssocs(Ticket ticket, NodeRef nodeRef,
			QName typeQNamePattern, QName qnamePattern)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);
		ParameterCheck.mandatory("typeQNamePattern", typeQNamePattern);
		ParameterCheck.mandatory("qnamePattern", qnamePattern);

		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETCHILDASSOCIATIONS;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_TYPEQNAMEPATTERN, typeQNamePattern.toString()));
		params.add(new NameValuePair(CmaConstants.PARAM_QNAMEPATTERN, qnamePattern.toString()));
		
		try {
			return (List<ChildAssociationRef>) execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public NodeRef getChildByName(Ticket ticket, NodeRef nodeRef,
			QName assocTypeQName, String childName) throws InvalidNodeRefException,
			InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);
		ParameterCheck.mandatory("assocTypeQName", assocTypeQName);
		ParameterCheck.mandatoryString("childName", childName);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETCHILDBYNAME;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_ASSOCTYPEQNAME, assocTypeQName));
		params.add(new NameValuePair(CmaConstants.PARAM_CHILDNAME, childName));
		
		try {
			return (NodeRef) execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public List<ChildAssociationRef> getParentAssocs(Ticket ticket, NodeRef nodeRef)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETPARENTASSOCS;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		
		try {
			return (List<ChildAssociationRef>) execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public List<ChildAssociationRef> getParentAssocs(Ticket ticket,
			NodeRef nodeRef, QName typeQNamePattern,
			QName qnamePattern) throws InvalidNodeRefException,
			InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);
		ParameterCheck.mandatory("typeQNamePattern", typeQNamePattern);
		ParameterCheck.mandatory("qnamePattern", qnamePattern);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETPARENTASSOCIATIONS;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_TYPEQNAMEPATTERN, typeQNamePattern.toString()));
		params.add(new NameValuePair(CmaConstants.PARAM_QNAMEPATTERN, qnamePattern.toString()));
		
		try {
			return (List<ChildAssociationRef>) execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public ChildAssociationRef getPrimaryParent(Ticket ticket, NodeRef nodeRef)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETPRIMARYPARENT;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		
		try {
			return (ChildAssociationRef) execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public NodeRef getRootNode(Ticket ticket, StoreRef storeRef)
			throws InvalidStoreRefException, InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("StoreRef", storeRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETROOTNODE;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_STOREREF, storeRef));
		
		try {
			return (NodeRef) execute(ticket, methodName, params);
		} catch (InvalidStoreRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public List<AssociationRef> getSourceAssocs(Ticket ticket, NodeRef targetRef,
			QName qnamePattern) throws InvalidNodeRefException,
			InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("targetRef", targetRef);
		ParameterCheck.mandatory("qnamePattern", qnamePattern);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETSOURCEASSOCS;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, targetRef));
		params.add(new NameValuePair(CmaConstants.PARAM_QNAMEPATTERN, qnamePattern.toString()));
		
		try {
			return (List<AssociationRef>) execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public NodeRef getStoreArchiveNode(Ticket ticket, StoreRef storeRef)
			throws InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("StoreRef", storeRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETSTOREARCHIVENODE;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_STOREREF, storeRef));
		
		try {
			return (NodeRef) execute(ticket, methodName, params);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public List<StoreRef> getStores(Ticket ticket) throws InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETSTORES;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		
		try {
			return (List<StoreRef>) execute(ticket, methodName, params);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public List<AssociationRef> getTargetAssocs(Ticket ticket, NodeRef sourceRef,
			QName qnamePattern) throws InvalidNodeRefException,
			InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", sourceRef);
		ParameterCheck.mandatory("qnamePattern", qnamePattern);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETTARGETASSOCS;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, sourceRef));
		params.add(new NameValuePair(CmaConstants.PARAM_QNAMEPATTERN, qnamePattern.toString()));
		
		try {
			return (List<AssociationRef>) execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public QName getType(Ticket ticket, NodeRef nodeRef)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETTYPE;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		
		try {
			return (QName) execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public boolean hasAspect(Ticket ticket, NodeRef nodeRef, QName aspectTypeQName)
			throws InvalidNodeRefException, InvalidAspectException,
			InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);
		ParameterCheck.mandatory("aspectTypeQName", aspectTypeQName);

		//create parameters
		String methodName = CmaConstants.METHOD_NODE_HASASPECT;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_ASPECTTYPEQNAME, aspectTypeQName));
		
		try {
			return ((Boolean) execute(ticket, methodName, params)).booleanValue();
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidAspectException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public void removeAspect(Ticket ticket, NodeRef nodeRef, QName aspectTypeQName)
			throws InvalidNodeRefException, InvalidAspectException,
			InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);
		ParameterCheck.mandatory("aspectTypeQName", aspectTypeQName);

		//create parameters
		String methodName = CmaConstants.METHOD_NODE_REMOVEASPECT;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_ASPECTTYPEQNAME, aspectTypeQName));
		
		try {
			execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidAspectException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public void removeAssociation(Ticket ticket, NodeRef sourceRef,
			NodeRef targetRef, QName assocTypeQName)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", sourceRef);
		ParameterCheck.mandatory("destinationNodeRef", targetRef);
		ParameterCheck.mandatory("assocTypeQName", assocTypeQName);

		//create parameters
		String methodName = CmaConstants.METHOD_NODE_REMOVEASSOCIATION;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, sourceRef));
		params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, targetRef));
		params.add(new NameValuePair(CmaConstants.PARAM_ASSOCTYPEQNAME, assocTypeQName));
		
		try {
			execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public void removeChild(Ticket ticket, NodeRef parentRef, NodeRef childRef)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", childRef);
		ParameterCheck.mandatory("destinationNodeRef", parentRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_REMOVECHILD;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, parentRef));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, childRef));
		
		try {
			execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public boolean removeChildAssociation(Ticket ticket,
			ChildAssociationRef childAssocRef) throws InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("childAssocRef", childAssocRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_REMOVECHILDASSOCIATION;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_CHILDASSOCREF, childAssocRef));
		
		try {
			return ((Boolean) execute(ticket, methodName, params)).booleanValue();
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public void removeProperty(Ticket ticket, NodeRef nodeRef, QName qname)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);
		ParameterCheck.mandatory("qname", qname);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_REMOVEPROPERTY;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_QNAME, qname));
		
		try {
			execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	@Deprecated
	public boolean removeSeconaryChildAssociation(Ticket ticket,
			ChildAssociationRef childAssocRef)
			throws InvalidChildAssociationRefException, InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("childAssocRef", childAssocRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_REMOVESECONDARYCHILDASSOCIATION;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_CHILDASSOCREF, childAssocRef));
		
		try {
			return ((Boolean) execute(ticket, methodName, params)).booleanValue();
		} catch (InvalidChildAssociationRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public boolean removeSecondaryChildAssociation(Ticket ticket,
			ChildAssociationRef childAssocRef)
			throws InvalidChildAssociationRefException, InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("childAssocRef", childAssocRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_REMOVESECONDARYCHILDASSOCIATION;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_CHILDASSOCREF, childAssocRef));
		
		try {
			return ((Boolean) execute(ticket, methodName, params)).booleanValue();
		} catch (InvalidChildAssociationRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public NodeRef restoreNode(Ticket ticket, NodeRef archivedNodeRef,
			NodeRef destinationParentNodeRef, QName assocTypeQName, QName assocQName)
			throws InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", archivedNodeRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_RESTORENODE;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, archivedNodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, destinationParentNodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_ASSOCTYPEQNAME, assocTypeQName));
		params.add(new NameValuePair(CmaConstants.PARAM_ASSOCQNAME, assocQName));
		
		try {
			return (NodeRef) execute(ticket, methodName, params);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public void setChildAssociationIndex(Ticket ticket,
			ChildAssociationRef childAssocRef, int index)
			throws InvalidChildAssociationRefException, InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("childAssocRef", childAssocRef);
		ParameterCheck.mandatory("index", index);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_SETCHILDASSOCIATIONINDEX;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_CHILDASSOCREF, childAssocRef));
		params.add(new NameValuePair(CmaConstants.PARAM_INDEX, index));
		
		try {
			execute(ticket, methodName, params);
		} catch (InvalidChildAssociationRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public void setType(Ticket ticket, NodeRef nodeRef, QName typeQName)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);
		ParameterCheck.mandatory("typeQName", typeQName);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_SETTYPE;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_TYPEQNAME, typeQName));
		
		try {
			execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	

	public Path getPath(Ticket ticket, NodeRef nodeRef)
			throws InvalidNodeRefException, InvalidTicketException,
			CmaRuntimeException {
			//check parameters
			ParameterCheck.mandatory("ticket", ticket);
			ParameterCheck.mandatory("NodeRef", nodeRef);
			
			//create parameters
			String methodName = CmaConstants.METHOD_NODE_GETPATH;
			Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
			params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
			
			try {
				return (Path) execute(ticket, methodName, params);
			} catch (InvalidNodeRefException e) {
				throw e;
			} catch (InvalidTicketException e) {
				throw e;
			} catch (CmaRuntimeException e) {
				throw e;
			} catch (Exception e) {
				throw new CmaRuntimeException(e);
			}
	}

	public List<Path> getPaths(Ticket ticket, NodeRef nodeRef,
			boolean primaryOnly) throws InvalidNodeRefException,
			InvalidTicketException, CmaRuntimeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETPATHS;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_PRIMARYONLY, primaryOnly));
		
		try {
			return (List<Path>) execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	public String getEncoding(Ticket ticket, NodeRef nodeRef,
			QName propertyQName) throws InvalidNodeRefException,
			InvalidTicketException, CmaRuntimeException, InvalidTypeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETENCODING;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_CONTENTQNAME, propertyQName));
		
		try {
			return (String) execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (InvalidTypeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}

	public String getMimeType(Ticket ticket, NodeRef nodeRef,
			QName propertyQName) throws InvalidNodeRefException,
			InvalidTicketException, CmaRuntimeException, InvalidTypeException {
		//check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);
		
		//create parameters
		String methodName = CmaConstants.METHOD_NODE_GETMIMETYPE;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_CONTENTQNAME, propertyQName));
		
		try {
			return (String) execute(ticket, methodName, params);
		} catch (InvalidNodeRefException e) {
			throw e;
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (InvalidTypeException e) {
			throw e;
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		}
	}
	
	/**
	 * create a vector of parameters that contain general parameters 
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param methodName
	 *         method name
	 * @return a vector that contains general parameters 
	 */
	private Vector<NameValuePair> createGeneralParams(Ticket ticket, String methodName) {
		Vector<NameValuePair> params = new Vector<NameValuePair>();
		params.add(new NameValuePair(CmaConstants.PARAM_VERSION, CmaConstants.CMA_VERSION));
		params.add(new NameValuePair(CmaConstants.PARAM_SERVICE, CmaConstants.SERVICE_NODE));
		params.add(new NameValuePair(CmaConstants.PARAM_METHOD, methodName));
		params.add(new NameValuePair(CmaConstants.PARAM_ALFRESCO_TICKET, ticket.getTicket()));
		return params;
	}
	
	/**
	 * execute a method call with the given parameters
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param methodName
	 *            method name
	 * @param params
	 *            parameters for the method call
	 * @return a result object from the method call
	 * @throws InvalidTicketException
	 * @throws NodeLockedException
	 * @throws InvalidNodeRefException
	 * @throws InvalidChildAssociationRefException
	 * @throws InvalidStoreRefException
	 * @throws InvalidAspectException
	 * @throws AssociationExistsException
	 * @throws CmaRuntimeException
	 */
	private Object execute(Ticket ticket, String methodName, Vector<NameValuePair> params) 
	throws InvalidTicketException, NodeLockedException, InvalidNodeRefException, 
		InvalidChildAssociationRefException, InvalidStoreRefException, InvalidAspectException, 
			AssociationExistsException, CmaRuntimeException {
		return execute(ticket, methodName, params, false);
	}
	
	/**
	 * execute a method call with the given parameters
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param methodName
	 *            method name
	 * @param params
	 *            parameters for the method call
	 * @param useSerialization
	 *            whether to serialize return values
	 * @return a result object from the method call
	 * @throws InvalidTicketException
	 * @throws NodeLockedException
	 * @throws InvalidNodeRefException
	 * @throws InvalidChildAssociationRefException
	 * @throws InvalidStoreRefException
	 * @throws InvalidAspectException
	 * @throws AssociationExistsException
	 * @throws CmaRuntimeException
	 */
	private Object execute(Ticket ticket, String methodName, Vector<NameValuePair> params,
			boolean useSerialization) 
	throws InvalidTicketException, NodeLockedException, InvalidNodeRefException, 
		InvalidChildAssociationRefException, InvalidStoreRefException, InvalidAspectException, 
			AssociationExistsException, CmaRuntimeException {
	
		String targetUri = ticket.getRepositoryUri() + serviceUri;
		try {
			CmaResult result;
			if (useSerialization) {
				result = restExecuter.execute(HttpMethod.POST, targetUri, params);
				 
			} else {
				String mappingFile = cmaMappingService.getMapping(CmaConstants.CMA_VERSION, 
						CmaConstants.SERVICE_NODE, methodName);
				result = restExecuter.execute(HttpMethod.POST, targetUri, mappingFile, params);
			}
			return result.getResult();
		} catch(InvalidTicketException e) {
			throw e;
		} catch(NodeLockedException e) {
			throw e;
		} catch(InvalidNodeRefException e) {
			throw e;
		} catch(InvalidChildAssociationRefException e) {
			throw e;
		} catch(InvalidStoreRefException e) {
			throw e;
		} catch(InvalidAspectException e) {
			throw e;
		} catch(AssociationExistsException e) {
			throw e;
		} catch(CmaRuntimeException e) {
			throw e;
		} catch(Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}
	
	/**
	 * Set the service uri to a web script 
	 * 
	 * @param serviceUri
	 */
	public void setServiceUri(String serviceUri) {
		this.serviceUri = serviceUri;
	}
	
	/**
	 * set mapping service 
	 * @param cmaMappingService
	 */
	public void setCmaMappingService(CmaMappingService cmaMappingService) {
		this.cmaMappingService = cmaMappingService;
	}
	
	/**
	 * set RestExecuter to execute HttpMethod call
	 * @param restExecuter
	 *        RestExecuter
	 */
	public void setRestExecuter(RestExecuter restExecuter) {
		this.restExecuter = restExecuter;
	}
}
