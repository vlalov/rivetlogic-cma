/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping.impl;

import java.io.InputStream;
import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.mapping.CmaMapping;
import com.rivetlogic.core.cma.mapping.CmaMappingService;
import com.rivetlogic.core.cma.mapping.CmaUnmarshaller;

/**
 * This class provides mapping information for CMA services
 * 
 * @author Hyanghee Lim
 *
 */
public class CmaMappingServiceImpl implements CmaMappingService {

	private static Log logger = LogFactory.getLog(CmaMappingServiceImpl.class);

	/**
	 * CMA Mapping information
	 */
	private CmaMapping cmaMapping = null;

	/**
	 * a mapping file name used for unmarshalling
	 */
	private String mappingFile;

	/**
	 * a mapping source file to read mapping information from
	 */
	private String mappingConfiguration;

	/**
	 * Unmarshaller
	 */
	private CmaUnmarshaller cmaUnmarshaller;

	public String getMapping(String version, String serviceName,
			String methodName) throws CmaRuntimeException {
		// TODO: version is no longer used. remove version in a later release
		if (cmaMapping != null) {
			String mappingFile = cmaMapping.getMapping(serviceName, methodName); 
			if (logger.isDebugEnabled()) {
				logger.debug("Cma Ver " + version +  " - " + serviceName + "." 
							+ methodName + ": " + mappingFile);
			}
			return mappingFile;
		} else {
			throw new CmaRuntimeException("no mapping information exists");
		}

	}

	public String getErrorMapping(String version) throws CmaRuntimeException {
		// TODO: version is no longer used. remove version in a later release
		if (cmaMapping != null) {
			String errorMappingFile = cmaMapping.getErrorMapping(); 
			if (logger.isDebugEnabled()) {
				logger.debug("Cma Ver " + version +  ": " + errorMappingFile);
			}
			return errorMappingFile;
		} else {
			throw new CmaRuntimeException("no mapping information exists");
		}
	}

	public void setUnMarshaller(CmaUnmarshaller unmarshaller) {
		this.cmaUnmarshaller = unmarshaller;
	}

	public void setMappingFile(String mappingFile) {
		this.mappingFile = mappingFile;
	}

	public void setMappingConfiguration(String mappingConfiguration) {
		this.mappingConfiguration = mappingConfiguration;
	}
	
	public void setCmaUnmarshaller(CmaUnmarshaller cmaUnmarshaller) {
		this.cmaUnmarshaller = cmaUnmarshaller;
	}

	/**
	 * init method for Spring bean creation
	 */
	public void init() throws IOException, CmaRuntimeException {

		InputStream input = null;
		try {
			input = this.getClass().getClassLoader().getResourceAsStream(mappingConfiguration);
			cmaMapping = (CmaMapping) cmaUnmarshaller.unmarshal(input, mappingFile);
		} finally {
			if (input != null) {
				input.close();
			}
		}
	}

}
