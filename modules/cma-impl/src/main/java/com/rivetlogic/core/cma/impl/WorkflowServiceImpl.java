/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.workflow.WorkflowDefinition;
import org.alfresco.service.cmr.workflow.WorkflowDeployment;
import org.alfresco.service.cmr.workflow.WorkflowInstance;
import org.alfresco.service.cmr.workflow.WorkflowPath;
import org.alfresco.service.cmr.workflow.WorkflowTask;
import org.alfresco.service.cmr.workflow.WorkflowTaskQuery;
import org.alfresco.service.cmr.workflow.WorkflowTaskState;
import org.alfresco.service.cmr.workflow.WorkflowTimer;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rivetlogic.core.cma.api.CmaConstants;
import com.rivetlogic.core.cma.api.WorkflowService;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.mapping.CmaMappingService;
import com.rivetlogic.core.cma.mapping.CmaResult;
import com.rivetlogic.core.cma.repo.Ticket;
import com.rivetlogic.core.cma.rest.api.NameValuePair;
import com.rivetlogic.core.cma.rest.api.RestExecuter;
import com.rivetlogic.core.cma.rest.api.RestExecuter.HttpMethod;
import com.rivetlogic.core.cma.util.ParameterCheck;

/**
 * @author Sweta Chalasani
 *
 */
public class WorkflowServiceImpl implements WorkflowService {

	private static Log logger = LogFactory.getLog(WorkflowServiceImpl.class);

	/**
	 * search service uri template
	 */
	private String serviceUri;

	/**
	 * mapping service
	 */
	private CmaMappingService cmaMappingService;

	/**
	 * RestExecuter
	 */
	private RestExecuter restExecuter;
	
	public WorkflowInstance cancelWorkflow(Ticket ticket, String workflowId)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("workflowId", workflowId);

		String methodName = CmaConstants.METHOD_WORKFLOW_CANCELWORKFLOW;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		parameters.add(new NameValuePair(CmaConstants.PARAM_WORKFLOWID, workflowId));

		return (WorkflowInstance) execute(ticket, methodName, parameters);
	}

	public NodeRef createPackage(Ticket ticket, NodeRef container)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);

		String methodName = CmaConstants.METHOD_WORKFLOW_CREATEPACKAGE;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		parameters.add(new NameValuePair(CmaConstants.PARAM_NODEREF, container));

		return (NodeRef) execute(ticket, methodName, parameters);
	}

	public WorkflowInstance deleteWorkflow(Ticket ticket, String workflowId)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("workflowId", workflowId);

		String methodName = CmaConstants.METHOD_WORKFLOW_DELETEWORKFLOW;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		parameters.add(new NameValuePair(CmaConstants.PARAM_WORKFLOWID, workflowId));

		return (WorkflowInstance) execute(ticket, methodName, parameters);
	}

	public WorkflowDeployment deployDefinition(Ticket ticket, String engineId,
			InputStream definitionFile, String mimetype)
			throws InvalidTicketException, CmaRuntimeException {
		throw new UnsupportedOperationException("The method " + CmaConstants.METHOD_WORKFLOW_DEPLOYDEFINITION + " is not supported");
		// check parameters
		/*ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("EngineId", engineId);
		ParameterCheck.mandatory("definitionFile", definitionFile);
		ParameterCheck.mandatoryString("MimeType", mimetype);

		String methodName = CmaConstants.METHOD_WORKFLOW_DEPLOYDEFINITION;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_ENGINEID, engineId));
		parameters.add(new NameValuePair(CmaConstants.PARAM_MIMETYPE, mimetype));
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		String targetUri = ticket.getRepositoryUri() + serviceUri;
		String mappingFile = cmaMappingService.getMapping(
				CmaConstants.CMA_VERSION,
				CmaConstants.SERVICE_WORKFLOW,
				CmaConstants.METHOD_WORKFLOW_DEPLOYDEFINITION);
		try {
			CmaResult result = (CmaResult) restExecuter.execute(HttpMethod.POST, targetUri, mappingFile, parameters, definitionFile);
			return (WorkflowDeployment) result.getResult();
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}*/
	}

	public WorkflowDeployment deployDefintion(Ticket ticket,
			NodeRef workflowDefinition) throws InvalidTicketException,
			CmaRuntimeException {
		throw new UnsupportedOperationException("The method " + 
				CmaConstants.METHOD_WORKFLOW_DEPLOYDEFINITIONWITHNODEREFOFCONTENTOBJECT 
				+ " is not supported");
		/*// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("workflowDefinition", workflowDefinition);

		String methodName = CmaConstants.METHOD_WORKFLOW_DEPLOYDEFINITIONWITHNODEREFOFCONTENTOBJECT;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_ENGINEID, workflowDefinition));
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		return (WorkflowDeployment) execute(ticket, methodName, parameters);*/
	}

	public WorkflowTask endTask(Ticket ticket, String taskId,
			String transitionId) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("taskId", taskId);
		// transition id should be optional (CMA-45)
		//ParameterCheck.mandatoryString("transitionId", transitionId);

		String methodName = CmaConstants.METHOD_WORKFLOW_ENDTASK;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_TASKID, taskId));
		parameters.add(new NameValuePair(CmaConstants.PARAM_TRANSITIONID, transitionId));
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		return (WorkflowTask) execute(ticket, methodName, parameters);
	}

	public WorkflowPath fireEvent(Ticket ticket, String pathId, String event)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("pathId", pathId);
		ParameterCheck.mandatoryString("event", event);

		String methodName = CmaConstants.METHOD_WORKFLOW_FIREEVENT;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_PATHID, pathId));
		parameters.add(new NameValuePair(CmaConstants.PARAM_EVENT, event));
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		return (WorkflowPath) execute(ticket, methodName, parameters);
	}

	public List<WorkflowDefinition> getDefinitions(Ticket ticket,
			boolean includePrevious) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("includePrevious", includePrevious);

		String methodName = CmaConstants.METHOD_WORKFLOW_GETDEFINITIONS;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_INCLUDEPREVIOUS, includePrevious));

		return (List<WorkflowDefinition>) execute(ticket, methodName, parameters);
	}

	public List<WorkflowInstance> getActiveWorkflows(Ticket ticket,
			String workflowDefinitionId) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("workflowDefinitionId", workflowDefinitionId);

		String methodName = CmaConstants.METHOD_WORKFLOW_GETACTIVEWORKFLOWS;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_WORKFLOWDEFINITIONID, workflowDefinitionId));

		return (List<WorkflowInstance>) execute(ticket, methodName, parameters);
	}

	public List<WorkflowDefinition> getAllDefinitionsByName(Ticket ticket,
			String workflowName) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("workflowName", workflowName);

		String methodName = CmaConstants.METHOD_WORKFLOW_GETALLDEFINITIONSBYNAME;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_WORKFLOWNAME, workflowName));

		return (List<WorkflowDefinition	>) execute(ticket, methodName, parameters);
	}

	public List<WorkflowTask> getAssignedTasks(Ticket ticket,
			String authorityName, WorkflowTaskState state)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("authorityName", authorityName);
		ParameterCheck.mandatory("state", state);

		String methodName = CmaConstants.METHOD_WORKFLOW_GETASSIGNEDTASKS;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_AUTHORITYNAME, authorityName));
		parameters.add(new NameValuePair(CmaConstants.PARAM_STATE, state));

		return (List<WorkflowTask>) execute(ticket, methodName, parameters);
	}

	public WorkflowDefinition getDefinitionById(Ticket ticket,
			String workflowDefinitionId) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("workflowDefinitionId", workflowDefinitionId);

		String methodName = CmaConstants.METHOD_WORKFLOW_GETDEFINITIONBYID;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_WORKFLOWDEFINITIONID, workflowDefinitionId));

		return (WorkflowDefinition) execute(ticket, methodName, parameters);
	}

	public WorkflowDefinition getDefinitionByName(Ticket ticket,
			String workflowName) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("workflowName", workflowName);

		String methodName = CmaConstants.METHOD_WORKFLOW_GETDEFINITIONBYNAME;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_WORKFLOWNAME, workflowName));

		return (WorkflowDefinition) execute(ticket, methodName, parameters);
	}

	public byte[] getDefintionImage(Ticket ticket, String workflowDefinitionId)
			throws InvalidTicketException, CmaRuntimeException {
		throw new UnsupportedOperationException("The method " + CmaConstants.METHOD_WORKFLOW_GETDEFINITIONIMAGE + " is not supported");

		/*// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("workflowDefinitionId", workflowDefinitionId);

		String methodName = CmaConstants.METHOD_WORKFLOW_GETDEFINITIONIMAGE;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_WORKFLOWDEFINITIONID, workflowDefinitionId));

		return (byte[]) execute(ticket, methodName, parameters);*/
	}

	public List<WorkflowTask> getPooledTasks(Ticket ticket, String authorityName)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("authorityName", authorityName);

		String methodName = CmaConstants.METHOD_WORKFLOW_GETPOOLEDTASKS;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_AUTHORITYNAME, authorityName));

		return (List<WorkflowTask>) execute(ticket, methodName, parameters);
	}

	public WorkflowTask getTaskById(Ticket ticket, String taskId)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("taskId", taskId);

		String methodName = CmaConstants.METHOD_WORKFLOW_GETTASKBYID;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_TASKID, taskId));

		return (WorkflowTask) execute(ticket, methodName, parameters);
	}

	public List<WorkflowTask> getTaskForWorkflowPath(Ticket ticket,
			String pathId) throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("pathId", pathId);

		String methodName = CmaConstants.METHOD_WORKFLOW_GETTASKFORWORKFLOWPATH;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_PATHID, pathId));

		return (List<WorkflowTask>) execute(ticket, methodName, parameters);
	}

	public List<WorkflowTimer> getTimers(Ticket ticket, String workflowId)
			throws InvalidTicketException, CmaRuntimeException {
		throw new UnsupportedOperationException("The method " + CmaConstants.METHOD_WORKFLOW_GETTIMERS + " is not supported");
		/*// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("workflowId", workflowId);

		String methodName = CmaConstants.METHOD_WORKFLOW_GETTIMERS;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_WORKFLOWID, workflowId));

		return (List<WorkflowTimer>) execute(ticket, methodName, parameters);*/
	}

	public WorkflowInstance getWorkflowById(Ticket ticket, String workflowId)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("workflowId", workflowId);

		String methodName = CmaConstants.METHOD_WORKFLOW_GETWORKFLOWBYID;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_WORKFLOWID, workflowId));

		return (WorkflowInstance) execute(ticket, methodName, parameters);
	}

	public List<WorkflowPath> getWorkflowPaths(Ticket ticket, String workflowId)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("workflowId", workflowId);

		String methodName = CmaConstants.METHOD_WORKFLOW_GETWORKFLOWPATHS;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_WORKFLOWID, workflowId));

		return (List<WorkflowPath>) execute(ticket, methodName, parameters);
	}

	public List<WorkflowInstance> getWorkflowsForContent(Ticket ticket,
			NodeRef packageItem, boolean active) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("packageItem", packageItem);

		String methodName = CmaConstants.METHOD_WORKFLOW_GETWORKFLOWSFORCONTENT;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_PACKAGEITEM, packageItem));
		parameters.add(new NameValuePair(CmaConstants.PARAM_ACTIVE, active));

		return (List<WorkflowInstance>) execute(ticket, methodName, parameters);
	}

	public boolean isDefinitionDeployed(Ticket ticket, NodeRef workflowdefintion)
			throws InvalidTicketException, CmaRuntimeException {
		throw new UnsupportedOperationException("The method " + CmaConstants.METHOD_WORKFLOW_ISDEFINITIONDEPLOYED + " is not supported");
		/*// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("workflowdefintion", workflowdefintion);

		String methodName = CmaConstants.METHOD_WORKFLOW_ISDEFINITIONDEPLOYED;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_WORKFLOWDEFINITIONID, workflowdefintion));

		return ((Boolean) execute(ticket, methodName, parameters)).booleanValue();*/
	}

	public List<WorkflowTask> queryTasks(Ticket ticket, WorkflowTaskQuery query)
			throws InvalidTicketException, CmaRuntimeException {
		throw new UnsupportedOperationException("The method " + CmaConstants.METHOD_WORKFLOW_QUERYTASKS + " is not supported");
		/*// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("query", query);

		String methodName = CmaConstants.METHOD_WORKFLOW_QUERYTASKS;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_QUERY, (Serializable)query));

		return (List<WorkflowTask>) execute(ticket, methodName, parameters);*/
	}

	public WorkflowPath signal(Ticket ticket, String pathId, String transitionId)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("pathId", pathId);

		String methodName = CmaConstants.METHOD_WORKFLOW_SIGNAL;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_PATHID, pathId));
		parameters.add(new NameValuePair(CmaConstants.PARAM_TRANSITIONID, transitionId));
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
 
		return (WorkflowPath) execute(ticket, methodName, parameters);
	}

	public WorkflowPath startWorkflow(Ticket ticket,
			String workflowDefinitionId, Map<QName, Serializable> params)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("workflowDefinitionId", workflowDefinitionId);

		String methodName = CmaConstants.METHOD_WORKFLOW_STARTWORKFLOW;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		parameters.add(new NameValuePair(CmaConstants.PARAM_WORKFLOWDEFINITIONID, workflowDefinitionId));
		parameters.add(new NameValuePair(CmaConstants.PARAM_PROPERTIES, (Serializable)params));
 
		return (WorkflowPath) execute(ticket, methodName, parameters);
	}

	public WorkflowPath startWorkflowFromTemplate(Ticket ticket,
			NodeRef templateDefinition) throws InvalidTicketException,
			CmaRuntimeException {
		throw new UnsupportedOperationException("The method " + CmaConstants.METHOD_WORKFLOW_STARTWORKFLOWFROMTEMPLATE + " is not supported");
		/*// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("templateDefinition", templateDefinition);

		String methodName = CmaConstants.METHOD_WORKFLOW_STARTWORKFLOWFROMTEMPLATE;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_TEMPLATEDEFINITION, templateDefinition));
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
 
		return (WorkflowPath) execute(ticket, methodName, parameters);*/
	}

	public void undeployDefinition(Ticket ticket, String workflowDefinitionId)
			throws InvalidTicketException, CmaRuntimeException {
		throw new UnsupportedOperationException("The method " + CmaConstants.METHOD_WORKFLOW_STARTWORKFLOWFROMTEMPLATE + " is not supported");
		/*// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("templateDefinition", workflowDefinitionId);

		String methodName = CmaConstants.METHOD_WORKFLOW_UNDEPLOYDEFINITION;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_WORKFLOWDEFINITIONID, workflowDefinitionId));
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
 
		execute(ticket, methodName, parameters);*/
	}

	public WorkflowTask updateTask(Ticket ticket, String taskId,
			Map<QName, Serializable> properties, Map<QName, List<NodeRef>> add,
			Map<QName, List<NodeRef>> remove) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("taskId", taskId);

		String methodName = CmaConstants.METHOD_WORKFLOW_UPDATETASK;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		parameters.add(new NameValuePair(CmaConstants.PARAM_TASKID, taskId));
		parameters.add(new NameValuePair(CmaConstants.PARAM_PROPERTIES, (Serializable)properties));
		if(add != null) {
			parameters.add(new NameValuePair(CmaConstants.PARAM_ADD, (Serializable)add));
		} else {
			parameters.add(new NameValuePair(CmaConstants.PARAM_ADD, null));
		}
		if(remove != null) {
			parameters.add(new NameValuePair(CmaConstants.PARAM_REMOVE, (Serializable)remove));
		} else {
			parameters.add(new NameValuePair(CmaConstants.PARAM_REMOVE, null));
		}
 
		return (WorkflowTask) execute(ticket, methodName, parameters);
	}
	
	public WorkflowTask getStartTask(Ticket ticket, String workflowInstanceId)
	throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString(CmaConstants.PARAM_WORKFLOWINSTANCEID, workflowInstanceId);
		
		String methodName = "getStartTask";
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair("workflowInstanceId", workflowInstanceId));
		
		return (WorkflowTask) execute(ticket, methodName, parameters);
		}
	
	/**
	 * create a vector of parameters that contain general parameters
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param methodName
	 *         method name
	 * @return a vector that contains general parameters 
	 */
	private Vector<NameValuePair> createGeneralParameters(Ticket ticket, String methodName) {
		Vector<NameValuePair> parameters = new Vector<NameValuePair>();
		parameters.add(new NameValuePair(CmaConstants.PARAM_VERSION, CmaConstants.CMA_VERSION));
		parameters.add(new NameValuePair(CmaConstants.PARAM_SERVICE, CmaConstants.SERVICE_WORKFLOW));
		parameters.add(new NameValuePair(CmaConstants.PARAM_METHOD, methodName));
		parameters.add(new NameValuePair(CmaConstants.PARAM_ALFRESCO_TICKET, ticket.getTicket()));
		return parameters;
	}
	
	/**
	 * execute a method call with the given parameters
	 * 
	 * @param methodName
	 *            method name
	 * @param parameters
	 *            parameters for the method call
	 * @return a result object from the method call
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	private Object execute(Ticket ticket, String methodName, Vector<NameValuePair> parameters) 
		throws InvalidTicketException, CmaRuntimeException {
		
		String targetUri = ticket.getRepositoryUri() + serviceUri;
		String mappingFile = cmaMappingService.getMapping(CmaConstants.CMA_VERSION, 
										CmaConstants.SERVICE_WORKFLOW, methodName);
		
		try {
			CmaResult result = restExecuter.execute(HttpMethod.POST, targetUri, 
									mappingFile, parameters);
			return result.getResult();
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}

	/**
	 * set service uri of web script
	 * @param serviceUri
	 */
	public void setServiceUri(String serviceUri) {
		this.serviceUri = serviceUri;
	}
		
	/**
	 * set mapping service 
	 * @param cmaMappingService
	 */
	public void setCmaMappingService(CmaMappingService cmaMappingService) {
		this.cmaMappingService = cmaMappingService;
	}
	
	/**
	 * set RestExecuter to execute HttpMethod call
	 * @param restExecuter
	 *        RestExecuter
	 */
	public void setRestExecuter(RestExecuter restExecuter) {
		this.restExecuter = restExecuter;
	}

}
