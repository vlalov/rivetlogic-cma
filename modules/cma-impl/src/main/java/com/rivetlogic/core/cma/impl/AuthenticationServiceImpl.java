/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.util.List;
import java.util.Vector;

import org.alfresco.repo.security.authentication.AuthenticationException;
import org.apache.commons.httpclient.Cookie;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.exception.AuthenticationFailure;
import com.rivetlogic.core.cma.repo.Ticket;
import com.rivetlogic.core.cma.rest.api.NameValuePair;
import com.rivetlogic.core.cma.rest.api.RestExecuter;
import com.rivetlogic.core.cma.rest.api.RestExecuter.HttpMethod;

import com.rivetlogic.core.cma.api.AuthenticationService;
import com.rivetlogic.core.cma.api.CmaConstants;
import com.rivetlogic.core.cma.api.SsoTicket;

import com.rivetlogic.core.cma.mapping.CmaResult;
import com.rivetlogic.core.cma.mapping.CmaMappingService;

import com.rivetlogic.core.cma.util.ParameterCheck;

/**
 * 
 * @author Hyanghee Lim
 *
 */
public class AuthenticationServiceImpl implements AuthenticationService {

	private static Log logger = LogFactory
			.getLog(AuthenticationServiceImpl.class);

	/**
	 * authentication service uri template
	 */
	private String serviceUri;

	/**
	 * mapping service
	 */
	private CmaMappingService cmaMappingService;

	/**
	 * RestExecuter
	 */
	private RestExecuter restExecuter;
	

	public Ticket ssoAuthenticate(String repositoryUri, String userName,
			SsoTicket ssoTicket, List<Cookie> cookies)
			throws AuthenticationFailure {
		// check parameters
		ParameterCheck.mandatoryString("repositoryUri", repositoryUri);
		ParameterCheck.mandatoryString("userName", userName);
		ParameterCheck.mandatory("SsoTicket", ssoTicket);
		
		String methodName = CmaConstants.METHOD_AUTHENTICATION_SSOAUTHENTICATE;
		// create parameters 
		Vector<NameValuePair> params = createGeneralParams(null, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USERNAME, userName));
		params.add(new NameValuePair(CmaConstants.PARAM_SSOICKET, ssoTicket));
		params.add(new NameValuePair(CmaConstants.PARAM_USESERIALIZATION, true));

		String targetUri = repositoryUri + serviceUri;
		try {
			CmaResult result = restExecuter.execute(HttpMethod.POST, targetUri, params, cookies);
			Ticket ticket = (Ticket) result.getResult();
			ticket.setRepositoryUri(repositoryUri);
			return ticket;
		} catch (Throwable t) {
			throw new AuthenticationFailure(t);
		}
	}
	
	public Ticket authenticate(String repositoryUri, String userName,
			char[] password) throws AuthenticationFailure {
		// check parameters
		ParameterCheck.mandatoryString("repositoryUri", repositoryUri);
		ParameterCheck.mandatoryString("userName", userName);
		ParameterCheck.mandatory("password", password);
		
		String methodName = CmaConstants.METHOD_AUTHENTICATION_AUTHENTICATE;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(null, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USERNAME, userName));
		params.add(new NameValuePair(CmaConstants.PARAM_PASSWORD, new String(password)));
		params.add(new NameValuePair(CmaConstants.PARAM_USESERIALIZATION, true));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		String targetUri = repositoryUri + serviceUri;
		try {
			CmaResult result = restExecuter.execute(HttpMethod.POST, targetUri, params);
			Ticket ticket = (Ticket) result.getResult();
			ticket.setRepositoryUri(repositoryUri);
			return ticket;
		} catch (Throwable t) {
			throw new AuthenticationFailure(t);
		}
	}

	public void changePassword(Ticket ticket, String userName,
			char [] oldPassword, char [] newPassword)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameter
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("userName", userName);
		ParameterCheck.mandatory("oldPassword", oldPassword);
		ParameterCheck.mandatory("newPassword", newPassword);
		
		// create parameters
		String methodName = CmaConstants.METHOD_AUTHENTICATION_CHANGEPASSWORD;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USERNAME, userName));
		params.add(new NameValuePair(CmaConstants.PARAM_OLDPASSWORD, new String(oldPassword)));
		params.add(new NameValuePair(CmaConstants.PARAM_NEWPASSWORD, new String(newPassword)));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		String targetUri = ticket.getRepositoryUri() + serviceUri;

		try {
			restExecuter.execute(HttpMethod.POST, targetUri, params);
		} catch (AuthenticationException e) {
			// ignore AuthenticationException
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		} 
	}

	public void invalidateTicket(Ticket ticket) throws CmaRuntimeException {
		// check parameter
		ParameterCheck.mandatory("ticket", ticket);
		
		String methodName = CmaConstants.METHOD_AUTHENTICATION_INVALIDATETICKET;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		String targetUri = ticket.getRepositoryUri() + serviceUri;

		try {
			restExecuter.execute(HttpMethod.POST, targetUri, params);
		} catch (AuthenticationException e) {
			// ignore AuthenticationException
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		} 

	}

	public void validate(Ticket ticket) throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		
		String methodName = CmaConstants.METHOD_AUTHENTICATION_VALIDATE;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);

		String targetUri = ticket.getRepositoryUri() + serviceUri;

		try {
			restExecuter.execute(HttpMethod.POST, targetUri, params);
			
		} catch (InvalidTicketException e) {
			throw e;
		} catch (AuthenticationException e) {
			throw new InvalidTicketException(e);
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}

	public void createAuthentication(Ticket ticket, String userName,
			char[] password) throws InvalidTicketException, CmaRuntimeException {
		// check parameter
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("userName", userName);
		ParameterCheck.mandatory("password", password);
		
		// create parameters
		String methodName = CmaConstants.METHOD_AUTHENTICATION_CREATEAUTHENTICATION;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USERNAME, userName));
		params.add(new NameValuePair(CmaConstants.PARAM_PASSWORD, new String(password)));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		String targetUri = ticket.getRepositoryUri() + serviceUri;

		try {
			restExecuter.execute(HttpMethod.POST, targetUri, params);
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		} 
	}

	public void deleteAuthentication(Ticket ticket, String userName)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameter
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("userName", userName);
		
		// create parameters
		String methodName = CmaConstants.METHOD_AUTHENTICATION_DELETEAUTHENTICATION;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USERNAME, userName));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		String targetUri = ticket.getRepositoryUri() + serviceUri;

		try {
			restExecuter.execute(HttpMethod.POST, targetUri, params);
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		} 
	}

	public boolean getAuthenticationEnabled(Ticket ticket, String userName)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameter
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("userName", userName);
		
		// create parameters
		String methodName = CmaConstants.METHOD_AUTHENTICATION_GETAUTHENTICATIONENABLED;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USERNAME, userName));
		String targetUri = ticket.getRepositoryUri() + serviceUri;
		String mappingFile = cmaMappingService.getMapping(
				CmaConstants.CMA_VERSION, CmaConstants.SERVICE_AUTHENTICATION,
				methodName);

		try {
			CmaResult result = restExecuter.execute(HttpMethod.POST, targetUri,
					mappingFile, params);
			return (Boolean) result.getResult();
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		} 
	}

	public void invalidateUserSession(Ticket ticket, String userName)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameter
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("userName", userName);
		
		// create parameters
		String methodName = CmaConstants.METHOD_AUTHENTICATION_INVALIDATEUSERSESSION;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USERNAME, userName));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		String targetUri = ticket.getRepositoryUri() + serviceUri;

		try {
			restExecuter.execute(HttpMethod.POST, targetUri, params);
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		} 
	}

	public void setAuthenticationEnabled(Ticket ticket, String userName,
			boolean enabled) throws InvalidTicketException, CmaRuntimeException {
		// check parameter
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("userName", userName);
		
		// create parameters
		String methodName = CmaConstants.METHOD_AUTHENTICATION_SETAUTHENTICATIONENABLED;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USERNAME, userName));
		params.add(new NameValuePair(CmaConstants.PARAM_ENABLED, String.valueOf(enabled)));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		String targetUri = ticket.getRepositoryUri() + serviceUri;

		try {
			restExecuter.execute(HttpMethod.POST, targetUri, params);
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		} 
	}

	public void setPassword(Ticket ticket, String userName, char[] password)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameter
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("userName", userName);
		ParameterCheck.mandatory("password", password);
		
		// create parameters
		String methodName = CmaConstants.METHOD_AUTHENTICATION_SETPASSWORD;
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USERNAME, userName));
		params.add(new NameValuePair(CmaConstants.PARAM_PASSWORD, new String(password)));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		String targetUri = ticket.getRepositoryUri() + serviceUri;

		try {
			restExecuter.execute(HttpMethod.POST, targetUri, params);
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		} 
	}
	
	/**
	 * create a vector of parameters that contain general parameters
	 * 
	 * @param methodName
	 *         method name
	 * @return a vector that contains general parameters 
	 */
	private Vector<NameValuePair> createGeneralParams(Ticket ticket, String methodName) {
		Vector<NameValuePair> params = new Vector<NameValuePair>();
		params.add(new NameValuePair(CmaConstants.PARAM_VERSION, CmaConstants.CMA_VERSION));
		params.add(new NameValuePair(CmaConstants.PARAM_SERVICE, CmaConstants.SERVICE_AUTHENTICATION));
		params.add(new NameValuePair(CmaConstants.PARAM_METHOD, methodName));
		if (ticket != null) {
			params.add(new NameValuePair(CmaConstants.PARAM_ALFRESCO_TICKET, ticket.getTicket()));
		}
		return params;
	}


	/**
	 * set service uri of web script
	 * @param serviceUri
	 */
	public void setServiceUri(String serviceUri) {
		this.serviceUri = serviceUri;
	}
		
	/**
	 * set mapping service 
	 * @param cmaMappingService
	 */
	public void setCmaMappingService(CmaMappingService cmaMappingService) {
		this.cmaMappingService = cmaMappingService;
	}
	
	/**
	 * set RestExecuter to execute HttpMethod call
	 * @param restExecuter
	 *        RestExecuter
	 */
	public void setRestExecuter(RestExecuter restExecuter) {
		this.restExecuter = restExecuter;
	}


}
