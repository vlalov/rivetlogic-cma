/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionCondition;
import org.alfresco.service.cmr.action.CompositeAction;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rivetlogic.core.cma.api.ActionService;
import com.rivetlogic.core.cma.api.CmaConstants;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.mapping.CmaMappingService;
import com.rivetlogic.core.cma.mapping.CmaResult;
import com.rivetlogic.core.cma.repo.Ticket;
import com.rivetlogic.core.cma.rest.api.NameValuePair;
import com.rivetlogic.core.cma.rest.api.RestExecuter;
import com.rivetlogic.core.cma.rest.api.RestExecuter.HttpMethod;
import com.rivetlogic.core.cma.util.ParameterCheck;
/**
 * 
 * @author Sweta Chalasani
 *
 */
public class ActionServiceImpl implements ActionService{

	private static Log logger = LogFactory.getLog(ActionServiceImpl.class);

	/**
	 * search service uri template
	 */
	private String serviceUri;

	/**
	 * mapping service
	 */
	private CmaMappingService cmaMappingService;

	/**
	 * RestExecuter
	 */
	private RestExecuter restExecuter;

	public Action createAction(Ticket ticket, String name)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("name", name);

		String methodName = CmaConstants.METHOD_ACTION_CREATEACTION;
		// create parameters
		Vector<NameValuePair> params = createGeneralparams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NAME, name));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		return (Action) execute(ticket, methodName, params);
	}

	public Action createAction(Ticket ticket, String name,
			Map<String, Serializable> parameters) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("name", name);

		String methodName = CmaConstants.METHOD_ACTION_CREATEACTIONWITHPARAMS;
		// create parameters
		Vector<NameValuePair> params = createGeneralparams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NAME, name));
		params.add(new NameValuePair(CmaConstants.PARAM_PARAMS, (Serializable)parameters));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		
		return (Action) execute(ticket, methodName, params);
	}

	public ActionCondition createActionCondition(Ticket ticket, String name)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("name", name);

		String methodName = CmaConstants.METHOD_ACTION_CREATEACTIONCONDITION;
		// create parameters
		Vector<NameValuePair> params = createGeneralparams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NAME, name));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		return (ActionCondition) execute(ticket, methodName, params);
	}

	public ActionCondition createActionCondition(Ticket ticket, String name,
			Map<String, Serializable> parameters) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("name", name);

		String methodName = CmaConstants.METHOD_ACTION_CREATEACTIONCONDITIONWITHPARAMS;
		// create parameters
		Vector<NameValuePair> params = createGeneralparams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NAME, name));
		params.add(new NameValuePair(CmaConstants.PARAM_PARAMS, (Serializable)parameters));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		
		return (ActionCondition) execute(ticket, methodName, params);
	}

	public CompositeAction createCompositeAction(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);

		String methodName = CmaConstants.METHOD_ACTION_CREATECOMPOSITEACTION;
		// create parameters
		Vector<NameValuePair> params = createGeneralparams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		return (CompositeAction) execute(ticket, methodName, params);
	}

	public boolean evaluateAction(Ticket ticket, Action action,
			NodeRef actionedUponNodeRef) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("Action", action);
		ParameterCheck.mandatory("NodeRef", actionedUponNodeRef);

		String methodName = CmaConstants.METHOD_ACTION_EVALUATEACTION;
		// create parameters
		Vector<NameValuePair> params = createGeneralparams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_ACTION, (Serializable)action));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, actionedUponNodeRef));

		return ((Boolean) execute(ticket, methodName, params)).booleanValue();
	}

	public boolean evaluateActionCondition(Ticket ticket,
			ActionCondition condition, NodeRef actionedUponNodeRef)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("Condition", condition);
		ParameterCheck.mandatory("NodeRef", actionedUponNodeRef);

		String methodName = CmaConstants.METHOD_ACTION_EVALUATEACTIONCONDITION;
		// create parameters
		Vector<NameValuePair> params = createGeneralparams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_ACTIONCONDITION, (Serializable)condition));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, actionedUponNodeRef));

		return ((Boolean) execute(ticket, methodName, params)).booleanValue();
	}

	public Serializable executeAction(Ticket ticket, Action action,
			NodeRef actionedUponNodeRef) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("Action", action);
		ParameterCheck.mandatory("NodeRef", actionedUponNodeRef);

		String methodName = CmaConstants.METHOD_ACTION_EXECUTEACTION;
		// create parameters
		Vector<NameValuePair> params = createGeneralparams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_ACTION, (Serializable)action));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, actionedUponNodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		return (Serializable) execute(ticket, methodName, params);	
	}

	public Serializable executeAction(Ticket ticket, Action action,
			NodeRef actionedUponNodeRef, boolean checkConditions)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("Action", action);
		ParameterCheck.mandatory("NodeRef", actionedUponNodeRef);

		String methodName = CmaConstants.METHOD_ACTION_EXECUTEACTION;
		// create parameters
		Vector<NameValuePair> params = createGeneralparams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_ACTION, (Serializable)action));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, actionedUponNodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_CHECKCONDITIONS, checkConditions));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		return (Serializable) execute(ticket, methodName, params);	
	}

	public Serializable executeAction(Ticket ticket, Action action,
			NodeRef actionedUponNodeRef, boolean checkConditions,
			boolean executeAsynchronously) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("Action", action);
		ParameterCheck.mandatory("NodeRef", actionedUponNodeRef);

		String methodName = CmaConstants.METHOD_ACTION_EXECUTEACTION;
		// create parameters
		Vector<NameValuePair> params = createGeneralparams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_ACTION, (Serializable)action));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, actionedUponNodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_CHECKCONDITIONS, checkConditions));
		params.add(new NameValuePair(CmaConstants.PARAM_EXECUTEASYNCHRONOUSLY, executeAsynchronously));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		
		return (Serializable) execute(ticket, methodName, params);
	}

	public Action getAction(Ticket ticket, NodeRef nodeRef, String actionId)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("ActionId", actionId);
		ParameterCheck.mandatory("NodeRef", nodeRef);

		String methodName = CmaConstants.METHOD_ACTION_GETACTION;
		// create parameters
		Vector<NameValuePair> params = createGeneralparams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_ACTIONID, actionId));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		
		return (Action) execute(ticket, methodName, params);
	}

	public List<Action> getActions(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);

		String methodName = CmaConstants.METHOD_ACTION_GETACTIONS;
		// create parameters
		Vector<NameValuePair> params = createGeneralparams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		
		return (List<Action>) execute(ticket, methodName, params);
	}

	public void removeAction(Ticket ticket, NodeRef nodeRef, Action action)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("Action", action);
		ParameterCheck.mandatory("NodeRef", nodeRef);

		String methodName = CmaConstants.METHOD_ACTION_REMOVEACTION;
		// create parameters
		Vector<NameValuePair> params = createGeneralparams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_ACTION, (Serializable)action));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		
		execute(ticket, methodName, params);
	}

	public void removeAllActions(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("NodeRef", nodeRef);

		String methodName = CmaConstants.METHOD_ACTION_REMOVEALLACTIONS;
		// create parameters
		Vector<NameValuePair> params = createGeneralparams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		
		execute(ticket, methodName, params);
	}

	public void saveAction(Ticket ticket, NodeRef nodeRef, Action action)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("Action", action);
		ParameterCheck.mandatory("NodeRef", nodeRef);

		String methodName = CmaConstants.METHOD_ACTION_SAVEACTION;
		// create parameters
		Vector<NameValuePair> params = createGeneralparams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_ACTION, (Serializable)action));
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		
		execute(ticket, methodName, params);
	}
	
	/**
	 * create a vector of params that contain general params
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param methodName
	 *         method name
	 * @return a vector that contains general params 
	 */
	private Vector<NameValuePair> createGeneralparams(Ticket ticket, String methodName) {
		Vector<NameValuePair> params = new Vector<NameValuePair>();
		params.add(new NameValuePair(CmaConstants.PARAM_VERSION, CmaConstants.CMA_VERSION));
		params.add(new NameValuePair(CmaConstants.PARAM_SERVICE, CmaConstants.SERVICE_ACTION));
		params.add(new NameValuePair(CmaConstants.PARAM_METHOD, methodName));
		params.add(new NameValuePair(CmaConstants.PARAM_ALFRESCO_TICKET, ticket.getTicket()));
		return params;
	}
	
	/**
	 * execute a method call with the given params
	 * 
	 * @param methodName
	 *            method name
	 * @param params
	 *            params for the method call
	 * @return a result object from the method call
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	private Object execute(Ticket ticket, String methodName, Vector<NameValuePair> params) 
		throws InvalidTicketException, CmaRuntimeException {
		
		String targetUri = ticket.getRepositoryUri() + serviceUri;
		String mappingFile = cmaMappingService.getMapping(CmaConstants.CMA_VERSION, 
										CmaConstants.SERVICE_ACTION, methodName);
		
		try {
			CmaResult result = restExecuter.execute(HttpMethod.POST, targetUri, 
									mappingFile, params);
			return result.getResult();
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}

	/**
	 * set service uri of web script
	 * @param serviceUri
	 */
	public void setServiceUri(String serviceUri) {
		this.serviceUri = serviceUri;
	}
		
	/**
	 * set mapping service 
	 * @param cmaMappingService
	 */
	public void setCmaMappingService(CmaMappingService cmaMappingService) {
		this.cmaMappingService = cmaMappingService;
	}
	
	/**
	 * set RestExecuter to execute HttpMethod call
	 * @param restExecuter
	 *        RestExecuter
	 */
	public void setRestExecuter(RestExecuter restExecuter) {
		this.restExecuter = restExecuter;
	}

}
