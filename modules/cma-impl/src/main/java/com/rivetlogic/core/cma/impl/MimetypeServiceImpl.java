/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rivetlogic.core.cma.api.CmaConstants;
import com.rivetlogic.core.cma.api.MimetypeService;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.mapping.CmaMappingService;
import com.rivetlogic.core.cma.mapping.CmaResult;
import com.rivetlogic.core.cma.repo.Ticket;
import com.rivetlogic.core.cma.rest.api.NameValuePair;
import com.rivetlogic.core.cma.rest.api.RestExecuter;
import com.rivetlogic.core.cma.rest.api.RestExecuter.HttpMethod;
import com.rivetlogic.core.cma.util.ParameterCheck;

public class MimetypeServiceImpl implements MimetypeService {
	
	private static Log logger = LogFactory.getLog(MimetypeServiceImpl.class);
	
	/**
	 * search service uri template
	 */
	private String serviceUri;

	/**
	 * mapping service
	 */
	private CmaMappingService cmaMappingService;

	/**
	 * RestExecuter
	 */
	private RestExecuter restExecuter;

	@SuppressWarnings("unchecked")
	public Map<String, String> getDisplaysByExtension(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		
		String methodName = CmaConstants.METHOD_MIMETYPE_GETDISPLAYSBYEXTENSION;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);

		return (Map<String, String>) execute(ticket, methodName, parameters);
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getDisplaysByMimetype(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		
		String methodName = CmaConstants.METHOD_MIMETYPE_GETDISPLAYSBYMIMETYPE;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);

		return (Map<String, String>) execute(ticket, methodName, parameters);
	}

	public String getExtension(Ticket ticket, String mimetype)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("mimetype", mimetype);
		
		String methodName = CmaConstants.METHOD_MIMETYPE_GETEXTENSION;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_MIMETYPE, mimetype));

		return (String) execute(ticket, methodName, parameters);
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getExtensionsByMimetype(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		
		String methodName = CmaConstants.METHOD_MIMETYPE_GETEXTENSIONSBYMIMETYPE;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);

		return (Map<String, String>) execute(ticket, methodName, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<String> getMimetypes(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		
		String methodName = CmaConstants.METHOD_MIMETYPE_GETMIMETYPES;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);

		return (List<String>) execute(ticket, methodName, parameters);
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getMimetypesByExtension(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		
		String methodName = CmaConstants.METHOD_MIMETYPE_GETMIMETYPESBYEXTENSION;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);

		return (Map<String, String>) execute(ticket, methodName, parameters);
	}

	public String guessMimetype(Ticket ticket, String fileName)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("fileName", fileName);
		
		String methodName = CmaConstants.METHOD_MIMETYPE_GUESSMIMETYPE;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_FILENAME, fileName));

		return (String) execute(ticket, methodName, parameters);
	}

	public boolean isText(Ticket ticket, String mimetype)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("mimetype", mimetype);
		
		String methodName = CmaConstants.METHOD_MIMETYPE_ISTEXT;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_MIMETYPE, mimetype));

		return (Boolean) execute(ticket, methodName, parameters);
	}

	/**
	 * create a vector of parameters that contain general parameters
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param methodName
	 *         method name
	 * @return a vector that contains general parameters 
	 */
	private Vector<NameValuePair> createGeneralParameters(Ticket ticket, String methodName) {
		Vector<NameValuePair> parameters = new Vector<NameValuePair>();
		parameters.add(new NameValuePair(CmaConstants.PARAM_VERSION, CmaConstants.CMA_VERSION));
		parameters.add(new NameValuePair(CmaConstants.PARAM_SERVICE, CmaConstants.SERVICE_MIMETYPE));
		parameters.add(new NameValuePair(CmaConstants.PARAM_METHOD, methodName));
		parameters.add(new NameValuePair(CmaConstants.PARAM_ALFRESCO_TICKET, ticket.getTicket()));
		return parameters;
	}
	
	/**
	 * execute a method call with the given parameters
	 * 
	 * @param methodName
	 *            method name
	 * @param parameters
	 *            parameters for the method call
	 * @return a result object from the method call
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	private Object execute(Ticket ticket, String methodName, Vector<NameValuePair> parameters) 
		throws InvalidTicketException, CmaRuntimeException {
		
		String targetUri = ticket.getRepositoryUri() + serviceUri;
		String mappingFile = cmaMappingService.getMapping(CmaConstants.CMA_VERSION, 
										CmaConstants.SERVICE_MIMETYPE, methodName);
		
		try {
			CmaResult result = restExecuter.execute(HttpMethod.POST, targetUri, 
									mappingFile, parameters);
			return result.getResult();
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}

	/**
	 * set service uri of web script
	 * @param serviceUri
	 */
	public void setServiceUri(String serviceUri) {
		this.serviceUri = serviceUri;
	}
		
	/**
	 * set mapping service 
	 * @param cmaMappingService
	 */
	public void setCmaMappingService(CmaMappingService cmaMappingService) {
		this.cmaMappingService = cmaMappingService;
	}
	
	/**
	 * set RestExecuter to execute HttpMethod call
	 * @param restExecuter
	 *        RestExecuter
	 */
	public void setRestExecuter(RestExecuter restExecuter) {
		this.restExecuter = restExecuter;
	}
}
