/**
 * 
 */
package com.rivetlogic.core.cma.mapping.handler;

import java.util.ArrayList;
import java.util.List;

import org.alfresco.service.cmr.workflow.WorkflowNode;
import org.alfresco.service.cmr.workflow.WorkflowTransition;

import com.rivetlogic.core.cma.mapping.CmaMappingHelper;

/**
 * This class is a helper class for unmarshalling WorkflowNode
 * 
 * @author Sweta Chalasani
 *
 */
public class WorkflowNodeMappingHelper implements CmaMappingHelper {

	private String name;
	private String title;
	private String description;
	private String type;
	private boolean taskNode;
	private List<WorkflowTransitionMappingHelper> transitions;
	
	public WorkflowNodeMappingHelper() {
		transitions = new ArrayList<WorkflowTransitionMappingHelper>();
	}
	
	public Object getContainedObject() {
		WorkflowNode node = new WorkflowNode(name, title, description, type, taskNode, getWorkflowTransitions(transitions));
		return node;
	}
	
	private WorkflowTransition[] getWorkflowTransitions(List<WorkflowTransitionMappingHelper> transitions) {
		WorkflowTransition[] workflowTransitions = new WorkflowTransition[transitions.size()];
			for (int i=0; i< transitions.size(); i++) {
				workflowTransitions[i] = (WorkflowTransition) transitions.get(i).getContainedObject();
			}
		return workflowTransitions;
	}
	
	public void addTransitionsToArray(WorkflowTransitionMappingHelper transition) {
		transitions.add(transition);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isTaskNode() {
		return taskNode;
	}

	public void setTaskNode(boolean taskNode) {
		this.taskNode = taskNode;
	}

	public List<WorkflowTransitionMappingHelper> getTransitions() {
		return transitions;
	}

	public void setTransitions(List<WorkflowTransitionMappingHelper> transitions) {
		this.transitions = transitions;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
