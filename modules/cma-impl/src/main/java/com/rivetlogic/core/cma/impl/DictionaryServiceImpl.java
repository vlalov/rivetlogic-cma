/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Vector;

import org.alfresco.service.cmr.dictionary.AspectDefinition;
import org.alfresco.service.cmr.dictionary.AssociationDefinition;
import org.alfresco.service.cmr.dictionary.ClassDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.dictionary.ModelDefinition;
import org.alfresco.service.cmr.dictionary.PropertyDefinition;
import org.alfresco.service.cmr.dictionary.TypeDefinition;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rivetlogic.core.cma.api.CmaConstants;
import com.rivetlogic.core.cma.api.DictionaryService;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.mapping.CmaResult;
import com.rivetlogic.core.cma.mapping.CmaMappingService;
import com.rivetlogic.core.cma.repo.Ticket;
import com.rivetlogic.core.cma.rest.api.NameValuePair;
import com.rivetlogic.core.cma.rest.api.RestExecuter;
import com.rivetlogic.core.cma.rest.api.RestExecuter.HttpMethod;
import com.rivetlogic.core.cma.util.ParameterCheck;

/**
 * 
 * @author Hyanghee Lim
 *
 */
public class DictionaryServiceImpl implements DictionaryService {

	private static Log logger = LogFactory.getLog(DictionaryServiceImpl.class);

	/**
	 * search service uri template
	 */
	private String serviceUri;

	/**
	 * mapping service
	 */
	private CmaMappingService cmaMappingService;

	/**
	 * RestExecuter
	 */
	private RestExecuter restExecuter;

	public Collection<QName> getAllAspects(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);

		String methodName = CmaConstants.METHOD_DICTIONARY_GETALLASPECTS;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);

		return (Collection<QName>) execute(ticket, methodName, params);
	}

	public Collection<QName> getAllDataTypes(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);

		String methodName = CmaConstants.METHOD_DICTIONARY_GETALLDATATYPES;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);

		return (Collection<QName>) execute(ticket, methodName, params);
	}

	public Collection<QName> getAllProperties(Ticket ticket, QName dataType)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);

		String methodName = CmaConstants.METHOD_DICTIONARY_GETALLPROPERTIES;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);

		if (dataType != null) {
			params
					.add(new NameValuePair(CmaConstants.PARAM_DATATYPE,
							dataType));
		}

		return (Collection<QName>) execute(ticket, methodName, params);
	}

	public Collection<QName> getAllTypes(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);

		String methodName = CmaConstants.METHOD_DICTIONARY_GETALLTYPES;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);

		return (Collection<QName>) execute(ticket, methodName, params);
	}

	public Collection<QName> getAspects(Ticket ticket, QName model)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("model", model);

		String methodName = CmaConstants.METHOD_DICTIONARY_GETASPECTS;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_MODEL, model));

		return (Collection<QName>) execute(ticket, methodName, params);
	}

	public Collection<QName> getDataTypes(Ticket ticket, QName model)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("model", model);

		String methodName = CmaConstants.METHOD_DICTIONARY_GETDATATYPES;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_MODEL, model));

		return (Collection<QName>) execute(ticket, methodName, params);
	}

	public Collection<QName> getProperties(Ticket ticket, QName model,
			QName dataType) throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("model", model);

		String methodName = CmaConstants.METHOD_DICTIONARY_GETPROPERTIES;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_MODEL, model));

		if (dataType != null) {
			params
					.add(new NameValuePair(CmaConstants.PARAM_DATATYPE,
							dataType));
		}

		return (Collection<QName>) execute(ticket, methodName, params);
	}

	public Collection<QName> getProperties(Ticket ticket, QName model)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("model", model);

		String methodName = CmaConstants.METHOD_DICTIONARY_GETPROPERTIES;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_MODEL, model));

		return (Collection<QName>) execute(ticket, methodName, params);
	}

	public Collection<QName> getTypes(Ticket ticket, QName model)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("model", model);

		String methodName = CmaConstants.METHOD_DICTIONARY_GETTYPES;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_MODEL, model));

		return (Collection<QName>) execute(ticket, methodName, params);
	}

	public boolean isSubClass(Ticket ticket, QName className, QName ofClassName)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("className", className);
		ParameterCheck.mandatory("ofClassName", ofClassName);

		String methodName = CmaConstants.METHOD_DICTIONARY_ISSUBCLASS;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_CLASSNAME, className));
		params.add(new NameValuePair(CmaConstants.PARAM_OFCLASSNAME,
				ofClassName));

		return ((Boolean) execute(ticket, methodName, params)).booleanValue();
	}

	public Collection<QName> getAllModels(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		String methodName = CmaConstants.METHOD_DICTIONARY_GETALLMODELS;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);

		return ((Collection<QName>) execute(ticket, methodName, params));
	}


	public TypeDefinition getAnonymousType(Ticket ticket, QName type,
			Collection<QName> aspects) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("type", type);
		ParameterCheck.mandatory("aspects", aspects);
		
		String methodName = CmaConstants.METHOD_DICTIONARY_GETANONYMOUSTYPE;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_TYPE, type));
		params.add(new NameValuePair(CmaConstants.PARAM_ASPECTS, (Serializable)aspects));

		return (TypeDefinition) execute(ticket, methodName, params);
	}

	public AspectDefinition getAspect(Ticket ticket, QName name)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("QName", name);
		
		String methodName = CmaConstants.METHOD_DICTIONARY_GETASPECT;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NAME, name));

		return (AspectDefinition) execute(ticket, methodName, params);
	}

	public AssociationDefinition getAssociation(Ticket ticket,
			QName associationName) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("associationQName", associationName);
		
		String methodName = CmaConstants.METHOD_DICTIONARY_GETASSOCIATION;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_ASSOCQNAME, associationName));

		return (AssociationDefinition) execute(ticket, methodName, params);
	}

	public ClassDefinition getClass(Ticket ticket, QName name)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("QName", name);
		
		String methodName = CmaConstants.METHOD_DICTIONARY_GETCLASS;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NAME, name));

		return (ClassDefinition) execute(ticket, methodName, params);
	}

	public DataTypeDefinition getDataType(Ticket ticket, QName name)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("QName", name);
		
		String methodName = CmaConstants.METHOD_DICTIONARY_GETDATATYPEBYQNAME;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NAME, name));

		return (DataTypeDefinition) execute(ticket, methodName, params);
	}

	public DataTypeDefinition getDataType(Ticket ticket, Class javaClass)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("class", javaClass);
		
		String methodName = CmaConstants.METHOD_DICTIONARY_GETDATATYPEBYCLASS;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_JAVACLASS, javaClass));

		return (DataTypeDefinition) execute(ticket, methodName, params);
	}

	public ModelDefinition getModel(Ticket ticket, QName model)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("model", model);
		
		String methodName = CmaConstants.METHOD_DICTIONARY_GETMODEL;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_MODEL, model));

		return (ModelDefinition) execute(ticket, methodName, params);
	}

	public PropertyDefinition getProperty(Ticket ticket, QName className,
			QName propertyName) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("className", className);
		ParameterCheck.mandatory("propertyName", propertyName);
		
		String methodName = CmaConstants.METHOD_DICTIONARY_GETPROPERTY;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_CLASSNAME, className));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTYQNAME, propertyName));

		return (PropertyDefinition) execute(ticket, methodName, params);
	}

	public PropertyDefinition getProperty(Ticket ticket, QName propertyName)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("propertyName", propertyName);
		
		String methodName = CmaConstants.METHOD_DICTIONARY_GETPROPERTY;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTYQNAME, propertyName));

		return (PropertyDefinition) execute(ticket, methodName, params);
	}

	public TypeDefinition getType(Ticket ticket, QName name)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("QName", name);
		
		String methodName = CmaConstants.METHOD_DICTIONARY_GETTYPE;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NAME, name));

		return (TypeDefinition) execute(ticket, methodName, params);
	}

	/**
	 * create a vector of parameters that contain general parameters
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param methodName
	 *            method name
	 * @return a vector that contains general parameters
	 */
	private Vector<NameValuePair> createGeneralParams(Ticket ticket,
			String methodName) {
		Vector<NameValuePair> params = new Vector<NameValuePair>();
		params.add(new NameValuePair(CmaConstants.PARAM_VERSION,
				CmaConstants.CMA_VERSION));
		params.add(new NameValuePair(CmaConstants.PARAM_SERVICE,
				CmaConstants.SERVICE_DICTIONARY));
		params.add(new NameValuePair(CmaConstants.PARAM_METHOD, methodName));
		params.add(new NameValuePair(CmaConstants.PARAM_ALFRESCO_TICKET, ticket
				.getTicket()));
		return params;
	}

	/**
	 * execute a method call with the given parameters
	 * 
	 * @param methodName
	 *            method name
	 * @param params
	 *            parameters for the method call
	 * @return a result object from the method call
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	private Object execute(Ticket ticket, String methodName,
			Vector<NameValuePair> params) throws InvalidTicketException,
			CmaRuntimeException {

		String targetUri = ticket.getRepositoryUri() + serviceUri;
		String mappingFile = cmaMappingService.getMapping(
				CmaConstants.CMA_VERSION, CmaConstants.SERVICE_DICTIONARY,
				methodName);

		try {
			CmaResult result = restExecuter.execute(HttpMethod.POST, targetUri,
					mappingFile, params);
			return result.getResult();
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}

	/**
	 * set service uri of web script
	 * 
	 * @param serviceUri
	 */
	public void setServiceUri(String serviceUri) {
		this.serviceUri = serviceUri;
	}

	/**
	 * set mapping service
	 * 
	 * @param cmaMappingService
	 */
	public void setCmaMappingService(CmaMappingService cmaMappingService) {
		this.cmaMappingService = cmaMappingService;
	}

	/**
	 * set RestExecuter to execute HttpMethod call
	 * 
	 * @param restExecuter
	 *            RestExecuter
	 */
	public void setRestExecuter(RestExecuter restExecuter) {
		this.restExecuter = restExecuter;
	}

}