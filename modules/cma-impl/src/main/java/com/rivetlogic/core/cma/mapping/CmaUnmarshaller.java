/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping;

import java.io.InputStream;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;

public interface CmaUnmarshaller {

	/**
	 * unmarshal an object from InputStream
	 * 
	 * @param input
	 * 			InputStream
	 * @param mappingFile
	 *          String
	 * @return Object
	 * @throws CmaRuntimeException
	 */
	public Object unmarshal(InputStream input, String mappingFile) throws CmaRuntimeException;
	
	
}