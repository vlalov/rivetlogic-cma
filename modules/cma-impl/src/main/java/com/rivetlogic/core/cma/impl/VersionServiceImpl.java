/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.repository.AspectMissingException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.version.ReservedVersionNameException;
import org.alfresco.service.cmr.version.Version;
import org.alfresco.service.cmr.version.VersionHistory;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rivetlogic.core.cma.api.CmaConstants;
import com.rivetlogic.core.cma.api.VersionService;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.mapping.CmaMappingService;
import com.rivetlogic.core.cma.mapping.CmaResult;
import com.rivetlogic.core.cma.repo.Ticket;
import com.rivetlogic.core.cma.rest.api.NameValuePair;
import com.rivetlogic.core.cma.rest.api.RestExecuter;
import com.rivetlogic.core.cma.rest.api.RestExecuter.HttpMethod;
import com.rivetlogic.core.cma.util.ParameterCheck;

/**
 * @author Sweta Chalasani
 *
 */
public class VersionServiceImpl implements VersionService{

	private static Log logger = LogFactory.getLog(VersionServiceImpl.class);

	/**
	 * search service uri template
	 */
	private String serviceUri;

	/**
	 * mapping service
	 */
	private CmaMappingService cmaMappingService;

	/**
	 * RestExecuter
	 */
	private RestExecuter restExecuter;
	
	public Version createVersion(Ticket ticket, NodeRef nodeRef,
			Map<String, Serializable> versionProperties)
			throws InvalidTicketException, CmaRuntimeException,
			ReservedVersionNameException, AspectMissingException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);

		String methodName = CmaConstants.METHOD_VERSION_CREATEVERSION;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTIES, (Serializable) versionProperties));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		
		return (Version) execute(ticket, methodName, params);
	}

	public Collection<Version> createVersion(Ticket ticket, NodeRef nodeRef,
			Map<String, Serializable> versionProperties, boolean versionChildren)
			throws InvalidTicketException, CmaRuntimeException,
			ReservedVersionNameException, AspectMissingException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);

		String methodName = CmaConstants.METHOD_VERSION_CREATEVERSIONWITHVERSIONCHILDREN;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_VERSIONCHILDREN, versionChildren));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTIES, (Serializable) versionProperties));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		
		return (Collection<Version>) execute(ticket, methodName, params);
	}

	public Collection<Version> createVersion(Ticket ticket,
			Collection<NodeRef> nodeRefs,
			Map<String, Serializable> versionProperties)
			throws InvalidTicketException, CmaRuntimeException,
			ReservedVersionNameException, AspectMissingException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRefs);

		String methodName = CmaConstants.METHOD_VERSION_CREATEVERSIONWITHNODEREFS;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, (Serializable) nodeRefs));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTIES, (Serializable) versionProperties));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		
		return (Collection<Version>) execute(ticket, methodName, params);
	}

	public void deleteVersionHistory(Ticket ticket, NodeRef nodeRef)
			throws AspectMissingException, InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);

		String methodName = CmaConstants.METHOD_VERSION_DELETEVERSIONHISTORY;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
	     
		try {
		execute(ticket, methodName, params);
		} catch (ReservedVersionNameException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public Version getCurrentVersion(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);

		String methodName = CmaConstants.METHOD_VERSION_GETCURRENTVERSION;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
	     
		try {
			return (Version) execute(ticket, methodName, params);
		} catch (AspectMissingException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (ReservedVersionNameException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public VersionHistory getVersionHistory(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, CmaRuntimeException,
			AspectMissingException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);

		String methodName = CmaConstants.METHOD_VERSION_GETVERSIONHISTORY;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
	     
		try {
			return (VersionHistory) execute(ticket, methodName, params);
		} catch (ReservedVersionNameException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public StoreRef getVersionStoreReference(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);

		String methodName = CmaConstants.METHOD_VERSION_GETVERSIONSTOREREFERENCE;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
	     
		try {
			return (StoreRef) execute(ticket, methodName, params);
		} catch (AspectMissingException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (ReservedVersionNameException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public NodeRef restore(Ticket ticket, NodeRef nodeRef,
			NodeRef parentNodeRef, QName assocTypeQName, QName assocQName)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		ParameterCheck.mandatory("parentNodeRef", parentNodeRef);

		String methodName = CmaConstants.METHOD_VERSION_RESTORE;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, parentNodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_ASSOCTYPEQNAME, assocTypeQName));
		params.add(new NameValuePair(CmaConstants.PARAM_ASSOCQNAME, assocQName));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
	     
		try {
			return (NodeRef) execute(ticket, methodName, params);
		} catch (AspectMissingException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (ReservedVersionNameException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public NodeRef restore(Ticket ticket, NodeRef nodeRef,
			NodeRef parentNodeRef, QName assocTypeQName, QName assocQName,
			boolean deep) throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		ParameterCheck.mandatory("parentNodeRef", parentNodeRef);

		String methodName = CmaConstants.METHOD_VERSION_RESTOREDEEP;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, parentNodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_ASSOCTYPEQNAME, assocTypeQName));
		params.add(new NameValuePair(CmaConstants.PARAM_ASSOCQNAME, assocQName));
		params.add(new NameValuePair(CmaConstants.PARAM_DEEP, deep));
	     
		try {
			return (NodeRef) execute(ticket, methodName, params);
		} catch (AspectMissingException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (ReservedVersionNameException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public void revert(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);

		String methodName = CmaConstants.METHOD_VERSION_REVERT;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
	   
		try {
			execute(ticket, methodName, params);
		} catch (AspectMissingException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (ReservedVersionNameException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public void revert(Ticket ticket, NodeRef nodeRef, boolean deep)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);

		String methodName = CmaConstants.METHOD_VERSION_REVERTDEEP;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_DEEP, deep));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
	     
		try {
			execute(ticket, methodName, params);
		} catch (AspectMissingException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (ReservedVersionNameException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public void revert(Ticket ticket, NodeRef nodeRef, Version version)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);

		String methodName = CmaConstants.METHOD_VERSION_REVERTTOVERSION;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_VERSIONSERVICEVERSION, version)); 
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		
		try {
			execute(ticket, methodName, params);
		} catch (AspectMissingException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (ReservedVersionNameException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public void revert(Ticket ticket, NodeRef nodeRef, Version version,
			boolean deep) throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);

		String methodName = CmaConstants.METHOD_VERSION_REVERTTOVERSIONDEEP;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_VERSIONSERVICEVERSION, version)); 
		params.add(new NameValuePair(CmaConstants.PARAM_DEEP, deep));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		
		try {
			execute(ticket, methodName, params);
		} catch (AspectMissingException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (ReservedVersionNameException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	/**
	 * create a vector of params that contain general params
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param methodName
	 *         method name
	 * @return a vector that contains general params 
	 */
	private Vector<NameValuePair> createGeneralParams(Ticket ticket, String methodName) {
		Vector<NameValuePair> params = new Vector<NameValuePair>();
		params.add(new NameValuePair(CmaConstants.PARAM_VERSION, CmaConstants.CMA_VERSION));
		params.add(new NameValuePair(CmaConstants.PARAM_SERVICE, CmaConstants.SERVICE_VERSION));
		params.add(new NameValuePair(CmaConstants.PARAM_METHOD, methodName));
		params.add(new NameValuePair(CmaConstants.PARAM_ALFRESCO_TICKET, ticket.getTicket()));
		return params;
	}
	
	/**
	 * execute a method call with the given params
	 * 
	 * @param methodName
	 *            method name
	 * @param params
	 *            params for the method call
	 * @return a result object from the method call
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	private Object execute(Ticket ticket, String methodName, Vector<NameValuePair> params) 
		throws InvalidTicketException, CmaRuntimeException, ReservedVersionNameException, AspectMissingException {
		
		String targetUri = ticket.getRepositoryUri() + serviceUri;
		String mappingFile = cmaMappingService.getMapping(CmaConstants.CMA_VERSION, 
										CmaConstants.SERVICE_VERSION, methodName);
		
		try {
			CmaResult result = restExecuter.execute(HttpMethod.POST, targetUri, 
									mappingFile, params);
			return result.getResult();
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (AspectMissingException e) {
			throw e;
		} catch (ReservedVersionNameException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}

	/**
	 * set service uri of web script
	 * @param serviceUri
	 */
	public void setServiceUri(String serviceUri) {
		this.serviceUri = serviceUri;
	}
		
	/**
	 * set mapping service 
	 * @param cmaMappingService
	 */
	public void setCmaMappingService(CmaMappingService cmaMappingService) {
		this.cmaMappingService = cmaMappingService;
	}
	
	/**
	 * set RestExecuter to execute HttpMethod call
	 * @param restExecuter
	 *        RestExecuter
	 */
	public void setRestExecuter(RestExecuter restExecuter) {
		this.restExecuter = restExecuter;
	}
}
