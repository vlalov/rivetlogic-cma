/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.util.Vector;

import org.alfresco.service.cmr.lock.NodeLockedException;
import org.alfresco.service.cmr.repository.AspectMissingException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rivetlogic.core.cma.api.CmaConstants;
import com.rivetlogic.core.cma.api.LibraryService;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.mapping.CmaMappingService;
import com.rivetlogic.core.cma.mapping.CmaResult;
import com.rivetlogic.core.cma.repo.Ticket;
import com.rivetlogic.core.cma.rest.api.NameValuePair;
import com.rivetlogic.core.cma.rest.api.RestExecuter;
import com.rivetlogic.core.cma.rest.api.RestExecuter.HttpMethod;
import com.rivetlogic.core.cma.util.ParameterCheck;

/**
 * 
 * @author Hyanghee Lim
 *
 */
public class LibraryServiceImpl implements LibraryService {

	private static Log logger = LogFactory.getLog(LibraryServiceImpl.class);

	/**
	 * library service uri template
	 */
	private String serviceUri;

	/**
	 * mapping service
	 */
	private CmaMappingService cmaMappingService;

	/**
	 * RestExecuter
	 */
	private RestExecuter restExecuter;

	public NodeRef checkOut(Ticket ticket, NodeRef nodeRef, NodeRef destinationNodeRef)
			throws InvalidTicketException, NodeLockedException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		
		String methodName = CmaConstants.METHOD_LIBRARY_CHECKOUT;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		if (destinationNodeRef != null) {
			params.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, destinationNodeRef));
		}

		try {
			return (NodeRef) execute(ticket, methodName, params);
		} catch (AspectMissingException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public NodeRef checkIn(Ticket ticket, NodeRef nodeRef,
			String revisionHistory, boolean majorVersion)
			throws InvalidTicketException, AspectMissingException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		
		String methodName = CmaConstants.METHOD_LIBRARY_CHECKIN;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_REVISIONHISTORY, revisionHistory));
		params.add(new NameValuePair(CmaConstants.PARAM_MAJORVERSION, majorVersion));

		try {
			return (NodeRef) execute(ticket, methodName, params);
		} catch (NodeLockedException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public NodeRef cancelCheckOut(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, AspectMissingException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		
		String methodName = CmaConstants.METHOD_LIBRARY_CANCELCHECKOUT;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));

		try {
			return (NodeRef) execute(ticket, methodName, params);
		} catch (NodeLockedException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}
	
	/**
	 * create a vector of parameters that contain general parameters
	 * 
	 * @param methodName
	 *         method name
	 * @return a vector that contains general parameters 
	 */
	private Vector<NameValuePair> createGeneralParams(Ticket ticket, String methodName) {
		Vector<NameValuePair> params = new Vector<NameValuePair>();
		params.add(new NameValuePair(CmaConstants.PARAM_VERSION, CmaConstants.CMA_VERSION));
		params.add(new NameValuePair(CmaConstants.PARAM_SERVICE, CmaConstants.SERVICE_LIBRARY));
		params.add(new NameValuePair(CmaConstants.PARAM_METHOD, methodName));
		params.add(new NameValuePair(CmaConstants.PARAM_ALFRESCO_TICKET, ticket.getTicket()));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		return params;
	}
	
	/**
	 * execute a method call with the given parameters
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param methodName
	 *            method name
	 * @param params
	 *            parameters for the method call
	 * @return a result object from the method call
	 * @throws InvalidTicketException
	 * @throws ContentLockedException
	 * @throws ContentNotLockedException
	 * @throws CmaRuntimeException
	 */
	private Object execute(Ticket ticket, String methodName, Vector<NameValuePair> params) 
		throws InvalidTicketException, NodeLockedException, AspectMissingException, CmaRuntimeException {
		
		String targetUri = ticket.getRepositoryUri() + serviceUri;
		String mappingFile = cmaMappingService.getMapping(CmaConstants.CMA_VERSION, 
										CmaConstants.SERVICE_LIBRARY, methodName);
		
		try {
			CmaResult result = restExecuter.execute(HttpMethod.POST, targetUri, 
										mappingFile, params);
			return result.getResult();
		} catch (InvalidTicketException e) {
			throw e;
		} catch (NodeLockedException e) {
			throw e;
		} catch (AspectMissingException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}


	/**
	 * set service uri of web script
	 * 
	 * @param serviceUri
	 */
	public void setServiceUri(String serviceUri) {
		this.serviceUri = serviceUri;
	}

	/**
	 * set mapping service
	 * 
	 * @param cmaMappingService
	 */
	public void setCmaMappingService(CmaMappingService cmaMappingService) {
		this.cmaMappingService = cmaMappingService;
	}

	/**
	 * set RestExecuter to execute HttpMethod call
	 * 
	 * @param restExecuter
	 *            RestExecuter
	 */
	public void setRestExecuter(RestExecuter restExecuter) {
		this.restExecuter = restExecuter;
	}

}
