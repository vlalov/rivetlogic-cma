/**
 * 
 */
package com.rivetlogic.core.cma.mapping.handler;

import org.alfresco.service.cmr.workflow.WorkflowNode;
import org.alfresco.service.cmr.workflow.WorkflowTaskDefinition;

import com.rivetlogic.core.cma.mapping.CmaMappingHelper;

/**
 * This class is a helper class for unmarshalling WorkflowTaskDefinition
 *
 * @author Sweta Chalasani
 *
 */
public class WorkflowTaskDefinitionMappingHelper implements CmaMappingHelper {

	private String id;
	private org.alfresco.service.cmr.dictionary.TypeDefinition metadata;
	private WorkflowNodeMappingHelper node;
	
	public Object getContainedObject() {
		WorkflowTaskDefinition workflowTaskDefinition = new WorkflowTaskDefinition(id, (WorkflowNode) node.getContainedObject(), metadata);
		return workflowTaskDefinition;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public org.alfresco.service.cmr.dictionary.TypeDefinition getMetadata() {
		return metadata;
	}

	public void setMetadata(
			org.alfresco.service.cmr.dictionary.TypeDefinition metadata) {
		this.metadata = metadata;
	}

	public WorkflowNodeMappingHelper getNode() {
		return node;
	}

	public void setNode(WorkflowNodeMappingHelper node) {
		this.node = node;
	}

}
