/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping;

import java.util.Map;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rivetlogic.core.cma.api.CmaConstants;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;

/**
 * CmaMapping
 * 
 * This class holds CMA mapping configuration
 * @author Hyanghee Lim
 *
 */
public class CmaMapping {
	
	private static Log logger = LogFactory.getLog(CmaMapping.class);

	/**
	 * the root path of all mapping files
	 */
	private String rootPath;
	
	/**
	 * default error mapping file
	 */
	private String errorMapping;
	
	/**
	 * mapping service map <version, MethodMapping>
	 */
	private Map<String, MethodMapping> serviceMap;

	public CmaMapping() {
		serviceMap = new HashMap<String, MethodMapping>(CmaConstants.DEFAULT_NUMBER_OF_SERVICES);
	}

	/**
	 * find a mapping file name by a given service name and a method name
	 * 
	 * @param serviceName
	 *            CMA service name
	 * @param methodName
	 *            method name
	 * @return a mapping file name
	 */
	public String getMapping(String serviceName, String methodName) {
		
		MethodMapping methodMapping = serviceMap.get(serviceName);
		
		if (methodMapping != null) {
			return rootPath + "/" + methodMapping.getMapping(methodName);
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("methodMapping is not found for " + serviceName);
			}
			return null;
		}
	}

	public String getErrorMapping() {
		return errorMapping;
	}

	public Map<String, MethodMapping> getServiceMap() {
		return serviceMap;
	}

	public void setErrorMapping(String errorMapping) {
		this.errorMapping = errorMapping;
	}
	
	public void setServiceMap(Map<String, MethodMapping> serviceMap) {
		this.serviceMap = serviceMap;
	}

	public String getRootPath() {
		return rootPath;
	}

	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}
}

