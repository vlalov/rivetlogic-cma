/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping;

/**
 * CmaMappingHelper
 * 
 * This class is used for the data types that cannot be directly unmarshalled.
 * (e.g. WorkflowDefinition)
 * 
 * @author Hyanghee Lim
 * 
 */
public interface CmaMappingHelper {

	/**
	 * Get an object the helper class contains
	 * 
	 * @return an object the helper class contains
	 */
	public Object getContainedObject();
}
