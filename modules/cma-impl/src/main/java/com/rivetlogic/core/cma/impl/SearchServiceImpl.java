/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;

import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rivetlogic.core.cma.rest.api.NameValuePair;
import com.rivetlogic.core.cma.rest.api.RestExecuter;
import com.rivetlogic.core.cma.rest.api.RestExecuter.HttpMethod;

import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;

import com.rivetlogic.core.cma.api.CmaConstants;
import com.rivetlogic.core.cma.api.SearchService;

import com.rivetlogic.core.cma.repo.Node;
import com.rivetlogic.core.cma.repo.SortDefinition;
import com.rivetlogic.core.cma.repo.Ticket;
import com.rivetlogic.core.cma.util.ParameterCheck;

import com.rivetlogic.core.cma.mapping.CmaResult;
import com.rivetlogic.core.cma.mapping.CmaMappingService;

/**
 * 
 * @author Hyanghee Lim
 *
 */
public class SearchServiceImpl implements SearchService {

	private static Log logger = LogFactory.getLog(SearchServiceImpl.class);
	
	/**
	 * search service uri template
	 */
	private String serviceUri;

	/**
	 * mapping service
	 */
	private CmaMappingService cmaMappingService;
	
	/**
	 * RestExecuter
	 */
	private RestExecuter restExecuter;
	
	@Deprecated
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			List<QName> requiredProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects, 
			boolean returnCurrentUserPermissions, int maxResults)
			throws InvalidTicketException, CmaRuntimeException {
		return query(ticket, store, queryLanguage, query, requiredProperties,
				returnPeerAssocs, returnChildAssocs, returnAspects, 
				returnCurrentUserPermissions,
				maxResults, (List<SortDefinition>)null);
	}

	@Deprecated
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			List<QName> requiredProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects, 
			boolean returnCurrentUserPermissions, int startResult,
			int endResult) throws InvalidTicketException, CmaRuntimeException {
		return query(ticket, store, queryLanguage, query, requiredProperties,
				returnPeerAssocs, returnChildAssocs, returnAspects, 
				returnCurrentUserPermissions,
				startResult, endResult, (List<SortDefinition>)null);
	}

	@Deprecated
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			boolean returnAllProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects, 
			boolean returnCurrentUserPermissions, int maxResults)
			throws InvalidTicketException, CmaRuntimeException {
		return query(ticket, store, queryLanguage, query, returnAllProperties,
				returnPeerAssocs, returnChildAssocs, returnAspects, 
				returnCurrentUserPermissions, maxResults, (List<SortDefinition>)null);
	}

	@Deprecated
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			boolean returnAllProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects, 
			boolean returnCurrentUserPermissions, int startResult,
			int endResult) throws InvalidTicketException, CmaRuntimeException {
		return query(ticket, store, queryLanguage, query, returnAllProperties,
				returnPeerAssocs, returnChildAssocs, returnAspects, 
				returnCurrentUserPermissions, 
				startResult, endResult, (List<SortDefinition>)null);
	}

	@Deprecated
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			List<QName> requiredProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects, 
			boolean returnCurrentUserPermissions, int maxResults,
			List<SortDefinition> sortDefinitions)
			throws InvalidTicketException, CmaRuntimeException {
		return query(ticket, store, queryLanguage, query, requiredProperties, 
				returnPeerAssocs, returnChildAssocs, returnAspects, 
				returnCurrentUserPermissions, null,
				maxResults, sortDefinitions);
	}

	@Deprecated
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,  
			List<QName> requiredProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects, 
			boolean returnCurrentUserPermissions, int startResult,
			int endResult, List<SortDefinition> sortDefinitions) 
			throws InvalidTicketException, CmaRuntimeException {
		return query(ticket, store, queryLanguage, query, requiredProperties, 
				returnPeerAssocs, returnChildAssocs, returnAspects, 
				returnCurrentUserPermissions, null,
				startResult, endResult, sortDefinitions);
	}
	
	@Deprecated
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,  
			boolean returnAllProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects, 
			boolean returnCurrentUserPermissions, int maxResults,
			List<SortDefinition> sortDefinitions)
			throws InvalidTicketException, CmaRuntimeException {
		return query(ticket, store, queryLanguage, query, returnAllProperties, 
				returnPeerAssocs, returnChildAssocs, returnAspects, 
				returnCurrentUserPermissions, null, 
				maxResults, sortDefinitions);
	}

	@Deprecated
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query, 
			boolean returnAllProperties, boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects, 
			boolean returnCurrentUserPermissions, int startResult,
			int endResult, List<SortDefinition> sortDefinitions)
			throws InvalidTicketException, CmaRuntimeException {
		return query(ticket, store, queryLanguage, query, returnAllProperties, 
				returnPeerAssocs, returnChildAssocs, returnAspects, 
				returnCurrentUserPermissions, null, 
				startResult, endResult, sortDefinitions);
	}
	
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,
			List<QName> requiredProperties, 
			boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects, 
			boolean returnCurrentUserPermissions, 
			List<String> permissionsToCheckFor,
			int maxResults, List<SortDefinition> sortDefinitions)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("store", store);
		ParameterCheck.mandatory("queryLanguage", queryLanguage);
		ParameterCheck.mandatoryString("query", query);
		ParameterCheck.mandatoryCollection("requiredProperties", requiredProperties);

		String methodName = CmaConstants.METHOD_SEARCH_QUERY;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);
		// create parameters of search
		params.add(new NameValuePair(CmaConstants.PARAM_STORE, store));
		params.add(new NameValuePair(CmaConstants.PARAM_QUERY_LANGUAGE, 
					queryLanguage.toString()));
		params.add(new NameValuePair(CmaConstants.PARAM_QUERY, query));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTIES, 
					(Serializable)requiredProperties));
		params.add(new NameValuePair(CmaConstants.PARAM_PERMISSIONSTOCHECKFOR, 
					(Serializable)permissionsToCheckFor));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNPEERASSOCS, returnPeerAssocs));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNCHILDASSOCS, returnChildAssocs));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNASPECTS, returnAspects));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNPERMISSIONS, 
					returnCurrentUserPermissions));
		params.add(new NameValuePair(CmaConstants.PARAM_STARTRESULT, 0));
		params.add(new NameValuePair(CmaConstants.PARAM_ENDRESULT, maxResults));
		params.add(new NameValuePair(CmaConstants.PARAM_USESERIALIZATION, true));
		// add sort defintions
		if (sortDefinitions != null) {
			params.add(new NameValuePair(CmaConstants.PARAM_SORTDEFINITIONS, 
					(Serializable)sortDefinitions));
		}
		
		return (List<Node>) execute(ticket, methodName, params);
	}

	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,  
			List<QName> requiredProperties, 
			boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects, 
			boolean returnCurrentUserPermissions, 
			List<String> permissionsToCheckFor,
			int startResult, int endResult, List<SortDefinition> sortDefinitions) 
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("store", store);
		ParameterCheck.mandatory("queryLanguage", queryLanguage);
		ParameterCheck.mandatoryString("query", query);
		ParameterCheck.mandatoryCollection("requiredProperties", requiredProperties);
				
		String methodName = CmaConstants.METHOD_SEARCH_QUERY;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);
		// create parameters of search
		params.add(new NameValuePair(CmaConstants.PARAM_STORE, store));
		params.add(new NameValuePair(CmaConstants.PARAM_QUERY_LANGUAGE, 
					queryLanguage.toString()));
		params.add(new NameValuePair(CmaConstants.PARAM_QUERY, query));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTIES, 
					(Serializable)requiredProperties));
		params.add(new NameValuePair(CmaConstants.PARAM_PERMISSIONSTOCHECKFOR, 
					(Serializable)permissionsToCheckFor));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNPEERASSOCS, returnPeerAssocs));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNCHILDASSOCS, returnChildAssocs));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNASPECTS, returnAspects));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNPERMISSIONS, 
					returnCurrentUserPermissions));
		params.add(new NameValuePair(CmaConstants.PARAM_STARTRESULT, startResult));
		params.add(new NameValuePair(CmaConstants.PARAM_ENDRESULT, endResult));
		params.add(new NameValuePair(CmaConstants.PARAM_USESERIALIZATION, true));
		// add sort defintions
		if (sortDefinitions != null) {
			params.add(new NameValuePair(CmaConstants.PARAM_SORTDEFINITIONS, (Serializable)sortDefinitions));
		}
		
		return (List<Node>) execute(ticket, methodName, params);
	}
	
	
	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query,  
			boolean returnAllProperties, 
			boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects, 
			boolean returnCurrentUserPermissions, 
			List<String> permissionsToCheckFor,
			int maxResults, List<SortDefinition> sortDefinitions)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("store", store);
		ParameterCheck.mandatory("queryLanguage", queryLanguage);
		ParameterCheck.mandatoryString("query", query);
		
		String methodName = CmaConstants.METHOD_SEARCH_QUERY;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);
		// create parameters of search
		params.add(new NameValuePair(CmaConstants.PARAM_STORE, store));
		params.add(new NameValuePair(CmaConstants.PARAM_QUERY_LANGUAGE, 
					queryLanguage.toString()));
		params.add(new NameValuePair(CmaConstants.PARAM_QUERY, query));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNALLPROPERTIES, 
					returnAllProperties));
		params.add(new NameValuePair(CmaConstants.PARAM_PERMISSIONSTOCHECKFOR, 
					(Serializable)permissionsToCheckFor));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNPEERASSOCS, returnPeerAssocs));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNCHILDASSOCS, returnChildAssocs));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNASPECTS, returnAspects));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNPERMISSIONS, 
					returnCurrentUserPermissions));
		params.add(new NameValuePair(CmaConstants.PARAM_STARTRESULT, 0));
		params.add(new NameValuePair(CmaConstants.PARAM_ENDRESULT, maxResults));
		params.add(new NameValuePair(CmaConstants.PARAM_USESERIALIZATION, true));
		// add sort defintions
		if (sortDefinitions != null) {
			params.add(new NameValuePair(CmaConstants.PARAM_SORTDEFINITIONS, 
					(Serializable)sortDefinitions));
		}

		return (List<Node>) execute(ticket, methodName, params);
	}


	public List<Node> query(Ticket ticket, StoreRef store,
			QueryLanguage queryLanguage, String query, 
			boolean returnAllProperties,
			boolean returnPeerAssocs,
			boolean returnChildAssocs, boolean returnAspects, 
			boolean returnCurrentUserPermissions, 
			List<String> permissionsToCheckFor,
			int startResult, int endResult, List<SortDefinition> sortDefinitions)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("store", store);
		ParameterCheck.mandatory("queryLanguage", queryLanguage);
		ParameterCheck.mandatoryString("query", query);
		
		String methodName = CmaConstants.METHOD_SEARCH_QUERY;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);
		// create parameters of search
		params.add(new NameValuePair(CmaConstants.PARAM_STORE, store));
		params.add(new NameValuePair(CmaConstants.PARAM_QUERY_LANGUAGE, 
					queryLanguage.toString()));
		params.add(new NameValuePair(CmaConstants.PARAM_QUERY, query));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNALLPROPERTIES, 
					returnAllProperties));
		params.add(new NameValuePair(CmaConstants.PARAM_PERMISSIONSTOCHECKFOR, 
					(Serializable)permissionsToCheckFor));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNPEERASSOCS, returnPeerAssocs));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNCHILDASSOCS, returnChildAssocs));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNASPECTS, returnAspects));
		params.add(new NameValuePair(CmaConstants.PARAM_RETURNPERMISSIONS, 
					returnCurrentUserPermissions));
		params.add(new NameValuePair(CmaConstants.PARAM_STARTRESULT, startResult));
		params.add(new NameValuePair(CmaConstants.PARAM_ENDRESULT, endResult));
		params.add(new NameValuePair(CmaConstants.PARAM_USESERIALIZATION, true));
		// add sort defintions
		if (sortDefinitions != null) {
			params.add(new NameValuePair(CmaConstants.PARAM_SORTDEFINITIONS, 
					(Serializable)sortDefinitions));
		}

		return (List<Node>) execute(ticket, methodName, params);
	}
	
	/**
	 * create a vector of parameters that contain general parameters
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param methodName
	 *         method name
	 * @return a vector that contains general parameters 
	 */
	private Vector<NameValuePair> createGeneralParmas(Ticket ticket, String methodName) {
		Vector<NameValuePair> params = new Vector<NameValuePair>();
		params.add(new NameValuePair(CmaConstants.PARAM_VERSION, CmaConstants.CMA_VERSION));
		params.add(new NameValuePair(CmaConstants.PARAM_SERVICE, CmaConstants.SERVICE_SEARCH));
		params.add(new NameValuePair(CmaConstants.PARAM_METHOD, methodName));
		params.add(new NameValuePair(CmaConstants.PARAM_ALFRESCO_TICKET, ticket.getTicket()));
		return params;
	}

	/**
	 * execute a method call with the given parameters
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param methodName
	 *            method name
	 * @param params
	 *            parameters for the method call
	 * @return a result object from the method call
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	private Object execute(Ticket ticket, String methodName, Vector<NameValuePair> params) 
		throws InvalidTicketException, CmaRuntimeException {
		
		String targetUri = ticket.getRepositoryUri() + serviceUri;
		try {
			CmaResult result = restExecuter.execute(HttpMethod.POST, targetUri, params);
			return result.getResult();
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}


	/**
	 * set service uri of web script
	 * @param serviceUri
	 */
	public void setServiceUri(String serviceUri) {
		this.serviceUri = serviceUri;
	}
		
	/**
	 * set mapping service 
	 * @param cmaMappingService
	 */
	public void setCmaMappingService(CmaMappingService cmaMappingService) {
		this.cmaMappingService = cmaMappingService;
	}
	
	/**
	 * set RestExecuter to execute HttpMethod call
	 * @param restExecuter
	 *        RestExecuter
	 */
	public void setRestExecuter(RestExecuter restExecuter) {
		this.restExecuter = restExecuter;
	}

}
