/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.util;

import java.util.Collection;
import java.util.Map;
import java.lang.IllegalArgumentException;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;

/**
 * This class is a util class for checking parameters
 * 
 * @author Hyanghee Lim
 *
 */
public final class ParameterCheck {

	/**
	 * check if a parameter is null
	 * @param parameterName
	 *        a name of parameter
	 * @param parameter
	 *        Object
	 * @throws CmaRuntimeException
	 *          when a parameter is null
	 */
	public static final void mandatory(final String parameterName,
			final Object parameter) throws IllegalArgumentException {
		if (parameter == null) {
			throw new IllegalArgumentException(parameterName + " cannot be null");
		}
	}

	/**
	 * check if a parameter is null or an empty string
	 * @param parameterName
	 *        a name of parameter
	 * @param parameter
	 *        String
	 * @throws CmaRuntimeException
	 *          when a parameter is null or an empty string
	 */
	public static final void mandatoryString(final String parameterName,
			final String parameter) throws IllegalArgumentException {
		if (parameter == null || parameter.length() <= 0) {
			throw new IllegalArgumentException(parameterName
					+ " cannot be null or an empty string");
		}
	}

	/**
	 * check if a parameter is null or an empty collection
	 * @param parameterName
	 *        a name of parameter
	 * @param parameter
	 *        Collection
	 * @throws CmaRuntimeException
	 *          when a parameter is null or empty 
	 */
	public static final void mandatoryCollection(final String parameterName,
			final Collection collection) throws IllegalArgumentException {
		if (collection == null || collection.size() == 0) {
			throw new IllegalArgumentException(parameterName + " must contain at least one item");
		}
	}

	/**
	 * check if a parameter is null or an empty map
	 * @param parameterName
	 *        a name of parameter
	 * @param parameter
	 *        Map
	 * @throws CmaRuntimeException
	 *          when a parameter is null or empty 
	 */
	public static final void mandatoryMap(final String parameterName,
			final Map map) throws IllegalArgumentException {
		if (map == null || map.size() == 0) {
			throw new IllegalArgumentException(parameterName + " must contain at least one item");
		}
	}

}
