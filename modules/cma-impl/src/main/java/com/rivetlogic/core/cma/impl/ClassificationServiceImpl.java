/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.util.Collection;
import java.util.Vector;

import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.CategoryService.Depth;
import org.alfresco.service.cmr.search.CategoryService.Mode;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rivetlogic.core.cma.api.ClassificationService;
import com.rivetlogic.core.cma.api.CmaConstants;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.mapping.CmaMappingService;
import com.rivetlogic.core.cma.mapping.CmaResult;
import com.rivetlogic.core.cma.repo.Ticket;
import com.rivetlogic.core.cma.rest.api.NameValuePair;
import com.rivetlogic.core.cma.rest.api.RestExecuter;
import com.rivetlogic.core.cma.rest.api.RestExecuter.HttpMethod;
import com.rivetlogic.core.cma.util.ParameterCheck;

/**
 * @author Sweta Chalasani
 *
 */
public class ClassificationServiceImpl implements ClassificationService {

	private static Log logger = LogFactory.getLog(ClassificationServiceImpl.class);

	/**
	 * search service uri template
	 */
	private String serviceUri;

	/**
	 * mapping service
	 */
	private CmaMappingService cmaMappingService;

	/**
	 * RestExecuter
	 */
	private RestExecuter restExecuter;

	public NodeRef createCategory(Ticket ticket, NodeRef parent, String name)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("parent", parent);
		ParameterCheck.mandatoryString("name", name);
		
		String methodName = CmaConstants.METHOD_CLASSIFICATION_CREATECATEGORY;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_NODEREF, parent));
		parameters.add(new NameValuePair(CmaConstants.PARAM_NAME, name));
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		return (NodeRef) execute(ticket, methodName, parameters);
	}

	public NodeRef createClassification(Ticket ticket, StoreRef storeRef,
			QName aspectName, String name) throws InvalidTicketException,
			CmaRuntimeException {
		throw new UnsupportedOperationException("The method " + CmaConstants.METHOD_CLASSIFICATION_CREATECLASSIFICATION + " is not supported");
		// check parameters
		/*ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("storeRef", storeRef);
		ParameterCheck.mandatory("aspectName", aspectName);
		ParameterCheck.mandatoryString("name", name);
		
		String methodName = CmaConstants.METHOD_CLASSIFICATION_CREATECLASSIFICATION;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_STOREREF, storeRef));
		parameters.add(new NameValuePair(CmaConstants.PARAM_ASPECTTYPEQNAME, aspectName));
		parameters.add(new NameValuePair(CmaConstants.PARAM_ATTRIBUTENAME, name));
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		return (NodeRef) execute(ticket, methodName, parameters);*/
	}

	public NodeRef createRootCategory(Ticket ticket, StoreRef storeRef,
			QName aspectName, String name) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("storeRef", storeRef);
		ParameterCheck.mandatory("aspectName", aspectName);
		ParameterCheck.mandatoryString("name", name);
		
		String methodName = CmaConstants.METHOD_CLASSIFICATION_CREATEROOTCATEGORY;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_STOREREF, storeRef));
		parameters.add(new NameValuePair(CmaConstants.PARAM_ASPECTTYPEQNAME, aspectName));
		parameters.add(new NameValuePair(CmaConstants.PARAM_NAME, name));
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		return (NodeRef) execute(ticket, methodName, parameters);
	}

	public void deleteCategory(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		
		String methodName = CmaConstants.METHOD_CLASSIFICATION_DELETECATEGORY;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		execute(ticket, methodName, parameters);
	}

	public void deleteClassification(Ticket ticket, StoreRef storeRef,
			QName aspectName) throws InvalidTicketException,
			CmaRuntimeException {
		throw new UnsupportedOperationException("The method " + CmaConstants.METHOD_CLASSIFICATION_DELETECLASSIFICATION+ " is not supported");
		// check parameters
		/*ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("storeRef", storeRef);
		
		String methodName = CmaConstants.METHOD_CLASSIFICATION_DELETECLASSIFICATION;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_STOREREF, storeRef));
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		execute(ticket, methodName, parameters);*/
	}

	public Collection<ChildAssociationRef> getCategories(Ticket ticket,
			StoreRef storeRef, QName aspectQName, Depth depth)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("storeRef", storeRef);
		ParameterCheck.mandatory("aspectName", aspectQName);
		ParameterCheck.mandatory("depth", depth);
		
		String methodName = CmaConstants.METHOD_CLASSIFICATION_GETCATEGORIES;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_STOREREF, storeRef));
		parameters.add(new NameValuePair(CmaConstants.PARAM_ASPECTTYPEQNAME, aspectQName));
		parameters.add(new NameValuePair(CmaConstants.PARAM_DEPTH, depth));

		return (Collection<ChildAssociationRef>) execute(ticket, methodName, parameters);
	}

	public Collection<ChildAssociationRef> getChildren(Ticket ticket,
			NodeRef categoryRef, Mode mode, Depth depth)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("categoryRef", categoryRef);
		ParameterCheck.mandatory("mode", mode);
		ParameterCheck.mandatory("depth", depth);
		
		String methodName = CmaConstants.METHOD_CLASSIFICATION_GETCHILDREN;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_CATEGORYREF, categoryRef));
		parameters.add(new NameValuePair(CmaConstants.PARAM_MODE, mode));
		parameters.add(new NameValuePair(CmaConstants.PARAM_DEPTH, depth));

		return (Collection<ChildAssociationRef>) execute(ticket, methodName, parameters);
	}

	public Collection<QName> getClassificationAspects(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		
		String methodName = CmaConstants.METHOD_CLASSIFICATION_GETCLASSIFICATIONASPECTS;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);

		return (Collection<QName>) execute(ticket, methodName, parameters);
	}

	public Collection<ChildAssociationRef> getClassifications(Ticket ticket,
			StoreRef storeRef) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("storeRef", storeRef);
		
		String methodName = CmaConstants.METHOD_CLASSIFICATION_GETCLASSIFICATIONS;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_STOREREF, storeRef));

		return (Collection<ChildAssociationRef>) execute(ticket, methodName, parameters);
	}

	public Collection<ChildAssociationRef> getRootCategories(Ticket ticket,
			StoreRef storeRef, QName aspectName) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("storeRef", storeRef);
		ParameterCheck.mandatory("aspectName", aspectName);
		
		String methodName = CmaConstants.METHOD_CLASSIFICATION_GETROOTCATEGORIES;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_STOREREF, storeRef));
		parameters.add(new NameValuePair(CmaConstants.PARAM_ASPECTTYPEQNAME, aspectName));

		return (Collection<ChildAssociationRef>) execute(ticket, methodName, parameters);
	}
	

	/**
	 * create a vector of parameters that contain general parameters
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param methodName
	 *         method name
	 * @return a vector that contains general parameters 
	 */
	private Vector<NameValuePair> createGeneralParameters(Ticket ticket, String methodName) {
		Vector<NameValuePair> parameters = new Vector<NameValuePair>();
		parameters.add(new NameValuePair(CmaConstants.PARAM_VERSION, CmaConstants.CMA_VERSION));
		parameters.add(new NameValuePair(CmaConstants.PARAM_SERVICE, CmaConstants.SERVICE_CLASSIFICATION));
		parameters.add(new NameValuePair(CmaConstants.PARAM_METHOD, methodName));
		parameters.add(new NameValuePair(CmaConstants.PARAM_ALFRESCO_TICKET, ticket.getTicket()));
		return parameters;
	}
	
	/**
	 * execute a method call with the given parameters
	 * 
	 * @param methodName
	 *            method name
	 * @param parameters
	 *            parameters for the method call
	 * @return a result object from the method call
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	private Object execute(Ticket ticket, String methodName, Vector<NameValuePair> parameters) 
		throws InvalidTicketException, CmaRuntimeException {
		
		String targetUri = ticket.getRepositoryUri() + serviceUri;
		String mappingFile = cmaMappingService.getMapping(CmaConstants.CMA_VERSION, 
										CmaConstants.SERVICE_CLASSIFICATION, methodName);
		
		try {
			CmaResult result = restExecuter.execute(HttpMethod.POST, targetUri, 
									mappingFile, parameters);
			return result.getResult();
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}

	/**
	 * set service uri of web script
	 * @param serviceUri
	 */
	public void setServiceUri(String serviceUri) {
		this.serviceUri = serviceUri;
	}
		
	/**
	 * set mapping service 
	 * @param cmaMappingService
	 */
	public void setCmaMappingService(CmaMappingService cmaMappingService) {
		this.cmaMappingService = cmaMappingService;
	}
	
	/**
	 * set RestExecuter to execute HttpMethod call
	 * @param restExecuter
	 *        RestExecuter
	 */
	public void setRestExecuter(RestExecuter restExecuter) {
		this.restExecuter = restExecuter;
	}

}

