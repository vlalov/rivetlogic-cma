/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping;

import java.util.Map;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rivetlogic.core.cma.api.CmaConstants;

/**
 * MethodMapping
 * 
 * This class holds mapping configuration at method level
 * 
 * @author Hyanghee Lim
 *
 */
public class MethodMapping {

	private static Log logger = LogFactory.getLog(MethodMapping.class);

	/**
	 * default mapping file for this service
	 */
	private String defaultMapping;

	/**
	 * method mapping map
	 */
	private Map<String, String> methodMap;

	public MethodMapping() {
		methodMap = new HashMap<String, String>(
				CmaConstants.DEFAULT_NUMBER_OF_METHODS);
	}

	public String getDefaultMapping() {
		return defaultMapping;
	}

	public Map<String, String> getMethodMap() {
		return methodMap;
	}

	public void setDefaultMapping(String defaultMapping) {
		this.defaultMapping = defaultMapping;
	}

	public void setMethodMap(Map<String, String> methodMap) {
		this.methodMap = methodMap;
	}

	/**
	 * get a mapping file name for a given method
	 * 
	 * if a mapping file does not exist for the given method, return the default
	 * mapping file name
	 * 
	 * @param methodName
	 *            the name of the method to get a mapping file for
	 * @return a mapping file name or null if no mapping file for the method and
	 *         no default mapping file for this service
	 */
	public String getMapping(String methodName) {

		String mappingFile = methodMap.get(methodName);
		if (mappingFile == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("no mapping found for " + methodName
						+ ". returning default mapping");
			}
			mappingFile = defaultMapping;
		}
		return mappingFile;
	}
}
