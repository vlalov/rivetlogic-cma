/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.util.Vector;

import org.alfresco.service.cmr.repository.ContentIOException;
import org.alfresco.service.cmr.repository.NoTransformerException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rivetlogic.core.cma.api.CmaConstants;
import com.rivetlogic.core.cma.api.TransformationService;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.mapping.CmaMappingService;
import com.rivetlogic.core.cma.mapping.CmaResult;
import com.rivetlogic.core.cma.repo.Ticket;
import com.rivetlogic.core.cma.rest.api.NameValuePair;
import com.rivetlogic.core.cma.rest.api.RestExecuter;
import com.rivetlogic.core.cma.rest.api.RestExecuter.HttpMethod;
import com.rivetlogic.core.cma.util.ParameterCheck;

/**
 * @author Sweta Chalasani
 *
 */
public class TransformationServiceImpl implements TransformationService {

	private static Log logger = LogFactory.getLog(TransformationServiceImpl.class);

	/**
	 * search service uri template
	 */
	private String serviceUri;

	/**
	 * mapping service
	 */
	private CmaMappingService cmaMappingService;

	/**
	 * RestExecuter
	 */
	private RestExecuter restExecuter;
	
	public boolean isTransformable(Ticket ticket, NodeRef source,
			NodeRef destination) throws InvalidTicketException,
			CmaRuntimeException {
			// check parameters
			ParameterCheck.mandatory("ticket", ticket);
			ParameterCheck.mandatory("SourceNodeRef", source);
			ParameterCheck.mandatory("destinationNodeRef", destination);

			String methodName = CmaConstants.METHOD_TRANSFORMATION_ISTRANSFORMABLE;
			// create parameters
			Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
			parameters.add(new NameValuePair(CmaConstants.PARAM_NODEREF, source));
			parameters.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, destination));

			try {
				return ((Boolean) execute(ticket, methodName, parameters)).booleanValue();
			} catch (NoTransformerException e) {
				throw new CmaRuntimeException(e.getMessage());
			} catch (ContentIOException e) {
				throw new CmaRuntimeException(e.getMessage());
			}
	}

	public void transform(Ticket ticket, NodeRef source, NodeRef destination)
			throws InvalidTicketException, CmaRuntimeException,
			NoTransformerException, ContentIOException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("SourceNodeRef", source);
		ParameterCheck.mandatory("destinationNodeRef", destination);

		String methodName = CmaConstants.METHOD_TRANSFORMATION_TRANSFORM;
		// create parameters
		Vector<NameValuePair> parameters = createGeneralParameters(ticket, methodName);
		parameters.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		parameters.add(new NameValuePair(CmaConstants.PARAM_NODEREF, source));
		parameters.add(new NameValuePair(CmaConstants.PARAM_DESTINATIONNODEREF, destination));

		execute(ticket, methodName, parameters);
	}
	
	/**
	 * create a vector of parameters that contain general parameters
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param methodName
	 *         method name
	 * @return a vector that contains general parameters 
	 */
	private Vector<NameValuePair> createGeneralParameters(Ticket ticket, String methodName) {
		Vector<NameValuePair> parameters = new Vector<NameValuePair>();
		parameters.add(new NameValuePair(CmaConstants.PARAM_VERSION, CmaConstants.CMA_VERSION));
		parameters.add(new NameValuePair(CmaConstants.PARAM_SERVICE, CmaConstants.SERVICE_TRANSFORMATION));
		parameters.add(new NameValuePair(CmaConstants.PARAM_METHOD, methodName));
		parameters.add(new NameValuePair(CmaConstants.PARAM_ALFRESCO_TICKET, ticket.getTicket()));
		return parameters;
	}
	
	/**
	 * execute a method call with the given parameters
	 * 
	 * @param methodName
	 *            method name
	 * @param parameters
	 *            parameters for the method call
	 * @return a result object from the method call
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 * @throws ContentIOException 
	 * @throws NoTransformerException
	 */
	private Object execute(Ticket ticket, String methodName, Vector<NameValuePair> parameters) 
		throws InvalidTicketException, CmaRuntimeException, ContentIOException, NoTransformerException {
		
		String targetUri = ticket.getRepositoryUri() + serviceUri;
		String mappingFile = cmaMappingService.getMapping(CmaConstants.CMA_VERSION, 
										CmaConstants.SERVICE_TRANSFORMATION, methodName);
		
		try {
			CmaResult result = restExecuter.execute(HttpMethod.POST, targetUri, 
									mappingFile, parameters);
			return result.getResult();
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (NoTransformerException e) {
			throw e;
		} catch (ContentIOException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}

	/**
	 * set service uri of web script
	 * @param serviceUri
	 */
	public void setServiceUri(String serviceUri) {
		this.serviceUri = serviceUri;
	}
		
	/**
	 * set mapping service 
	 * @param cmaMappingService
	 */
	public void setCmaMappingService(CmaMappingService cmaMappingService) {
		this.cmaMappingService = cmaMappingService;
	}
	
	/**
	 * set RestExecuter to execute HttpMethod call
	 * @param restExecuter
	 *        RestExecuter
	 */
	public void setRestExecuter(RestExecuter restExecuter) {
		this.restExecuter = restExecuter;
	}

}
