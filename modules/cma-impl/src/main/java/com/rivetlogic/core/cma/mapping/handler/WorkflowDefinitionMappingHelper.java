/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping.handler;

import org.alfresco.service.cmr.workflow.WorkflowDefinition;
import org.alfresco.service.cmr.workflow.WorkflowTaskDefinition;

import com.rivetlogic.core.cma.mapping.CmaMappingHelper;

/**
 * This class is a helper class for unmarshalling WorkflowDefinition
 * 
 * @author Hyanghee Lim
 *
 */
public class WorkflowDefinitionMappingHelper implements CmaMappingHelper {
	
	private String description;
	private String id;
	private String name;
	private String title;
	private String version;
	private WorkflowTaskDefinitionMappingHelper startTaskDefinition;
	
	public String getDescription() {
		return description;
	}
	
	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getVersion() {
		return version;
	}
	
	public WorkflowTaskDefinitionMappingHelper getStartTaskDefinition() {
		return startTaskDefinition;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}
	
	public void setStartTaskDefinition(WorkflowTaskDefinitionMappingHelper startTaskDefinition) {
		this.startTaskDefinition = startTaskDefinition;
	}
	
	/**
	 * Get a WorkflowDefinition from contained variables
	 */
	public Object getContainedObject() {
		return new WorkflowDefinition(id, name, version, title, description, (WorkflowTaskDefinition) startTaskDefinition.getContainedObject());
	}
}
