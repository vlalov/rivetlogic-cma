/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping.handler;

import java.util.StringTokenizer;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.exolab.castor.mapping.GeneralizedFieldHandler;

/**
 * This class handles creation and retrieval of AssociationRef
 * 
 * @author Hyanghee Lim
 *
 */
public class AssociationRefMappingHandler extends GeneralizedFieldHandler {
	
	private static final String FILLER = "|";
	 
	public AssociationRefMappingHandler() {}
	
	/**
	 * Get String representation of AssociationRef
	 * 
	 * @param value
	 * 		AssociationRef object to retrieve information from
	 * @return string representation of AssociationRef
	 */
	public Object convertUponGet(Object value) {
		if (value == null) { 
			return null;
		} else {
			return value.toString();
		}
	}

	/**
	 * Create AssociationRef from String value
	 * 
	 * @param value
	 * 		a parameter to create AssociationRef
	 * @return AssociationRef
	 */
	public Object convertUponSet(Object value) {
		if (value == null || ((String)value).length() == 0) {
			return null;
		} else {
			//June 2010:This is workaround for parsing association exception using new AssociationRef((String) value) in Alfresco 3.3C
			//Will be changed back once the fix is available.
			StringTokenizer tokenizer = new StringTokenizer((String) value, FILLER);
			
			 if (tokenizer.countTokens() != 4)
		        {
		            throw new AlfrescoRuntimeException("Unable to parse association string: " + (String)value);
		        }
			 
	        String idStr = tokenizer.nextToken();
	        String sourceNodeRefStr = tokenizer.nextToken();
	        String targetNodeRefStr = tokenizer.nextToken();
	        String assocTypeQNameStr = tokenizer.nextToken();
	        
			return new AssociationRef(new Long(idStr), new NodeRef(sourceNodeRefStr), QName.createQName(assocTypeQNameStr), new NodeRef(targetNodeRefStr));
			
			//return new AssociationRef((String) value);
		}
	}
	
	public Class getFieldType() {
		return AssociationRef.class;
	}

}
