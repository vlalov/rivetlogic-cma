/**
 * 
 */
package com.rivetlogic.core.cma.mapping.handler;

import org.alfresco.service.cmr.workflow.WorkflowTransition;

import com.rivetlogic.core.cma.mapping.CmaMappingHelper;

/**
 * This class is a helper class for unmarshalling WorkflowTransition
 * 
 * @author Sweta Chalasani
 *
 */
public class WorkflowTransitionMappingHelper implements CmaMappingHelper {

	private String id;
	private String title;
	private String description;
	private boolean isDefault;
	
	public Object getContainedObject() {
		WorkflowTransition transition = new WorkflowTransition(id, title, description, isDefault);
		return transition;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isDefault() {
		return isDefault;
	}
	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

}
