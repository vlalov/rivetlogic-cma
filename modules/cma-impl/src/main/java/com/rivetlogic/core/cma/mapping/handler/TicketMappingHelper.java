package com.rivetlogic.core.cma.mapping.handler;

import org.alfresco.service.cmr.repository.NodeRef;

import com.rivetlogic.core.cma.mapping.CmaMappingHelper;
import com.rivetlogic.core.cma.repo.Ticket;

public class TicketMappingHelper implements CmaMappingHelper{

	private String repositoryUri;
	private String ticket;
	private NodeRef userNodeRef; 
	
	public Object getContainedObject() {
		return new Ticket(repositoryUri, ticket, userNodeRef);
	}

	public void setRepositoryUri(String repositoryUri) {
		this.repositoryUri = repositoryUri;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public void setUserNodeRef(NodeRef userRef) {
		this.userNodeRef = userRef;
	}

	public String getRepositoryUri() {
		return repositoryUri;
	}

	public String getTicket() {
		return ticket;
	}

	public NodeRef getUserNodeRef() {
		return userNodeRef;
	}

}
