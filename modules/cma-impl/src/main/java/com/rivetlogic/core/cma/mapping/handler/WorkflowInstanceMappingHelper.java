/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping.handler;

import java.util.Date;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.workflow.WorkflowDefinition;
import org.alfresco.service.cmr.workflow.WorkflowInstance;

import com.rivetlogic.core.cma.mapping.CmaMappingHelper;

/**
 * This class is a helper class for unmarshalling WorkflowInstance
 * 
 * @author Hyanghee Lim
 *
 */
public class WorkflowInstanceMappingHelper implements CmaMappingHelper {

	private String id;
	private boolean active;
	private WorkflowDefinitionMappingHelper definition;
	private String description;
	private NodeRef context;
	private NodeRef initiator;
	private NodeRef workflowPackage;
	private Date startDate;
	private Date endDate;
	private Integer priority;
	private Date dueDate;
	
	public String getId() {
		return id;
	}
	
//	public boolean getActive() {
//		return active;
//	}
//	
	public WorkflowDefinitionMappingHelper getDefinition() {
		return definition;
	}
	
	public String getDescription() {
		return description;
	}
	
	public NodeRef getContext() {
		return context;
	}
	
	public NodeRef getInitiator() {
		return initiator;
	}
	
	public NodeRef getWorkflowPackage() {
		return workflowPackage;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	
	public Date getDueDate() {
		return dueDate;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public void setDefinition(WorkflowDefinitionMappingHelper definition) {
		this.definition = definition;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setContext(NodeRef context) {
		this.context = context;
	}
	
	public void setInitiator(NodeRef initiator) {
		this.initiator = initiator;
	}

	public void setWorkflowPackage(NodeRef workflowPackage) {
		this.workflowPackage = workflowPackage;
	}
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
	/**
	 * Get a WorkflowInstance from contained variables
	 */
	public Object getContainedObject() {
		WorkflowInstance instance = new WorkflowInstance(id, (WorkflowDefinition) definition.getContainedObject(), 
									description, initiator, workflowPackage, context, active, 
									startDate, endDate);
		return instance;
	}

	public boolean isActive() {
		return active;
	}

	public Integer getPriority() {
		return priority;
	}

}
