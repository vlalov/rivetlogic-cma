/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping.handler;

import java.util.StringTokenizer;

import org.alfresco.repo.security.permissions.impl.AccessPermissionImpl;
import org.alfresco.service.cmr.security.AccessPermission;
import org.alfresco.service.cmr.security.AccessStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.exolab.castor.mapping.GeneralizedFieldHandler;


/**
 * This class handles creation and retrieval of Alfresco AccessPermission
 * 
 * @author Hyanghee Lim
 *
 */
public class AccessPermissionMappingHandler extends GeneralizedFieldHandler {
	
	
	private static Log logger = LogFactory.getLog(AccessPermissionMappingHandler.class);
	
	private String separater = ",";
	
	public AccessPermissionMappingHandler() {}
	
	/**
	 * Get String representation of Alfresco AccessPermission
	 * 
	 * @param value
	 * 		Alfresco AccessPermission object to retrieve information from
	 * @return string representation of Alfresco AccessPermission
	 */
	public Object convertUponGet(Object value) {
		if (value == null) 
			return "";
		
		AccessPermission permission = (AccessPermission) value;
		return permission.getPermission() + separater + 
					 permission.getAccessStatus().name() + separater +
					 permission.getAuthority() + separater +
					 permission.getPosition();
	}

	/**
	 * Create Alfresco AccessPermission from String value
	 * 
	 * @param value
	 * 		a parameter to create Alfresco AccessPermission
	 * @return Alfresco AccessPermission
	 */
	public Object convertUponSet(Object value) {
		if (logger.isDebugEnabled()) {
			logger.debug("value to create AccessPermission from: " + value);
		}
		StringTokenizer tokenizer = new StringTokenizer((String)value, separater);
		if (tokenizer.countTokens() != 4) {
			return null;
		}
		String permission = tokenizer.nextToken();
		AccessStatus status = AccessStatus.valueOf(tokenizer.nextToken());
		String authority = tokenizer.nextToken();
		int position = 0;
		try {
			position = Integer.parseInt(tokenizer.nextToken());
		} catch (Exception e) {
			// ignore exception
		}
		return new AccessPermissionImpl(permission, status, authority, position);
	}
	
	public Class getFieldType() {
		return AccessPermission.class;
	}

}
