/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping.impl;

//import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.xml.Unmarshaller;
import org.xml.sax.InputSource;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.mapping.CmaUnmarshaller;

/**
 * This class marshals an object using Castor
 * 
 * @author Hyanghee Lim
 * 
 */
public class CmaCastorUnmarshaller implements CmaUnmarshaller {

	private static Log logger = LogFactory.getLog(CmaCastorUnmarshaller.class);

	public Object unmarshal(InputStream input, String mappingFile)
			throws CmaRuntimeException {

		if (logger.isDebugEnabled()) {
			logger.debug("marshal mapping file: " + mappingFile);
		}
		InputStream fileStream = null;
		try {
			fileStream = this.getClass().getClassLoader().getResourceAsStream(mappingFile);
			Mapping mapping = new Mapping();
			mapping.loadMapping(new InputSource(fileStream));
			Unmarshaller unmarshaller = new Unmarshaller(mapping);
			return unmarshaller.unmarshal(new InputSource(input));
		} catch (Exception e) {
			throw new CmaRuntimeException(e);
		} finally {
			// cleanup resources
			try {
				if (fileStream != null) {
					fileStream.close();
				}
				if (input != null) {
					input.close();
				}
			} catch (IOException e) {
				throw new CmaRuntimeException(e);
			}
		}
	}

}
