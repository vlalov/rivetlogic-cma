/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping;

import com.rivetlogic.core.cma.exception.CmaRuntimeException;

public interface CmaMappingService {

	/**
	 * Get a mapping file path name by a given version, service name, and method name
	 * 
	 * @param version
	 *            CMA Client Version
	 * @param serviceName
	 *            CMA service name
	 * @param methodName
	 *            CMA method name called
	 * @return a mapping file path
	 * @throws CmaRuntimeException
	 *          if failed to locate a mapping file
	 */
	public String getMapping(String version, String serviceName,
			String methodName) throws CmaRuntimeException;
	
	/**
	 * Get a mapping file path for error response
	 * 
	 * @param version
	 *            CMA Client Version
	 * @return a mapping file path
	 * @throws CmaRuntimeException
	 *          if failed to locate a mapping file
	 */
	public String getErrorMapping(String version) throws CmaRuntimeException;
	
}
