/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.security.AccessPermission;
import org.alfresco.service.cmr.security.AccessStatus;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.api.CmaConstants;
import com.rivetlogic.core.cma.api.SecurityService;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.mapping.CmaMappingService;
import com.rivetlogic.core.cma.mapping.CmaResult;
import com.rivetlogic.core.cma.repo.Ticket;
import com.rivetlogic.core.cma.rest.api.NameValuePair;
import com.rivetlogic.core.cma.rest.api.RestExecuter;
import com.rivetlogic.core.cma.rest.api.RestExecuter.HttpMethod;
import com.rivetlogic.core.cma.util.ParameterCheck;

/**
 * 
 * @author Hyanghee Lim
 *
 */
public class SecurityServiceImpl implements SecurityService {

	private static Log logger = LogFactory.getLog(SecurityServiceImpl.class);

	/**
	 * authentication service uri template
	 */
	private String serviceUri;

	/**
	 * mapping service
	 */
	private CmaMappingService cmaMappingService;

	/**
	 * RestExecuter
	 */
	private RestExecuter restExecuter;

	public void clearPermission(Ticket ticket, NodeRef nodeRef, String authority)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		ParameterCheck.mandatoryString("authority", authority);
		
		String methodName = CmaConstants.METHOD_SECURITY_CLEARPERMISSION;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_AUTHORITY, authority));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		
		execute(ticket, methodName, params);
	}

	public void deletePermission(Ticket ticket, NodeRef nodeRef, String authority,
			String permission) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		ParameterCheck.mandatoryString("authority", authority);
		ParameterCheck.mandatoryString("permission", permission);
		
		String methodName = CmaConstants.METHOD_SECURITY_DELETEPERMISSION;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_AUTHORITY, authority));
		params.add(new NameValuePair(CmaConstants.PARAM_PERMISSION, permission));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		
		execute(ticket, methodName, params);
	}

	public void deletePermissions(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		
		String methodName = CmaConstants.METHOD_SECURITY_DELETEPERMISSIONS;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		
		execute(ticket, methodName, params);
	}

	@Deprecated
	public Set<NodeRef> findNodesByAssignedPermission(Ticket ticket, String authority,
			String permission, boolean allow,
			boolean includeContainingAuthorities, boolean exactPermissionMatch)
			throws InvalidTicketException, CmaRuntimeException {
		throw new UnsupportedOperationException();
	}

	@Deprecated
	public Set<NodeRef> findNodesByAssignedPermissionForCurrentUser(Ticket ticket, 
			String permission, boolean allow,
			boolean includeContainingAuthorities,
			boolean includeContainingPermissions)
			throws InvalidTicketException, CmaRuntimeException {
		throw new UnsupportedOperationException();
	}

	public String getAllAuthorities(Ticket ticket) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		
		String methodName = CmaConstants.METHOD_SECURITY_GETALLAUTHORITIES;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		
		return (String) execute(ticket, methodName, params);
	}

	public String getAllPermission(Ticket ticket) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		
		String methodName = CmaConstants.METHOD_SECURITY_GETALLPERMISSION;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		
		return (String) execute(ticket, methodName, params);
	}

	public Set<AccessPermission> getAllSetPermissions(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		
		String methodName = CmaConstants.METHOD_SECURITY_GETALLSETPERMISSIONS;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		
		return (Set<AccessPermission>) execute(ticket, methodName, params);
	}
	
	@Deprecated
	public Map<NodeRef, Set<AccessPermission>> getAllSetPermissionsForAuthority(Ticket ticket, 
			String authority) throws InvalidTicketException,
			CmaRuntimeException {
		throw new UnsupportedOperationException();
	}

	@Deprecated
	public Map<NodeRef, Set<AccessPermission>> getAllSetPermissionsForCurrentUser(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException {
		throw new UnsupportedOperationException();
	}

	public boolean getInheritParentPermissions(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		
		String methodName = CmaConstants.METHOD_SECURITY_GETINHERITPARENTPERMISSIONS;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		
		return (Boolean) execute(ticket, methodName, params);
	}

	public String getOwnerAuthority(Ticket ticket) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		
		String methodName = CmaConstants.METHOD_SECURITY_GETOWNERAUTHORITY;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		
		return (String) execute(ticket, methodName, params);
	}

	public Set<AccessPermission> getPermissions(Ticket ticket, NodeRef nodeRef)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		
		String methodName = CmaConstants.METHOD_SECURITY_GETPERMISSIONS;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		
		return (Set<AccessPermission>) execute(ticket, methodName, params);
	}

	public Set<String> getSettablePermissions(Ticket ticket, NodeRef nodeRef)
		throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		
		String methodName = CmaConstants.METHOD_SECURITY_GETSETTABLEPERMISSIONSBYNODEREF;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		
		return (Set<String>) execute(ticket, methodName, params);
	}

	public Set<String> getSettablePermissions(Ticket ticket, QName type)
		throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		
		String methodName = CmaConstants.METHOD_SECURITY_GETSETTABLEPERMISSIONSBYQNAME;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_QNAME, type));
		
		return (Set<String>) execute(ticket, methodName, params);
	}

	public AccessStatus hasPermission(Ticket ticket, NodeRef nodeRef, String permission)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		ParameterCheck.mandatoryString("permission", permission);
		
		String methodName = CmaConstants.METHOD_SECURITY_HASPERMISSION;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_PERMISSION, permission));
		
		return (AccessStatus) execute(ticket, methodName, params);
	}

	public void setInheritParentPermissions(Ticket ticket, NodeRef nodeRef,
			boolean inheritParentPermissions) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		
		String methodName = CmaConstants.METHOD_SECURITY_SETINHERITPARENTPERMISSIONS;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_INHERITPARENTPERMISSIONS, inheritParentPermissions));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		
		execute(ticket, methodName, params);
	}

	public void setPermission(Ticket ticket, NodeRef nodeRef, String authority,
			String permission, boolean allow) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		ParameterCheck.mandatoryString("authority", authority);
		ParameterCheck.mandatoryString("permission", permission);
		
		String methodName = CmaConstants.METHOD_SECURITY_SETPERMISSION;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_AUTHORITY, authority));
		params.add(new NameValuePair(CmaConstants.PARAM_PERMISSION, permission));
		params.add(new NameValuePair(CmaConstants.PARAM_ALLOW, allow));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		
		execute(ticket, methodName, params);
	}

	/**
	 * create a vector of parameters that contain general parameters
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param methodName
	 *            method name
	 * @return a vector that contains general parameters
	 */
	private Vector<NameValuePair> createGeneralParams(Ticket ticket, String methodName) {
		Vector<NameValuePair> params = new Vector<NameValuePair>();
		params.add(new NameValuePair(CmaConstants.PARAM_VERSION, CmaConstants.CMA_VERSION));
		params.add(new NameValuePair(CmaConstants.PARAM_SERVICE, CmaConstants.SERVICE_SECURITY));
		params.add(new NameValuePair(CmaConstants.PARAM_METHOD, methodName));
		params.add(new NameValuePair(CmaConstants.PARAM_ALFRESCO_TICKET, ticket.getTicket()));
		return params;
	}

	/**
	 * execute a Http method call with given parameters
	 * 
	 * @param methodName
	 *            method name
	 * @param params
	 *            parameters for the method call
	 * @return a result object from the method call
	 * @throws InvalidTicketException
	 * @throws CmaRuntimeException
	 */
	private Object execute(Ticket ticket, String methodName,
			Vector<NameValuePair> params) throws InvalidTicketException,
			CmaRuntimeException {

		String targetUri = ticket.getRepositoryUri() + serviceUri;
		String mappingFile = cmaMappingService.getMapping(
				CmaConstants.CMA_VERSION, CmaConstants.SERVICE_SECURITY,
				methodName);

		try {
			CmaResult result = restExecuter.execute(HttpMethod.POST, targetUri,
					mappingFile, params);
			return result.getResult();
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}

	/**
	 * set service uri of web script
	 * 
	 * @param serviceUri
	 */
	public void setServiceUri(String serviceUri) {
		this.serviceUri = serviceUri;
	}

	/**
	 * set mapping service
	 * 
	 * @param cmaMappingService
	 */
	public void setCmaMappingService(CmaMappingService cmaMappingService) {
		this.cmaMappingService = cmaMappingService;
	}

	/**
	 * set RestExecuter to execute HttpMethod call
	 * 
	 * @param restExecuter
	 *            RestExecuter
	 */
	public void setRestExecuter(RestExecuter restExecuter) {
		this.restExecuter = restExecuter;
	}
}
