/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;

import org.alfresco.service.cmr.lock.NodeLockedException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rivetlogic.core.cma.api.CmaConstants;
import com.rivetlogic.core.cma.api.ContentService;
import com.rivetlogic.core.cma.mapping.CmaResult;
import com.rivetlogic.core.cma.mapping.CmaMappingService;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;

import com.rivetlogic.core.cma.repo.Ticket;
import com.rivetlogic.core.cma.rest.api.NameValuePair;
import com.rivetlogic.core.cma.rest.api.RestExecuter;
import com.rivetlogic.core.cma.rest.api.RestExecuter.HttpMethod;
import com.rivetlogic.core.cma.util.ParameterCheck;

/**
 * 
 * @author Hyanghee Lim
 *
 */
public class ContentServiceImpl implements ContentService {
	
	private static Log logger = LogFactory.getLog(ContentServiceImpl.class);

	/**
	 * search service uri template
	 */
	private String serviceUri;

	/**
	 * mapping service
	 */
	private CmaMappingService cmaMappingService;
	
	/**
	 * RestExecuter
	 */
	private RestExecuter restExecuter;

	public void writeContent(Ticket ticket, NodeRef nodeRef, QName property,
			String fileName) throws InvalidTicketException,
			NodeLockedException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		ParameterCheck.mandatory("property", property);
		ParameterCheck.mandatoryString("fileName", fileName);

		String methodName = CmaConstants.METHOD_CONTENT_WRITECONTENT;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_CONTENTQNAME, property));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		String targetUri = ticket.getRepositoryUri() + serviceUri;
		
		try {
			restExecuter.execute(HttpMethod.POST, targetUri, "", params, fileName);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (NodeLockedException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}

	public void writeContentFromStream(Ticket ticket, NodeRef nodeRef, QName property,
			InputStream input) throws InvalidTicketException,
			NodeLockedException, CmaRuntimeException {
				
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		ParameterCheck.mandatory("property", property);
		ParameterCheck.mandatory("inputStream", input);
		
		String methodName = CmaConstants.METHOD_CONTENT_WRITECONTENT;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_CONTENTQNAME, property));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		String targetUri = ticket.getRepositoryUri() + serviceUri;
		try {
			restExecuter.execute(HttpMethod.POST, targetUri, "", params, input);		
		} catch (InvalidTicketException e) {
			throw e;
		} catch (NodeLockedException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}

	public void readContent(Ticket ticket, NodeRef nodeRef, QName property,
			String fileName) throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		ParameterCheck.mandatory("property", property);
		ParameterCheck.mandatoryString("fileName", fileName);

		String methodName = CmaConstants.METHOD_CONTENT_READCONTENT;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTY, property));

		String targetUri = ticket.getRepositoryUri() + serviceUri;
		try {
			restExecuter.execute(HttpMethod.GET, targetUri, "", params, fileName);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}

	public void readContentIntoStream(Ticket ticket, NodeRef nodeRef, QName property,
			OutputStream output) throws InvalidTicketException,
			CmaRuntimeException {
		
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		ParameterCheck.mandatory("property", property);
		ParameterCheck.mandatory("outputStream", output);

		String methodName = CmaConstants.METHOD_CONTENT_READCONTENT;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTY, property));

		String targetUri = ticket.getRepositoryUri() + serviceUri;
		try {
			restExecuter.execute(HttpMethod.GET, targetUri, "", params, output);
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}

	public String getContentUri(Ticket ticket, NodeRef nodeRef, QName property)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("nodeRef", nodeRef);
		ParameterCheck.mandatory("property", property);

		String methodName = CmaConstants.METHOD_CONTENT_GETCONTENTURI;
		// create parameters
		Vector<NameValuePair> params = createGeneralParams(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_NODEREF, nodeRef));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTY, property));

		String targetUri = ticket.getRepositoryUri() + serviceUri;
		String mappingFile = cmaMappingService.getMapping(
											CmaConstants.CMA_VERSION,
											CmaConstants.SERVICE_CONTENT,
											CmaConstants.METHOD_CONTENT_GETCONTENTURI);
		try {
			CmaResult result = (CmaResult) restExecuter.execute(HttpMethod.POST, targetUri, 
									mappingFile, params);
			return result.getResult().toString();
		} catch (InvalidTicketException e) {
			throw e;
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		}
	}
	
	/**
	 * create a vector of parameters that contain general parameters
	 * 
	 * @param methodName
	 *         method name
	 * @return a vector that contains general parameters 
	 */
	private Vector<NameValuePair> createGeneralParams(Ticket ticket, String methodName) {
		Vector<NameValuePair> params = new Vector<NameValuePair>();
		params.add(new NameValuePair(CmaConstants.PARAM_VERSION, CmaConstants.CMA_VERSION));
		params.add(new NameValuePair(CmaConstants.PARAM_SERVICE, CmaConstants.SERVICE_CONTENT));
		params.add(new NameValuePair(CmaConstants.PARAM_METHOD, methodName));
		params.add(new NameValuePair(CmaConstants.PARAM_ALFRESCO_TICKET, ticket.getTicket()));
		return params;
	}

	/**
	 * set service uri of web script
	 * @param serviceUri
	 */
	public void setServiceUri(String serviceUri) {
		this.serviceUri = serviceUri;
	}
		
	/**
	 * set mapping service 
	 * @param cmaMappingService
	 */
	public void setCmaMappingService(CmaMappingService cmaMappingService) {
		this.cmaMappingService = cmaMappingService;
	}
	
	/**
	 * set RestExecuter to execute HttpMethod call
	 * @param restExecuter
	 *        RestExecuter
	 */
	public void setRestExecuter(RestExecuter restExecuter) {
		this.restExecuter = restExecuter;
	}

}
