/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.security.NoSuchPersonException;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rivetlogic.core.cma.api.CmaConstants;
import com.rivetlogic.core.cma.api.PeopleService;
import com.rivetlogic.core.cma.exception.AuthorityExistsException;
import com.rivetlogic.core.cma.exception.AuthorityNotFoundException;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.mapping.CmaMappingService;
import com.rivetlogic.core.cma.mapping.CmaResult;
import com.rivetlogic.core.cma.repo.Ticket;
import com.rivetlogic.core.cma.rest.api.NameValuePair;
import com.rivetlogic.core.cma.rest.api.RestExecuter;
import com.rivetlogic.core.cma.rest.api.RestExecuter.HttpMethod;
import com.rivetlogic.core.cma.util.ParameterCheck;

/**
 * 
 * @author Hyanghee Lim
 *
 */
public class PeopleServiceImpl implements PeopleService {

	private static Log logger = LogFactory.getLog(PeopleServiceImpl.class);

	/**
	 * library service uri template
	 */
	private String serviceUri;

	/**
	 * mapping service
	 */
	private CmaMappingService cmaMappingService;

	/**
	 * RestExecuter
	 */
	private RestExecuter restExecuter;

	public NodeRef createPerson(Ticket ticket, String userName, char [] password, Map<QName, Serializable> properties) 
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("userName", userName);
		ParameterCheck.mandatory("password", password);

		String methodName = CmaConstants.METHOD_PEOPLE_CREATEPERSON;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USERNAME, userName));
		params.add(new NameValuePair(CmaConstants.PARAM_PASSWORD, new String(password)));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTIES, (Serializable) properties));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		try {
			return (NodeRef) execute(ticket, methodName, params);
		} catch (AuthorityExistsException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (AuthorityNotFoundException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public void deletePerson(Ticket ticket, NodeRef person)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("person", person);

		String methodName = CmaConstants.METHOD_PEOPLE_DELETEPERSON;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_PERSON, person));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		try {
			execute(ticket, methodName, params);
		} catch (AuthorityExistsException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (AuthorityNotFoundException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public List<NodeRef> getAllPeople(Ticket ticket)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		
		String methodName = CmaConstants.METHOD_PEOPLE_GETALLPEOPLE;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);

		try {
			return (List<NodeRef>) execute(ticket, methodName, params);
		} catch (AuthorityExistsException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (AuthorityNotFoundException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public NodeRef getPerson(Ticket ticket, String userName)
			throws InvalidTicketException, AuthorityNotFoundException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("userName", userName);

		String methodName = CmaConstants.METHOD_PEOPLE_GETPERSON;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_USERNAME, userName));
		
		try{
			return (NodeRef) execute(ticket, methodName, params);
		} catch (AuthorityExistsException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public void setPersonProperties(Ticket ticket, NodeRef person,
			Map<QName, Serializable> properties) 
			throws InvalidTicketException, AuthorityNotFoundException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("person", person);
		ParameterCheck.mandatoryMap("properties", properties);

		String methodName = CmaConstants.METHOD_PEOPLE_SETPERSONPROPERTIES;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_PERSON, person));
		params.add(new NameValuePair(CmaConstants.PARAM_PROPERTIES, (Serializable) properties));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		try {
			execute(ticket, methodName, params);
		} catch (AuthorityExistsException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
			
	}

	public void addAuthority(Ticket ticket, NodeRef parentGroup, NodeRef authority)
		throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("parentGroup", parentGroup);
		ParameterCheck.mandatory("authority", authority);

		String methodName = CmaConstants.METHOD_PEOPLE_ADDAUTHORITY;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_PARENTGROUP, parentGroup));
		params.add(new NameValuePair(CmaConstants.PARAM_AUTHORITY, authority));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		try {
			execute(ticket, methodName, params);
		} catch (AuthorityExistsException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (AuthorityNotFoundException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public void addAuthorities(Ticket ticket, String parentGroupName,
			List<String> authorityNames) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("parentGroupName", parentGroupName);
		ParameterCheck.mandatory("authorityNames", authorityNames);

		String methodName = CmaConstants.METHOD_PEOPLE_ADDAUTHORITIESBYNAME;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_GROUPNAME, parentGroupName));
		params.add(new NameValuePair(CmaConstants.PARAM_AUTHORITYNAMES, (Serializable) authorityNames));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		try {
			execute(ticket, methodName, params);
		} catch (AuthorityExistsException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (AuthorityNotFoundException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public void addAuthority(Ticket ticket, String parentGroupName,
			String authorityName) throws InvalidTicketException,
			CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("parentGroupName", parentGroupName);
		ParameterCheck.mandatory("authorityName", authorityName);

		String methodName = CmaConstants.METHOD_PEOPLE_ADDAUTHORITYBYNAME;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_GROUPNAME, parentGroupName));
		params.add(new NameValuePair(CmaConstants.PARAM_USERNAME, authorityName));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		try {
			execute(ticket, methodName, params);
		} catch (AuthorityExistsException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (AuthorityNotFoundException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public NodeRef createGroup(Ticket ticket, String groupName)
			throws InvalidTicketException, AuthorityExistsException, CmaRuntimeException {
		return createGroup(ticket, null, groupName);
	}

	public NodeRef createGroup(Ticket ticket, NodeRef parentGroup,
			String groupName) throws InvalidTicketException,
			AuthorityExistsException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("groupName", groupName);

		String methodName = CmaConstants.METHOD_PEOPLE_CREATEGROUP;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_GROUPNAME, groupName));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));
		if (parentGroup != null) {
			params.add(new NameValuePair(CmaConstants.PARAM_PARENTGROUP, parentGroup));
		}

		try {
			return (NodeRef) execute(ticket, methodName, params);
		} catch (AuthorityNotFoundException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public void deleteGroup(Ticket ticket, NodeRef group)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("group", group);

		String methodName = CmaConstants.METHOD_PEOPLE_DELETEGROUP;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_GROUP, group));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		try {
			execute(ticket, methodName, params);
		} catch (AuthorityExistsException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (AuthorityNotFoundException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public List<NodeRef> getContainerGroups(Ticket ticket, NodeRef person)
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("person", person);

		String methodName = CmaConstants.METHOD_PEOPLE_GETCONTAINERGROUPS;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_PERSON, person));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		try {
			return (List<NodeRef>) execute(ticket, methodName, params);
		} catch (AuthorityExistsException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (AuthorityNotFoundException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public NodeRef getGroup(Ticket ticket, String groupName)
			throws InvalidTicketException, AuthorityNotFoundException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatoryString("groupName", groupName);

		String methodName = CmaConstants.METHOD_PEOPLE_GETGROUP;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_GROUPNAME, groupName));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		try {
			return (NodeRef) execute(ticket, methodName, params);
		} catch (AuthorityExistsException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public List<NodeRef> getMembers(Ticket ticket, NodeRef group)
			throws InvalidTicketException, CmaRuntimeException {
		return getMembers(ticket, group, false);
	}

	public List<NodeRef> getMembers(Ticket ticket, NodeRef group,
			boolean recurse) throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("group", group);

		String methodName = CmaConstants.METHOD_PEOPLE_GETMEMBERS;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_GROUP, group));
		params.add(new NameValuePair(CmaConstants.PARAM_RECURSE, String.valueOf(recurse)));

		try {
			return (List<NodeRef>) execute(ticket, methodName, params);
		} catch (AuthorityExistsException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (AuthorityNotFoundException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	public void removeAuthority(Ticket ticket, NodeRef parentGroup, NodeRef authority) 
			throws InvalidTicketException, CmaRuntimeException {
		// check parameters
		ParameterCheck.mandatory("ticket", ticket);
		ParameterCheck.mandatory("parentGroup", parentGroup);
		ParameterCheck.mandatory("authority", authority);

		String methodName = CmaConstants.METHOD_PEOPLE_REMOVEAUTHORITY;
		// create parameters
		Vector<NameValuePair> params = createGeneralParmas(ticket, methodName);
		params.add(new NameValuePair(CmaConstants.PARAM_PARENTGROUP, parentGroup));
		params.add(new NameValuePair(CmaConstants.PARAM_AUTHORITY, authority));
		params.add(new NameValuePair(CmaConstants.PARAM_USETRANSACTION, true));

		try {
			execute(ticket, methodName, params);
		} catch (AuthorityExistsException e) {
			throw new CmaRuntimeException(e.getMessage());
		} catch (AuthorityNotFoundException e) {
			throw new CmaRuntimeException(e.getMessage());
		}
	}

	/**
	 * create a vector of parameters that contain general parameters
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param methodName
	 *         method name
	 * @return a vector that contains general parameters 
	 */
	private Vector<NameValuePair> createGeneralParmas(Ticket ticket, String methodName) {
		Vector<NameValuePair> params = new Vector<NameValuePair>();
		params.add(new NameValuePair(CmaConstants.PARAM_VERSION, CmaConstants.CMA_VERSION));
		params.add(new NameValuePair(CmaConstants.PARAM_SERVICE, CmaConstants.SERVICE_PEOPLE));
		params.add(new NameValuePair(CmaConstants.PARAM_METHOD, methodName));
		params.add(new NameValuePair(CmaConstants.PARAM_ALFRESCO_TICKET, ticket.getTicket()));
		return params;
	}
	
	/**
	 * execute a method call with the given parameters
	 * 
	 * @param ticket
	 *            Ticket received post authentication
	 * @param methodName
	 *            method name
	 * @param params
	 *            parameters for the method call
	 * @return a result object from the method call
	 * @throws InvalidTicketException
	 * @throws AuthorityExistsException
	 * @throws AuthorityNotFoundException
	 * @throws CmaRuntimeException
	 */
	private Object execute(Ticket ticket, String methodName, Vector<NameValuePair> params) 
		throws InvalidTicketException, AuthorityExistsException, AuthorityNotFoundException, CmaRuntimeException {
		
		String targetUri = ticket.getRepositoryUri() + serviceUri;
		String mappingFile = cmaMappingService.getMapping(CmaConstants.CMA_VERSION, 
										CmaConstants.SERVICE_PEOPLE, methodName);
		
		try {
			CmaResult result = restExecuter.execute(HttpMethod.POST, targetUri, 
									mappingFile, params);
			return result.getResult();
		} catch (InvalidTicketException e) {
			throw e;
		} catch (AuthorityExistsException e) {
			throw e;
		} catch (AuthorityNotFoundException e) {
			throw e;
		} catch (NoSuchPersonException e) {
			throw new AuthorityNotFoundException(e);
		} catch (CmaRuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new CmaRuntimeException(t);
		} 
	}
	
	/**
	 * set service uri of web script
	 * 
	 * @param serviceUri
	 */
	public void setServiceUri(String serviceUri) {
		this.serviceUri = serviceUri;
	}

	/**
	 * set mapping service
	 * 
	 * @param cmaMappingService
	 */
	public void setCmaMappingService(CmaMappingService cmaMappingService) {
		this.cmaMappingService = cmaMappingService;
	}

	/**
	 * set RestExecuter to execute HttpMethod call
	 * 
	 * @param restExecuter
	 *            RestExecuter
	 */
	public void setRestExecuter(RestExecuter restExecuter) {
		this.restExecuter = restExecuter;
	}

}
