/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping.handler;

import org.alfresco.service.cmr.workflow.WorkflowInstance;
import org.alfresco.service.cmr.workflow.WorkflowNode;
import org.alfresco.service.cmr.workflow.WorkflowPath;

import com.rivetlogic.core.cma.mapping.CmaMappingHelper;

/**
 * This class is a helper class for unmarshalling WorkflowPath
 * 
 * @author Hyanghee Lim
 *
 */
public class WorkflowPathMappingHelper implements CmaMappingHelper {
	
	private String id;
	private boolean active;
	private WorkflowInstanceMappingHelper instance;
	private WorkflowNodeMappingHelper node;
	
	public String getId() {
		return id;
	}
	
//	public boolean getActive() {
//		return active;
//	}
	
	public WorkflowInstanceMappingHelper getInstance() {
		return instance;
	}
	
	public WorkflowNodeMappingHelper getNode() {
		return node;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
//	public void setActive(boolean active) {
//		this.active = active;
//	}
	
	public void setInstance(WorkflowInstanceMappingHelper instance) {
		this.instance = instance;
	}
	
	public void setNode(WorkflowNodeMappingHelper node) {
		this.node = node;
	}
	
	/**
	 * Get a WorkflowPath from contained variables
	 */
	public Object getContainedObject() {
		WorkflowPath path = new WorkflowPath(id, (WorkflowInstance) instance.getContainedObject(), (WorkflowNode) node.getContainedObject(), active);
		return path;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
