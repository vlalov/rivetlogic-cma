/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping.handler;

import org.alfresco.service.namespace.QName;
import org.exolab.castor.mapping.GeneralizedFieldHandler;

/**
 * This class handles creation and retrieval of QName
 * 
 * @author Hyanghee Lim
 *
 */
public class QNameMappingHandler extends GeneralizedFieldHandler {
	
	public QNameMappingHandler() {}
	
	/**
	 * Get String representation of QName
	 * 
	 * @param value
	 * 		QName object to retrieve information from
	 * @return string representation of QName
	 */
	public Object convertUponGet(Object value) {
		
		if (value == null) { 
			return null;
		} else {
			return value.toString();
		}
		
	}
	
	/**
	 * Create QName from String value
	 * 
	 * @param value
	 * 		a parameter to create QName
	 * @return QName
	 */
	public Object convertUponSet(Object value) {
		
		if (value == null || ((String)value).length() == 0) {
			return null;
		} else {
			return QName.createQName((String)value);
		}
	}
	
	public Class getFieldType() {
		return QName.class;
	}

}
