/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping;

import java.lang.StackTraceElement;

/**
 * CmaErrorResponse
 * 
 * This class is used to map an error response from the repository
 * (NOT SUPPORTED)
 * 
 * @author Hyanghee Lim
 *
 */
public class CmaErrorResponse {
	private int code;
	private int cmaErrorCode;
	private String name;
	private String description;
	private String message;
	private String exception;
	private String callstack;
	private String server;
	private String time;
	
	public int getCode() {
		return code;
	}
	
	public int getCmaErrorCode() {
		return cmaErrorCode;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getMessage() {
		return message;
	}
	
	public String getException() {
		return exception;
	}
	
	public String getCallstack() {
		return callstack;
	}
	
	public String getServer() {
		return server;
	}
	
	public String getTime() {
		return time;
	}
	
	public void setCode(int code) {
		this.code = code;
	}
	
	public void setCmaErrorCode(int cmaErrorCode) {
		this.cmaErrorCode = cmaErrorCode;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public void setException(String exception) {
		this.exception = exception;
	}
	
	public void setCallstack(StackTraceElement[] callstack) {
		StringBuffer buffer = new StringBuffer();
		
		if (callstack != null) {
			for (int index = 0; index < callstack.length; index++) {
				buffer.append(callstack[index].toString());
				if (index < callstack.length) {
					buffer.append("\n");
				}
			}
		}
		this.callstack = buffer.toString();
	}
	
	public void setCallstack(String callstack) {
		this.callstack = callstack;
	}
	
	public void setServer(String server) {
		this.server = server;
	}
	
	public void setTime(String time) {
		this.time = time;
	}
	
}
