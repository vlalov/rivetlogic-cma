/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.rest.api;

import java.util.List;
import java.util.Vector;

import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.httpclient.Cookie;

import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.exception.CmaRuntimeException;
import com.rivetlogic.core.cma.mapping.CmaResult;

public interface RestExecuter {
	
	/**
	 * the default maximum number of connections per host
	 */
	public static final int DEFAULT_MAX_HOST_CONNECTIONS = 25;
	
	/**
	 * the default maximum total number of connections
	 */
	public static final int DEFAULT_MAX_TOTAL_CONNECTIONS = 100;
	
	enum HttpMethod {
		GET, POST, DELETE
	}
	
	/**
	 * Execute a request to the target host and return a result using xml mapping
	 * 
	 * @param method       HttpMethod type
	 * @param targeturi    a repository uri
	 * @param mappingFile  a mapping file name for unmarshalling
	 * @param params       a collection of parameters for post method
	 * @return an object as a result of service execution 
	 * @exception InvalidTicketException if the authentication ticket is invalid
	 * @exception CmaRuntimeException if fails to get a valid response
	 *            or fails to get an unmarshalled object
	 * @exception Throwable if an exception was raised from the server 
	 */
	public CmaResult execute(HttpMethod method, String targetUri, 
			String mappingFile, Vector<NameValuePair> params)
			throws InvalidTicketException, CmaRuntimeException, Throwable;	

	/**
	 * Execute a request to the target host and return a result using xml mapping
	 * 
	 * @param method       HttpMethod type
	 * @param targeturi    a repository uri
	 * @param mappingFile  a mapping file name for unmarshalling
	 * @param params       a collection of parameters for post method
	 * @param cookies      a collection of HTTP cookies
	 * @return an object as a result of service execution 
	 * @exception InvalidTicketException if the authentication ticket is invalid
	 * @exception CmaRuntimeException if fails to get a valid response
	 *            or fails to get an unmarshalled object
	 * @exception Throwable if an exception was raised from the server 
	 */
	public CmaResult execute(HttpMethod method, String targetUri, 
			String mappingFile, Vector<NameValuePair> params, List<Cookie> cookies)
			throws InvalidTicketException, CmaRuntimeException, Throwable;	

	/**
	 * Execute a file upload request to the target host
	 * 
	 * @param method       HttpMethod type
	 * @param targeturi    a repository uri
	 * @param mappingFile  a mapping file name for unmarshalling
	 * @param params       a collection of String parameters for post method
	 * @param fileName	   a file name to upload or download
	 * @return an object as a result of service execution 
	 * @exception InvalidTicketException if the authentication ticket is invalid
	 * @exception CmaRuntimeException if fails to get a valid response
	 *            or fails to get an unmarshalled object
	 * @exception Throwable if an exception was raised from the server 
	 */
	@Deprecated
	public CmaResult execute(HttpMethod method, String targetUri, 
			String mappingFile, Vector<NameValuePair> params, String fileName)
			throws InvalidTicketException, CmaRuntimeException, Throwable;	
	
	/**
	 * Execute a file upload request to the target host
	 * 
	 * @param method       HttpMethod type
	 * @param targeturi    a repository uri
	 * @param mappingFile  a mapping file name for unmarshalling
	 * @param params       a collection of String parameters for post method
	 * @param input		   stream where to get the uploaded content 
	 * @return an object as a result of service execution
	 * @exception InvalidTicketException if the authentication ticket is invalid
	 * @exception CmaRuntimeException if fails to get a valid response
	 *            or fails to get an unmarshalled object
	 * @exception Throwable if an exception was raised from the server 
	 */
	@Deprecated
	public CmaResult execute(HttpMethod method, String targetUri, 
			String mappingFile, Vector<NameValuePair> params, InputStream input)
			throws InvalidTicketException, CmaRuntimeException, Throwable;	

	/**
	 * Execute a file upload request to the target host
	 * 
	 * @param method       HttpMethod type
	 * @param targeturi    a repository uri
	 * @param mappingFile  a mapping file name for unmarshalling
	 * @param params       a collection of String parameters for post method
	 * @param fileName	   a file name to upload or download
	 * @param contentLength The content size in bytes
	 * @return an object as a result of service execution 
	 * @exception InvalidTicketException if the authentication ticket is invalid
	 * @exception CmaRuntimeException if fails to get a valid response
	 *            or fails to get an unmarshalled object
	 * @exception Throwable if an exception was raised from the server 
	 */
	public CmaResult execute(HttpMethod method, String targetUri, 
			String mappingFile, Vector<NameValuePair> params, String fileName, 
			long contentLength)
			throws InvalidTicketException, CmaRuntimeException, Throwable;	
	
	/**
	 * Execute a file upload request to the target host
	 * 
	 * @param method       HttpMethod type
	 * @param targeturi    a repository uri
	 * @param mappingFile  a mapping file name for unmarshalling
	 * @param params       a collection of String parameters for post method
	 * @param input		   stream where to get the uploaded content 
	 * @param contentLength The content size in bytes
	 * @return an object as a result of service execution
	 * @exception InvalidTicketException if the authentication ticket is invalid
	 * @exception CmaRuntimeException if fails to get a valid response
	 *            or fails to get an unmarshalled object
	 * @exception Throwable if an exception was raised from the server 
	 */
	public CmaResult execute(HttpMethod method, String targetUri, 
			String mappingFile, Vector<NameValuePair> params, InputStream input, 
			long contentLength)
			throws InvalidTicketException, CmaRuntimeException, Throwable;	

	/**
	 * Execute a file upload request to the target host
	 * 
	 * @param method       HttpMethod type
	 * @param targeturi    a repository uri
	 * @param mappingFile  a mapping file name for unmarshalling
	 * @param params       a collection of String parameters for post method
	 * @param output	   stream where to put the downloaded content 
	 * @return an object as a result of service execution 
	 * @exception InvalidTicketException if the authentication ticket is invalid
	 * @exception CmaRuntimeException if fails to get a valid response
	 *            or fails to get an unmarshalled object
	 * @exception Throwable if an exception was raised from the server 
	 */
	public CmaResult execute(HttpMethod method, String targetUri, 
			String mappingFile, Vector<NameValuePair> params, OutputStream output)
			throws InvalidTicketException, CmaRuntimeException, Throwable;	


	/**
	 * Execute a request to the target host and return a result using serialization
	 * 
	 * @param method       HttpMethod type
	 * @param targeturi    a repository uri
	 * @param params       a collection of parameters for post method
	 * @return an object as a result of service execution 
	 * @exception InvalidTicketException if the authentication ticket is invalid
	 * @exception CmaRuntimeException if fails to get a valid response
	 *            or fails to get an unmarshalled object
	 * @exception Throwable if an exception was raised from the server 
	 */
	public CmaResult execute(HttpMethod method, String targetUri, 
			Vector<NameValuePair> params)
			throws InvalidTicketException, CmaRuntimeException, Throwable;	

	/**
	 * Execute a request to the target host and return a result using serialization
	 * 
	 * @param method       HttpMethod type
	 * @param targeturi    a repository uri
	 * @param params       a collection of parameters for post method
	 * @param cookies      a collection of HTTP cookies
	 * @return an object as a result of service execution 
	 * @exception InvalidTicketException if the authentication ticket is invalid
	 * @exception CmaRuntimeException if fails to get a valid response
	 *            or fails to get an unmarshalled object
	 * @exception Throwable if an exception was raised from the server 
	 */
	public CmaResult execute(HttpMethod method, String targetUri, 
			Vector<NameValuePair> params, List<Cookie> cookies)
			throws InvalidTicketException, CmaRuntimeException, Throwable;	

	/**
	 * Execute a file upload request to the target host return a result using serialization
	 * 
	 * @param method       HttpMethod type
	 * @param targeturi    a repository uri
	 * @param params       a collection of String parameters for post method
	 * @param fileName	   a file name to upload or download
	 * @return an object as a result of service execution 
	 * @exception InvalidTicketException if the authentication ticket is invalid
	 * @exception CmaRuntimeException if fails to get a valid response
	 *            or fails to get an unmarshalled object
	 * @exception Throwable if an exception was raised from the server 
	 */
	@Deprecated
	public CmaResult execute(HttpMethod method, String targetUri, 
			Vector<NameValuePair> params, String fileName)
			throws InvalidTicketException, CmaRuntimeException, Throwable;	

	/**
	 * Execute a file upload request to the target host return a result using serialization
	 * 
	 * @param method       HttpMethod type
	 * @param targeturi    a repository uri
	 * @param params       a collection of String parameters for post method
	 * @param fileName	   a file name to upload or download
	 * @param contentLength The content size in bytes
	 * @return an object as a result of service execution 
	 * @exception InvalidTicketException if the authentication ticket is invalid
	 * @exception CmaRuntimeException if fails to get a valid response
	 *            or fails to get an unmarshalled object
	 * @exception Throwable if an exception was raised from the server 
	 */
	public CmaResult execute(HttpMethod method, String targetUri, 
			Vector<NameValuePair> params, String fileName, long contentLength)
			throws InvalidTicketException, CmaRuntimeException, Throwable;	

	/**
	 * Execute a file upload request to the target host
	 * 
	 * @param method       HttpMethod type
	 * @param targeturi    a repository uri
	 * @param params       a collection of String parameters for post method
	 * @param input		   stream where to get the uploaded content 
	 * @return an object as a result of service execution
	 * @exception InvalidTicketException if the authentication ticket is invalid
	 * @exception CmaRuntimeException if fails to get a valid response
	 *            or fails to get an unmarshalled object
	 * @exception Throwable if an exception was raised from the server 
	 */
	@Deprecated
	public CmaResult execute(HttpMethod method, String targetUri, 
			Vector<NameValuePair> params, InputStream input)
			throws InvalidTicketException, CmaRuntimeException, Throwable;	

	/**
	 * Execute a file upload request to the target host
	 * 
	 * @param method       HttpMethod type
	 * @param targeturi    a repository uri
	 * @param params       a collection of String parameters for post method
	 * @param input		   stream where to get the uploaded content 
	 * @param contentLength The content size in bytes
	 * @return an object as a result of service execution
	 * @exception InvalidTicketException if the authentication ticket is invalid
	 * @exception CmaRuntimeException if fails to get a valid response
	 *            or fails to get an unmarshalled object
	 * @exception Throwable if an exception was raised from the server 
	 */
	public CmaResult execute(HttpMethod method, String targetUri, 
			Vector<NameValuePair> params, InputStream input, long contentLength)
			throws InvalidTicketException, CmaRuntimeException, Throwable;	
	
}