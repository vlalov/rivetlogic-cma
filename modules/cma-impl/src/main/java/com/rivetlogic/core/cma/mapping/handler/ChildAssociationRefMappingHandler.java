/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping.handler;

import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.exolab.castor.mapping.GeneralizedFieldHandler;

/**
 * This class handles creation and retrieval of ChildAssociationRef
 * 
 * @author Hyanghee Lim
 *
 */
public class ChildAssociationRefMappingHandler extends GeneralizedFieldHandler {
	
	/**
	 * Get String representation of ChildAssociationRef
	 * 
	 * @param value
	 * 		ChildAssociationRef object to retrieve information from
	 * @return string representation of ChildAssociationRef
	 */
	public Object convertUponGet(Object value) {
		if (value == null) {
			return null;
		} else {
			return value.toString();
		}
	}
	
	/**
	 * Create ChildAssociationRef from String value
	 * 
	 * @param value
	 * 		a parameter to create ChildAssociationRef
	 * @return ChildAssociationRef
	 */
	public Object convertUponSet(Object value) {

		if (value == null || ((String)value).length() == 0) {
			return null;
		} else {
			return new ChildAssociationRef( (String) value);
		}
	}
	
	public Class getFieldType() {
		return ChildAssociationRef.class;
	}


}
