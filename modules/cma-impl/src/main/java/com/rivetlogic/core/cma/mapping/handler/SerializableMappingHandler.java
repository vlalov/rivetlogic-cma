/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.mapping.handler;

import java.io.Serializable;

import org.exolab.castor.mapping.GeneralizedFieldHandler;

import com.rivetlogic.crypto.Base64;

/**
 * This class handles creation and retrieval of Serializable Object
 * 
 * @author Hyanghee Lim
 *
 */
public class SerializableMappingHandler extends GeneralizedFieldHandler {
	
	public SerializableMappingHandler() {}
	
	/**
	 * Get a string that represents a Serializable object
	 * 
	 * @param value
	 * 		Serializable object to retrieve information from
	 * @return a string that represents a Serializable object
	 */
	public Object convertUponGet(Object value) {
		
		if (value == null) {
			return "";
		} else if (value instanceof Serializable) {
			return Base64.encodeObject((Serializable)value);
		} else {
			throw new IllegalArgumentException("A Serializable object is required");
		}
	}
	
	/**
	 * Create Serializable from String value
	 * 
	 * @param value
	 * 		a parameter to create Serializable
	 * @return Serializable
	 */
	public Object convertUponSet(Object value) {
		if (value == null || ((String)value).length() == 0) {
			return null;
		} else { 
			return Base64.decodeToObject((String)value);
		}
	}
	
	public Class getFieldType() {
		return Object.class;
	}

}
