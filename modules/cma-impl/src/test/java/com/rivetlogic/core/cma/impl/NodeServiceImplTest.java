/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.Path;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.repo.Node;
import com.rivetlogic.core.cma.repo.Ticket;
import com.rivetlogic.core.cma.api.SecurityService;
import com.rivetlogic.core.cma.impl.NodeServiceImpl;

/**
 * @author Sweta Chalasani
 *
 */

public class NodeServiceImplTest extends CmaTestBase {
	
	private NodeServiceImpl nodeService = null;
	private ContentServiceImpl contentService = null;

	private String fileName= "NodeServiceCreateFile.doc";
	private String folderName ="Test " + System.currentTimeMillis();
	private String content = "createFile.doc";
	private static final Boolean recursive = true;
	private String title = "nodeServiceTest";
	private String titleFolder = "nodeServiceFolder";
	private String mimetype = "mimetype=application/msword|encoding=UTF-8";
    private String author = "Sweta";
    private String name = "nodeServiceTest " + System.currentTimeMillis();
    
    private Ticket ticket = null;
	private NodeRef destinationNodeRef = null;
	private QName assocTypeQName = ContentModel.ASSOC_CONTAINS;
	private QName assocQName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, "yournewnodeName");
	private QName nodeTypeQName = ContentModel.PROP_CONTENT;
    private QName titleQName = ContentModel.PROP_TITLE;
    private QName nameQName = ContentModel.PROP_NAME;
    private QName mimetypeQName = ContentModel.PROP_CONTENT;
    private QName authorQName = ContentModel.PROP_AUTHOR;
    
    // TODO: is this qname still usable?
    private QName memberQName = QName.createQName(ContentModel.USER_MODEL_URI, "members");
	private NodeRef nodeRef = null;
	private NodeRef result = null;
	private NodeRef originalFolder = null;
	private NodeRef destinationFolder = null;
	private NodeRef folder1 = null;
	private NodeRef folder2 = null;

	private ChildAssociationRef childNode1 = null;
	private ChildAssociationRef childNode2 = null;
	private List<QName> properties;
	private Map<QName, Serializable> nodeProperties;
	private Map<QName, Serializable> folderProperties;
	private Map<QName, Serializable> fileProperties;
	
	public void onSetUp() throws Exception {
		nodeService = (NodeServiceImpl) configurationApplicationContext.getBean("nodeService");
		contentService = (ContentServiceImpl) configurationApplicationContext.getBean("contentService");
		assertNotNull(nodeService);
		assertNotNull(contentService);
		
		ticket = getTicket();
		assertNotNull(ticket);
		
		StoreRef storeRef = new StoreRef("workspace://SpacesStore");
		NodeRef root = nodeService.getRootNode(ticket, storeRef);
		QName typeQNamePattern = QName.createQName("{http://www.alfresco.org/model/system/1.0}children");
		QName qnamePattern = QName.createQName("{http://www.alfresco.org/model/application/1.0}company_home");
		List<ChildAssociationRef> assocRefs = nodeService.getChildAssocs(ticket, root, typeQNamePattern, qnamePattern);
		destinationNodeRef = assocRefs.get(0).getChildRef();
		assertNotNull(destinationNodeRef);
		properties = new Vector<QName>();
		// add properties
		properties.add(nameQName);
		properties.add(memberQName);
	    
		fileProperties = new HashMap<QName, Serializable>(2);
	    fileProperties.put(titleQName, title);
	    fileProperties.put(authorQName, author);
	    
		folderProperties = new HashMap<QName, Serializable>(3);
	    folderProperties.put(titleQName, titleFolder);
	    folderProperties.put(authorQName, author);
	    folderProperties.put(nameQName, folderName);
	    
	    nodeProperties = new HashMap<QName, Serializable>(4);
	    List<String> members = new ArrayList<String>(2);
	    members.add("Cma Test Member 1");
	    members.add("Cma Test Member 2");
	    nodeProperties.put(titleQName, title);
	    nodeProperties.put(memberQName, (Serializable) members);
	    nodeProperties.put(mimetypeQName, mimetype);
	    nodeProperties.put(nameQName, name);
	}

	public void tearDown() {
		File file = new File(content);
		if (file.exists()) {
			file.delete();
		}
		// cleanup
		try {
			nodeService.deleteNode(ticket, nodeRef);
		} catch (Exception e) {}
		try {
			nodeService.deleteNode(ticket, result);
		} catch (Exception e) {}
		try {
			nodeService.deleteNode(ticket, childNode1.getChildRef());
		} catch (Exception e) {}
		try {
			nodeService.deleteNode(ticket, childNode2.getChildRef());
		} catch (Exception e) {}
		try {
			nodeService.deleteNode(ticket, folder1);
		} catch (Exception e) {}
		try {
			nodeService.deleteNode(ticket, folder2);
		} catch (Exception e) {}
		try {
			nodeService.deleteNode(ticket, originalFolder);
		} catch (Exception e) {}
		try {
			nodeService.deleteNode(ticket, destinationFolder);
		} catch (Exception e) {}
	}
	
	public void testGetProperties() throws Exception {
		ChildAssociationRef childAssocRef 
			= nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, nodeProperties);
		assertNotNull(childAssocRef);
		nodeRef = childAssocRef.getChildRef();
		String value = (String) nodeService.getProperty(ticket, nodeRef, titleQName);
		assertEquals(title, value);
		Map<QName, Serializable> nodePropertiesFromResult = nodeService.getProperties(ticket, nodeRef);
		assertNotNull(nodePropertiesFromResult);
		assertNotNull(nodePropertiesFromResult.get(titleQName));
		assertNotNull(nodePropertiesFromResult.get(memberQName));
	}

	
	public void testMimeType() throws Exception {
		Ticket ticket = getTicket(userid, password);
		ChildAssociationRef childAssocRef = nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, nodeProperties);
		assertNotNull(childAssocRef);
		nodeRef = childAssocRef.getChildRef();
		
		File file1 = new File(content);
		if (!file1.exists()) {
			file1.createNewFile();
		} else {
			file1.delete();
			file1.createNewFile();
		}
		FileInputStream input = new FileInputStream(content);
		contentService.writeContentFromStream(ticket, nodeRef, nodeTypeQName, input);
		input.close();
		QName propertyQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}content");
		String mimeType = nodeService.getMimeType(ticket, nodeRef, propertyQName);
		String encoding = nodeService.getEncoding(ticket, nodeRef, propertyQName);
		assertNotNull(mimeType);
		assertNotNull(encoding);
	}
	

	public void testCreateFileFromStream() throws Exception {
		File file = new File(content);
		if (!file.exists()) {
			file.createNewFile();
		} else {
			file.delete();
			file.createNewFile();
		}
		FileWriter writer = new FileWriter(file);
		writer.write("test");
		writer.close();
		Ticket ticket = getTicket(userid, password);
		FileInputStream input = new FileInputStream(content);
		result = nodeService.createFileFromStream(ticket, fileName, destinationNodeRef, fileProperties, input);
		input.close();
	}
	
	
	public void testCreateFileFromCreateNode() throws Exception {
		File file = new File(content);
		if (!file.exists()) {
			file.createNewFile();
		} else {
			file.delete();
			file.createNewFile();
		}
		FileWriter writer = new FileWriter(file);
		writer.write("test");
		writer.close();
		Ticket ticket = getTicket(userid, password);
		ChildAssociationRef childAssocRef = nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, nodeProperties);
		result = childAssocRef.getChildRef();
		FileInputStream input = new FileInputStream(content);
		contentService.writeContentFromStream(ticket, result, nodeTypeQName, input);
		input.close();
	}
	
	
	public void testCreateFile() throws Exception {
		Ticket ticket = getTicket(userid, password);
			File file = new File(content);
		if (!file.exists()) {
			file.createNewFile();
		} else {
			file.delete();
			file.createNewFile();
		}
		FileWriter writer = new FileWriter(file);
		writer.write("test");
		writer.close();
		result = nodeService.createFile(ticket, fileName, destinationNodeRef, fileProperties, content);
		assertNotNull(result);
	}

	public void testCreateNodeAndFolder() throws Exception {
		Ticket ticket = getTicket(userid, password);
		ChildAssociationRef childAssocRef = nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, nodeProperties);
		assertNotNull(childAssocRef);
		nodeRef = childAssocRef.getChildRef();
		result = nodeService.createFolder(ticket, folderName, destinationNodeRef, folderProperties);
		assertNotNull(result);
		nodeService.getChildByName(ticket, result, assocTypeQName, "TEST");
	}
	
	public void testGetPath() throws Exception {
		Ticket ticket = getTicket(userid, password);
		ChildAssociationRef childAssocRef = nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, nodeProperties);
		assertNotNull(childAssocRef);
		nodeRef = childAssocRef.getChildRef();
		Path path = nodeService.getPath(ticket, nodeRef);
		assertNotNull(path);
		nodeService.setProperties(ticket, nodeRef, nodeProperties);
		nodeService.getProperty(ticket, nodeRef, QName.createQName("{http://www.alfresco.org/model/content/1.0}name"));
		nodeService.removeProperty(ticket, nodeRef, QName.createQName("{http://www.alfresco.org/model/content/1.0}author"));
	}

	public void testAspects() throws Exception {
		ChildAssociationRef childAssocRef = nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, nodeProperties);
		assertNotNull(childAssocRef);
		nodeRef = childAssocRef.getChildRef();
		nodeService.addAspect(ticket, nodeRef, QName.createQName("{http://www.alfresco.org/model/content/1.0}versionable"));
		assertTrue(nodeService.hasAspect(ticket, nodeRef, QName.createQName("{http://www.alfresco.org/model/content/1.0}versionable")));
		nodeService.getAspects(ticket, nodeRef);
		nodeService.removeAspect(ticket, nodeRef, QName.createQName("{http://www.alfresco.org/model/content/1.0}versionable"));
		assertFalse(nodeService.hasAspect(ticket, nodeRef, QName.createQName("{http://www.alfresco.org/model/content/1.0}versionable")));
		
	}
	
	public void testGetStores() throws Exception {
		List<StoreRef> stores = nodeService.getStores(ticket);
		assertNotNull(stores);
	}
	
	public void testGetNode() throws Exception {
		ChildAssociationRef childAssocRef = nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, nodeProperties);
		assertNotNull(childAssocRef);
		nodeRef = childAssocRef.getChildRef();
		nodeService.setProperties(ticket, nodeRef, nodeProperties);
		Node node = nodeService.getNode(ticket, nodeRef, properties, true, true, true, true);
		assertNotNull(node);
		
		Map<QName, Serializable> propertiesMap = node.getProperties();
		List<String> members = (List<String>) propertiesMap.get(memberQName);
		assertNotNull(members);
		assertEquals(2, members.size());
		assertTrue(node.getPermissions().size() > 0);
		QName type = nodeService.getType(ticket, nodeRef);
		assertEquals(ContentModel.TYPE_CONTENT, type);
		
		List<String> requiredPermissions = new ArrayList<String>(4);
		requiredPermissions.add(SecurityService.READ_CONTENT);
		requiredPermissions.add(SecurityService.WRITE_CONTENT);
		requiredPermissions.add(SecurityService.WRITE_PROPERTIES);
		requiredPermissions.add(SecurityService.DELETE);
		
		node = nodeService.getNode(ticket, nodeRef, properties,  
				true, true, true, true, requiredPermissions);
		assertNotNull(node);
		
		propertiesMap = node.getProperties();
		members = (List<String>) propertiesMap.get(memberQName);
		assertNotNull(members);
		assertEquals(2, members.size());
		assertTrue(node.getPermissions().size() > 0);
		type = nodeService.getType(ticket, nodeRef);
		assertEquals(ContentModel.TYPE_CONTENT, type);
		assertTrue(node.getPermissionsToCheckFor().size() == 4);
	}
	
	public void testCopy() throws Exception {
		ChildAssociationRef childAssocRef = nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, nodeProperties);
		assertNotNull(childAssocRef);
		nodeRef = childAssocRef.getChildRef();
		folder1 = nodeService.createFolder(ticket, folderName, destinationNodeRef, folderProperties);
		result = nodeService.copyNode(ticket, nodeRef, folder1, recursive);
	}
	
	public void testMove() throws Exception {
		ChildAssociationRef childAssocRef = nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, nodeProperties);
		assertNotNull(childAssocRef);
		nodeRef = childAssocRef.getChildRef();
		folder1 = nodeService.createFolder(ticket, folderName, destinationNodeRef, folderProperties);
		result = nodeService.moveNode(ticket, nodeRef, folder1);
		List<ChildAssociationRef> children = nodeService.getChildAssocs(ticket, destinationNodeRef);
		assertFalse(children.contains(childAssocRef));
	}
	
	public void testCopyNodes() throws Exception {
	    folderProperties.put(nameQName, "original folder " + System.currentTimeMillis());
		originalFolder = nodeService.createFolder(ticket, folderName, destinationNodeRef, folderProperties);
	    folderProperties.put(nameQName, "destination folder " + System.currentTimeMillis());
		destinationFolder = nodeService.createFolder(ticket, folderName, destinationNodeRef, folderProperties);
		
	    Map<QName, Serializable> nodeProperties1 = new HashMap<QName, Serializable>(4);
	    nodeProperties1.put(titleQName, title);
	    nodeProperties1.put(authorQName, author);
	    nodeProperties1.put(mimetypeQName, mimetype);
	    nodeProperties1.put(nameQName, "child node name 1 " + System.currentTimeMillis());

	    Map<QName, Serializable> nodeProperties2 = new HashMap<QName, Serializable>(4);
	    nodeProperties2.put(titleQName, title);
	    nodeProperties2.put(authorQName, author);
	    nodeProperties2.put(mimetypeQName, mimetype);
	    nodeProperties2.put(nameQName, "child node name 2 " + System.currentTimeMillis());
	    
		childNode1 = nodeService.createNode(ticket, originalFolder, assocTypeQName, assocQName, nodeTypeQName, nodeProperties1);
		childNode2 = nodeService.createNode(ticket, originalFolder, assocTypeQName, assocQName, nodeTypeQName, nodeProperties2);
		
		List<NodeRef> childRefs = new Vector<NodeRef>(2);
		childRefs.add(childNode1.getChildRef());
		childRefs.add(childNode2.getChildRef());
		
		List<NodeRef> copiedNodes = nodeService.copyNodes(ticket, childRefs, destinationFolder, recursive);
		assertEquals(2, copiedNodes.size());
		List<ChildAssociationRef> childern = nodeService.getChildAssocs(ticket, destinationFolder);
		assertEquals(2, childern.size());
		nodeService.deleteNode(ticket, copiedNodes.get(0));
		nodeService.deleteNode(ticket, copiedNodes.get(1));
	}
	
	public void testMoveNodes() throws Exception {
	    folderProperties.put(nameQName, "original folder " + System.currentTimeMillis());
		originalFolder = nodeService.createFolder(ticket, folderName, destinationNodeRef, folderProperties);
	    folderProperties.put(nameQName, "destination folder " + System.currentTimeMillis());
		destinationFolder = nodeService.createFolder(ticket, folderName, destinationNodeRef, folderProperties);
		
	    Map<QName, Serializable> nodeProperties1 = new HashMap<QName, Serializable>(4);
	    nodeProperties1.put(titleQName, title);
	    nodeProperties1.put(authorQName, author);
	    nodeProperties1.put(mimetypeQName, mimetype);
	    nodeProperties1.put(nameQName, "child node name 1 " + System.currentTimeMillis());

	    Map<QName, Serializable> nodeProperties2 = new HashMap<QName, Serializable>(4);
	    nodeProperties2.put(titleQName, title);
	    nodeProperties2.put(authorQName, author);
	    nodeProperties2.put(mimetypeQName, mimetype);
	    nodeProperties2.put(nameQName, "child node name 2 " + System.currentTimeMillis());
	    
		childNode1 = nodeService.createNode(ticket, originalFolder, assocTypeQName, assocQName, nodeTypeQName, nodeProperties1);
		childNode2 = nodeService.createNode(ticket, originalFolder, assocTypeQName, assocQName, nodeTypeQName, nodeProperties2);
		
		List<NodeRef> childRefs = new Vector<NodeRef>(2);
		childRefs.add(childNode1.getChildRef());
		childRefs.add(childNode2.getChildRef());
		
		List<NodeRef> movedNodes = nodeService.moveNodes(ticket, childRefs, destinationFolder);
		assertEquals(2, movedNodes.size());
		List<ChildAssociationRef> childern = nodeService.getChildAssocs(ticket, originalFolder);
		if (childern != null) {
			assertEquals(0, childern.size());
		}
		
		childern = nodeService.getChildAssocs(ticket, destinationFolder);
		assertEquals(2, childern.size());
		
		nodeService.deleteNode(ticket, childern.get(0).getChildRef());
		nodeService.deleteNode(ticket, childern.get(1).getChildRef());
	}
	
	public void testAssociations() throws Exception {
		folder1 = nodeService.createFolder(ticket, folderName, destinationNodeRef, folderProperties);
		folderProperties.put(ContentModel.PROP_NAME, "Test " + System.currentTimeMillis());
		folder2 = nodeService.createFolder(ticket, folderName, destinationNodeRef, folderProperties);
		ChildAssociationRef childAssoc = nodeService.createNode(ticket, folder1, assocTypeQName, assocQName, nodeTypeQName, nodeProperties);
		nodeRef = childAssoc.getChildRef();
		AssociationRef assoc = nodeService.createAssociation(ticket, folder2, nodeRef, ContentModel.ASSOC_CONTAINS);
		List<AssociationRef> assocs = nodeService.getTargetAssocs(ticket, folder2, ContentModel.ASSOC_CONTAINS);
		assertNotNull(assocs);
		assertTrue(assocs.contains(assoc));
		nodeService.removeAssociation(ticket, folder2, nodeRef, ContentModel.ASSOC_CONTAINS);
		assocs = nodeService.getTargetAssocs(ticket, folder2, ContentModel.ASSOC_CONTAINS);
		assertNull(assocs);
	}

	public void testParentChildAssociation() throws Exception {
		
		ChildAssociationRef childAssocRef 
			= nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, nodeProperties);
		nodeRef = childAssocRef.getChildRef();
		folder1 = nodeService.createFolder(ticket, folderName, destinationNodeRef, folderProperties);
		nodeService.setType(ticket, folder1, ContentModel.TYPE_CONTAINER);
		nodeService.addChild(ticket, folder1, nodeRef, ContentModel.ASSOC_CHILDREN, ContentModel.ASSOC_CHILDREN);
		List<ChildAssociationRef> children = nodeService.getChildAssocs(ticket, folder1);
		List<ChildAssociationRef> parents = nodeService.getParentAssocs(ticket, nodeRef);
		assertEquals(1, children.size());
		assertEquals(2, parents.size());
		nodeService.removeChildAssociation(ticket, children.get(0));
	}
	
	
	public void testdeleteAndRestore() throws Exception {
		Ticket ticket = getTicket(userid, password);
		ChildAssociationRef childAssocRef = nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, nodeProperties);
		nodeRef = childAssocRef.getChildRef();
		nodeService.exists(ticket, childAssocRef.getChildRef());
		nodeService.getStoreArchiveNode(ticket, new StoreRef("workspace://SpacesStore"));
		nodeService.getParentAssocs(ticket, childAssocRef.getChildRef());
		nodeService.getPrimaryParent(ticket, childAssocRef.getChildRef());
		String id = childAssocRef.getChildRef().getId();
		nodeService.deleteNode(ticket, childAssocRef.getChildRef());
		StoreRef storeRefArchived = new StoreRef("archive://SpacesStore");
		NodeRef archived = new NodeRef(storeRefArchived, id);
		result = nodeService.restoreNode(ticket, archived, destinationNodeRef, QName.createQName("{http://www.alfresco.org/model/content/1.0}contains"), QName.createQName("{http://www.alfresco.org/model/content/1.0}", "archive"));
		assertNotNull(result);
	}
}
