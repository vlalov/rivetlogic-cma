/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.Iterator;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.security.AccessPermission;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.repo.Node;
import com.rivetlogic.core.cma.repo.SortDefinition;
import com.rivetlogic.core.cma.repo.Ticket;
import com.rivetlogic.core.cma.api.SearchService;
import com.rivetlogic.core.cma.impl.SearchServiceImpl;

import com.rivetlogic.core.cma.api.SecurityService;

/**
 * test searchService methods
 * 
 * @author Hyanghee Lim
 *
 */
public class SearchServiceImplTest extends CmaTestBase {

	private String storeName = "workspace://SpacesStore";
	
	private QName name = QName.createQName("{http://www.alfresco.org/model/content/1.0}name");
	private QName created = QName.createQName("{http://www.alfresco.org/model/content/1.0}created");
	//private QName content = QName.createQName("{http://www.alfresco.org/model/content/1.0}content");
	
	// query parameters @cm\:abstract.mimetype:"text/plain"
	private String queryTerm = "@cm\\:name:\"Cma SearchService Test Node \"";
	boolean returnAllProperties = true;
	boolean returnPeerAssocs = true;
	boolean returnChildAssocs = true;
	boolean returnAspects = true;
	boolean returnPermissions = true;
	int start = 1, end = 2, max = 3;
	
	private String userName = "SearchServiceCmaTestUser";
	private SearchServiceImpl searchService = null;
	private SecurityServiceImpl securityService = null;
	
	boolean printEnabled = false;
	
	private NodeRef nodeRef1 = null;
	private NodeRef nodeRef2 = null;
	private NodeRef nodeRef3 = null;
	
	private NodeRef userRef = null; 	
	StoreRef store = null;
	List<QName> properties;
	Ticket userTicket = null;
	List<SortDefinition> sortDefinitions;
	
	public void onSetUp() throws Exception {
		searchService = (SearchServiceImpl) configurationApplicationContext.getBean("searchService");
		// FIXME: create nodes before testing
		securityService = (SecurityServiceImpl) configurationApplicationContext.getBean("securityService");
				
		nodeRef1 = createNode("Cma SearchService Test Node " +  System.currentTimeMillis());
		nodeRef2 = createNode("Cma SearchService Test Node " + System.currentTimeMillis() + 1);
		nodeRef3 = createNode("Cma SearchService Test Node " + System.currentTimeMillis() + 2);
		
		userRef = createNewUser(userName);
		
		store = new StoreRef(storeName);
		properties = new Vector<QName>();
		// add properties
		properties.add(name);
		properties.add(created);
		
		sortDefinitions = new Vector<SortDefinition>();
		sortDefinitions.add(new SortDefinition(SortDefinition.SortType.FIELD, "@{http://www.alfresco.org/model/content/1.0}name", false));
	}
	
	public void tearDown() {
		try {
			peopleService.deletePerson(ticket, userRef);
		} catch (Exception e) {}
		try {
			nodeService.deleteNode(ticket, nodeRef1);
		} catch (Exception e) {}
		try {
			nodeService.deleteNode(ticket, nodeRef2);
		} catch (Exception e) {}
		try {
			nodeService.deleteNode(ticket, nodeRef3);
		} catch (Exception e) {}
	}

	@SuppressWarnings("deprecation")
	public void testSearchService() throws Exception {
		// FIXME: create nodes prior to testing and search for the nodes
		userTicket = getTicket(userName, userName);
		
		securityService.setInheritParentPermissions(ticket, nodeRef1, false);
		securityService.setPermission(ticket, nodeRef1, userName, SecurityService.CONSUMER, true);
		
		// call search
		List<Node> nodes = searchService.query(userTicket, store,
								SearchService.QueryLanguage.lucene, queryTerm, 
								returnAllProperties, returnPeerAssocs,
								returnChildAssocs, returnAspects, returnPermissions, start, end, sortDefinitions);
		if (printEnabled)
			printNodes("return all properteis with start and end", nodes);
		assertEquals(end - start + 1, nodes.size());
		assertTrue(nodes.get(0).getPermissions().size() > 0);
		
		searchService.query(userTicket, store,
				SearchService.QueryLanguage.lucene, queryTerm, 
				returnAllProperties, returnPeerAssocs,
				returnChildAssocs, returnAspects, returnPermissions, start, end);
		
		if (printEnabled)
		printNodes("return all properteis with start and end", nodes);
		assertEquals(end - start + 1, nodes.size());
		assertTrue(nodes.get(0).getPermissions().size() > 0);
		
		List<SortDefinition> sortDefinitions2 = new Vector<SortDefinition>();
		sortDefinitions2.add(new SortDefinition(SortDefinition.SortType.FIELD, "@{http://www.alfresco.org/model/content/1.0}name", true));
		
		// call search
		nodes = searchService.query(userTicket, store,
							SearchService.QueryLanguage.lucene, queryTerm,  
							returnAllProperties, returnPeerAssocs,
							returnChildAssocs, returnAspects, returnPermissions, max, sortDefinitions2);
		if (printEnabled)
			printNodes("return all properteis with max", nodes);
		assertEquals(max, nodes.size());
		assertTrue(nodes.get(0).getPermissions().size() > 0);
		
		// call search
		nodes = searchService.query(userTicket, store,
							SearchService.QueryLanguage.lucene, queryTerm,  
							returnAllProperties, returnPeerAssocs,
							returnChildAssocs, returnAspects, returnPermissions, max);
		if (printEnabled)
			printNodes("return all properteis with max", nodes);
		assertEquals(max, nodes.size());
		assertTrue(nodes.get(0).getPermissions().size() > 0);
		
		// call search
		nodes = searchService.query(userTicket, store,
					SearchService.QueryLanguage.lucene, queryTerm, 
					properties, returnPeerAssocs, returnChildAssocs,
					returnAspects, returnPermissions, start, end, sortDefinitions);
		if (printEnabled)
			printNodes("return some properties with start and end", nodes);
		assertEquals(end - start + 1, nodes.size());
		Node node = (Node) nodes.get(0);
		assertEquals(properties.size(), node.getProperties().size());
		assertTrue(nodes.get(0).getPermissions().size() > 0);

		// call search
		nodes = searchService.query(userTicket, store,
					SearchService.QueryLanguage.lucene, queryTerm, 
					properties, returnPeerAssocs, returnChildAssocs,
					returnAspects, returnPermissions, start, end);
		if (printEnabled)
			printNodes("return some properties with start and end", nodes);
		assertEquals(end - start + 1, nodes.size());
		node = (Node) nodes.get(0);
		assertEquals(properties.size(), node.getProperties().size());
		assertTrue(nodes.get(0).getPermissions().size() > 0);
		
		// call search
		nodes = searchService.query(userTicket, store,
					SearchService.QueryLanguage.lucene, queryTerm,  
					properties, returnPeerAssocs, returnChildAssocs,
					returnAspects, true, max, sortDefinitions);
		
		if (printEnabled)
			printNodes("return some properties with max", nodes);
		assertEquals(max, nodes.size());
		node = (Node) nodes.get(0);
		assertEquals(properties.size(), node.getProperties().size());
		assertTrue(nodes.get(0).getPermissions().size() > 0);

		// call search
		nodes = searchService.query(userTicket, store,
					SearchService.QueryLanguage.lucene, queryTerm,  
					properties, returnPeerAssocs, returnChildAssocs,
					returnAspects, false, max);
		
		if (printEnabled)
			printNodes("return some properties with max", nodes);
		assertEquals(max, nodes.size());
		node = (Node) nodes.get(0);
		assertEquals(properties.size(), node.getProperties().size());
		assertTrue(nodes.get(0).getPermissions() == null);
		
	}
	
	
	public void testNewSearch() throws Exception {
		List<Node> nodes;
		Node node;
		ticket = getTicket();
		
		List<String> requiredPermissions = new ArrayList<String>(4);
		requiredPermissions.add(SecurityService.READ_CONTENT);
		requiredPermissions.add(SecurityService.WRITE_CONTENT);
		requiredPermissions.add(SecurityService.WRITE_PROPERTIES);
		requiredPermissions.add(SecurityService.DELETE);
		// call search
		nodes = searchService.query(ticket, store,
					SearchService.QueryLanguage.lucene, queryTerm,  
					properties, returnPeerAssocs, returnChildAssocs,
					returnAspects, true, requiredPermissions,
					max, sortDefinitions);
		
		if (printEnabled)
			printNodes("return some properties with max", nodes);
		assertEquals(max, nodes.size());
		node = (Node) nodes.get(0);
		assertEquals(properties.size(), node.getProperties().size());
		assertTrue(nodes.get(0).getPermissions().size() > 0);
		assertTrue(nodes.get(0).getPermissionsToCheckFor().size() == 4);
		
		// call search
		nodes = searchService.query(ticket, store,
					SearchService.QueryLanguage.lucene, queryTerm,  
					returnAllProperties, returnPeerAssocs, returnChildAssocs,
					returnAspects, false, requiredPermissions,
					max, sortDefinitions);
		
		if (printEnabled)
			printNodes("return some properties with max", nodes);
		assertEquals(max, nodes.size());
		node = (Node) nodes.get(0);
		assertTrue(nodes.get(0).getPermissions() == null);
		assertTrue(nodes.get(0).getPermissionsToCheckFor().size() == 4);
		
		// call search
		nodes = searchService.query(ticket, store,
					SearchService.QueryLanguage.lucene, queryTerm,  
					returnAllProperties, returnPeerAssocs, returnChildAssocs,
					returnAspects, true, requiredPermissions,
					start, end, sortDefinitions);
		
		if (printEnabled)
			printNodes("return some properties with max", nodes);
		assertEquals(end - start + 1, nodes.size());
		node = (Node) nodes.get(0);
		assertTrue(nodes.get(0).getPermissions().size() > 0);
		assertTrue(nodes.get(0).getPermissionsToCheckFor().size() == 4);
		
		// call search
		nodes = searchService.query(ticket, store,
					SearchService.QueryLanguage.lucene, queryTerm,  
					properties, returnPeerAssocs, returnChildAssocs,
					returnAspects, false, requiredPermissions,
					start, end, sortDefinitions);
		
		if (printEnabled)
			printNodes("return some properties with max", nodes);
		assertEquals(end - start + 1, nodes.size());
		assertEquals(properties.size(), nodes.get(0).getProperties().size());
		node = (Node) nodes.get(0);
		assertTrue(nodes.get(0).getPermissions() == null);
		assertTrue(nodes.get(0).getPermissionsToCheckFor().size() == 4);
		
	}
	
	public void printNodes(String testName, List<Node> nodes) {
		System.out.println(testName);
		// print the result
		if (nodes != null) {
			Iterator<Node> itr = nodes.iterator();

			while (itr.hasNext()) {
				Node node = (Node) itr.next();
				System.out.println((node.getNodeRef()) + ", " + node.getType() + ", " + node.getProperties().get(name)
						+ ", " + node.getProperties().get(created) );
				printCollection("permission", node.getPermissions());
			}
		} else {
			System.out.println("no result");
		}
	}

	public void printCollection(String name, Collection collection) {
		System.out.println("printing a collection of " + name);
		if (collection != null) {
			Iterator itr = collection.iterator();
			while(itr.hasNext()) {
				System.out.println("- " + itr.next());
			}
		} else {
			System.out.println(name + " is empty");
		}
		System.out.println("");
	}
	
}
