/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import org.alfresco.service.cmr.repository.NodeRef;

import com.rivetlogic.core.cma.exception.AuthenticationFailure;
import com.rivetlogic.core.cma.exception.InvalidTicketException;
import com.rivetlogic.core.cma.repo.Ticket;


/**
 * test authentication service methods
 * 
 * @author Hyanghee Lim
 *
 */
public class AuthenticationServiceImplTest extends CmaTestBase {
	
	private String authorityUser1 = "SecurityServiceCmaTestUser " + System.currentTimeMillis();
	private String authorityUser2 = "SecurityServiceCmaTestUser " + System.currentTimeMillis();
	private NodeRef userRef = null;
	
	public void testAuthenticate() throws Exception {
		assertNotNull(ticket);
	}
	
	public void testValidateTicket() throws Exception {
		try {
			authenticationService.validate(ticket);
		} catch (Exception e){
			e.printStackTrace();
			fail();
		}
	}
	
	public void testInvalidateTicket() throws Exception {
		try {
			authenticationService.invalidateTicket(ticket);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		ticket = getTicket();
	}
	
	public void testChangePassword() throws Exception {
		userRef = createNewUser(authorityUser1);
		Ticket userTicket = getTicket(authorityUser1, authorityUser1);
		String newPassword = "newPassword";
		authenticationService.changePassword(userTicket, authorityUser1, 
				authorityUser1.toCharArray(), newPassword.toCharArray());
		try {
			userTicket = authenticationService.authenticate(
					repositoryUri, authorityUser1, newPassword.toCharArray());
			assertNotNull(userTicket);
		} catch (AuthenticationFailure e) {
			// passed
			fail();
		}
	}
	
	public void testSetPassword() throws Exception {
		userRef = createNewUser(authorityUser1);
		String password = "newPassword";
		authenticationService.setPassword(ticket, authorityUser1, 
				password.toCharArray());
		try {
			Ticket userTicket = authenticationService.authenticate(
					repositoryUri, authorityUser1, password.toCharArray());
			assertNotNull(userTicket);
		} catch (AuthenticationFailure e) {
			// passed
			fail();
		}
	}
	
	public void testCreateAndDeleteAuthentication() throws Exception {
		userRef = createNewUser(authorityUser1);
		authenticationService.deleteAuthentication(ticket, authorityUser1);
		try {
			Ticket userTicket = authenticationService.authenticate(
					repositoryUri, authorityUser1, authorityUser1.toCharArray());
			fail();
		} catch (AuthenticationFailure e) {
			// passed
		}
		String password = "newPassword";
		authenticationService.createAuthentication(ticket, authorityUser1, password.toCharArray());
		try {
			Ticket userTicket = authenticationService.authenticate(
					repositoryUri, authorityUser1, password.toCharArray());
			assertNotNull(userTicket);
		} catch (AuthenticationFailure e) {
			e.printStackTrace();
			fail();
		}
	}
	
	public void testSetAndGetAuthenticationEnabled() throws Exception {
		userRef = createNewUser(authorityUser2);
		boolean enabled = authenticationService.getAuthenticationEnabled(ticket, authorityUser2);
		assertTrue(enabled);
		authenticationService.setAuthenticationEnabled(ticket, authorityUser2, false);
		enabled = authenticationService.getAuthenticationEnabled(ticket, authorityUser2);
		assertFalse(enabled);
		try {
			Ticket userTicket = authenticationService.authenticate(
					repositoryUri, authorityUser2, authorityUser2.toCharArray());
			fail();
		} catch (AuthenticationFailure e) {
			// pass
		}
	}
	
	public void testInvalidateUserSession() throws Exception {
		userRef = createNewUser(authorityUser1);
		Ticket userTicket = authenticationService.authenticate(
				repositoryUri, authorityUser1, authorityUser1.toCharArray());
		authenticationService.invalidateUserSession(ticket, authorityUser1);
		try {
			authenticationService.validate(userTicket);
			fail();
		} catch (InvalidTicketException e) {
			// passed
		}
	}
	
	
	public void tearDown() {
		try {
			peopleService.deletePerson(ticket, userRef);
		} catch (Exception e) {}
	}	
}