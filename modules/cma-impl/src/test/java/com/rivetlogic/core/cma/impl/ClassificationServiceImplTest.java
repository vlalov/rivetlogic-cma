/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.util.Collection;
import java.util.List;

import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.CategoryService.Depth;
import org.alfresco.service.cmr.search.CategoryService.Mode;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.repo.Ticket;


/**
 * @author Sweta Chalasani
 *
 */
public class ClassificationServiceImplTest extends CmaTestBase {
	
	private ClassificationServiceImpl classificationService = null;
	private NodeServiceImpl nodeService = null;
	private Ticket ticket = null;
	
	private NodeRef nodeRef = null;
	private NodeRef destinationNodeRef = null;
	private StoreRef storeRef = null;
	private NodeRef categoryNodeRef = null;
	private NodeRef categoryRef = null;
	private NodeRef result = null;
	private String storeRefStr = "workspace://SpacesStore";

	public void onSetUp() throws Exception {
		classificationService = (ClassificationServiceImpl) configurationApplicationContext.getBean("classificationService");
		assertNotNull(classificationService);
		nodeService = (NodeServiceImpl) configurationApplicationContext.getBean("nodeService");
		assertNotNull(nodeService);
		ticket = getTicket();
		assertNotNull(ticket);
		
		StoreRef storeRef = new StoreRef("workspace://SpacesStore");
		NodeRef root = nodeService.getRootNode(ticket, storeRef);
		QName typeQNamePattern = QName.createQName("{http://www.alfresco.org/model/system/1.0}children");
		QName qnamePattern = QName.createQName("{http://www.alfresco.org/model/content/1.0}categoryRoot");
		List<ChildAssociationRef> assocRefs = nodeService.getChildAssocs(ticket, root, typeQNamePattern, qnamePattern);
		destinationNodeRef = assocRefs.get(0).getChildRef();
		
		QName typeQNamePattern1 = QName.createQName("{http://www.alfresco.org/model/content/1.0}categories");
		QName qnamePattern1 = QName.createQName("{http://www.alfresco.org/model/content/1.0}generalclassifiable");
		List<ChildAssociationRef> categoryAssocRefs = nodeService.getChildAssocs(ticket, destinationNodeRef, typeQNamePattern1, qnamePattern1);
		categoryNodeRef = categoryAssocRefs.get(0).getChildRef();
		
		QName typeQNamePattern2 = QName.createQName("{http://www.alfresco.org/model/content/1.0}subcategories");
		QName qnamePattern2 = QName.createQName("{http://www.alfresco.org/model/content/1.0}Languages");
		List<ChildAssociationRef> subcategoryAssocRefs = nodeService.getChildAssocs(ticket, categoryNodeRef, typeQNamePattern2, qnamePattern2);
		nodeRef = subcategoryAssocRefs.get(0).getChildRef();
		
		QName typeQNamePattern3 = QName.createQName("{http://www.alfresco.org/model/content/1.0}subcategories");
		QName qnamePattern3 = QName.createQName("{http://www.alfresco.org/model/content/1.0}English");
		List<ChildAssociationRef> subcategoriesAssocRefs = nodeService.getChildAssocs(ticket, nodeRef, typeQNamePattern3, qnamePattern3);
		categoryRef = subcategoriesAssocRefs.get(0).getChildRef();
	}

	public void tearDown() {
		try {
			classificationService.deleteCategory(ticket, result);
		} catch (Exception e) {}
	}
	
	public void testCreateCategory() throws Exception {
		Ticket ticket = getTicket(userid, password);
		result = classificationService.createCategory(ticket, nodeRef, "classificationServiceTestLanguage");
		assertNotNull(result);
	}
	
	public void testCreateRootCategory() throws Exception {
		Ticket ticket = getTicket(userid, password);
		storeRef = new StoreRef(storeRefStr);
		result = classificationService.createRootCategory(ticket, storeRef, 
					QName.createQName("{http://www.alfresco.org/model/content/1.0}generalclassifiable"), 
					"classificationService testCategory");
		assertNotNull(result);
	}
		
	public void testGetCategories() throws Exception {
		Ticket ticket = getTicket(userid, password);
		storeRef = new StoreRef(storeRefStr);
		Collection<ChildAssociationRef> result 
			= classificationService.getCategories(ticket, storeRef, 
					QName.createQName("{http://www.alfresco.org/model/content/1.0}generalclassifiable"), 
					Depth.ANY);
		assertNotNull(result);
	}
	
	public void testGetChildren() throws Exception {
		Ticket ticket = getTicket(userid, password);
		Collection<ChildAssociationRef> result = classificationService.getChildren(ticket, categoryRef, Mode.ALL, Depth.IMMEDIATE);
		assertNotNull(result);
	}
	
	public void testGetClassificationAspects() throws Exception {
		Ticket ticket = getTicket(userid, password);
		Collection<QName> result = classificationService.getClassificationAspects(ticket);
		assertNotNull(result);
	}
	
	public void testGetRootCategories() throws Exception {
		storeRef = new StoreRef(storeRefStr);
		Ticket ticket = getTicket(userid, password);
		Collection<ChildAssociationRef> result 
			= classificationService.getRootCategories(ticket, storeRef, 
					QName.createQName("{http://www.alfresco.org/model/content/1.0}generalclassifiable"));
		assertNotNull(result);
	}
	
	public void testGetClassifications() throws Exception {
		storeRef = new StoreRef(storeRefStr);
		Ticket ticket = getTicket(userid, password);
		Collection<ChildAssociationRef> result = classificationService.getClassifications(ticket, storeRef);
		assertNotNull(result);
	}
}
