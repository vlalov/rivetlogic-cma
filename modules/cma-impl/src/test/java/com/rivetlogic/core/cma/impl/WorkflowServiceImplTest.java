/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.workflow.WorkflowModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.workflow.WorkflowDefinition;
import org.alfresco.service.cmr.workflow.WorkflowPath;
import org.alfresco.service.cmr.workflow.WorkflowTask;
import org.alfresco.service.cmr.workflow.WorkflowTaskState;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.repo.Ticket;

/**
 * @author Sweta Chalasani
 *
 */
public class WorkflowServiceImplTest extends CmaTestBase {
	
	private WorkflowServiceImpl workflowService = null;
	private NodeServiceImpl nodeService = null;
	private PeopleServiceImpl peopleService = null;

	private NodeRef nodeRef = null;
	private Ticket ticket = null;
	private NodeRef folderRef = null;
	private NodeRef packageRef = null;
	
	private String title = "workflowServiceTest";
    private String author = "Sweta";
    NodeRef testNodeRef;
    
	private NodeRef destinationNodeRef = null;
	private QName assocTypeQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}contains");
	private QName assocQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}", "yournewnodeName");
	private QName nodeTypeQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}content");
    private QName titleQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}title"); 
    private QName authorQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}author");
	private QName packageQName = QName.createQName("{http://www.alfresco.org/model/bpm/1.0}package");
	private Map<QName, Serializable> nodeProperties;
	
	Map<QName, Serializable> params = new HashMap<QName, Serializable>();
	public void onSetUp() throws Exception{
		workflowService = (WorkflowServiceImpl) configurationApplicationContext.getBean("workflowService");
		assertNotNull(workflowService);
		nodeService = (NodeServiceImpl) configurationApplicationContext.getBean("nodeService");
		assertNotNull(nodeService);
		peopleService = (PeopleServiceImpl) configurationApplicationContext.getBean("peopleService");
		assertNotNull(peopleService);
		ticket=getTicket(userid, password);
		
		StoreRef storeRef = new StoreRef("workspace://SpacesStore");
		NodeRef root = nodeService.getRootNode(ticket, storeRef);
		QName typeQNamePattern = QName.createQName("{http://www.alfresco.org/model/system/1.0}children");
		QName qnamePattern = QName.createQName("{http://www.alfresco.org/model/application/1.0}company_home");
		List<ChildAssociationRef> assocRefs = nodeService.getChildAssocs(ticket, root, typeQNamePattern, qnamePattern);
		destinationNodeRef = assocRefs.get(0).getChildRef();
		assertNotNull(destinationNodeRef);
	    
		folderRef = nodeService.createFolder(ticket, "WfTestFoldertest2 " + System.currentTimeMillis(), destinationNodeRef, null);
		
	    nodeProperties = new HashMap<QName, Serializable>(4);
	    nodeProperties.put(titleQName, title);
	    nodeProperties.put(authorQName, author);
	    
       // params.put(WorkflowModel.PROP_TASK_ID, 3);  // protected - shouldn't be written
        params.put(WorkflowModel.PROP_DUE_DATE, new Date());  // task instance field
        params.put(WorkflowModel.PROP_PRIORITY, 1);  // task instance field
        params.put(WorkflowModel.PROP_PERCENT_COMPLETE, 10);  // context variable
        params.put(QName.createQName("", "Message"), "Hello World");  // context variable outside of task definition
        params.put(QName.createQName("", "Array"), new String[] { "one", "two" });  // context variable outside of task definition
        params.put(QName.createQName("", "NodeRef"), new NodeRef("workspace://1/1001"));  // context variable outside of task definition
        params.put(ContentModel.PROP_OWNER, peopleService.getPerson(ticket, "admin"));  // task assignment
        params.put(WorkflowModel.ASSOC_ASSIGNEE, peopleService.getPerson(ticket, "admin"));  // assignee assignment

        testNodeRef = nodeService.getRootNode(ticket, new StoreRef(StoreRef.PROTOCOL_WORKSPACE, "SpacesStore"));
        
        packageRef = workflowService.createPackage(ticket, folderRef);
        params.put(packageQName, packageRef);  // package assignment
        
	}
	
	public void tearDown() {
		try { 
			nodeService.deleteNode(ticket, nodeRef);
		} catch (Exception e) {}
		try { 
			nodeService.deleteNode(ticket, folderRef);
		} catch (Exception e) {}
		try { 
			nodeService.deleteNode(ticket, packageRef);
		} catch (Exception e) {}
	}
	
	public void testGetWorkflowDefinitions() throws Exception {
		ticket = getTicket(userid, password);
	    workflowService.getAllDefinitionsByName(ticket, "activiti$activitiReview");
	    workflowService.getDefinitionById(ticket, "activiti$activitiParallelReview:1:16");
	    workflowService.getDefinitionByName(ticket, "activiti$activitiAdhoc");
	}
	
	public void testWorkflowInstance() throws Exception {
		ticket = getTicket(userid, password);
		ChildAssociationRef childAssocRef = nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, nodeProperties);
		assertNotNull(childAssocRef);
		nodeRef = childAssocRef.getChildRef();
		workflowService.getActiveWorkflows(ticket, "activiti$activitiAdhoc:1:4");
        workflowService.getWorkflowsForContent(ticket, nodeRef, true);
        nodeService.deleteNode(ticket, nodeRef);
	}

	
	public void testUpdateTask() throws Exception {
		Ticket ticket = getTicket(userid, password);
        
        //getDefinitions
        List<WorkflowDefinition> workflowDefs = workflowService.getDefinitions(ticket, false);
        WorkflowDefinition workflowDef = workflowDefs.get(0);
        
//        //Start workflow
        WorkflowPath path = workflowService.startWorkflow(ticket, workflowDef.getId(), params);
        
        
        WorkflowTask startTask = workflowService.getStartTask(ticket, path.getInstance().getId());
        String startTaskId = startTask.getId();
        workflowService.endTask(ticket, startTaskId, null);
        
        List<WorkflowTask> tasks1 = workflowService.getTaskForWorkflowPath(ticket, path.getId());

        WorkflowTask task = tasks1.get(0);
        
//      workflowService.fireEvent(ticket, path.getId(), "After_Signal");

     	WorkflowTask task1 = workflowService.getTaskById(ticket, task.getId());
     	assertNotNull(task1);

        // update with null parameters
//       try
//        {
//            WorkflowTask taskU1 = workflowService.updateTask(ticket, task.getId(), null, null, null);
//            assertNotNull(taskU1);
//        }
//        catch(Throwable e)
//        {
//            fail("Task update failed with null parameters");
//        } 
//        
        // update property value
        Map<QName, Serializable> updateProperties2 = new HashMap<QName, Serializable>();
        updateProperties2.put(WorkflowModel.PROP_PERCENT_COMPLETE, 50);
        WorkflowTask taskU2 = workflowService.updateTask(ticket, task1.getId(), updateProperties2, null, null);
        System.out.println(taskU2.getProperties().get(WorkflowModel.PROP_PERCENT_COMPLETE));
        
        // add to association
        QName assocName = QName.createQName("{http://www.alfresco.org/model/bpm/1.0}Test");
        List<NodeRef> toAdd = new ArrayList<NodeRef>();
        toAdd.add(new NodeRef("workspace://1/1001"));
        toAdd.add(new NodeRef("workspace://1/1002"));
        toAdd.add(new NodeRef("workspace://1/1003"));
        Map<QName, List<NodeRef>> addAssocs = new HashMap<QName, List<NodeRef>>();
        addAssocs.put(assocName, toAdd);
        WorkflowTask taskU3 = workflowService.updateTask(ticket, task1.getId(), updateProperties2, addAssocs, null);
        System.out.println(((List<NodeRef>)taskU3.getProperties().get(assocName)).size());//3
        
        // add to association again
        List<NodeRef> toAddAgain = new ArrayList<NodeRef>();
        toAddAgain.add(new NodeRef("workspace://1/1004"));
        toAddAgain.add(new NodeRef("workspace://1/1005"));
        Map<QName, List<NodeRef>> addAssocsAgain = new HashMap<QName, List<NodeRef>>();
        addAssocsAgain.put(assocName, toAddAgain);
        WorkflowTask taskU4 = workflowService.updateTask(ticket, task1.getId(), updateProperties2, addAssocsAgain, null);
        System.out.println(((List<NodeRef>)taskU4.getProperties().get(assocName)).size());//5
        
        // remove association
        List<NodeRef> toRemove = new ArrayList<NodeRef>();
        toRemove.add(new NodeRef("workspace://1/1002"));
        toRemove.add(new NodeRef("workspace://1/1003"));
        Map<QName, List<NodeRef>> removeAssocs = new HashMap<QName, List<NodeRef>>();
        removeAssocs.put(assocName, toRemove);
        WorkflowTask taskU5 = workflowService.updateTask(ticket, task1.getId(), updateProperties2, null, removeAssocs);
        System.out.println(((List<NodeRef>)taskU5.getProperties().get(assocName)).size()); //3         
        
        //delete workflow
        workflowService.deleteWorkflow(ticket, path.getInstance().getId());
	}
	
	public void testGetTasks() throws Exception {  
		ticket = getTicket(userid, password);
		workflowService.getAssignedTasks(ticket, "admin", WorkflowTaskState.COMPLETED);
		workflowService.getPooledTasks(ticket, "admin");
	}
	
	public void testCancelWorkflow() throws Exception {
        List<WorkflowDefinition> workflowDefs = workflowService.getDefinitions(ticket, false);
        WorkflowDefinition workflowDef = workflowDefs.get(0);
        WorkflowPath path = workflowService.startWorkflow(ticket, "activiti$activitiReview:1:8", params);
        
        workflowService.cancelWorkflow(ticket, path.getInstance().getId());
	}
	
}
