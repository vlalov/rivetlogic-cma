/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import junit.framework.TestCase;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.QName;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.rivetlogic.core.cma.exception.AuthenticationFailure;
import com.rivetlogic.core.cma.repo.Ticket;

/**
 * a base class for cma test cases
 * 
 * @author Hyanghee Lim
 *
 */
public class CmaTestBase extends TestCase {

	private static final String APPLICATION_CONTEXT = "classpath*:core/cma-core-context.xml";
	
	protected static ConfigurableApplicationContext configurationApplicationContext = null;
	protected AuthenticationServiceImpl authenticationService = null;
	protected NodeServiceImpl nodeService = null;
	protected PeopleServiceImpl peopleService = null;
	
	// test variables
	protected String repositoryUri = null;
	protected String password = null;
	protected String userid= null;
	protected Ticket ticket = null;
	protected NodeRef companyRef = null;
	
	private String propertyPath = "src/test/resources/cma_test.properties";
	private QName firstNameQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}firstName");
	private QName lastNameQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}lastName");

	public void setUp() throws Exception {
		super.setUp();
		
		// FIXME: update not to read from a filepath 
		File file = new File("");
		String path  = file.getAbsolutePath();
		if (path.indexOf("modules/cma-impl") < 0) {
			propertyPath = "modules/cma-impl/" + propertyPath;
		}
		Properties props = new Properties();
		InputStream is = new FileInputStream(propertyPath);
		props.load(is);
		is.close();
		repositoryUri = props.getProperty("cma.repository.uri");
		assertNotNull(repositoryUri);
		userid = props.getProperty("cma.repository.admin.id"); 
		assertNotNull(userid);
		password = props.getProperty("cma.repository.admin.password"); 
		assertNotNull(password);
		
		configurationApplicationContext = new ClassPathXmlApplicationContext(APPLICATION_CONTEXT);
		authenticationService = (AuthenticationServiceImpl) configurationApplicationContext.getBean("authenticationService");
		assertNotNull(authenticationService);
		nodeService = (NodeServiceImpl) configurationApplicationContext.getBean("nodeService");
		assertNotNull(nodeService);
		peopleService = (PeopleServiceImpl) configurationApplicationContext.getBean("peopleService");
		assertNotNull(peopleService);
		
		ticket = getTicket();

		StoreRef storeRef = new StoreRef("workspace://SpacesStore");
		NodeRef root = nodeService.getRootNode(ticket, storeRef);
		QName typeQNamePattern = QName.createQName("{http://www.alfresco.org/model/system/1.0}children");
		QName qnamePattern = QName.createQName("{http://www.alfresco.org/model/application/1.0}company_home");
		List<ChildAssociationRef> assocRefs = nodeService.getChildAssocs(ticket, root, typeQNamePattern, qnamePattern);
		companyRef = assocRefs.get(0).getChildRef();
		assertNotNull(companyRef);
		
		onSetUp();
	}

	public void onSetUp() throws Exception {
		// setup for child classes to override
	}
	
	public Ticket getTicket() throws AuthenticationFailure {
		authenticationService = (AuthenticationServiceImpl) configurationApplicationContext.getBean("authenticationService");
		return authenticationService.authenticate(repositoryUri, userid, password.toCharArray());
	}
	
	// TODO: remove this method later
	public Ticket getTicket(String userid, String password) throws AuthenticationFailure {
		authenticationService = (AuthenticationServiceImpl) configurationApplicationContext.getBean("authenticationService");
		return authenticationService.authenticate(repositoryUri, userid, password.toCharArray());
	}

	public NodeRef createNewUser(String userName) throws Exception {
		HashMap<QName, Serializable> properties = new HashMap<QName, Serializable>(4);
	    properties.put(firstNameQName, "John");
	    properties.put(lastNameQName, "Doe");
	    properties.put(ContentModel.PROP_EMAIL, "john.doe@testemail.com");

	    return peopleService.createPerson(ticket, userName, userName.toCharArray(), properties);
	}
	
	public NodeRef createNode(String name) throws Exception {
		
		// create property qnames
		QName nameQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}name");
	    QName titleQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}title");
	    QName authorQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}author");
		QName assocTypeQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}contains");
		QName assocQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}testNode");
		QName nodeTypeQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}content");
	    Map<QName, Serializable> nodeProperties = new HashMap<QName, Serializable>(2);
	    nodeProperties.put(titleQName, "content test");
	    nodeProperties.put(authorQName, "Mary");
	    nodeProperties.put(nameQName, name);

		ChildAssociationRef childAssocRef = nodeService.createNode(ticket, companyRef, assocTypeQName, assocQName, nodeTypeQName, nodeProperties);
		return childAssocRef.getChildRef();
	}

	
}
