/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.repo.Ticket;

/**
 * test contentService methods
 * 
 * @author Hyanghee Lim
 *
 */
public class ContentServiceImplTest extends CmaTestBase {

	private String fileName = "/Users/swetachalasani/Desktop/Archive.zip";
	
	private ContentServiceImpl contentService = null;
	private NodeServiceImpl nodeService = null;
	
	private Ticket ticket = null;
	
	private NodeRef nodeRef;
	private NodeRef folderRef;

	QName assocTypeQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}contains");
	QName assocChildrenQName = QName.createQName("{http://www.alfresco.org/model/system/1.0}children");
	QName assocQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}", "yournewnodeName");
	QName nodeTypeQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}content");
	QName qName = QName.createQName("{http://www.alfresco.org/model/content/1.0}content");

	public void onSetUp() throws Exception {
		contentService = (ContentServiceImpl) configurationApplicationContext.getBean("contentService");
		assertNotNull(contentService);
		nodeService = (NodeServiceImpl) configurationApplicationContext.getBean("nodeService");
		assertNotNull(nodeService);
		ticket = getTicket();
		assertNotNull(ticket);
	}
	
	public void tearDown() throws Exception {
		try {
			nodeService.deleteNode(ticket, nodeRef);
		} catch (Exception e) {}
		
		File file = new File(fileName);
		if (file.exists()) {
			file.delete();
		}
	}
	
	public NodeRef createFileNode() throws Exception {
		StoreRef storeRef = new StoreRef("workspace://SpacesStore");
		NodeRef root = nodeService.getRootNode(ticket, storeRef);
		QName typeQNamePattern = QName.createQName("{http://www.alfresco.org/model/system/1.0}children");
		QName qnamePattern = QName.createQName("{http://www.alfresco.org/model/application/1.0}company_home");
		List<ChildAssociationRef> assocRefs = nodeService.getChildAssocs(ticket, root, typeQNamePattern, qnamePattern);
		folderRef = assocRefs.get(0).getChildRef();
		
		assertNotNull(folderRef);
		
		// create property qnames
	    QName authorQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}author");
	    QName titleQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}content");
	    
	    Map<QName, Serializable> nodeProperties = new HashMap<QName, Serializable>(2);

	    nodeProperties.put(titleQName, "content test");
	    nodeProperties.put(authorQName, "Mary");

		File file = new File(fileName);
		if (!file.exists()) {
			file.createNewFile();
		} else {
			file.delete();
			file.createNewFile();
		}
		FileWriter writer = new FileWriter(file);
		writer.write("test");
		writer.close();
		
	    NodeRef nodeRef = nodeService.createFile(ticket, "content service test 5", folderRef, nodeProperties, fileName);
	    assertNotNull(nodeRef);
	    return nodeRef;
	}
	
	public void testContentServicebyFileName() throws Exception {
		nodeRef = createFileNode();
		contentService.writeContent(ticket, nodeRef, nodeTypeQName, fileName);
		contentService.readContent(ticket, nodeRef, nodeTypeQName, fileName);
		String contentUri = contentService.getContentUri(ticket, nodeRef, qName);
		assertNotNull(contentUri);
	}
	
	public void testContentServiceByStream() throws Exception {
		nodeRef = createFileNode();
		FileInputStream input = new FileInputStream(fileName);
		contentService.writeContentFromStream(ticket, nodeRef, nodeTypeQName, input);
		input.close();
		FileOutputStream output = new FileOutputStream(fileName);
		contentService.readContentIntoStream(ticket, nodeRef, nodeTypeQName, output);
		output.close();
		String contentUri = contentService.getContentUri(ticket, nodeRef, qName);
		assertNotNull(contentUri);
		nodeService.deleteNode(ticket, nodeRef);
	}
	
}
