/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.exception.AuthorityNotFoundException;
import com.rivetlogic.core.cma.repo.Ticket;


/**
 * test peopleService methods
 * 
 * @author Hyanghee Lim
 *
 */
public class PeopleServiceImplTest extends CmaTestBase {

	PeopleServiceImpl peopleService = null;
	
	// variables
	NodeRef userRef1 = null;
	NodeRef userRef2 = null;
	
	// group variables
	NodeRef parentGroupRef = null;
	NodeRef subGroupRef1 = null;
	NodeRef subGroupRef2 = null;
	String groupName = "cmaTestGroupParent " + System.currentTimeMillis();
	String subGroupName1 = "cmaTestSubGroup " + System.currentTimeMillis();
	String subGroupName2 = "cmaTestSubGroup " + System.currentTimeMillis() + 1;
	
	// user variables
    String firstName1 = "cmaTestUser1FirstName";
    String lastName1 = "cmaTestUser1LastName";
    String userName1 = "peopleServiceCmaTestUser " + System.currentTimeMillis(); 
    String password1 = "peopleServiceCmaTestUser1";
    String firstName2 = "cmaTestUser2FirstName";
    String lastName2 = "cmaTestUser2LastName";
    String userName2 = "peopleServiceCmaTestUser " + System.currentTimeMillis() + 1;
    String password2 = "peopleServiceCmaTestUser2";
	
    private Ticket ticket = null;
    Map<QName, Serializable> properties = null;
	// create property qnames
    QName firstNameQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}firstName");
    QName lastNameQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}lastName");
   
    
	public void onSetUp() throws Exception {
		peopleService = (PeopleServiceImpl) configurationApplicationContext.getBean("peopleService");
		assertNotNull(peopleService);
		ticket = getTicket();
		assertNotNull(ticket);
		
	}
	
	public void tearDown() {
		
		try {
			userRef1 = peopleService.getPerson(ticket, userName1);
			peopleService.deletePerson(ticket, userRef1);
		} catch (Exception e) {}
		try {
			userRef2 = peopleService.getPerson(ticket, userName2);
			peopleService.deletePerson(ticket, userRef2);
		} catch (Exception e) {}

		try {
			subGroupRef1 = peopleService.getGroup(ticket, subGroupName1);
			peopleService.deleteGroup(ticket, subGroupRef1);
		} catch (Exception e) {}
		
		try {
			subGroupRef2 = peopleService.getGroup(ticket, subGroupName2);
			peopleService.deleteGroup(ticket, subGroupRef2);
		} catch (Exception e) {}
		
		
		try {
			parentGroupRef = peopleService.getGroup(ticket, groupName);
			peopleService.deleteGroup(ticket, parentGroupRef);
		} catch (Exception e) {}
	}

	public void testPeopleService() throws Exception { 
    	parentGroupRef = peopleService.createGroup(ticket, groupName);
    	assertNotNull(parentGroupRef);
    	
		subGroupRef1 = peopleService.createGroup(ticket, parentGroupRef, subGroupName1);
    	assertNotNull(subGroupRef1);
    	
		subGroupRef2 = peopleService.createGroup(ticket, parentGroupRef, subGroupName2);
    	assertNotNull(subGroupRef2);
		
	    properties = new HashMap<QName, Serializable>(4);
	    // homeFolder value must be a nodeRef
	    properties.put(firstNameQName, firstName1);
	    properties.put(lastNameQName, lastName1);
	    
    	userRef1 = peopleService.createPerson(ticket, userName1, userName1.toCharArray(), properties);
    	assertNotNull(userRef1);
    	Ticket user1Ticket = getTicket(userName1, userName1);
    	assertNotNull(user1Ticket);

	    properties = new HashMap<QName, Serializable>(4);
	    properties.put(firstNameQName, firstName2);
	    properties.put(lastNameQName, lastName2);

	    userRef2 = peopleService.createPerson(ticket, userName2, userName2.toCharArray(), properties);
    	assertNotNull(userRef2);
    	Ticket user2Ticket = getTicket(userName2, userName2);
    	assertNotNull(user2Ticket);

    	// alfresco 3.1.1 - returns an empty list
//    	List<NodeRef> people = peopleService.getAllPeople(ticket);
//    	assertNotNull(people);
    	
		properties = new HashMap<QName, Serializable>(1);
		properties.put(lastNameQName, "NewLastName");
		peopleService.setPersonProperties(ticket, userRef2, properties);
		
    	subGroupRef1 = peopleService.getGroup(ticket, subGroupName1);
    	assertNotNull(subGroupRef1);
    	userRef1 = peopleService.getPerson(ticket, userName1);
    	assertNotNull(userRef1);
	    peopleService.addAuthority(ticket, subGroupRef1, userRef1);
	    
    	subGroupRef2 = peopleService.getGroup(ticket, subGroupName2);
    	assertNotNull(subGroupRef2);
	    peopleService.addAuthority(ticket, subGroupRef2, userRef1);
	    
    	userRef2 = peopleService.getPerson(ticket, userName2);
    	assertNotNull(userRef2);
    	parentGroupRef = peopleService.getGroup(ticket, groupName);
    	assertNotNull(parentGroupRef);
	    peopleService.addAuthority(ticket, parentGroupRef, userRef2);

	    // check # of members in parent group
		List<NodeRef> members = peopleService.getMembers(ticket, parentGroupRef);
		assertEquals(1, members.size());
		
	    // check # of members in parent group including sub-groups
		members = peopleService.getMembers(ticket, parentGroupRef, true);
		assertEquals(2, members.size());
		List<NodeRef> groups = peopleService.getContainerGroups(ticket, userRef1);
		assertEquals(3, groups.size());
			
		userRef1 = peopleService.getPerson(ticket, userName1);
		subGroupRef1 = peopleService.getGroup(ticket, subGroupName1);
		peopleService.removeAuthority(ticket, subGroupRef1, userRef1);
		groups = peopleService.getContainerGroups(ticket, userRef1);
		assertEquals(2, groups.size());
		
		try {
			userRef2 = peopleService.getPerson(ticket, userName2);
			peopleService.deletePerson(ticket, userRef2);
			userRef2 = peopleService.getPerson(ticket, userName2);
			fail(userName2 + " is not removed");
		} catch (AuthorityNotFoundException e) {
			// success
		}

		try {
			subGroupRef1 = peopleService.getGroup(ticket, subGroupName1);
			peopleService.deleteGroup(ticket, subGroupRef1);
			subGroupRef1 = peopleService.getGroup(ticket, subGroupName1);
			fail(subGroupName1 + " is not removed");
		} catch (AuthorityNotFoundException e) {
			// success
		}


	}
	
	public void testAddAuthorityByName() throws Exception {
    	parentGroupRef = peopleService.createGroup(ticket, groupName);
    	assertNotNull(parentGroupRef);
		
	    properties = new HashMap<QName, Serializable>(4);
	    // homeFolder value must be a nodeRef
	    properties.put(firstNameQName, firstName1);
	    properties.put(lastNameQName, lastName1);
	    
    	userRef1 = peopleService.createPerson(ticket, userName1, 
    			userName1.toCharArray(), properties);
    	assertNotNull(userRef1);
    	Ticket user1Ticket = getTicket(userName1, userName1);
    	assertNotNull(user1Ticket);

    	peopleService.addAuthority(ticket, groupName, userName1);
    	
		List<NodeRef> members = peopleService.getMembers(ticket, parentGroupRef);
		assertEquals(1, members.size());
 	}
		
	public void testAddAuthoritiesByName() throws Exception {
    	parentGroupRef = peopleService.createGroup(ticket, groupName);
    	assertNotNull(parentGroupRef);
		
	    properties = new HashMap<QName, Serializable>(4);
	    // homeFolder value must be a nodeRef
	    properties.put(firstNameQName, firstName1);
	    properties.put(lastNameQName, lastName1);
	    
    	userRef1 = peopleService.createPerson(ticket, userName1, 
    			userName1.toCharArray(), properties);
    	assertNotNull(userRef1);
    	Ticket user1Ticket = getTicket(userName1, userName1);
    	assertNotNull(user1Ticket);

	    properties = new HashMap<QName, Serializable>(4);
	    properties.put(firstNameQName, firstName2);
	    properties.put(lastNameQName, lastName2);

	    userRef2 = peopleService.createPerson(ticket, userName2, 
	    		userName2.toCharArray(), properties);
    	assertNotNull(userRef2);
    	Ticket user2Ticket = getTicket(userName2, userName2);
    	assertNotNull(user2Ticket);

    	List<String> authorities = new ArrayList<String>(2);
    	authorities.add(userName1);
    	authorities.add(userName2);
    	peopleService.addAuthorities(ticket, groupName, authorities);
    	
		List<NodeRef> members = peopleService.getMembers(ticket, parentGroupRef);
		assertEquals(2, members.size());
 	}
		
	public void printCollection(String name, Collection collection) {
		System.out.println("printing a collection of " + name);
		if (collection != null) {
			for (Object obj : collection)
				System.out.println("- " + obj);
		} else {
			System.out.println(name + " is empty");
		}
	}
		
}
