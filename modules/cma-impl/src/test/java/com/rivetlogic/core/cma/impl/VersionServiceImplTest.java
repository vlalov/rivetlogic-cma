/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.InvalidNodeRefException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.version.Version;
import org.alfresco.service.cmr.version.VersionHistory;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.repo.Ticket;

/**
 * @author Sweta Chalasani
 *
 */
public class VersionServiceImplTest extends CmaTestBase {
	
	private VersionServiceImpl versionService = null;
	private NodeServiceImpl nodeService = null;
	
	private String titlenode = "nodeServiceTest";
    private String author = "Sweta";
    private String name = "versionServiceTest";
	private Map<QName, Serializable> properties;

	private Map<String, Serializable> nodeProperties;
	private NodeRef nodeRef = null;
	private NodeRef nodeRef2 = null;
	private NodeRef restoredRef = null;
	private NodeRef destinationNodeRef = null;
	private QName assocTypeQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}contains");
	private QName assocQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}", "yournewnodeName");
	private QName nodeTypeQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}content");
    private QName titleQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}title");
    private QName nameQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}name");  
    private QName authorQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}author");
	private Ticket ticket = null;
	
	public void onSetUp() throws Exception {
		versionService = (VersionServiceImpl) configurationApplicationContext.getBean("versionService");
		assertNotNull(versionService);
		nodeService = (NodeServiceImpl) configurationApplicationContext.getBean("nodeService");
		assertNotNull(nodeService);
		ticket = getTicket();
		assertNotNull(ticket);
		
		StoreRef storeRef = new StoreRef("workspace://SpacesStore");
		NodeRef root = nodeService.getRootNode(ticket, storeRef);
		QName typeQNamePattern = QName.createQName("{http://www.alfresco.org/model/system/1.0}children");
		QName qnamePattern = QName.createQName("{http://www.alfresco.org/model/application/1.0}company_home");
		List<ChildAssociationRef> assocRefs = nodeService.getChildAssocs(ticket, root, typeQNamePattern, qnamePattern);
		destinationNodeRef = assocRefs.get(0).getChildRef();
		assertNotNull(destinationNodeRef);

		// create node
		// user variables
	    String description = "description";
	    String title = "title";
	    
		// create property qnames
	   String descriptionName = "creating version";
	   String titleName = "createVersion";
	    
	    nodeProperties = new HashMap<String, Serializable>(2);

	    nodeProperties.put(titleName, title);
	    nodeProperties.put(descriptionName, description);

		Ticket ticket = getTicket(userid, password);
	    ChildAssociationRef childAssocRef = nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, properties);
		assertNotNull(childAssocRef);
		nodeRef = childAssocRef.getChildRef();
		
		ChildAssociationRef childAssocRef2 = nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, properties);
		assertNotNull(childAssocRef2);
		nodeRef2 = childAssocRef2.getChildRef();
	
	    
	}
	
	public void tearDown() {
		try {
			nodeService.deleteNode(ticket, nodeRef);
		} catch (Exception e) {}
		
		try {
			nodeService.deleteNode(ticket, nodeRef2);
		} catch (Exception e) {}
		
		try {
			if (restoredRef != null)
				nodeService.deleteNode(ticket, restoredRef);
		} catch (Exception e) {
		}
	}
	
	
	public void testGetVersion() throws Exception {
		Ticket ticket = getTicket(userid, password);
		nodeService.addAspect(ticket, nodeRef, QName.createQName("{http://www.alfresco.org/model/content/1.0}versionable"));
		
		Version version = versionService.getCurrentVersion(ticket, nodeRef);
		assertNotNull(version);
		VersionHistory versionHistory = versionService.getVersionHistory(ticket, nodeRef);
		assertNotNull(versionHistory);
		StoreRef storeRef = versionService.getVersionStoreReference(ticket);
		assertNotNull(storeRef);
	}
	
	public void testCreateVersion() throws Exception {
	
		Version version1 = versionService.createVersion(ticket, nodeRef, nodeProperties);
		assertNotNull(version1);
		Collection<Version> version2 = versionService.createVersion(ticket, nodeRef, nodeProperties, true);
		assertNotNull(version2);
		Collection<NodeRef> nodeRefs = new Vector<NodeRef>();
					// add NodeRefs
		nodeRefs.add(nodeRef);
		nodeRefs.add(nodeRef2);
		Collection<Version> version3 = versionService.createVersion(ticket, nodeRefs, nodeProperties);
		assertNotNull(version3);
	}
	
	public void testRevert() throws Exception {
		nodeService.addAspect(ticket, nodeRef, QName.createQName("{http://www.alfresco.org/model/content/1.0}versionable"));
		
		versionService.revert(ticket, nodeRef);			
		versionService.revert(ticket, nodeRef, false);
		versionService.createVersion(ticket, nodeRef, nodeProperties);
		nodeService.setProperty(ticket, nodeRef, QName.createQName("{http://www.alfresco.org/model/content/1.0}title"), "TestRevert");
		Version version = versionService.createVersion(ticket, nodeRef, nodeProperties);
		nodeService.setProperty(ticket, nodeRef, QName.createQName("{http://www.alfresco.org/model/content/1.0}description"), "revert");
		versionService.revert(ticket, nodeRef, version);
		
	}
	
	public void testDeleteVersionHistory() throws Exception {
		nodeService.addAspect(ticket, nodeRef, QName.createQName("{http://www.alfresco.org/model/content/1.0}versionable"));
		versionService.deleteVersionHistory(ticket, nodeRef);
	}
	
	
	public void testRestore() throws Exception {
		//nodeService.deleteNode(ticket, nodeRef2);
		nodeService.addAspect(ticket, nodeRef, QName.createQName("{http://www.alfresco.org/model/content/1.0}versionable"));
		NodeRef deletedNode = nodeService.deleteNode(ticket, nodeRef);
		// Alfresco 2.2.1 does not remove the archived node from archive://SpacesStore when it is restored, causing the node becomes not removable
		//restoredRef = versionService.restore(ticket, deletedNode, destinationNodeRef, ContentModel.ASSOC_CONTAINS, QName.createQName("{http://www.alfresco.org/model/content/1.0}", "n2"));
		//versionService.restore(ticket, nodeRef2, parentNodeRef, QName.createQName("{http://www.alfresco.org/model/content/1.0}contains"), QName.createQName("{http://www.alfresco.org/model/content/1.0}", "n2"), true);
	}
}
