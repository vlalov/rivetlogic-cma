/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.util.List;
import java.util.Map;

public class MimetypeServiceImplTest extends CmaTestBase {

	MimetypeServiceImpl mimetypeService;
	
	public void onSetUp() throws Exception {
		
		mimetypeService = (MimetypeServiceImpl) configurationApplicationContext.getBean("mimetypeService");
		assertNotNull(mimetypeService);
		ticket = getTicket();
		assertNotNull(ticket);
	}
	
	public void tearDown() throws Exception {
		
	}
	
	
	public void testGetDisplaysByExtension() throws Exception {
		Map<String, String> mimetypes = mimetypeService.getDisplaysByExtension(ticket);
		assertNotNull(mimetypes);
	}

	public void testGetDisplaysByMimetype() throws Exception {
		Map<String, String> mimetypes = mimetypeService.getDisplaysByMimetype(ticket);
		assertNotNull(mimetypes);
	}


	public void testGetExtensionsByMimetype() throws Exception {
		Map<String, String> extensions = mimetypeService.getExtensionsByMimetype(ticket);
		assertNotNull(extensions);
	}


	public void testGetMimetypesByExtension() throws Exception {
		Map<String, String> mimetypes = mimetypeService.getMimetypesByExtension(ticket);
		assertNotNull(mimetypes);
	}

	public void testGetExtension() throws Exception {
		String mimetype = "image/jpeg";
		String extension = mimetypeService.getExtension(ticket, mimetype);
		assertEquals("jpg", extension);
	}
	
	public void testGetMimetypes() throws Exception {
		List<String> mimetypes = mimetypeService.getMimetypes(ticket);
		assertNotNull(mimetypes);
	}
	
	public void testGuessMimetype() throws Exception {
		String fileName = "test.html";
		String mimetype = mimetypeService.guessMimetype(ticket, fileName);
		assertEquals("text/html", mimetype);
	}

	public void testIsText() throws Exception {
		String mimetype = "image/jpeg";
		boolean isText = mimetypeService.isText(ticket, mimetype);
		assertFalse(isText);
		
		String mimetype2 = "text/plain";
		boolean isText2 = mimetypeService.isText(ticket, mimetype2);
		assertTrue(isText2);
	}
	
}
