/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.security.AccessPermission;
import org.alfresco.service.cmr.security.AccessStatus;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.api.SecurityService;
import com.rivetlogic.core.cma.repo.Ticket;


/**
 * test securityService methods
 * 
 * @author Hyanghee Lim
 *
 */
public class SecurityServiceImplTest extends CmaTestBase {

	private Ticket user1Ticket = null;
	private SecurityServiceImpl securityService = null;

	//private NodeRef nodeRef = new NodeRef("workspace://SpacesStore/ce54bcf1-ea08-11dc-be14-7b516cc4154c");; 
	private QName type = QName.createQName("{http://www.alfresco.org/model/user/1.0}user");

	private String authorityUser1 = "SecurityServiceCmaTestUser " + System.currentTimeMillis();
	private String authorityUser2 = "SecurityServiceCmaTestUser " + System.currentTimeMillis() + 1;

	private NodeRef nodeRef = null;
	private NodeRef userRef = null;
	
	PeopleServiceImpl peopleService = null;
	
	private boolean printEnabled = false;
	
	public void onSetUp() throws Exception {
		securityService = (SecurityServiceImpl) configurationApplicationContext.getBean("securityService");
		peopleService = (PeopleServiceImpl) configurationApplicationContext.getBean("peopleService");
				
		ticket = getTicket();
		assertNotNull(ticket);
	}
	
	public void tearDown() {
		try {
			peopleService.deletePerson(ticket, userRef);
		} catch (Exception e) {}
		try {
			nodeService.deleteNode(ticket, nodeRef);
		} catch (Exception e) {}
	}
	
	public void testGetOwnerAuthority() throws Exception {
		String authority = securityService.getOwnerAuthority(ticket);
		assertNotNull(authority);
	}
	
	public void testGetAllAuthorities() throws Exception {
		String allAuthorities = securityService.getAllAuthorities(ticket);
		assertNotNull(allAuthorities);
	}
	public void testGetAllPermission() throws Exception {
		String allPermission = securityService.getAllPermission(ticket);
		assertNotNull(allPermission);
	}
	
	public void testHasPermission() throws Exception {
		userRef = createNewUser(authorityUser1);
		nodeRef = createNode("Cma SecurityService Test Node " + System.currentTimeMillis());
		assertNotNull(nodeRef);
		
		Ticket userTicket = getTicket(authorityUser1, authorityUser1);
		
 		AccessStatus status = securityService.hasPermission(userTicket, nodeRef, SecurityService.WRITE);
 		assertEquals(status, AccessStatus.DENIED);
		status = securityService.hasPermission(ticket, nodeRef, SecurityService.WRITE);
 		assertEquals(status, AccessStatus.ALLOWED);
		
 		AccessStatus status1 = securityService.hasPermission(ticket, nodeRef, SecurityService.WRITE_CONTENT);
 		
		nodeRef = createNode("Cma LibraryService Test Node " + System.currentTimeMillis());
	
	}

	// FIXME: alfresco error on deleting users
	public void testPermission() throws Exception {
		NodeRef userRef = createNewUser(authorityUser1);
		nodeRef = createNode("Cma SecurityService Test Node " + System.currentTimeMillis());
		assertNotNull(nodeRef);
		
		Ticket user1Ticket = getTicket(authorityUser1, authorityUser1);
		assertNotNull(user1Ticket);		
		
		// set permission		
		securityService.setPermission(ticket, nodeRef, authorityUser1, SecurityService.CONSUMER, true);
		AccessStatus status = securityService.hasPermission(user1Ticket, nodeRef, SecurityService.READ);
		assertEquals(status, AccessStatus.ALLOWED);
		
		securityService.clearPermission(ticket, nodeRef, authorityUser1);
		
		if (printEnabled) {
			// set permission		
			securityService.setPermission(ticket, nodeRef, authorityUser1, SecurityService.EDITOR, true);
			System.out.println("set " + authorityUser1 + " permission : " + SecurityService.EDITOR);
			// set permission		
			securityService.setPermission(ticket, nodeRef, authorityUser2, SecurityService.CONTRIBUTOR, true);
			System.out.println("set " + authorityUser2 + " permission : " + SecurityService.CONTRIBUTOR);
			System.out.println("");
		}
		Set<AccessPermission> permissions = securityService.getAllSetPermissions(ticket, nodeRef);
		assertNotNull(permissions);
		if (printEnabled)
			printCollection("getPermissions", permissions);
		
		// delete all permissions
		//securityService.deletePermissions(ticket, nodeRef);
		securityService.deletePermission(ticket, nodeRef, authorityUser1, SecurityService.EDITOR);
		securityService.deletePermission(ticket, nodeRef, authorityUser2,  SecurityService.CONTRIBUTOR);
				
		permissions = securityService.getAllSetPermissions(ticket, nodeRef);
		assertNotNull(permissions);
		assertEquals(1, permissions.size());
		
		assertNotNull(permissions);
		if (printEnabled)
			printCollection("getPermissions", permissions);
		
		peopleService.deletePerson(ticket, userRef);
		nodeService.deleteNode(ticket, nodeRef);
	} 
	
	public void testInheritParentPermissions() throws Exception {
		nodeRef = createNode("Cma LibraryService Test Node " + System.currentTimeMillis());
		securityService.setInheritParentPermissions(ticket, nodeRef, true);
		boolean inherit = securityService.getInheritParentPermissions(ticket, nodeRef);
		assertTrue(inherit);
		securityService.setInheritParentPermissions(ticket, nodeRef, false);
		inherit = securityService.getInheritParentPermissions(ticket, nodeRef);
		assertFalse(inherit);
	}

	public void testGetAllSetPermissions() throws Exception {
		userRef = createNewUser(authorityUser1);
		Ticket userTicket = getTicket(authorityUser1, authorityUser1);
		nodeRef = createNode("Cma LibraryService Test Node " + System.currentTimeMillis());
		securityService.setPermission(ticket, nodeRef, authorityUser1, SecurityService.READ, true);
		securityService.setPermission(ticket, nodeRef, authorityUser1, SecurityService.DELETE, true);
		securityService.setPermission(ticket, nodeRef, authorityUser1, SecurityService.WRITE, false);
		
		AccessStatus hasPermission = securityService.hasPermission(userTicket, nodeRef, SecurityService.DELETE);
		assertEquals(hasPermission, AccessStatus.ALLOWED);
		hasPermission = securityService.hasPermission(userTicket, nodeRef, SecurityService.READ);
		assertEquals(hasPermission, AccessStatus.ALLOWED);
		hasPermission = securityService.hasPermission(userTicket, nodeRef, SecurityService.WRITE);
		assertEquals(hasPermission, AccessStatus.DENIED);
		Set<AccessPermission> permissions = securityService.getAllSetPermissions(ticket, nodeRef);
		assertEquals(4, permissions.size());
	}
	
	
	public void testGetSettableAllPermissions() throws Exception {
		nodeRef = createNode("Cma LibraryService Test Node " + System.currentTimeMillis());
		Set<String> permissions = securityService.getSettablePermissions(ticket, nodeRef);
		assertNotNull(permissions);
		
		permissions = securityService.getSettablePermissions(ticket, type);
		assertNotNull(permissions);
	}
	
	public void printCollection(String name, Collection collection) {
		System.out.println("printing a collection of " + name);
		if (collection != null) {
			for (Object obj : collection)
				System.out.println("- " + obj);
		} else {
			System.out.println(name + " is empty");
		}
		System.out.println("");
	}
	
	public void printMapCollection(String name, Map<NodeRef, Set<AccessPermission>> permissionsMap) {
		System.out.println("printing a collection of " + name);
		if (permissionsMap != null) {
			for (NodeRef nodeRef : permissionsMap.keySet()) {
				Set<AccessPermission> permissions = permissionsMap.get(nodeRef);
				System.out.println("NodeRef: " + nodeRef);
				for (AccessPermission permission : permissions)
					System.out.println("- " + permission);
			}
		} else {
			System.out.println(name + " is empty");
		}
	}
	
}
