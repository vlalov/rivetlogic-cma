/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.repo.Ticket;

/**
 * @author Sweta Chalasani
 *
 */
public class TransformationServiceImplTest extends CmaTestBase {
	
	private TransformationServiceImpl transformationService = null;
	private NodeServiceImpl nodeService = null;

	private NodeRef nodeRefPdfResult = null;
	private NodeRef nodeRefTxtResult = null;
	private NodeRef destinationNodeRef = null;
	private QName titleQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}title");
    private QName authorQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}author");
    private Map<QName, Serializable> fileProperties;
	
	private Ticket ticket = null;
	private String title = "TransformationServicetest";
    private String author = "Sweta";
    private String fileNameTxt= "TransforamtionServiceTxtFile.txt";
	private String contentTxt = "trasformTxtFile.txt";
	private String fileNamePdf = "TransforamtionServicePdfFile.pdf";
	private String contentPdf = "transformPdfFile.pdf";

	public void onSetUp() throws Exception {
		transformationService = (TransformationServiceImpl) configurationApplicationContext.getBean("transformationService");
		nodeService = (NodeServiceImpl) configurationApplicationContext.getBean("nodeService");
		ticket = getTicket();
		
		StoreRef storeRef = new StoreRef("workspace://SpacesStore");
		NodeRef root = nodeService.getRootNode(ticket, storeRef);
		QName typeQNamePattern = QName.createQName("{http://www.alfresco.org/model/system/1.0}children");
		QName qnamePattern = QName.createQName("{http://www.alfresco.org/model/application/1.0}company_home");
		List<ChildAssociationRef> assocRefs = nodeService.getChildAssocs(ticket, root, typeQNamePattern, qnamePattern);
		destinationNodeRef = assocRefs.get(0).getChildRef();
		assertNotNull(destinationNodeRef);
		
		fileProperties = new HashMap<QName, Serializable>(2);
	    fileProperties.put(titleQName, title);
	    fileProperties.put(authorQName, author);
	    
		File file1 = new File(contentTxt);
		if (!file1.exists()) {
			file1.createNewFile();
		} else {
			file1.delete();
			file1.createNewFile();
		}
		FileWriter writer1 = new FileWriter(file1);
		writer1.write("Transform text into pdf");
		writer1.close();
		nodeRefTxtResult = nodeService.createFile(ticket, fileNameTxt, destinationNodeRef, fileProperties, contentTxt);
		assertNotNull(nodeRefTxtResult);
		
		File file2 = new File(contentPdf);
		if (!file2.exists()) {
			file2.createNewFile();
		} else {
			file2.delete();
			file2.createNewFile();
		}
		FileWriter writer2 = new FileWriter(file2);
		writer2.write("pdf file");
		writer2.close();
		nodeRefPdfResult = nodeService.createFile(ticket, fileNamePdf, destinationNodeRef, fileProperties, contentPdf);
		assertNotNull(nodeRefPdfResult);
	}
	
	public void tearDown() {
		File file = new File(contentPdf);
		if (file.exists()) {
			file.delete();
		}
		file = new File(contentTxt);
		if (file.exists()) {
			file.delete();
		}
	}

	public void testIsTransformable() throws Exception {
		Ticket ticket = getTicket(userid, password);
		Boolean result = transformationService.isTransformable(ticket, nodeRefTxtResult, nodeRefPdfResult);
		assertNotNull(result);
		nodeService.deleteNode(ticket, nodeRefPdfResult);
		nodeService.deleteNode(ticket, nodeRefTxtResult);
	}
	 
	public void testTransform() throws Exception {
		Ticket ticket = getTicket(userid, password);
		transformationService.transform(ticket, nodeRefTxtResult, nodeRefPdfResult);	
		nodeService.deleteNode(ticket, nodeRefPdfResult);
		nodeService.deleteNode(ticket, nodeRefTxtResult);
	}
	
}

