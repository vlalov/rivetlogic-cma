/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.util.Collection;
import java.util.Iterator;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.dictionary.AspectDefinition;
import org.alfresco.service.cmr.dictionary.AssociationDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.dictionary.ModelDefinition;
import org.alfresco.service.cmr.dictionary.PropertyDefinition;
import org.alfresco.service.cmr.dictionary.TypeDefinition;
import org.alfresco.service.namespace.QName;

/**
 * test dictionaryService methods
 * 
 * @author Hyanghee Lim
 *
 */
public class DictionaryServiceImplTest extends CmaTestBase {
	
	// test variables
	private String dataTypeStr = "{http://www.alfresco.org/model/dictionary/1.0}text";
	private String typeStr = "{http://www.alfresco.org/model/content/1.0}content";
	private String modelStr = "{http://www.alfresco.org/model/content/1.0}contentmodel";
	private String dictionaryModelStr = "{http://www.alfresco.org/model/dictionary/1.0}dictionary";

	QName dataType = null;
	QName type = null;
	QName model = null;
	QName ofClassName = null;
	QName subClassName = null;
	QName dictionaryModel = null;

	private String ofClassStr = "{http://www.alfresco.org/model/system/1.0}container";
	private String subClassStr = "{http://www.alfresco.org/model/content/1.0}folder";
		
	DictionaryServiceImpl dictionaryService = null;
	
	public void onSetUp() throws Exception {
		dictionaryService = (DictionaryServiceImpl) configurationApplicationContext.getBean("dictionaryService");
		
		dataType = QName.createQName(dataTypeStr);
		type = QName.createQName(typeStr);
		model = QName.createQName(modelStr);
		ofClassName = QName.createQName(ofClassStr);
		subClassName = QName.createQName(subClassStr);
		dictionaryModel = QName.createQName(dictionaryModelStr);
	}
	
	public void testDictionaryService() throws Exception {
		Collection<QName> collection;
		
		collection = dictionaryService.getAllAspects(ticket);
		assertNotNull(collection);
		
		collection = dictionaryService.getAllDataTypes(ticket);
		assertNotNull(collection);
		
		collection = dictionaryService.getAllProperties(ticket, dataType);
		assertNotNull(collection);
		
		// should give all properties in repo
		collection = dictionaryService.getAllProperties(ticket, null);
		assertNotNull(collection);

		collection = dictionaryService.getAllTypes(ticket);
		assertNotNull(collection);

		collection = dictionaryService.getAspects(ticket, model);
		assertNotNull(collection);

		collection = dictionaryService.getDataTypes(ticket, dictionaryModel);
		assertNotNull(collection);

		collection = dictionaryService.getProperties(ticket, model);
		assertNotNull(collection);

		collection = dictionaryService.getProperties(ticket, model, dataType);
		assertNotNull(collection);
		
		// this should give all properties of Model
		collection = dictionaryService.getProperties(ticket, model, null);
		assertNotNull(collection);
		
		collection = dictionaryService.getTypes(ticket, model);
		assertNotNull(collection);

		boolean result = dictionaryService.isSubClass(ticket, subClassName, ofClassName);
		assertFalse(result);
	} 
	
	public void testGetType() throws Exception {
		TypeDefinition typeDef = dictionaryService.getType(ticket, type);
		assertNotNull(typeDef);
		
		/*
		// Alfresco fails to create an anonymous type with the given aspects here
		// it only creates with default aspects of the given type
		int numOfAspect = typeDef.getDefaultAspects().size();
		Collection<QName> aspects = new Vector<QName>(2);
		aspects.add(ContentModel.ASPECT_VERSIONABLE);
		aspects.add(ContentModel.ASPECT_CLASSIFIABLE);
		numOfAspect = numOfAspect + aspects.size();
		TypeDefinition anonymousType = dictionaryService.getAnonymousType(ticket, type, aspects);
		assertEquals(numOfAspect, anonymousType.getDefaultAspects().size());
		*/
	}
	
	public void testGetModel() throws Exception {
		ModelDefinition modelDef = dictionaryService.getModel(ticket, model);
		assertNotNull(modelDef);
	}
	
	public void testGetAspect() throws Exception {
		AspectDefinition aspectDef = dictionaryService.getAspect(ticket, ContentModel.ASPECT_CLASSIFIABLE);
		assertNotNull(aspectDef);
	} 

	public void testGetProperty() throws Exception {
		PropertyDefinition propertyDef = dictionaryService.getProperty(ticket, ContentModel.PROP_CONTENT);
		assertNotNull(propertyDef);
		propertyDef = dictionaryService.getProperty(ticket, ContentModel.TYPE_CONTENT, ContentModel.PROP_CONTENT);
		assertNotNull(propertyDef);
	} 
	
	public void testGetAssociation() throws Exception {
		AssociationDefinition assocDef = dictionaryService.getAssociation(ticket, ContentModel.ASSOC_CONTAINS);
		assertNotNull(assocDef);
	} 

	
	public void testGetDatatype() throws Exception {
		DataTypeDefinition dateTypeDef = dictionaryService.getDataType(ticket, dataType);
		assertNotNull(dateTypeDef);
		dateTypeDef = dictionaryService.getDataType(ticket, Integer.class);
		assertNotNull(dateTypeDef);
	} 

	public void printCollection(String name, Collection<QName> collection) {
		
		if (collection != null) {
			Iterator<QName> itr = collection.iterator();
			System.out.println(name);
			while(itr.hasNext()) {
				System.out.println("- QName : " + itr.next().toString());
			}
		} else {
			System.out.println("no result");
		}
	}
}
