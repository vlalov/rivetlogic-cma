/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.evaluator.ComparePropertyValueEvaluator;
import org.alfresco.repo.action.executer.AddFeaturesActionExecuter;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionCondition;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.QName;

import com.rivetlogic.core.cma.repo.Ticket;

/**
 * @author Sweta Chalasani
 *
 */
public class ActionServiceImplTest extends CmaTestBase {
	
	private Map<String, Serializable> nodeProperties;
	private NodeRef nodeRef = null;
	
	private NodeRef destinationNodeRef = null;
	private Map<QName, Serializable> properties;
	private Ticket ticket = null;
	
	private ActionServiceImpl actionService = null;
	private NodeServiceImpl nodeService = null;
	private QName assocTypeQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}contains");
	private QName assocQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}", "yournewnodeName");
	private QName nodeTypeQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}content");
    private QName titleQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}title");
    private QName nameQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}name");
    private QName authorQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}author");
    
    private String titlenode = "actionServiceTest";
    private String author = "Sweta";
    private String name = "actionServiceTest";
	
	public void onSetUp() throws Exception {
		actionService = (ActionServiceImpl) configurationApplicationContext.getBean("actionService");
		assertNotNull(actionService);
		nodeService = (NodeServiceImpl) configurationApplicationContext.getBean("nodeService");
		assertNotNull(nodeService);
		ticket = getTicket();
		assertNotNull(ticket);
		
		StoreRef storeRef = new StoreRef("workspace://SpacesStore");
		NodeRef root = nodeService.getRootNode(ticket, storeRef);
		QName typeQNamePattern = QName.createQName("{http://www.alfresco.org/model/system/1.0}children");
		QName qnamePattern = QName.createQName("{http://www.alfresco.org/model/application/1.0}company_home");
		List<ChildAssociationRef> assocRefs = nodeService.getChildAssocs(ticket, root, typeQNamePattern, qnamePattern);
		destinationNodeRef = assocRefs.get(0).getChildRef();
		assertNotNull(destinationNodeRef);
		
	    properties = new HashMap<QName, Serializable>(2);
	    properties.put(titleQName, titlenode);
	    properties.put(authorQName, author);
	    properties.put(nameQName, name);
	}
	
	public void tearDown() {
		try {
			nodeService.deleteNode(ticket, nodeRef);
		} catch (Exception e) {}
	}
	
	public void testCreateAction() throws Exception {	
		// user variables
	    String description = "Testing and creating Action";
	    String title = "CreatingAction";
	    
		// create property qnames
	   String descriptionName = "actionDescription";
	   String titleName = "actionTitle";
	 
	    nodeProperties = new HashMap<String, Serializable>(2);

	    nodeProperties.put(titleName, title);
	    nodeProperties.put(descriptionName, description);
	    	    
	    ChildAssociationRef childAssocRef = nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, properties);
		assertNotNull(childAssocRef);
		nodeRef = childAssocRef.getChildRef();
	
		Ticket ticket = getTicket();
		ActionCondition actionCondition = actionService.createActionCondition(ticket, AddFeaturesActionExecuter.NAME, nodeProperties);
		assertNotNull(actionCondition);
		Action actionResult = actionService.createAction(ticket, AddFeaturesActionExecuter.NAME, nodeProperties);
		assertNotNull(actionResult);
		Action action = actionService.createAction(ticket, AddFeaturesActionExecuter.NAME);
		assertNotNull(action);
		actionService.saveAction(ticket, nodeRef, action);
		actionService.createCompositeAction(ticket);
	}
	
	public void testGetAction() throws Exception{
		Ticket ticket = getTicket();
		ChildAssociationRef childAssocRef = nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, properties);
		assertNotNull(childAssocRef);
		nodeRef = childAssocRef.getChildRef();
			
		Action action = actionService.createAction(ticket, AddFeaturesActionExecuter.NAME);
		actionService.saveAction(ticket, nodeRef, action);
		Action getAction = actionService.getAction(ticket, nodeRef, action.getId());
		assertNotNull(getAction);
		List<Action> actions = actionService.getActions(ticket, nodeRef);	
		assertNotNull(actions);
	}
	
	public void testEvaluateAndExecuteAction() throws Exception{
		Ticket ticket = getTicket();
		ChildAssociationRef childAssocRef = nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, properties);
		assertNotNull(childAssocRef);
		nodeRef = childAssocRef.getChildRef();
		Action action = actionService.createAction(ticket, AddFeaturesActionExecuter.NAME);
		actionService.saveAction(ticket, nodeRef, action);
		actionService.evaluateAction(ticket, action, nodeRef);
		ActionCondition condition = actionService.createActionCondition(ticket, ComparePropertyValueEvaluator.NAME);
		condition.setParameterValue(ComparePropertyValueEvaluator.PARAM_VALUE, "*.doc");
		actionService.evaluateActionCondition(ticket, condition, nodeRef);
		action.setParameterValue(AddFeaturesActionExecuter.PARAM_ASPECT_NAME, ContentModel.ASPECT_VERSIONABLE);
		actionService.executeAction(ticket, action, nodeRef);
		actionService.executeAction(ticket, action, nodeRef, true);
		actionService.executeAction(ticket, action, nodeRef, false, true);
	}
	
	public void testRemoveAction() throws Exception{
		Ticket ticket = getTicket();
		ChildAssociationRef childAssocRef = nodeService.createNode(ticket, destinationNodeRef, assocTypeQName, assocQName, nodeTypeQName, properties);
		assertNotNull(childAssocRef);
		nodeRef = childAssocRef.getChildRef();
		Action action = actionService.createAction(ticket, AddFeaturesActionExecuter.NAME);
		actionService.saveAction(ticket, nodeRef, action);
		actionService.removeAction(ticket, nodeRef, action);
		actionService.removeAllActions(ticket, nodeRef);	
	}
}
