/*
 * Copyright (C) 2008 Rivet Logic Corporation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rivetlogic.core.cma.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

/**
 * test libraryService methods
 * 
 * @author Hyanghee Lim
 *
 */
public class LibraryServiceImplTest extends CmaTestBase {
	
	LibraryServiceImpl libraryService = null;
	NodeRef nodeRef = null;
	NodeRef workingCopy = null;
	NodeRef originalNodeRef = null;
	NodeRef destinationNodeRef = null;
	
	public void onSetUp() throws Exception {
		libraryService = (LibraryServiceImpl) configurationApplicationContext.getBean("libraryService");
		nodeRef = createNode("Cma LibraryService Test Node " + System.currentTimeMillis());
	}
	
	public void tearDown() {
		try {
			nodeService.deleteNode(ticket, nodeRef);
		} catch (Exception e) {}
		try {
			nodeService.deleteNode(ticket, workingCopy);
		} catch (Exception e) {}
		try {
			nodeService.deleteNode(ticket, originalNodeRef);
		} catch (Exception e) {}
		try {
			nodeService.deleteNode(ticket, destinationNodeRef);
		} catch (Exception e) {}
		
		
	}
	public void testDictionaryService() throws Exception {
		QName titleQName = QName.createQName("{http://www.alfresco.org/model/content/1.0}title");
	    Map<QName, Serializable> nodeProperties = new HashMap<QName, Serializable>(1);
	    nodeProperties.put(titleQName, "working space");
	
		destinationNodeRef = nodeService.createFolder(ticket, "workingSpace", companyRef, nodeProperties);
		assertNotNull(destinationNodeRef);
		NodeRef workingCopy;
		NodeRef originalNodeRef;

		// check out to the same space
		workingCopy = libraryService.checkOut(ticket, nodeRef, null);
		assertNotNull(workingCopy);
		
		// cancel checkout
		originalNodeRef = libraryService.cancelCheckOut(ticket, workingCopy);
		assertNotNull(originalNodeRef);

		// checking out to the destinationNodeRef
		workingCopy = libraryService.checkOut(ticket, nodeRef, destinationNodeRef);
		assertNotNull(originalNodeRef);
		
		// in order to see version change, the node must have versionable aspect applied
		// checking in with major version increase
		originalNodeRef = libraryService.checkIn(ticket, workingCopy, "testing", true);
		assertNotNull(originalNodeRef);
		
	}	
}
